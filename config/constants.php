<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    // 'reiss_master_api_url' => isset($_SERVER["HTTP_HOST"]) && $_SERVER["HTTP_HOST"] == 'localhost' ? 'http://localhost/LP/xcelhub/public/api/' : 'https://xcelhub.com/api/',
    // 'reiss_master_api_url' => 'http://localhost/LP/xcelhub/public/api/',
    'reiss_master_api_url' => 'https://xcelhub.com/api/',
    
    
];