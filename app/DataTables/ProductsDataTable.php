<?php

namespace App\DataTables;

use App\StoreProduct;
use App\User;
use Yajra\DataTables\Services\DataTable;

class ProductsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function (StoreProduct $sp){
                if($sp->is_bulk){
                    $edit = '<a class="btn btn-primary edit" href="'.route('product.bulkedit').'?edit=' . $sp->id . '"><i class="fa fa-pencil fa-lg"></i></a>&nbsp;';
                }else{
                    $edit = '<a class="btn btn-primary edit" href="'.route('product.add').'?edit=' . $sp->id . '"><i class="fa fa-pencil fa-lg"></i></a>&nbsp;';
                }
                

                $delete = '<button type="button" class="btn btn-danger delete btn-sm" product_id="' . $sp->id . '"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></button>&nbsp;';

                $disabled = "";
                
                //api categories are not in database, so it gives error "trying to get property of non object"
                // if($sp->category->isActive === 0)    
                //     $disabled = "disabled";

                // check category is not null (api case it is null), so, does the work...
                if ( !is_null($sp->category) && $sp->category->isActive === 0 )
                    $disabled = "disabled";

                $state = null;

                if ($sp->isActive)
                    $state = "<button type=\"button\" class=\"active btn btn-success btn-sm\" product_id=\"" .$sp->id. "\"><i class=\"fa fa-eye fa-lg\" aria-hidden=\"true\"></i></button>&nbsp;";
                else
                    $state =  "<button type=\"button\" class=\"deactive btn btn-warning btn-sm\" product_id=\"" .$sp->id. "\"   $disabled><i class=\"fa fa-eye-slash fa-lg\" aria-hidden=\"true\"></i></button>&nbsp;";

                return $state.$edit.$delete;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\StoreProduct $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(StoreProduct $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '113px']);
//                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'manufacturer', 'name', 'sku',
            'price',
            'manufacturer_code'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'productsdatatable_' . time();
    }
}
