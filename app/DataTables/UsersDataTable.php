<?php

namespace App\DataTables;

use App\FrontUser;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function (FrontUser $sp){
                
                $edit = '<a class="btn btn-primary edit" href="'.route('userEdit',['id'=>$sp->id]).'"><i class="fa fa-pencil fa-lg"></i></a>';

                $delete = '<a class="btn btn-danger delete btn-sm" onclick="return confirm(\'Are you sure you want to delete selected item(s)?\');" href="'.route('userDelete',['id'=>$sp->id]).'"><i class="fa fa-trash fa-lg" ></i></a>';

                return $edit.' '.$delete;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\StoreProduct $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(FrontUser $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns([
                        'created_at' => ['title' => 'Date'],
                        'first_name' => ['title' => 'First Name'],
                        'last_name' => ['title' => 'Last Name'],
                        'email' => ['title' => 'Email'],
                        'status' => ['title' => 'Status'],
                    ])
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters([
                                'order' => [
                                    0, // here is the column number
                                    'desc'
                                ]
                            ]);
//                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'created_at',
            'first_name',
            'last_name',
            'email',
            'status'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'contentdatatable_' . time();
    }
}
