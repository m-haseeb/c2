<?php

namespace App\DataTables;

use App\Pages;
use App\Homepage;
use Yajra\DataTables\Services\DataTable;

class ContentDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function (pages $sp){
                if($sp->id == 1){
                    $edit = '<a class="btn btn-primary edit" href="'.route('content.edit-home').'"><i class="fa fa-pencil fa-lg"></i></a>';
                }else{
                    $edit = '<a class="btn btn-primary edit" href="'.route('content.edit-page', ['id' => $sp->id]).'"><i class="fa fa-pencil fa-lg"></i></a>';
               }

                return $edit;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\StoreProduct $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Pages $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px']);
//                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'contentdatatable_' . time();
    }
}
