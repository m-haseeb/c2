<?php

namespace App\DataTables;

use App\Reports;
use Yajra\DataTables\Services\DataTable;

class ReportsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function (Reports $sp){
                
                $edit = '<a class="btn btn-primary play" href="#"><i class="fa fa-file-pdf-o fa-lg"></i></a>';
                return $edit;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\StoreProduct $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Reports $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns([
                                'created_at' => ['title' => 'Date'],
                                'title' => ['title' => 'Title'],
                            ])
                    ->minifiedAjax()
                    ->addAction(['width' => '80px']);
//                    ->parameters($this->getBuilderParameters());
                    //$this->getColumns()
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'created_at',
            'title',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reportsdatatable_' . time();
    }
}
