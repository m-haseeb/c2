<?php

namespace App\DataTables;

use App\Orders;
use Yajra\DataTables\Services\DataTable;

class OrderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function (Orders $sp){
                
                $edit = '<a class="btn btn-primary edit" href="'.route('orderDetailAdmin',['id'=>$sp->id]).'"><i class="fa fa-pencil fa-lg"></i></a>';
                return $edit;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\StoreProduct $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Orders $model)
    {
        //dd($model->newQuery());
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns([
                        'created_at' => ['title' => 'Date'],
                        'id' => ['title' => 'Order Id'],
                        'skus' => ['title' => 'Product SKU'],
                        'user_fname' => ['title' => 'User First Name'],
                        'user_lname' => ['title' => 'User Last Name'],
                        'user_email' => ['title' => 'User Email'],
                        'totalPrice' => ['title' => 'Total Price'],
                    ])
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters([
                                'order' => [
                                    1, // here is the column number
                                    'desc'
                                ]
                            ]);
//                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'created_at',
            'id',
            'skus',
            'user_fname',
            'user_lname',
            'user_email',
            'totalPrice'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'contentdatatable_' . time();
    }
}
