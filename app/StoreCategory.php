<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreCategory extends Model
{
    use SoftDeletes;

    public $incrementing = false;
    
    public function products()
    {
        return $this->hasMany('App\StoreProduct');
    }

    public function children()
    {
        return $this->hasMany('App\StoreCategory', 'parent_id');
    }
}
