<?php

namespace App\Http\Middleware;

use Closure;

class redirectIfNotLoggedInAsUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if( auth()->user() == null ){
            return redirect( route('login') );
        }

        return $response;
    }
}
