<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if($guard != 'front'){
                return redirect(route('content.all'));
            }else{
                return redirect(route('myaccount'));
            }
        }

        // if (!Auth::guard($guard)->check()){
        //     if($guard != 'front'){
        //         return dd($request);
        //     }else{
        //         return $next($request);
        //     }
        // }
        return $next($request);
    }
}
