<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Rules\OldPasswordValid;
use App\FrontUser;
use App\Orders;

class MyAccountController extends Controller
{
    //

    public function __construct(){
    	$this->middleware(function ($request, $next) {
    		if (!auth('front')->check()){
    			return redirect(route('loginRegister'))->send();
	    	}
    		return $next($request);
        });
    }

    

    public function index(){
    	$u = auth('front')->user();
    	$userData = FrontUser::find($u->id);
    	return view('front.account.profile', ['active'=>'account', 'userInfo' => $userData]);
    }

    public function editProfile(){
    	$u = auth('front')->user();
    	$userData = FrontUser::find($u->id);
    	return view('front.account.edit-profile', ['active'=>'account', 'userInfo' => $userData]);
    }

    public function updateProfile(Request $request){
    	$u = auth('front')->user();
    	$this->validate($request,[
			'regfname'=>'required|max:200',
			'reglname'=>'required|max:200',
			
			'regemail'=>'required|max:200|email|unique:front_users,email,'.$u->id.',id',
			
			'regstrt'=>'required|max:200',
			'regcity'=>'required|max:200',
			'regstate'=>'required|max:200',
			'regzip'=>'required|max:200',
		],
		[
			'required' => "This field is required.",
			'max' => "You have exceeded the maximum limit of 200 characters.",
			'email' => "Please enter the valid email address.",
			'regeconfrmemail.same' => "Email and confirmation email do not match.",
			'regpconfrmpass.same' => "Password and confirmation password do not match.",
			'unique'=>'This email address already exist.',
		]);

		$saveData = FrontUser::find($u->id);
    	$saveData->first_name = $request->regfname;
        $saveData->last_name = $request->reglname;
        $saveData->email = $request->regemail;
        $saveData->street_address = $request->regstrt;
        $saveData->city = $request->regcity;
        $saveData->state = $request->regstate;
        $saveData->zipcode = $request->regzip;
        $saveData->save();
        return redirect(route('myaccount'))->with('profileMsg','Profile updated successfully.');
    }


    public function changePassword(){
    	return view('front.account.change-password', ['active'=>'changePassword']);
    }

    public function changePasswordUpdate(Request $request){
    	$u = auth('front')->user();
    	$saveData = FrontUser::find($u->id);
    	$this->validate($request,[
			'oldpass'=>['required', 'max:200' ,new OldPasswordValid(decrypt($saveData->passwordtext))],
			'newpass'=>'required|max:200|different:oldpass',
			'cnfrmpass'=>'required|max:200|same:newpass',
		],
		[
			'required' => "This field is required.",
			'max' => "You have exceeded the maximum limit of 200 characters.",
			'cnfrmpass.same' => "Password and confirmation password do not match.",
			'newpass.different' => "New password must be different.",
		]);
    	$saveData->password = bcrypt($request->newpass);
        $saveData->passwordtext = encrypt($request->newpass);
        $saveData->save();
        return redirect(route('myaccount'))->with('profileMsg','Password has been updated successfully.');

    }

    public function myOrderList(){
        $u = auth('front')->user();
        $orders = Orders::where('user_id','=',$u->id)->orderBy('id', 'DESC')->get();
        return view('front.account.my-order-list', ['active'=>'my-order', 'orders'=>$orders]);
    }

    public function myOrderDetail($order_id = ''){
        if($order_id == '' || !is_numeric($order_id)){
            return redirect(route('myOrder'));
        }
        $u = auth('front')->user();
        $orders = Orders::where('user_id','=',$u->id)->where('id','=',$order_id)->first();
        if(empty($orders)){
            return redirect(route('myOrder'));
        }
        return view('front.account.my-order-detail', ['active'=>'my-order', 'orders'=>$orders]);
    }

    
}
