<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\ContentDataTable;
use App\Pages;
use App\Homepage;
use Image;
use App\Brands;

use Illuminate\Support\Facades\Input as input;

class ContentController extends Controller
{
    public function index(ContentDataTable $dataTable){
    	return $dataTable->render('content.all');

    }

    public function homepage(){
    	$page_data = Homepage::find(1)->toArray();
		return view('content.edit_home',['pageData' => $page_data]);
	}

    public function update_homepage(Request $request){
    	$this->validate($request,[
	            'banner_text1' => "required",
	            'banner_text2' => "required",
	            'banner_text3' => "required",
	            'why_us_text' => "required",
	            'desc1' => "required",
	            'desc2' => "required",
	            'text1' => "required",
	            'text2' => "required",
	        ]); //$validator->fails()
    	if($request->hasFile('banner_img1')){
	        $this->validate($request,[
	            'banner_img1' => 'required|max:10000|mimes:jpeg,png,jpg'
	        ]);
	    }
	    if($request->hasFile('banner_img2')){
	        $this->validate($request,[
	            'banner_img2' => 'required|max:10000|mimes:jpeg,png,jpg'
	        ]);
	    }
	    if($request->hasFile('banner_img3')){
	        $this->validate($request,[
	            'banner_img3' => 'required|max:10000|mimes:jpeg,png,jpg'
	        ]);
	    }
	    if($request->hasFile('why_us_img')){
	        $this->validate($request,[
	            'why_us_img' => 'required|max:10000|mimes:jpeg,png,jpg'
	        ]);
	    }
	    if($request->hasFile('image1')){
	        $this->validate($request,[
	           'image1' => 'required|max:10000|mimes:jpeg,png,jpg'
	        ]);
	    }
	    if($request->hasFile('image2')){
	        $this->validate($request,[
	           'image2' => 'required|max:10000|mimes:jpeg,png,jpg'
	        ]);
	    }
    	
    	$page =  Homepage::find(1);
    	$page->banner_text1 = $request->banner_text1;
    	$page->banner_text2 = $request->banner_text2;
    	$page->banner_text3 = $request->banner_text3;
    	$page->why_us_text = $request->why_us_text;
    	$page->desc1 = $request->desc1;
    	$page->desc2 = $request->desc2;
    	$page->text1 = $request->text1;
    	$page->text2 = $request->text2;

    	if($request->hasFile('banner_img1')){
            $image = $request->file('banner_img1');
			$input['banner_img1'] = 'pageImg'.time().'.'.$image->getClientOriginalExtension();
			/** - Generate Thumb of image Start*/
			$destinationPath = public_path('/pages_images/thumb');
	        $img = Image::make($image->getRealPath());
	        $img->resize(100, 100, function ($constraint) {
				    $constraint->aspectRatio();
				})->save($destinationPath.'/'.$input['banner_img1']);
			/** - Generate Thumb of Image End*/
			$destinationPath = public_path('/pages_images');
			$image->move($destinationPath, $input['banner_img1']);
			$page->banner_img1 = $input['banner_img1'];
        }
        if($request->hasFile('banner_img2')){
            $image = $request->file('banner_img2');
			$input['banner_img2'] = 'pageImg_'.time().'.'.$image->getClientOriginalExtension();
			/** - Generate Thumb of image Start*/
			$destinationPath = public_path('/pages_images/thumb');
	        $img = Image::make($image->getRealPath());
	        $img->resize(100, 100, function ($constraint) {
				    $constraint->aspectRatio();
				})->save($destinationPath.'/'.$input['banner_img2']);
			/* - Generate Thumb of Image End */
			$destinationPath = public_path('/pages_images');
			$image->move($destinationPath, $input['banner_img2']);
			$page->banner_img2 = $input['banner_img2'];
        }
        if($request->hasFile('banner_img3')){
            $image = $request->file('banner_img3');
			$input['banner_img3'] = 'page_img_'.time().'.'.$image->getClientOriginalExtension();
			/** - Generate Thumb of image Start*/
			$destinationPath = public_path('/pages_images/thumb');
	        $img = Image::make($image->getRealPath());
	        $img->resize(100, 100, function ($constraint) {
				    $constraint->aspectRatio();
				})->save($destinationPath.'/'.$input['banner_img3']);
			/** - Generate Thumb of Image End*/
			$destinationPath = public_path('/pages_images');
			$image->move($destinationPath, $input['banner_img3']);
			$page->banner_img3 = $input['banner_img3'];
        }
        if($request->hasFile('why_us_img')){
            $image = $request->file('why_us_img');
			$input['why_us_img'] = 'page_img_'.time().'.'.$image->getClientOriginalExtension();
			/** - Generate Thumb of image Start*/
			$destinationPath = public_path('/pages_images/thumb');
	        $img = Image::make($image->getRealPath());
	        $img->resize(100, 100, function ($constraint) {
				    $constraint->aspectRatio();
				})->save($destinationPath.'/'.$input['why_us_img']);
			/** - Generate Thumb of Image End*/
			$destinationPath = public_path('/pages_images');
			$image->move($destinationPath, $input['why_us_img']);
			$page->why_us_img = $input['why_us_img'];
        }
        if($request->hasFile('image1')){
            $image = $request->file('image1');
			$input['image1'] = 'page_img'.time().'.'.$image->getClientOriginalExtension();
			/** - Generate Thumb of image Start*/
			$destinationPath = public_path('/pages_images/thumb');
	        $img = Image::make($image->getRealPath());
	        $img->resize(100, 100, function ($constraint) {
				    $constraint->aspectRatio();
				})->save($destinationPath.'/'.$input['image1']);
			/** - Generate Thumb of Image End*/
			$destinationPath = public_path('/pages_images');
			$image->move($destinationPath, $input['image1']);
			$page->image1 = $input['image1'];
        }
        if($request->hasFile('image2')){
            $image = $request->file('image2');
			$input['image2'] = 'pageimg_'.time().'.'.$image->getClientOriginalExtension();
			/** - Generate Thumb of image Start*/
			$destinationPath = public_path('/pages_images/thumb');
	        $img = Image::make($image->getRealPath());
	        $img->resize(100, 100, function ($constraint) {
				    $constraint->aspectRatio();
				})->save($destinationPath.'/'.$input['image2']);
			/** - Generate Thumb of Image End*/
			$destinationPath = public_path('/pages_images');
			$image->move($destinationPath, $input['image2']);
			$page->image2 = $input['image2'];
        }
    	$page->save();
        return redirect(route('content.all'))->with('success','Item update successful');
    }



    public function edit_page($id = 0 ){
    	if($id <= 0){
    		redirect('content.all');
    	}
    	$page_data = Pages::find($id)->toArray();
    	if(count($page_data) <= 0){
    		redirect('content.all');
    	}
    	$img_data = array();
    	if($id == 4){
    		$img_data = Brands::all()->toArray();
    		return view('content.edit', ['pageData'=>$page_data, 'images'=>$img_data]);
    	}else{
	    	return view('content.edit', ['pageData'=>$page_data]);
	    }
    }

   	public function update_edit_page(Request $request){
   		
   		$this->validate($request,[
            'page_title' => "required|max:255",
            'page_description' => "required",
        ]);

        if($request->id == 2 || $request->id == 3 || $request->id == 5){
        	if($request->hasFile('page_image')){
	        	$this->validate($request,[
	            'page_image' => 'required|max:10000|mimes:jpeg,png,jpg'
	            ]);
	            $image = $request->file('page_image');
				$input['page_image'] = 'page_img_'.time().'.'.$image->getClientOriginalExtension();
				/** - Generate Thumb of image Start*/
				$destinationPath = public_path('/pages_images/thumb');
		        $img = Image::make($image->getRealPath());
		        $img->resize(100, 100, function ($constraint) {
					    $constraint->aspectRatio();
					})->save($destinationPath.'/'.$input['page_image']);
				/** - Generate Thumb of Image End*/
				$destinationPath = public_path('/pages_images');
				$image->move($destinationPath, $input['page_image']);
	        }
		}
		$page =  Pages::find($request->id);
		if($request->id == 2 || $request->id == 3 || $request->id == 5){
			if($request->hasFile('page_image')){
				$page->image = $input['page_image'];
			}
    	}
        $page->description = $request->page_description;
        $page->title = $request->page_title;
        $page->save();
        return redirect(route('content.all'))->with('success','Item update successful');
	}

	

	public function dropzoneStore(Request $request)
    {
        $image = $request->file('page_image');
        $imageName = time().$image->getClientOriginalName();
        $image->move(public_path('images'),$imageName);
        return response()->json(['success'=>$imageName]);
    }

	public function update_brands(Request $request){
		
$file = input::file('page_image');
if ($file !== null) {
    echo $file->getClientOriginalExtension();  
}
exit;
		 $image = $request->file('page_image');

		 $input['page_image'] = 'brand_'.date('m-d-Y H-i-s').'.'.$image->getClientOriginalExtension();
		 /** - Generate Thumb of image Start*/
		 $destinationPath = public_path('/pages_images/thumb');
	     $img = Image::make($image->getRealPath());
	     $img->resize(100, 100, function ($constraint) {
			    $constraint->aspectRatio();
			})->save($destinationPath.'/'.$input['page_image']); 
	     print_r($image);
		 exit;
		/** - Generate Thumb of Image End*/
		$destinationPath = public_path('/pages_images');
		$image->move($destinationPath, $input['page_image']);

		$brand = new Brands;
		$brand->banner_image = $input['page_image'];
		$brand->save();
		echo $input['page_image'];
	}
}
