<?php

namespace App\Http\Controllers;

use App\StoreCategory;
use App\StoreProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

 use Illuminate\Support\Facades\DB;
 use Maatwebsite\Excel\Facades\Excel;

// use function Sodium\add;

class Data_importController extends Controller
{

    public function populateCategories($rows)
    {
        $categoryCollection = new Collection();
        $i = 0;
        foreach ($rows as $row) {
            if ($i > 2 && $i < (count($rows) - 1)) {

                if (trim($row[111]) !== '' && !$categoryCollection->firstWhere('category_name', $row[111])) {
                    $category = new StoreCategory();
                    $category->name = $row[111];
                    $category->level = 1;
                    $category->parent_id = null;

                    $categoryCollection->push(collect(['category_name' => $row[111], 'category' => $category, 'children' => new Collection()]));
                }

                if (trim($row[112]) !== '') {
                    $category = new StoreCategory();
                    $category->name = $row[112];
                    $category->level = 2;

                    $parents = $categoryCollection->where('category_name', $row[111]);
                    foreach ($parents as $parent) {
                        $found = false;
                        foreach ($parent->get('children') as $children) {
                            if ($children->get('category_name') === $row[112]) {
                                $found = true;
                                break;
                            }
                        }
                        if (!$found) {
                            $category->parent_id = $parent->get('category')->id;
                            $parent->get('children')->push(collect(['category_name' => $row[112], 'category' => $category, 'children' => new Collection()]));
                            break;
                        }
                    }
                }
            }
            $i++;
        }
        $categoryCollection->dd();
    }

    public function populateCategoriesV2($rows)
    {
        echo "extracting categories<br>";
        $level1 = new Collection();
        $level2 = new Collection();
        $level3 = new Collection();
        $level4 = new Collection();

        $count = 0;
        $id = 1;
        foreach ($rows as $row) {
            if ($count > 2 && $count < (count($rows) - 1)) {

                if (trim($row[111]) !== '') {
                    $found = false;
                    foreach ($level1 as $category) {
                        if ($category->get('name') === $row[111]) {
                            $found = true;
                            break;
                        }
                    }

                    if (!$found) {
                        $level1->push(collect(['id' => $id, 'name' => $row[111], 'level' => 1, 'parent_id' => null]));
                        $id++;
                    }

                }

                if (trim($row[112]) !== '') {
                    $found = false;
                    foreach ($level2 as $category) {
                        if ($category->get('name') === $row[112]) {
                            $found = true;
                            break;
                        }
                    }


                    if (!$found) {
                        $parent = $level1->firstWhere('name', $row[111]);
                        $level2->push(collect(['id' => $id, 'name' => $row[112], 'level' => 2, 'parent_id' => $parent->get('id')]));
                        $id++;
                    }

                }

                if (trim($row[113]) !== '') {
                    $found = false;
                    foreach ($level3 as $category) {
                        if ($category->get('name') === $row[113]) {
                            $found = true;
                            break;
                        }
                    }

                    if (!$found) {
                        $parent = $level2->firstWhere('name', $row[112]);
                        $level3->push(collect(['id' => $id, 'name' => $row[113], 'level' => 3, 'parent_id' => $parent->get('id')]));
                        $id++;
                    }
                }

                if (trim($row[114]) !== '') {
                    $found = false;
                    foreach ($level4 as $category) {
                        if ($category->get('name') === $row[114]) {
                            $found = true;
                            break;
                        }
                    }

                    if (!$found) {
                        $parent = $level3->firstWhere('name', $row[113]);
                        $level4->push(collect(['id' => $id, 'name' => $row[114], 'level' => 4, 'parent_id' => $parent->get('id')]));
                        $id++;
                    }
                }
            }
            $count++;
        }


        echo "Deleting Previous categories<br>";
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        StoreCategory::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        echo "inserting categories<br>";
        StoreCategory::insert($level1->toArray());
        StoreCategory::insert($level2->toArray());
        StoreCategory::insert($level3->toArray());
        StoreCategory::insert($level4->toArray());
        return ['level_1' => $level1, 'level_2' => $level2, 'level_3' => $level3, 'level_4' => $level4];
    }

    public function import_php()
    {

        $file = public_path('file/WEB_PRODUCT.txt');
        $handle = fopen($file, "r");
        $read = file_get_contents($file);
        $lines = explode("\n", $read);

        echo "Reading File<br>";
        $i = 0;
        foreach ($lines as $key => $value) {
            $rows[$i] = explode("\t", $value);
            $i++;
        }

        $categories = $this->populateCategoriesV2($rows);
        $j = 0;
        $entries = new Collection();
        foreach ($rows as $row) {

            if ($j > 2 && $j < (count($rows) - 1)) {
                $rp = new StoreProduct();
                $rp->image = $row[110];
                $rp->manufacturer = $row[1];
                $rp->name = $row[2];
                $rp->sku = $row[0];
                $rp->upc = $row[72];
                $rp->manufacturer_code = $row[37];

                if ($row[45] === '')
                    $rp->package_quantity = null;
                else
                    $rp->package_quantity = $row[45];

                if ($row[48] === '')
                    $rp->case_quantity = null;
                else
                    $rp->case_quantity = $row[48];

                $rp->price = $row[101];
                
                // $dbData['qty1'] = $row[100];
                // $dbData['price1'] = $row[101];
                // $dbData['qty2'] = $row[102];
                // $dbData['price2'] = $row[103];
                // $dbData['qty3'] = $row[104];
                // $dbData['price3'] = $row[105];
                // $dbData['promo_start'] = $row[106];
                // $dbData['promo_end'] = $row[107];

                if (trim($row[114]) !== '') {
                    $rp->store_category_id = $categories['level_4']->firstWhere('name', $row[114])->get('id');
                } elseif (trim($row[113]) !== '') {
                    $rp->store_category_id = $categories['level_3']->firstWhere('name', $row[113])->get('id');
                } elseif (trim($row[112]) !== '') {
                    $rp->store_category_id = $categories['level_2']->firstWhere('name', $row[112])->get('id');
                } elseif ( !empty( trim($row[111]) ) ) {
                    $rp->store_category_id = $categories['level_1']->firstWhere('name', $row[111])->get('id');
                }
                //$rp->slug = $this->create_slug($row[2], 'store_products', 'slug');
                $entries->push($rp);
            }
            
//            if($j>2 && $j<(count($cols)-1)){
//                $dbData['id'] = $row[0];
//                $dbData['manufacturer'] = $row[1];
//                $dbData['name'] = $row[2];
//                $dbData['type'] = $row[3];
//                $dbData['sales_code'] = $row[4];
//                $dbData['crrent_vendor'] = $row[5];
//                $dbData['alternate_vendor'] = $row[6];
//                $dbData['commission_rate'] = $row[7];
//                $dbData['complementary'] = $row[8];
//                $dbData['substitute'] = $row[9];
//                $dbData['upgrade'] = $row[10];
//                $dbData['safety_sheet'] = $row[11];
//                $dbData['close_date'] = $row[12];
//                $dbData['replacement'] = $row[13];
//                $dbData['comments'] = $row[14];
//                $dbData['stock_uom'] = $row[15];
//                $dbData['purchase_uom'] = $row[16];
//                $dbData['convert_qty'] = $row[17];
//                $dbData['convert_uom'] = $row[18];
//                $dbData['unit'] = $row[19];
//                $dbData['weight'] = $row[20];
//                $dbData['volume'] = $row[21];
//                $dbData['cost'] = $row[22];
//                $dbData['list_price'] = $row[23];
//                $dbData['price_list'] = $row[24];
//                $dbData['pricing_uom'] = $row[25];
//                $dbData['pricing_current'] = $row[26];
//                $dbData['pricing_factor'] = $row[27];
//                $dbData['quantity_break'] = $row[28];
//                $dbData['start_date'] = $row[29];
//                $dbData['end_date'] = $row[30];
//                $dbData['new_cost'] = $row[31];
//                $dbData['new_list'] = $row[32];
//                $dbData['component'] = $row[33];
//                $dbData['component_uom'] = $row[34];
//                $dbData['component_qty'] = $row[35];
//                $dbData['vendor'] = $row[36];
//                $dbData['vendor_product'] = $row[37];
//                $dbData['vendor_cost'] = $row[38];
//                $dbData['customer'] = $row[39];
//                $dbData['customer_product'] = $row[40];
//                $dbData['kit'] = $row[41];
//                $dbData['sale_price'] = $row[42];
//                $dbData['product_detail'] = $row[43];
//                $dbData['sales_multiple'] = $row[44];
//                $dbData['package_qty'] = $row[45];
//                $dbData['price_book_sort'] = $row[46];
//                $dbData['pick_list_comment'] = $row[47];
//                $dbData['case_qty'] = $row[48];
//                $dbData['location'] = $row[49];
//                $dbData['old_upc'] = $row[50];
//                $dbData['minimum'] = $row[51];
//                $dbData['maximum'] = $row[52];
//                $dbData['location_max'] = $row[53];
//                $dbData['price_class'] = $row[54];
//                $dbData['cycle_count'] = $row[55];
//                $dbData['close_flag'] = $row[56];
//                $dbData['catalog'] = $row[57];
//                $dbData['pack_version'] = $row[58];
//                $dbData['price_book_ignore'] = $row[59];
//                $dbData['begin_date'] = $row[60];
//                $dbData['show_cost'] = $row[61];
//                $dbData['upc10'] = $row[62];
//                $dbData['upc_ok'] = $row[63];
//                $dbData['scanable'] = $row[64];
//                $dbData['discontinued'] = $row[65];
//                $dbData['include_po_worksheet'] = $row[66];
//                $dbData['web'] = $row[67];
//                $dbData['itf14'] = $row[68];
//                $dbData['itf_qty'] = $row[69];
//                $dbData['package_text'] = $row[70];
//                $dbData['upc_qty'] = $row[71];
//                $dbData['upc'] = $row[72];
//                $dbData['sort_order'] = $row[73];
//                $dbData['minimun_po_qty'] = $row[74];
//                $dbData['first_receipt'] = $row[75];
//                $dbData['vol_commission'] = $row[76];
//                $dbData['vol_commission_qty'] = $row[77];
//                $dbData['ytd_sales'] = $row[78];
//                $dbData['min_sale_qty'] = $row[79];
//                $dbData['mdk_qoh'] = $row[80];
//                $dbData['mdk_product_code'] = $row[81];
//                $dbData['mdk_seq'] = $row[82];
//                $dbData['primary_component'] = $row[83];
//                $dbData['replacement_for'] = $row[84];
//                $dbData['vendor_weight'] = $row[85];
//                $dbData['vendor_volume'] = $row[86];
//                $dbData['bulk_zone'] = $row[87];
//                $dbData['item_weight'] = $row[88];
//                $dbData['item_cube'] = $row[89];
//                $dbData['shelf_min'] = $row[90];
//                $dbData['shelf_max'] = $row[91];
//                $dbData['empty1'] = $row[92];
//                $dbData['empty2'] = $row[93];
//                $dbData['empty3'] = $row[94];
//                $dbData['empty4'] = $row[95];
//                $dbData['empty5'] = $row[96];
//                $dbData['empty6'] = $row[97];
//                $dbData['empty7'] = $row[98];
//                $dbData['empty8'] = $row[99];
//                $dbData['qty1'] = $row[100];
//                $dbData['price1'] = $row[101];
//                $dbData['qty2'] = $row[102];
//                $dbData['price2'] = $row[103];
//                $dbData['qty3'] = $row[104];
//                $dbData['price3'] = $row[105];
//                $dbData['promo_start'] = $row[106];
//                $dbData['promo_end'] = $row[107];
//                $dbData['title'] = $row[108];
//                $dbData['qoh'] = $row[109];
//                $dbData['image'] = $row[110];
//                $dbData['division'] = $row[111];
//                $dbData['class'] = $row[112];
//                $dbData['sub_class'] = $row[113];
//                $dbData['sub_sub_class'] = $row[114];
//                $this->db->insert('tbl_products',$dbData);
//            }
            $j++;
        }
        
       
        echo "Deleting Previous Products<br>";
        StoreProduct::truncate();
        
        echo "inserting products<br>";
        $chunks = $entries->chunk(500);
        foreach ($chunks as $chunk)
            StoreProduct::insert($chunk->toArray());

    }

    
}
