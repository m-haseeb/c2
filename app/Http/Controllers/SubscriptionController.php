<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\VideoDataTable;
use App\DataTables\ReportsDataTable;
class SubscriptionController extends Controller
{
    //
    public function index(){
    	$data['title'] = "Manage Subscription";
    	return view('settings.subscription',['data'=>$data]);
    }

    public function billing(){
    	$data['title'] = "Billing History";
    	return view('settings.billing',['data'=>$data]);
    }

    public function video_tutorial(VideoDataTable $dataTable){
    	return $dataTable->render('settings.video_tutorial');
    }

    public function monthly_reports(ReportsDataTable $dataTable){
    	return $dataTable->render('settings.monthly_reports');
    }
}
