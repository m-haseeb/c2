<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

use Illuminate\Support\Str;

use App\FrontUser;

class FrontLoginRegisterController extends Controller
{
    //

	use AuthenticatesUsers, SendsPasswordResetEmails;

	protected $redirectTo = '/my-profile'; // After Login redirect to home page.

	public function __construct()
    {
        //$this->middleware('guest')->except('logout');
        $this->middleware('guest:front')->except('logout');
        // auth('front');
    }
    //
    //
    public function showLoginRegisterForm(){
    	$title = 'Login and Register';
    	return view('front.login-register',['active'=>'lgnReg','title'=>$title]);
    }

    public function login(Request $request){
    	$this->validate($request,[
    		'loginemail'=>'required|max:200|email',
    		'loginpass'=>'required|max:200',
    	],
    	[
    		'required' => "This field is required.",
    		'max' => "You have exceeded the maximum limit of 200 characters.",
    		'email' => "Please enter the valid email address."
    	]);

    	// $details = $request->only('email', 'password');
    	$details = ['email' => $request->loginemail, 'password' => $request->loginpass];
        $details['status'] = 'Active';
        if (auth()->guard('front')->attempt($details)) {
            $return = $this->sendLoginResponse($request);
            
            $oldUserId = @$_COOKIE['userId'];

            $userId = auth('front')->user()->id;

           
            /*echo '<pre>';
            print_r(auth('front')->user());
            echo '</pre>';
            exit;*/
            $isCartExist = \DB::table('lists')->where(['userId' => $userId])->get();
            if(count($isCartExist) <= 0) {
                if($oldUserId != ''){
                    \DB::table('lists')->where('userId', $oldUserId)->update(['userId' => $userId]);
                }
            }
            setcookie('userId', $userId, time()+2592000, '/');

            
            return $return;
        }
        //setcookie('userId', $userId, time()+2592000);

        $request->session()->flash('login-message', "Invalid Email or Password , Please try again.");
        return $this->sendFailedLoginResponse($request);
    }

	public function register(Request $request){
		$this->validate($request,[
			'regfname'=>'required|max:200',
			'reglname'=>'required|max:200',
			
			'regemail'=>'required|max:200|email|unique:front_users,email,NULL,id',
			'regeconfrmemail'=>'required|max:200|email|same:regemail',

			'regpass'=>'required',
			'regpconfrmpass'=>'required|max:200|same:regpass',

			'regstrt'=>'required|max:200',
			'regcity'=>'required|max:200',
			'regstate'=>'required|max:200',
			'regzip'=>'required|max:200',
		],
		[
			'required' => "This field is required.",
			'max' => "You have exceeded the maximum limit of 200 characters.",
			'email' => "Please enter the valid email address.",
			'regeconfrmemail.same' => "Email and confirmation email do not match",
			'regpconfrmpass.same' => "Password and confirmation password do not match",
			'unique'=>'This email address already exist.',
		]);

		$customer = $this->create($request->except('_method', '_token'));
        //Auth::login($customer);
        return redirect( route('loginRegister') )->with('registerMsg', 'You have registered successfully.');
	}

	protected function create(array $data)
    {
        return FrontUser::create([
            
            'first_name' => $data['regfname'],
            'last_name' => $data['reglname'],
            'email' => $data['regemail'],
            'password' => bcrypt($data['regpass']),
            'passwordtext' => encrypt($data['regpass']),
            'street_address' => $data['regstrt'],
            'city' => $data['regcity'],
            'state' => $data['regstate'],
            'zipcode' => $data['regzip'],
            'status' => 'Active',
        ]);
    }

    public function ShowForgotPasswordForm(){
        $title = 'Forgot Password';
        return view('front.account.forget-password',['active'=>'forgotPassword','title'=>$title]);
    }

    public function ForgotPassword(Request $request){
        $this->validate($request,[
            'email' => 'required|email|exists:front_users,email,status,Active'

        ],[
            'required' => "This field is required.",
            'max' => "You have exceeded the maximum limit of 200 characters.",
            'email' => "Please enter the valid email address.",
            'exists' => "Our records do not indicate this account exists.",
        ]);
        //
        $tk = Str::random(30);
        \DB::table('password_resets')->where('email', '=', $request->email)->where('is_front',1)->delete();
        \DB::table('password_resets')->insert(
            ['email' => $request->email, 'token' => ($tk), 'is_front'=>1]
        );

        $resetLink = route('showResetPassword',['token'=>$tk]);
        $resetLink = "<a href='$resetLink'>Reset Password</a>";
        
        $textMessage = "<h2>Hello!</h2><br/>You are receiving this email because we received a password reset request for your account.
        <br/><br/>
        $resetLink
        <br/><br/>If you did not request a password reset, no further action is required.<br/><br/>Regards,<br/>SafetyLINE";
        /*
        \Mail::raw($textMessage, function ($message) use ($request) {
            $message->to($request->email);
            $message->subject('SafetyLINE: Reset Password');
        });
        */
        $data['content'] = $textMessage;
        $data['request'] = $request;

        \Mail::send([], [], function($message) use ($data) {
            $message->to($data['request']->email);
            $message->subject('SafetyLINE: Reset Password');
            $message->setBody($data['content'], 'text/html');
        });

        return redirect(route('showForgotPassword'))->with('status', "We have sent an email to $request->email.");

    }

    public function showResetForm($token = ''){
        if($token == ''){
            return redirect(route('showForgotPassword'))->with('status', 'Your password reset link has been expired');
        }
        $getInfo = \DB::table('password_resets')->select('token')->where('token',($token))->where('is_front', 1)->get();
        
        if( empty($getInfo[0]->token)){
            return redirect(route('showForgotPassword'))->with('status', 'Your password reset link has been expired');
        }
        //

        return view('front.account.reset-forget-password', ['active'=>'restPassword', 'ttoken'=>$token ]);

    }

    public function restPasswordUpdate(Request $request){
        $this->validate($request,[
            'pass'=>'required',
            'cpnPass'=>'required|max:200|same:pass',
        ],
        [
            'required' => "This field is required.",
            'max' => "You have exceeded the maximum limit of 200 characters.",
            'cpnPass.same' => "Password and confirmation password do not match.",
        ]);

        $getInfo = \DB::table('password_resets')->select('email')->where('token',($request->ttoken))->where('is_front', 1)->get();
        $getInfo = \DB::table('front_users')->select('id')->where('email',($getInfo[0]->email))->get();

        $saveData = FrontUser::find($getInfo[0]->id);
        $saveData->password = bcrypt($request->pass);
        $saveData->passwordtext = encrypt($request->pass);
        $saveData->save();

        \DB::table('password_resets')->where('token', '=', $request->ttoken)->where('is_front',1)->delete();
        return redirect(route('showForgotPassword'))->with('status', "Your password has been changed.");
    }

    public function logout(){
        auth('front')->logout();
        setcookie('userId', '', time()-25920000, '/');
        $random_string = md5(microtime());
        setcookie('userId', $random_string, time()+2592000, '/');
        return redirect(route('index'));
    }

   

}


