<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StoreCategory;
use App\StoreProduct;
use App\Settings;
use App\ProductsState;
use App\CategoriesState;


use Illuminate\Support\Facades\Input as input;
use Config;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class SearchController extends Controller
{

	public function __construct()
    {
        $this->api_url = Config::get('constants.reiss_master_api_url');
        
    }


    //
    public function index(Request $request){

    	$searchWord = $request->searchWord;
    	$catData = StoreCategory::where('name','like',$searchWord.'%')->where('isActive',1);
    	$siteData = getSetting();
		if($siteData['reiss_cat']==0){
			$catData = $catData->where('is_user_cat',1);
		}
		$catData = $catData->take(10)->get();
		//dd($catData);

		$html = '';
		foreach($catData as $cat){
			$html .= '<li><a href="'.route('reiss.products',['cat_url'=>$cat->slug]).'">'.$cat->name.' in <span class="higlite">Categories</span></a></li>';
		}

		if($siteData['reiss_cat']==1){

			$store_name = Settings::store_name();
			$in_active_categories = CategoriesState::all()->pluck('category_id')->toArray();
			//dd($in_active_categories);
			$parameters = ['searchWord' => $searchWord, 'store_name' => $store_name, 'in_active_categories' => json_encode($in_active_categories), ];
			$url = $this->api_url.'searchByCategory';
	        $client = new Client();
	        $result = $client->post($url, ['query' => $parameters]);
	        $response = json_decode($result->getBody()->getContents(), true);
	        // dd($response);
	        if(!empty($response['categories']) > 0)
		        foreach($response['categories'] as $cat){
		        	//dd($prod);
					$html .= '<li><a href="'.route('reiss.products',['cat_url'=>$cat['slug']]).'">'.$cat['name'].' in <span class="higlite">Categories</span></a></li>';
				}
	        
	    }



		$prodData = StoreProduct::where('name','like',$searchWord.'%')->where('isActive',1);
    	if($siteData['reiss_prod']==0){
			$prodData = $prodData->where('is_user_prod',1);
		}
		$prodData = $prodData->take(6)->get();

		foreach($prodData as $prod){
			$html .= '<li><a href="'.route('reiss.product.details',['prod_url'=>$prod->slug]).'">'.$prod->name.' in <span class="higlite">Products</span></a></li>';
		}
		
		if($siteData['reiss_prod']==1){
			$store_name = Settings::store_name();
			$in_active_products = ProductsState::all()->pluck('product_id')->toArray();
			$parameters = ['searchWord' => $searchWord, 'store_name' => $store_name, 'in_active_products' => json_encode($in_active_products), ];
			$url = $this->api_url.'searchByProduct';
	        $client = new Client();
	        $result = $client->post($url, ['query' => $parameters]);
	        $response = json_decode($result->getBody()->getContents(), true);
	        if(!empty($response['products']) > 0)
		        foreach($response['products'] as $prod){
		        	//dd($prod);
					$html .= '<li><a href="'.route('reiss.product.details',['prod_url'=>$prod['slug']]).'">'.$prod['name'].' in <span class="higlite">Products</span></a></li>';
				}
		}


		return $html;
    }
}
