<?php

namespace App\Http\Controllers\Reiss;


use Illuminate\Support\Facades\Input as input;
use Illuminate\Support\Facades\Mail;

use App\Mail\My_list;
use App\Mail\OrderReview;

use Illuminate\Support\Facades\Session;
use JMS\Serializer\Tests\Fixtures\Order;
use PDF;


use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\CategoriesState;
use App\StoreCategory;
use App\Settings;
use App\Lists;
use App\States;
use App\Orders;
use App\Order_details;
use App\Lib\Authorize;

use Carbon\Carbon;
use Config;
use DB;
use Route;


class CartController extends Controller
{
    public function __construct(Request $request)
    {
        $this->api_url = Config::get('constants.reiss_master_api_url');
        $this->request = $request;

    }

    public function list()
    {
        $products = DB::table('lists')
            ->join('store_products', 'store_products.id', '=', 'lists.product_id')
            ->select('store_products.id', 'store_products.product_image', 'store_products.is_bulk','store_products.ImageFile','store_products.sku', 'store_products.name', 'store_products.price', 'store_products.is_user_prod', 'store_products.is_price', 'lists.id as listId', 'lists.qty')
            ->where('isActive', 1)
            ->where('lists.type', 'local')
            ->where('lists.userId', @$_COOKIE['userId'])
            ->get();



        $remote_products = Lists::where('userId', @$_COOKIE['userId'])->whereNotIn('product_id', $products->pluck('id')->toArray());
        $remote_products_ids = $remote_products->pluck('product_id', 'id')->toArray();
        $remote_products_qtys = $remote_products->pluck('qty', 'product_id')->toArray();


        //convert array of objects into array of arrays...
        $products = $products->map(function($item, $index){
            $item->prodcut_location_type = 'local';
            return (array)$item;
        })->toArray();

        if ( count($remote_products_ids) )
        {
            $settings = Settings::first();

            $parameters = ['store_name' => $settings->store_name, 'product_ids' => $remote_products_ids];
            $url = $this->api_url.'getProductsByIds';

            try
            {
                $client = new Client();
                $result = $client->get($url, ['query' => $parameters]);

                $response = json_decode($result->getBody()->getContents(), true);

                if ( $response['status'] == 'success' )
                {
                    $remote_products_ids = array_flip($remote_products_ids);

                    $response = array_map(function($item) use ($remote_products_ids, $remote_products_qtys, $settings) {
                        $item['listId'] = $remote_products_ids[$item['id']];
                        $item['is_price'] = $settings->reiss_prod_price;
                        $item['qty'] = $remote_products_qtys[$item['id']];
                        return $item;
                    }, $response['products']);

                    $products = array_merge($products, $response);
                }
            }
            catch (GuzzleException $e)
            {
                // return ['error' => $e->getResponse()->getBody()->getContents()];
            }
        }

        // dd(['products' => collect($products), 'active' =>'my-list']);

        return view('front.cart', ['products' => collect($products), 'active' =>'my-list']);
    }


    public function billingShipping(){

        if (!auth('front')->check()){
            return redirect(route('loginRegister'))->send();
        }

        if(count($this->countList()) <= 0){
            return redirect(route('displayList'));
        }
        $states = States::all();
        return view('front.my-cart-shipping-billing',['states'=>$states, 'active' =>'my-list']);
    }

    public function billShipSave(Request $request){

        if (!auth('front')->check()){
            return redirect(route('loginRegister'))->send();
        }

        if(count($this->countList()) <= 0){
            return redirect(route('displayList'));
        }

        $this->validate($request,[
            'shipfname'=>'required',
            'shiplname'=>'required',
            'shipemail'=>'required|email',
            'shipphone'=>'required',
            'shipcity'=>'required',
            'shipaddress'=>'required',
            'shipstate'=>'required',
            'shipzip'=>'required',

            'billfname'=>'required',
            'billlname'=>'required',
            'billemail'=>'required|email',
            'billphone'=>'required',
            'billcity'=>'required',
            'billaddress'=>'required',
            'billstate'=>'required',
            'billzip'=>'required',
            'store_shiping'=>'required',
        ],
            [
                'required'=>'This field is required.',
                'email'=> 'Please enter the valid email address.',

            ]);

        $billShip = array(
            'shipfname' => $request->shipfname,
            'shiplname' => $request->shiplname,
            'shipemail' => $request->shipemail,
            'shipphone' => $request->shipphone,
            'shipaddress' => $request->shipaddress,
            'shipcity' => $request->shipcity,
            'shipstate' => $request->shipstate,
            'shipzip' => $request->shipzip,
            
            // 'shipfname' => $request->billfname,
            // 'shiplname' => $request->billlname,
            // 'shipemail' => $request->billemail,
            // 'shipphone' => $request->billphone,
            // 'shipaddress' => $request->billaddress,
            // 'shipcity' => $request->billcity,
            // 'shipstate' => $request->billstate,
            // 'shipzip' => $request->billzip,

            'billfname' => $request->billfname,
            'billlname' => $request->billlname,
            'billemail' => $request->billemail,
            'billphone' => $request->billphone,
            'billcity' => $request->billcity,
            'billaddress' => $request->billaddress,
            'billstate' => $request->billstate,
            'billzip' => $request->billzip,
            'store_shiping' => $request->store_shiping,
        );
        session(['billShip' => $billShip]);

        return redirect(route('paymentInfo'));
    }

    public function paymentInfo(Request $request){
        //dd($request->session()->all());
        if ( empty( session('billShip') ) ){
            return redirect(route('checkout'));
        }
        return view('front.my-cart-payment-info',['active' =>'my-list']);
    }

    public function paymentInfoGet(Request $request){
        $this->validate($request,
            [
                'cardName'=>'required',
                'cardNumber'=>'required',
                'month'=>'required',
                'year'=>'required',
                'CVCNumber' => 'required|numeric'
            ],
            [
                'required' => 'This field is required.',
                'numeric' => 'This field must be numeric.'

            ]
        );

        $totalPrice = 0.00;
        $products = $this->countList();
        foreach($products as $list){
            if ( isset($list['prodcut_location_type']) && $list['prodcut_location_type'] != '' )
                $totalPrice += ($list['price'] * $list['qty']);
            else
                $totalPrice += (($list['price']+($list['price']*getSettingPrice())) * $list['qty']);
        }

        $address = session('billShip');

        $expiry_date = $request->year.'-'.$request->month;

        $auth_data = array(
            'card_no' => $request->cardNumber,
            'card_cvc' => $request->CVCNumber,
            'card_exp' => $expiry_date,
            'amount' => $totalPrice,
            'bill_fname' => $address['billfname'],
            'bill_lname' => $address['billlname'],
            'bill_address' => $address['billaddress'],
            'bill_city' => $address['billcity'],
            'bill_state' => $address['billstate'],
            'bill_zip' => $address['billzip'],
            'bill_country' => "USA",
        );

        $test = new Authorize;

        $card_response = $test->authorize_card($auth_data);


        if(isset($card_response['auth_code']))
        {
            $cardInfo = array(
                'cardName' => $request->cardName,
                'cardNumber' => $request->cardNumber,
                'month' => $request->month,
                'year' => $request->year,
                'cvc' => $request->CVCNumber
            );
            session(['cardInfo'=>$cardInfo]);
            return redirect(route('cartConfirm'));
        }
        else
        {
            //dd($card_response);
            return redirect(route('paymentInfo'))->with('msg',$card_response);
        }
    }


    public function cartConfirm(){

        if (!auth('front')->check()){
            return redirect(route('loginRegister'))->send();
        }

        if(count($this->countList()) <= 0){
            return redirect(route('displayList'));
        }

        if ( empty( session('billShip') ) ){
            return redirect(route('checkout'));
        }
        //dd(session('billShip'));
        return view('front.cart-confirm',['products'=>$this->countList(), 'address'=>session('billShip'), 'cartInfo'=>session('cardInfo'), 'active' =>'my-list']);

    }

    public function payment(){

        if (!auth('front')->check()){
            return redirect(route('loginRegister'))->send();
        }
        if(empty(session('cardInfo'))){
            return redirect(route('checkout'));
        }
        //dd(session()->all());
        $totalPrice = 0.00;
        $tax = 0.0;
        $shiping_charges = 0.0;
        $products = $this->countList();
        foreach($products as $list){
            if ( isset($list['prodcut_location_type']) && $list['prodcut_location_type'] != '' )
                $totalPrice += ($list['price'] * $list['qty']);
            else
                $totalPrice += ( number_format(getReissNewPriceFront($list['id'], ($list['price']+($list['price']*getSettingPrice())) ) * $list['qty'],2,'.',''));
        }

        if (session()->all()['billShip']['shipstate'] == "New Jersey")
        {
            $tax = ($totalPrice * 6.625)/100;

        }
        //dd(session('billShip'));
        if ((session('billShip')['store_shiping'] == "shipping"))
        {
            if ($totalPrice <20)
            {
                $shiping_charges = 10.25;
            }
            elseif (($totalPrice <100)&&($totalPrice>=20))
            {
                $shiping_charges = 26.75;
            }
            elseif (($totalPrice <200)&&($totalPrice>=100))
            {
                $shiping_charges = 45.63;
            }
            elseif (($totalPrice <300)&&($totalPrice>=200))
            {
                $shiping_charges = 65.85;
            }
            elseif (($totalPrice <400)&&($totalPrice>=300))
            {
                $shiping_charges = 89.52;
            }
            elseif (($totalPrice <500)&&($totalPrice>=400))
            {
                $shiping_charges = 105.58;
            }
            else
            {
                $shiping_charges = 0.0;
            }
        }
        $grand = $totalPrice + $tax + $shiping_charges;
        //dd($grand);
        session(['grand' => $grand]);
        session(['totalPrice' => $totalPrice]);
        session(['tax' => $tax]);
        session(['shiping_charges' => $shiping_charges]);

        //dd($grand);
        $cartInfo = session('cardInfo');
        $address = session('billShip');

        //payment not through the saved payment
        $expiry_date = $cartInfo['year'].'-'.$cartInfo['month'];

        $auth_data = array(
            'card_no' => $cartInfo['cardNumber'],
            'card_cvc' => $cartInfo['cvc'],
            'card_exp' => $expiry_date,
            'email' => $address['billemail'],
            'user_id' => date('mdyHis'),
            'amount' => $grand
        );

        $test = new Authorize;

        $card_response = $test->charge_credit_card($auth_data);

        if(isset($card_response['auth_code']))
        {
            $array['order_auth_code'] = $card_response['auth_code'];
            $array['order_auth_transaction_id'] = $card_response['auth_transaction_id'];
            session(['apiResponse'=>$array]);

            //return redirect(route('thankyou'));
            return redirect(route('emailList'));
        }
        else
        {
            //dd($card_response);
            return redirect(route('paymentInfo'))->with('msg',$card_response);
        }


    }

    public function thankYou(){
        $api = session('apiResponse');
        $cartInfo = session('cardInfo');
        $address = session('billShip');
        return view('front.thank-you', ['active'=>'my-list']);
    }




    public function countList(){

        $products = DB::table('lists')
            ->join('store_products', 'store_products.id', '=', 'lists.product_id')
            ->select('store_products.id', 'store_products.is_bulk','store_products.ImageFile', 'store_products.product_image', 'store_products.sku', 'store_products.name', 'store_products.price', 'store_products.is_user_prod', 'store_products.is_price', 'lists.id as listId', 'lists.qty')
            ->where('isActive', 1)
            ->where('lists.type', 'local')
            ->where('lists.userId', @$_COOKIE['userId'])
            ->get();

        $remote_products = Lists::where('userId', @$_COOKIE['userId'])->whereNotIn('product_id', $products->pluck('id')->toArray());
        $remote_products_ids = $remote_products->pluck('product_id', 'id')->toArray();
        $remote_products_qtys = $remote_products->pluck('qty', 'product_id')->toArray();

        //convert array of objects into array of arrays...
        $products = $products->map(function($item, $index){
            $item->prodcut_location_type = 'local';
            return (array)$item;
        })->toArray();

        if ( count($remote_products_ids) )
        {
            $settings = Settings::first();

            $parameters = ['store_name' => $settings->store_name, 'product_ids' => $remote_products_ids];
            $url = $this->api_url.'getProductsByIds';

            try
            {
                $client = new Client();
                $result = $client->get($url, ['query' => $parameters]);

                $response = json_decode($result->getBody()->getContents(), true);

                if ( $response['status'] == 'success' )
                {
                    $remote_products_ids = array_flip($remote_products_ids);

                    $response = array_map(function($item) use ($remote_products_ids, $remote_products_qtys, $settings) {
                        $item['listId'] = $remote_products_ids[$item['id']];
                        $item['is_price'] = $settings->reiss_prod_price;
                        $item['qty'] = $remote_products_qtys[$item['id']];
                        return $item;
                    }, $response['products']);

                    $products = array_merge($products, $response);
                }
            }
            catch (GuzzleException $e)
            {
                // return ['error' => $e->getResponse()->getBody()->getContents()];
            }
        }

        return $products;

    }

    public function email_list1(Request $request){

        if (!auth('front')->check()){
            return redirect(route('loginRegister'))->send();
        }

        if(empty(session('cardInfo'))){
            return redirect(route('checkout'));
        }

        $all['listData'] = $this->countList();

        $all['cardInfo'] = session('cardInfo');
        $all['billShip'] = session('billShip');
        $all['transaction_id'] = session('order_auth_transaction_id');
        $trans = Session('apiResponse');
        $trans_id = $trans['order_auth_transaction_id'];
        $all['settings'] = getSetting();
        $all['logo'] = asset('site_logo').'/'.$all['settings']['logo'];

        /*echo '<pre>';
        print_r($all['cardInfo']);
        print_r($all['billShip']);
        echo $all['listData'][0]['name'];
        dd($all['listData']);*/

        $order = new Orders();
        $order->shipfname = $all['billShip']['shipfname'];
        $order->shiplname = $all['billShip']['shiplname'];
        $order->shipemail = $all['billShip']['shipemail'];
        $order->shipphone = $all['billShip']['shipphone'];
        $order->shipaddress = $all['billShip']['shipaddress'];
        $order->shipcity = $all['billShip']['shipcity'];
        $order->shipstate = $all['billShip']['shipstate'];
        $order->shipzip = $all['billShip']['shipzip'];

        $order->billfname = $all['billShip']['billfname'];
        $order->billlname = $all['billShip']['billlname'];
        $order->billemail = $all['billShip']['billemail'];
        $order->billphone = $all['billShip']['billphone'];
        $order->billaddress = $all['billShip']['billaddress'];
        $order->billcity = $all['billShip']['billcity'];
        $order->billstate = $all['billShip']['billstate'];
        $order->billzip = $all['billShip']['billzip'];

        $order->cardName = $all['cardInfo']['cardName'];
        $order->cardNumber = substr($all['cardInfo']['cardNumber'],-4);
        $order->month = $all['cardInfo']['month'];
        $order->year = $all['cardInfo']['year'];

        $order->user_id = auth('front')->user()->id;
        $order->user_fname = auth('front')->user()->first_name;
        $order->user_lname = auth('front')->user()->last_name;
        $order->user_email = auth('front')->user()->email;
        $order->transaction_id = $trans_id;

        $order->save();

        $order_id = $order->id;
        $totalPrice = 0.00;
        $skus = '';
        foreach($all['listData'] as $list){
            if ($skus =='')
            {
                $skus = $list['sku'];
            }
            else
            {
                $skus = $skus . ',' . $list['sku'];
            }

            $order_detail = new Order_details();

            $order_detail->orders_id = $order_id;
            $order_detail->product_id = $list['sku'];
            $order_detail->name = $list['name'];

            $order_detail->qty = $list['qty'];

            $order_detail->price =  number_format(getReissNewPriceFront($list['id'], ($list['price']+($list['price']*getSettingPrice())) ) ,2,'.','');
            $order_detail->subTotal =  number_format(getReissNewPriceFront($list['id'], ($list['price']+($list['price']*getSettingPrice())) ) * $list['qty'],2,'.','');

            if ( isset($list['prodcut_location_type']) && $list['prodcut_location_type'] != '' ){
                $order_detail->price = number_format($list['price'],2,'.','');
                $order_detail->subTotal = number_format($list['qty'] * $list['price'],2,'.','');
            }
            //$order_detail->subTotal = number_format($list['qty'] * $list['price'],2,'.','');

            $totalPrice += $order_detail->subTotal;

            $order_detail->imgPath = "https://reisshardware.com/product_images/".$list['sku'].".JPG";
            if(@$list['is_bulk'] == 1)
                if (file_exists(public_path('bulk_upload_img').'/'.$list['ImageFile'].'/'.$list['ImageFile'].'-01.jpg' ) )
                    $order_detail->imgPath =  asset('bulk_upload_img')."/".$list['ImageFile']."/".$list['ImageFile'].'-01.jpg';
                elseif (file_exists(public_path('bulk_upload_img').'/'.$list['ImageFile'].'/'.$list['ImageFile'].'-01.JPG' ) )
                    $order_detail->imgPath =  asset('bulk_upload_img')."/".$list['ImageFile']."/".$list['ImageFile'].'-01.JPG';
                elseif (file_exists(public_path('bulk_upload_img').'/'.$list['ImageFile'].'/'.$list['ImageFile'].'-01.png' ) )
                    $order_detail->imgPath =  asset('bulk_upload_img')."/".$list['ImageFile']."/".$list['ImageFile'].'-01.png';
                elseif (file_exists(public_path('bulk_upload_img').'/'.$list['ImageFile'].'/'.$list['ImageFile'].'-01.PNG' ) )
                    $order_detail->imgPath =  asset('bulk_upload_img')."/".$list['ImageFile']."/".$list['ImageFile'].'-01.PNG';

                else{
                    if ( isset($list['prodcut_location_type']) && $list['prodcut_location_type'] != '' ){
                        $order_detail->imgPath = asset('products_images/thumbs')."/".$list['product_image'];
                    }
                }
            $order_detail->save();
        }
        $order = Orders::find($order_id);

        $order->skus = $skus;
        $order->update();
        $orderUpdate = Orders::find($order_id);
        //dd(session('billShip')['store_shiping']);
        $orderUpdate->totalPrice = number_format(session('grand'),2,'.','');
        $orderUpdate->shiping_charges = number_format(session('shiping_charges'),2,'.','');
        $orderUpdate->tax = number_format(session('tax'),2,'.','');
        $orderUpdate->sub_total = number_format(session('totalPrice'),2,'.','');
        $orderUpdate->store_shiping = session('billShip')['store_shiping'];
        $orderUpdate->save();

        //Mail::to( "mikesmith1166@gmail.com", "Mike Smith" )->send(new OrderReview( $all ));

        // $request->session()->flush();
        $request->session()->forget('cardInfo');
        $request->session()->forget('billShip');
        //cardInfo
        // billShip
        $deletedRows = Lists::where('userId', $_COOKIE['userId'])->delete();
        $cookie = \Cookie::forget('userId');
        return redirect()->route('thankyou');
    }



    public function email_list(){
        $email = 'safetylinecorp@gmail.com';
        $name = 'safetylinecorp';

        $listData = $this->countList();
        $cartInfo = session('cardInfo');
        $address = session('billShip');
        //$_COOKIE['userId']
        $siteData = getSetting();




        $html = new My_list(['lData'=>$listData]);

        $pdf = PDF::loadView('front.pdf.my-cart-pdf',['lData' => $listData, 'siteData'=>$siteData]);
        return $pdf->download('my-list.pdf');


        //Mail::to('mikesmith1166@gmail.com', 'name')->send($html);
        Mail::to($email, $name)->send($html);
        //return redirect(route('displayList'));
        echo route('displayList');// home page
    }







}