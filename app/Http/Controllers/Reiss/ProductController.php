<?php

namespace App\Http\Controllers\Reiss;

use App\Http\Controllers\Controller;
use App\ProductsState;
use App\Settings;
use App\StoreCategory;
use App\StoreProduct;
use Config;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use App\CategoriesState;
use Image;

class ProductController extends Controller
{
    public function __construct(Request $request)
    {
        $this->api_url = Config::get('constants.reiss_master_api_url');
        $this->request = $request;
    }

    function list($category_slug) {
        $per = Settings::find(1);
        try
        {
            if ($category_slug == '') {
                return redirect()->route('index');
            }


            if (StoreCategory::where('slug', $category_slug)->where('isActive',1)->count()) {
                return $this->local_products($category_slug);
            }

            $page = 1;

            if ($this->request->has('page')) {
                $page = $this->request->get('page');
            }

            $perPage = 15;
            $offset  = ($page * $perPage) - $perPage;

            $store_name = Settings::store_name();

            $in_active_products = implode(',', ProductsState::all()->pluck('product_id')->toArray());
            $in_active_categories = implode(',', CategoriesState::all()->pluck('category_id')->toArray());
            // dd($in_active_categories);

            $parameters = ['category_name' => $category_slug, 'store_name' => $store_name, 'in_active_categories'=>$in_active_categories, 'in_active_products' => $in_active_products, 'page' => $page, 'perPage' => $perPage];

            $url = $this->api_url . 'productsByCategory';
            $client = new Client();
            $result = $client->get($url, ['query' => $parameters]);
// dd($result);
            

            $response = json_decode($result->getBody()->getContents(), true);
            // exit;

            if ($response['status'] == 'success') {
                $categories = reissAndOwnCategories();

                $levelOneCategories = collect($categories)->filter(function ($item, $key) {
                    return $item->level == 1;
                })->values();

                $foreign_category_id = collect($categories)->filter(function ($item, $key) use ($category_slug) {
                    return $item->slug == $category_slug;
                })->first();

                $merged_products = $response['products'];
                //echo $perPage.' -> '.$offset.' -> '.count($merged_products);

                if ($foreign_category_id && $foreign_category_id->id) {
                    $local_per_page = 5;
                    $offset         = ($page * $local_per_page) - $local_per_page;

                    $local_products = StoreProduct::where(['store_category_id' => $foreign_category_id->id, 'type' => 'master']);
                    //echo count($local_products->get());

                    $merged_products = array_merge($response['products'], $local_products->take($local_per_page)->skip($offset)->get()->toArray());
                }
                $paginator = array();
                //echo $perPage.' -> '.$offset.' -> '.count($merged_products);

                if (count($merged_products) > 0 && $per->reiss_prod) {
                    $paginator = new LengthAwarePaginator($merged_products, $response['total'], count($merged_products), $page, ['path' => Paginator::resolveCurrentPath()]);
                }

                // to add extra query parameters with pagination urls...
                // $paginator->appends(['cat_url' => $category_slug]);
                // dd($response['paren_category_ids']);
                return view('front.reiss-product-listing', ['products' => $paginator,
                    'total'                                                => $response['total'],
                    'catName'                                              => $response['category']['name'],
                    'parent_categories'                                    => $response['paren_category_ids'],
                    'categories'                                           => $categories,
                    'levelOneCategories'                                   => $levelOneCategories,
                    'active'                                               => 'products',
                    'product_type'                                         => 'reiss',
                ]);
            } else {
                return redirect()->route('index');
                // return ['error' => $response['message']];
            }
        } catch (GuzzleException $e) {
            return ['error' => $e->getResponse()->getBody()->getContents()];
            return redirect()->route('index');
        }
    }

    public function details($product_slug)
    {
        if ($product_slug == '') {
            return redirect()->route('index');
        }

        if (StoreProduct::where('slug', $product_slug)->where('isActive',1)->count()) {
            return $this->local_product($product_slug);
        }

        try
        {
            $in_active_products = implode(',', ProductsState::all()->pluck('product_id')->toArray());
            $in_active_categories = implode(',', CategoriesState::all()->pluck('category_id')->toArray());

            $parameters = ['store_name' => Settings::store_name(),'in_active_products'=>$in_active_products, 'in_active_categories'=>$in_active_categories];

            $url = $this->api_url . "productDetails/$product_slug";

            $client = new Client();
            $result = $client->get($url, ['query' => $parameters]);

            $response = json_decode($result->getBody()->getContents());
            // dd($response);

            if ($response->status == 'success') {
                return view('front.reiss-product-details', ['prodData' => $response->product, 'active' => 'products', 'product_type' => 'reiss']);
            } else {
                // return ['error' => $response['message']];
                return redirect()->route('index');
            }
        } catch (GuzzleException $e) {
            return ['error' => $e->getResponse()->getBody()->getContents()];
            return redirect()->route('index');
        }
    }

    public function create()
    {
        $this->validate($this->request, [
            'product_name'      => "required|string|max:255",
            'manufacturer_name' => "required|string|max:255",
            'sku'               => "required|string|max:255",
            'Description'       => "required",
            'manufacturer_code' => "required|string|max:255",
            'upc'               => "required|string|max:255",
            'package_quantity'  => "required|numeric|min:1",
            'price'             => "required|numeric|min:1",
            'case_quantity'     => "required|numeric|min:1",
            'product_thumb'     => 'required|max:10000|mimes:jpeg,png,jpg',
        ]);

        if ($this->request->hasFile('product_thumb')) {
            $path = realpath(public_path('products_images/'));

            $image = $this->request->file('product_thumb');

            $image_name = time() . '.' . $image->getClientOriginalExtension();

            $thumbs_path = "$path/thumbs";

            $img = Image::make($image->getRealPath());

            $img->resize(100, 150, function ($constraint) {
                $constraint->aspectRatio();
            })->save("$thumbs_path/$image_name");

            $image->move($path, $image_name);

            $product = new StoreProduct();
            //$product->id = $this->random_number();
            $product->name              = $this->request->product_name;
            $product->manufacturer      = $this->request->manufacturer_name;
            $product->manufacturer_code = $this->request->manufacturer_code;
            $product->upc               = $this->request->upc;
            $product->sku               = $this->request->sku;
            $product->Description       = preg_replace('/[\x95\x99]/', '', str_replace("\x94", '"', $this->request->Description));
            $product->price             = $this->request->price;
            $product->package_quantity  = $this->request->package_quantity;
            $product->case_quantity     = $this->request->case_quantity;
            $product->product_image     = $image_name;
            $product->store_category_id = $this->request->category;
            $product->slug              = $this->create_slug($this->request->product_name, 'store_products', 'slug');
            $product->type              = is_numeric($this->request->category) ? 'master' : 'local';
            $product->is_feature            = $this->request->feature_product;
            $product->save();
        }
    }

    private function random_number()
    {
        $random = str_random(20);

        if (StoreProduct::where('id', $random)->count() == 0) {
            return $random;
        }

        $this->random_number();
    }

    public function get($id)
    {
        $columns = ['case_quantity',
            'manufacturer',
            'manufacturer_code',
            'name',
            'package_quantity',
            'price',
            'product_image',
            'sku',
            'Description',
            'slug',
            'store_category_id',
            'upc',
            'id',
        ];
        $columns = '*';

        return StoreProduct::select($columns)->where('id', $id)->first();
    }

    public function update()
    {
        $this->validate($this->request, [
            'product_name'      => "required|string|max:255",
            'manufacturer_name' => "required|string|max:255",
            'sku'               => "required|string|max:255",
            'Description'       => "required",
            'manufacturer_code' => "required|string|max:255",
            'upc'               => "required|string|max:255",
            'package_quantity'  => "required|numeric|min:1",
            'price'             => "required|numeric|min:1",
            'case_quantity'     => "required|numeric|min:1",
        ]);

        $image_name = '';

        $product = StoreProduct::find($this->request->id);

        if ($this->request->hasFile('product_thumb')) {
            $this->validate($this->request, ['product_thumb' => 'required|max:10000|mimes:jpeg,png,jpg']);

            if (!is_null($product)) {
                $path = realpath(public_path('products_images/'));

                $image = $this->request->file('product_thumb');

                if ($product->product_image == '') {
                    $image_name = time() . '.' . $image->getClientOriginalExtension();
                } else {
                    //this will over write the previous image...
                    $image_name = $product->product_image;
                }

                $thumbs_path = "$path/thumbs";

                $img = Image::make($image->getRealPath());

                $img->resize(100, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save("$thumbs_path/$image_name");

                $image->move($path, $image_name);
            }
        }
        // dd(request()->all());
        if (!is_null($product)) {
            $product->name              = $this->request->product_name;
            $product->manufacturer      = $this->request->manufacturer_name;
            $product->manufacturer_code = $this->request->manufacturer_code;
            $product->upc               = $this->request->upc;
            $product->sku               = $this->request->sku;
            $product->Description       = preg_replace('/[\x95\x99]/', '', str_replace("\x94", '"', $this->request->Description));
            $product->price             = $this->request->price;
            $product->package_quantity  = $this->request->package_quantity;
            $product->case_quantity     = $this->request->case_quantity;

            if ($image_name !== '') {
                $product->product_image = $image_name;
            }

            $product->store_category_id = $this->request->category;
            $product->slug              = $this->create_slug($this->request->product_name, 'store_products', 'slug', 'id', $this->request->id);
            $product->type              = is_numeric($this->request->category) ? 'master' : 'local';
            $product->is_feature            = $this->request->feature_product;
            $product->save();
        }
    }

    public function bulkupdate()
    {
        $image_name = '';
        $this->validate($this->request, [
            'product_name'          => "string|required|max:255",
            'sku'                   => "string|required|max:255",
            'upc'                   => "string|required|max:255",
            'price'                 => "numeric|min:1|required",
            'AverageShippingCost'   => "required|max:200",
            'Description'           => "required",
            'BulletPointsClosure'   => "required|max:200",
            'BulletPointsColor'     => "required|max:200",
            'BulletPointsMaterial'  => "required|max:200",
            'BulletPointsPockets'   => "required|max:200",
            'ItemNumber'            => "required|max:200",
            'MinimumOrder'          => "max:200",
            'ShipWeight'            => "required|max:200",
            'ShippingBoxDimensions' => "required|max:200",
            'ShipsVia'              => "required|max:200",
            'Standard'              => "required|max:200",
            'TariffCode'            => "max:200",
            'UsuallyShipsIn'        => "required|max:200",
        ], [
            'required' => 'This field is required.',
        ]);
        // dd($request->all());
        $product = StoreProduct::find($this->request->id);

        if ($this->request->hasFile('product_thumb')) {
            $this->validate($this->request, ['page_image' => 'required|max:10000|mimes:jpeg,png,jpg']);

            if (!is_null($product)) {
                $path  = realpath(public_path('products_images/'));
                $image = $this->request->file('product_thumb');
                if ($product->product_image == '') {
                    $image_name = time() . '.' . $image->getClientOriginalExtension();
                } else {
                    //this will over write the previous image...
                    $image_name = $product->product_image;
                }
                $thumbs_path = "$path/thumbs";
                $img         = Image::make($image->getRealPath());
                $img->resize(100, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save("$thumbs_path/$image_name");
                $image->move($path, $image_name);
            }
        }
    

        if (!is_null($product)) {
            $product->name                  = preg_replace('/[\x95\x99]/', '', str_replace("\x94", '"', $this->request->product_name));
            $product->sku                   = $this->request->sku;
            $product->upc                   = $this->request->upc;
            $product->price                 = $this->request->price;
            $product->is_feature            = $this->request->feature_product;
            $product->AverageShippingCost   = $this->request->AverageShippingCost;
            $product->Description           = preg_replace('/[\x95\x99]/', '', str_replace("\x94", '"', $this->request->Description));
            $product->BulletPointsClosure   = $this->request->BulletPointsClosure;
            $product->BulletPointsColor     = $this->request->BulletPointsColor;
            $product->BulletPointsMaterial  = preg_replace('/[\x95\x99]/', '', str_replace("\x94", '"', $this->request->BulletPointsMaterial));
            $product->BulletPointsPockets   = $this->request->BulletPointsPockets;
            $product->ItemNumber            = $this->request->ItemNumber;
            $product->MinimumOrder          = $this->request->MinimumOrder;
            $product->ShipWeight            = $this->request->ShipWeight;
            $product->ShippingBoxDimensions = $this->request->ShippingBoxDimensions;
            $product->ShipsVia              = $this->request->ShipsVia;
            $product->Standard              = $this->request->Standard;
            $product->TariffCode            = $this->request->TariffCode;
            $product->UsuallyShipsIn        = $this->request->UsuallyShipsIn;

            if ($image_name !== '') {
                $product->product_image = $image_name;
            }

            $product->store_category_id = $this->request->category;
            $product->slug              = $this->create_slug($this->request->product_name, 'store_products', 'slug', 'id', $this->request->id);
            $product->save();
        }

    }

    public function bulkEdit()
    {
        print_r($_GET);
        $product = StoreProduct::find($_GET['edit']);
        return view('product.bulkEdit', ['product' => $product]);
        //product.bulkEdit
    }

    public function delete()
    {
        $product = StoreProduct::find($this->request->id);

        if (!is_null($product)) {
            $product->forceDelete();
        }

    }

    public function toggle()
    {
        if ($this->request->toggle == 'enable') {
            ProductsState::where('product_id', $this->request->id)->delete();
        } else {
            $product             = new ProductsState();
            $product->product_id = $this->request->id;
            $product->save();
        }
    }

    private function local_products($cat_url)
    {
        $getDataByUrl = StoreCategory::where('slug', $cat_url)->where('isActive', 1)->first();

        if ($getDataByUrl == null) {
            return redirect(route('index'));
        }

        $newArray    = explode(',', $this->getAllSubChildIds($getDataByUrl->id));
        $parentArray = explode(',', $this->getParents($getDataByUrl->id));

        $CategoryProducts = StoreProduct::whereIn('store_category_id', $newArray)->where('isActive', 1);
        $total            = count($CategoryProducts->get());
        //dd($CategoryProducts->get());
        $CategoryProducts = $CategoryProducts->simplePaginate(15);

        $categories = reissAndOwnCategories();

        $levelOneCategories = collect($categories)->filter(function ($item, $key) {
            return $item->level == 1;
        })->values();
        return view('front.reiss-product-listing', ['products' => $CategoryProducts,
            'total'                                                => $total,
            'catName'                                              => $getDataByUrl['name'],
            'parent_categories'                                    => $parentArray,
            'categories'                                           => $categories,
            'levelOneCategories'                                   => $levelOneCategories,
            'active'                                               => 'products',
            'product_type'                                         => 'local',
        ]);
    }

    private function local_product($product_slug)
    {
        $product = StoreProduct::where('slug', $product_slug)->where('isActive', 1)->first();

        if (!is_null($product)) {
            return view('front.reiss-product-details', ['prodData' => $product, 'active' => 'products', 'product_type' => 'local']);
        }

        return redirect()->route('index');
    }

    protected function getAllSubChildIds($id)
    {
        $newArray = $id;

        $cat = StoreCategory::where('parent_id', $id)->where('isActive', 1)->get()->pluck('id');

        foreach ($cat as $ca) {
            $newArray .= ',' . $this->getAllSubChildIds($ca);
        }

        return $newArray;
    }

    protected function getParents($id)
    {
        $parent = $id;

        $cat = StoreCategory::where('id', $id)->where('isActive', 1)->first();

        if (!is_null($cat->parent_id)) {
            $parent .= ',' . $this->getParents($cat->parent_id);
        }

        return $parent;
    }

    protected function create_slug($title, $table_name, $field_name, $nId_name = '', $nId = '', $count = 0)
    {
        //echo "Product Id = ".$nId."<br>";
        $slug = strtolower(trim($title));

        $slug = str_replace(' ', '-', $slug); // Replaces all spaces with hyphens.
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug); // Removes special chars.
        $slug = preg_replace('/-+/', '-', $slug); // Replaces multiple hyphens with single one.
        //echo "count = ".$count;
        if ($count == 0) {
            $slug = $slug;
        } else {
            $slug = $slug . '-' . $count;
        }

        $query = DB::table($table_name)->where($field_name, 'like', $slug . '%')->orWhere($field_name, 'like', $slug);

        if ($nId_name != '' && $nId != '') {
            $query = $query->where($nId_name, '<>', $nId);" AND " . $nId_name . " != " . $nId;
        }

        $query         = $query->get();
        $total_records = count($query);
        if ($total_records > 0) {
            return $this->create_slug($title, $table_name, $field_name, $nId_name, $nId, ++$count);
        } else {
            return $slug;
        }
    }

    public function reissProductPriceEdit($id = '')
    {
        if ($id == '' || !is_numeric($id)) {
            return redirect(route('product.reiss'));
        }
        $reissNewPrice = DB::table('reiss_product_price')->where('product_id', $id)->first();
        if (!empty($reissNewPrice)) {
            $reissNewPrice = $reissNewPrice->product_price;
        } else {
            $reissNewPrice = 0.00;
        }

        $newPrice = number_format($reissNewPrice, 2, '.', '');
        return view('product.reiss-price', ['id' => $id, 'newPrice' => $newPrice]);
    }

    public function reissProductPriceUpdated($id)
    {
        $this->validate($this->request,
            [
                'price' => 'required|min:1|numeric',
            ],
            [
                'required' => 'This field is required.',
                'min'      => 'Price must be greater than zero(0).',
            ]
        );

        DB::table('reiss_product_price')->where('product_id', '=', $id)->delete();

        DB::table('reiss_product_price')->insert(
            ['product_id' => $id, 'product_price' => number_format($this->request->price, 2, '.', '')]
        );

        return redirect(route('product.reiss'));
    }

    public function reissProductNewPrice()
    {
        $id       = $this->request->id;
        $newPrice = getReissNewPrice($id);
        echo $newPrice;
    }

}
