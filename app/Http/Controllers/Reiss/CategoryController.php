<?php

namespace App\Http\Controllers\Reiss;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\CategoriesState;
use App\StoreCategory;

use Config;
use DB;
use Carbon\Carbon;

class CategoryController extends Controller
{
    public function __construct(Request $request)
    {
        $this->api_url = Config::get('constants.reiss_master_api_url');
        $this->request = $request;
    }

    public function create()
    {
        $this->validate($this->request, ['cat_name' => 'required|string|max:255', 'cat_parent' => 'string'],
                                  ['cat_name.required' => "This field cannot be left empty."]
                       );

        $newCategory = new StoreCategory();
        $newCategory->id = $this->random_number();
        $newCategory->name = $this->request->cat_name;

        $dataSlug = $this->create_slug($this->request->cat_name, 'store_categories', 'slug');
       
        if ( $this->request->filled('cat_parent') && $this->request->cat_parent !== "no_parent" )
        {
            $parent = StoreCategory::find($this->request->cat_parent);
            
            if ( !is_null($parent) )
            {
                $newCategory->parent_id = $this->request->cat_parent;
                $newCategory->level = $parent->level + 1;
            }

            $newCategory->slug = $dataSlug;
            $newCategory->save();
        }
        else
        {
            $newCategory->level = 1;
            $newCategory->slug = $dataSlug;
            $newCategory->save();
        }
    }

    public function update()
    {
        $this->validate($request, ['cat_name' => 'required|string|max:255', 'cat_parent' => 'string', 'id' => 'string|required'],
                                  ['cat_name.required' => "This field cannot be left empty"]
                        );

        $newCategory = StoreCategory::find($request->id);

        $newCategory->name = $request->cat_name;
        $dataSlug = $this->create_slug($request->cat_name, 'store_categories', 'slug', 'id', $request->id);

        if ( $request->cat_parent !== "no_parent" )
        {
            $parent = StoreCategory::find($request->cat_parent);

            if ( !is_null($parent) )
            {
                $newCategory->parent_id = $request->cat_parent;
                $newCategory->level = $parent->level + 1;
            }

            $newCategory->slug = $dataSlug;
            $newCategory->save();
        }
        else
        {
            $newCategory->slug = $dataSlug;            
            $newCategory->parent_id = null;
            $newCategory->level = 1;
            $newCategory->save();
        }
    }

    // toggle reiss categories...
    public function toggle()
    {
        $categories = getCategories();

        $id = $this->request->id;

        $exists = collect($categories)->filter(function ($item, $key) use ($id) {
                    return $item->id == $id;
                  })->count();

        if ( $exists )
        {
            $category_ids = explode(',', $this->getSubCategoriesIds($id, $categories));

            //store all categories ids in reiss categories table...
            if (  $this->request->toggle == 'disable' )
            {
                $category_list = [];

                $now = Carbon::now()->toDateTimeString();

                foreach ($category_ids as $category_id)
                {
                    $newArr['category_id'] = $category_id;
                    $newArr['created_at'] = $now;
                    $newArr['updated_at'] = $now;
                    $category_list[] = $newArr;
                }

                CategoriesState::insert($category_list);
            }
            //remove all categories ids from reiss categories table...
            else
            {
                CategoriesState::whereIn('category_id', $category_ids)->delete();
            }

            return ['success' => true];
        }

        return ['success' => false];
    }

    private function getSubCategoriesIds($id, $categories)
    {
        $ids = $id;
        $categoryIds = collect($categories)->filter(function ($item, $key) use ($id) {
                            return $item->parent_id == $id;
                       })->pluck('id');

        foreach($categoryIds as $category)
            $ids .= ','. $this->getSubCategoriesIds($category, $categories);

        return $ids;
    }

    private function random_number()
    {
        $random = str_random(10);
        
        if ( StoreCategory::where('id', $random)->count() == 0 )
            return $random;

        $this->random_number();
    }

    private function create_slug($title, $table_name, $field_name, $nId_name = '', $nId = '', $count = 0)
    {
        //echo "Product Id = ".$nId."<br>";
        $slug = strtolower(trim($title));
        
        $slug = str_replace(' ', '-', $slug); // Replaces all spaces with hyphens.
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug); // Removes special chars.
        $slug = preg_replace('/-+/', '-', $slug); // Replaces multiple hyphens with single one.
        
        //echo "count = ".$count;
        if ( $count == 0 )
        {   
            $slug = $slug;
        }
        else
        {
            $slug = $slug.'-'.$count;
        }
        
        $query = DB::table($table_name)->where($field_name, 'like', $slug.'%')->orWhere($field_name, 'like', $slug);
    
        if ( $nId_name != '' && $nId != '')
        {
            $query = $query->where($nId_name, '<>', $nId); " AND ".$nId_name." != ".$nId;
        }
        
        $query = $query->get();

        $total_records = count($query);
        
        if ( $total_records > 0 )
        {
            return $this->create_slug($title, $table_name, $field_name, $nId_name, $nId, ++$count);
        }
        else
        {
            return $slug;
        }
    }
}