<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StoreCategory;
use App\StoreProduct;
use App\Pages;
use App\Homepage;
use App\Contact;
use DB;

class HomepageController extends Controller
{
    private $storeProducts;

    function __construct(StoreProduct $storeProduct)
    {
        $this->storeProducts = $storeProduct;
    }

    public function index()
    {
        $featured = $this->storeProducts->getFeaturedProducts();
		$pageData = Homepage::where(['id' => 1])->get()->first();
    	return view('front.home', array('pData' => $pageData, 'active'=>'home', 'featured'=> $featured));
    }

	public function about()
	{
		$pageData = Pages::where(['id' => 2])->get()->first();
		return view('front.about',array('pData' => $pageData, 'active'=>'about'));
	}

	public function contact()
	{
		return view('front.contact', array('active'=>'contact'));
	}
	
	public function contact_send(Request $request){
		$this->validate($request,[
	            'uname' => "required",
	            'uemail' => "required|email",
	            'uphn' => "required",
	            'umessg' => "required",
	        ],[
	        	'required'=>'This field is required.',
	        	'email'=>'Please enter a valid email address.'
	        ]); //$validator->fails()

		$contact = new Contact();
        $contact->user_name = $request->uname;
        $contact->user_email = $request->uemail;
        $contact->user_phone = $request->uphn;
        $contact->user_message = $request->umessg;
        $contact->save();
        // Send email to admin

        
		
        $textMessage = "Name: $request->uname\r\n\r\nEmail: $request->uemail\r\n\r\nPhone: $request->uphn\r\n\r\nMessage: $request->umessg";
        \Mail::raw($textMessage, function ($message) {
        		$siteData = getSetting();
			   $message->to($siteData['site_email']);
			   $message->subject('Contact Email');
			});

        // Send email to user
        \Mail::raw($request, function ($message) use ($request) {
				$message->to($request->uemail)->subject('Contact Email')->setBody('Thank you for your contacting us.');
			});
        return redirect(route('contact'))->with('success','Thank you for your contacting us.');
	}

	public function services(){
		$pageData = Pages::where(['id' => 3])->get()->first();
		return view('front.services',array('pData' => $pageData, 'active'=>'service'));
	}
	
	public function brand(){
		$pageData = Pages::where(['id' => 4])->get()->first();
		return view('front.brands', array('pData'=>$pageData, 'active'=>'brands'));
	}

	public function pormotions(){
		$pageData = Pages::where(['id' => 5])->get()->first();
		return view('front.promotions', array('pData'=>$pageData, 'active'=>'pormotion'));
	}

	public function terms(){
		return view('front.terms', array('active'=>'terms'));
	}

	public function privacy(){
		return view('front.privacy', array('active'=>'privacy'));
	}

	public function returns(){
		return view('front.returns', array('active'=>'returns'));
	}
}
