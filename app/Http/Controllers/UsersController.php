<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\UsersDataTable;
use App\FrontUser;

class UsersController extends Controller
{
    //
    public function index(UsersDataTable $dataTable){
    	return $dataTable->render('admin-users.list');
    }

    public function addUser(){
        //
        return view('admin-users.add');
    }
    public function AddUserSave(Request $request){
        //
        $this->validate($request,[
            'first_name'=>'required|max:200',
            'last_name'=>'required|max:200',
            
            'email'=>'required|max:200|email|unique:front_users,email,NULL,id',
            'password'=>'required|max:200',
            'street'=>'required|max:200',
            'city'=>'required|max:200',
            'state'=>'required|max:200',
            'zip'=>'required|max:200',
            'status'=>'required',
        ],
        [
            'required' => "This field is required.",
            'max' => "You have exceeded the maximum limit of 200 characters.",
            'email' => "Please enter the valid email address.",
            'unique'=>'This email address already exist.',
        ]);

        FrontUser::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'passwordtext' => encrypt($request->password),
            'street_address' => $request->street,
            'city' => $request->city,
            'state' => $request->state,
            'zipcode' => $request->zip,
            'status' => $request->status,
        ]);

        return redirect (route('userManagement'))->with('success','Item added successfully.');
    }
    public function editUser($id = ''){
        //
        if($id == '' && !is_numeric($id)){
            return redirect (route('userManagement'));
        }
        $userD = FrontUser::find($id);
        return view('admin-users.edit',['userD'=>$userD]);
    }
    public function EditUserUpdate($id = '', Request $request){
        //
        $this->validate($request,[
            'first_name'=>'required|max:200',
            'last_name'=>'required|max:200',
            
            'email'=>'required|max:200|email|unique:front_users,email,'.$id.',id',
            'password'=>'required|max:200',
            'street'=>'required|max:200',
            'city'=>'required|max:200',
            'state'=>'required|max:200',
            'zip'=>'required|max:200',
            'status'=>'required',
        ],
        [
            'required' => "This field is required.",
            'max' => "You have exceeded the maximum limit of 200 characters.",
            'email' => "Please enter the valid email address.",
            'unique'=>'This email address already exist.',
        ]);
        
        $userD = FrontUser::find($id);
        $userD->first_name = $request->first_name;
        $userD->last_name = $request->last_name;
        $userD->email = $request->email;
        $userD->password = bcrypt($request->password);
        $userD->passwordtext = encrypt($request->password);

        $userD->street_address = $request->street;
        $userD->city = $request->city;
        $userD->state = $request->state;
        $userD->zipcode = $request->zip;
        $userD->status = $request->status;
        $userD->save();
        return redirect (route('userManagement'))->with('success','Item updated successfully.');
        
    }
    public function UserDelete($id = 0){
        //
        $userD = FrontUser::find($id);
        $userD->delete();
        return redirect (route('userManagement'))->with('success','Item deleted successfully.');
    }
}
