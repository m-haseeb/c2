<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Settings;
use App\StoreCategory;
use App\StoreProduct;
use App\CategoriesState;
use App\ProductsState;
use App\Lists;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Illuminate\Http\Request;
use Config;
use DB;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->api_url = Config::get('constants.reiss_master_api_url');
    }

    public function getCategories(Request $request)
    {
        /* by curl...
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://localhost/reiss_store/masterAdmin/public/api/test?store_name=Test%20Store");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        dd(curl_exec($ch));
        */

        $queryString = [];

        if ( $request->get('product') == 'yes' )
                $queryString['product_count'] = true;

        return $this->products_or_categories(2, $queryString);
    }

    public function getProducts(Request $request)
    {
        return $this->products_or_categories(1);
    }

    public function products_or_categories($type, $query = [])
    {
        try
        {
            $settings = Settings::first();
            $parameters = ['store_name' => $settings->store_name];

            if ( $type == 1 )
            {
                $url = $this->api_url.'getStoreProducts';
            }
            else
            {
                $url = $this->api_url.'getStoreCategories';
            }

            $client = new Client();
            $result = $client->get($url, ['query' => $parameters]);
            
            $response = json_decode($result->getBody()->getContents(), true);
        
            if ( $response['status'] == 'success' )
            {
                $permissions = $response['store_permissions'];

                $settings->reiss_cat_add = $permissions['reiss_cat_add'];
                $settings->reiss_cat_edit = $permissions['reiss_cat_edit'];
                $settings->reiss_cat_delete = $permissions['reiss_cat_delete'];
                $settings->reiss_prod_add = $permissions['reiss_prod_add'];
                $settings->reiss_prod_edit = $permissions['reiss_prod_edit'];
                $settings->reiss_prod_delete = $permissions['reiss_prod_delete'];
                $settings->reiss_prod_price = $permissions['reiss_prod_price'] == 'Enable' ? 1 : 0;
                $settings->save();

                if ( $type == 1 )
                {
                    $in_active_products = ProductsState::all()->pluck('product_id')->toArray();

                    $filtered = collect($response['products'])->map(function($item, $index) use ($in_active_products){
                                    $item['isActive'] = in_array($item['id'], $in_active_products) ? false : true;
                                    $prPrice = (float)$item['price']+((float)$item['price']*getSettingPrice());
                                    $item['newPrice'] = getReissNewPriceFront($item['id'], $prPrice );
                                    return $item;
                                })->values();

                    // $newArray= array();
                    // foreach($filtered as $key => $prod){
                    //     $newArray[$prod['id']] = getReissNewPrice($prod['id']);
                    // }
                    
                    return ['products' => $filtered, 'permissions' => $permissions/*, 'newPrice' => $newArray*/];
                }
                else
                {
                    //in-active categories..
                    $in_active_categories = CategoriesState::all()->pluck('category_id')->toArray();

                    $filtered = collect($response['categories'])->map(function($item, $index) use ($in_active_categories){
                                    $item['isActive'] = in_array($item['id'], $in_active_categories) ? false : true;
                                    return $item;
                                })->values();

                    return ['categories' => $filtered, 'permissions' => $permissions];
                }
            }
            else
            {
                //error...
               return ['error' => $response['message']];
            }
        }
        catch (GuzzleException $e)
        {
            return ['error' => $e->getResponse()->getBody()->getContents()];
        }
    }
}