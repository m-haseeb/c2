<?php

namespace App\Http\Controllers;

use App\DataTables\ProductsDataTable;
use App\StoreProduct;
use Illuminate\Http\Request;
use DB;
use App\StoreCategory;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(ProductsDataTable $dataTable)
    {
        return $dataTable->render('product.all');
    }

    public function getProduct($id)
    {
        return StoreProduct::find($id);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'product_name' => "string|required|max:255",
            'manufacturer_name' => "string|required|max:255",
            'sku' => "string|required|max:255",
            'manufacturer_code' => "string|required|max:255",
            'upc' => "string|required|max:255",
            'package_quantity' => "numeric|min:1|required",
            'price' => "numeric|min:1|required",
            'case_quantity' => "numeric|min:1|required",
        ]);

        $product = new StoreProduct();
        $product->name = $request->product_name;
        $product->manufacturer = $request->manufacturer_name;
        $product->manufacturer_code = $request->manufacturer_code;
        $product->upc = $request->upc;
        $product->sku = $request->sku;
        $product->price = $request->price;
        $product->package_quantity = $request->package_quantity;
        $product->case_quantity = $request->case_quantity;
        $product->store_category_id = $request->category;
        $dataSlug = $this->create_slug($request->product_name, 'store_products', 'slug');
        $product->slug = $dataSlug;
        $product->save();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'product_name' => "string|required|max:255",
            'manufacturer_name' => "string|required|max:255",
            'sku' => "string|required|max:255",
            'manufacturer_code' => "string|required|max:255",
            'upc' => "string|required|max:255",
            'package_quantity' => "numeric|min:1|required",
            'price' => "numeric|min:1|required",
            'case_quantity' => "numeric|min:1|required",
        ]);

        $product =  StoreProduct::find($request->id);
        $product->name = $request->product_name;
        $product->manufacturer = $request->manufacturer_name;
        $product->manufacturer_code = $request->manufacturer_code;
        $product->upc = $request->upc;
        $product->sku = $request->sku;
        $product->price = $request->price;
        $product->package_quantity = $request->package_quantity;
        $product->case_quantity = $request->case_quantity;
        $product->store_category_id = $request->category;
        $dataSlug = $this->create_slug($request->product_name, 'store_products', 'slug', 'id', $request->id);
        $product->slug = $dataSlug;
        $product->save();
    }

    

    public function toggleActive(Request $request)
    {
        info($request);
        if ($request->toggle === "enable") {
            $toggle = 1;
        } else {
            $toggle = 0;
        }

        $product = StoreProduct::find($request->id);
        $product->isActive = $toggle;
        $product->save();
    }

    /**
    - Code by Haseeb
    */
    protected function create_slug($title, $table_name, $field_name, $nId_name = '', $nId = '', $count=0)
    {
        //echo "Product Id = ".$nId."<br>";
        $slug = strtolower(trim($title));
        
        $slug = str_replace(' ', '-', $slug); // Replaces all spaces with hyphens.
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug); // Removes special chars.
        $slug = preg_replace('/-+/', '-', $slug); // Replaces multiple hyphens with single one.
        //echo "count = ".$count;
        if($count==0)
        {   
            $slug = $slug;
        }
        else
        {
            $slug = $slug.'-'.$count;
        }
        
        $query = DB::table($table_name)->where($field_name, 'like', $slug.'%')->orWhere($field_name, 'like', $slug);
    
        if($nId_name != '' && $nId != '')
        {
            $query = $query->where($nId_name, '<>',$nId); " AND ".$nId_name." != ".$nId;
        }
        
        
        $query = $query->get();
        $total_records = count($query);
        if($total_records > 0)
        {
            return $this->create_slug($title, $table_name, $field_name, $nId_name, $nId, ++$count);
        }
        else
        {
            return $slug;
        }
    }

    public function bulkUpload(){
        return view('product.bulk-upload');

    }

    public function submitBulkUpload(Request $request){
        
        $this->validate($request, [
            'page_image' => 'required|mimes:txt'
        ],[
            'required' => 'This field is required.',
            'mimes' => 'This field must be a file of type: txt.'
        ]);
        
        if ( $request->hasFile('page_image') ){
            $path = realpath(public_path('products_bulk_upload/'));
            $file = $request->file('page_image');
            //if($file->getClientOriginalExtension() == "csv"){
                $file_name = date('m-d-Y-h-i-s').'.'.$file->getClientOriginalExtension();
                $file->move($path, $file_name);
            //}else{
            //    redirect(base_url('bulkUpload'))->with('csverror','This field must be a file of type: csv.');
            //}
        }
        
        $catArray = array();
        $newCatArray = array();

        $array = array();
        $file = public_path ('products_bulk_upload').'/'.$file_name;// Your Temp Uploaded file
        $handle = fopen($file, "r"); // Make all conditions to avoid errors
        $read = file_get_contents($file); //read

        $lines = explode("\r", $read);//get
        

        // foreach($lines as $key => $value){
        //     dd($value);
        // }

        // while (($line = fgetcsv($handle)) !== FALSE) {
        //     dd($line);
        // }

        
        $i= 0;//initialize
        foreach($lines as $key => $value){
            $array[$i] = explode("\t", $value);
            if($i > 0 && !empty($array[$i][1])){
                $catArray[] = $array[$i][1];
            }
            $i++;
        }
        
        $catArray = array_unique($catArray);
        sort($catArray);

        foreach($catArray as $cat){
            if($cat != ''){
                $ifExist = StoreCategory::where('name',$cat)->first();
                if(!empty($ifExist)){
                    $newCatArray[$cat] = $ifExist->id;
                }else{
                    $newCategory = new StoreCategory();
                    $newCategory->id = $this->random_number();
                    $newCategory->name = $cat;
                    $newCategory->slug = $this->create_slug($cat, 'store_categories', 'slug');
                    $newCategory->level = 1;
                    $newCategory->is_user_cat = 1;
                    $newCategory->save();

                    $newCatArray[$cat] = $newCategory->id;
                }
            }
        }

        //dd($newCatArray);

        $i = 1;

        foreach($array as $data){
            if(array_key_exists($i,$array)){
                if($i > 1 &&  array_key_exists(2,$array[$i]) && !empty( trim( $array[$i][2] ) ) ){
                    
                    $product = new StoreProduct();
                    $product->ItemNumber = $data[0];
                    $product->name = preg_replace('/[\x95\x99\xB2\x0A]/', '',str_replace("\x94\xB2\x0A",'"',$data[2]) );
                    $product->BulletPointsMaterial = preg_replace('/[\x95\x99\xB2\x0A]/', '',str_replace("\x94\xB2\x0A",'"',$data[4]) );
                    $product->BulletPointsColor = $data[5];
                    $product->BulletPointsClosure = $data[6];
                    $product->BulletPointsPockets = $data[7];
                    $product->MinimumOrder = $data[8];
                    $product->AverageShippingCost = $data[10];
                    $product->ShipsVia = $data[11];
                    $product->UsuallyShipsIn = $data[12];
                    $product->ShippingBoxDimensions = $data[13];
                    $product->ShipWeight = $data[14];
                    $product->upc = $data[15];
                    $product->ImageFile = $data[16];
                    $product->TariffCode = $data[17];
                    $product->Standard = $data[18];
                    $product->sku = $data[19];
                    $product->price = $data[20];
                    $product->is_bulk = 1;
                    $product->isActive = 1;
                    $product->is_user_prod = 1;

                    if(array_key_exists($data[1],$newCatArray)){
                        $product->store_category_id = $newCatArray[$data[1]];
                    }else{
                        $product->store_category_id = '';
                    }
                    $product->slug = $this->create_slug($data[2], 'store_products', 'slug');
                    //preg_replace('/[\x95\x99]/', '', $string)
                    $product->Description = preg_replace('/[\x95\x99\xB2\x0A]/', '',str_replace("\x94\xB2\x0A",'"',$data[3]) );
                     // return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                    $product->type = 'local';
                    //dd($product);
                    $product->save();
                }
            }
            $i++;
        }
        return redirect(route('bulkUpload'))->with('msg', 'Item(s) added successfully.');
        /*
        if (($handle = fopen ( public_path ('products_bulk_upload').'/'.$file_name, 'r' )) !== FALSE) {
            $i = 1;
            while ( ($data = fgetcsv ( $handle, 2000, ',' )) !== FALSE ) {
                dd($data);
                if($i > 1){
                    $catArray[] = $data[1];
                    
                }
                $i++;
            }
            fclose ( $handle );

            //
            $catArray = array_unique($catArray);
            sort($catArray);
            foreach($catArray as $cat){
                $newCategory = new StoreCategory();
                $newCategory->id = $this->random_number();
                $newCategory->name = $cat;
                $newCategory->slug = $this->create_slug($cat, 'store_categories', 'slug');
                $newCategory->level = 1;
                $newCategory->save();
                $newCatArray[$cat] = $newCategory->id;
            }
            $i = 1;
            $handle = fopen ( public_path ('products_bulk_upload').'/'.$file_name, 'r' );
            
            while ( ($data = fgetcsv ( $handle, 2000, ',' )) !== FALSE ) {
                
                if($i > 1){
                    $product = new StoreProduct();

                    $product->ItemNumber = $data[0];
                    $product->name = $data[2];
                    $product->BulletPointsMaterial = $data[4];
                    $product->BulletPointsColor = $data[5];
                    $product->BulletPointsClosure = $data[6];
                    $product->BulletPointsPockets = $data[7];
                    $product->MinimumOrder = $data[8];
                    $product->AverageShippingCost = $data[10];
                    $product->ShipsVia = $data[11];
                    $product->UsuallyShipsIn = $data[12];
                    $product->ShippingBoxDimensions = $data[13];
                    $product->ShipWeight = $data[14];
                    $product->upc = $data[15];
                    $product->ImageFile = $data[16];
                    $product->TariffCode = $data[17];
                    $product->Standard = $data[18];
                    $product->sku = $data[19];

                    $product->is_bulk = 1;
                    $product->isActive = 1;
                    $product->store_category_id = $newCatArray[$data[1]];
                    $product->slug = $this->create_slug($data[2], 'store_products', 'slug');
                    $product->Description = $data[3];
                    $product->type = 'local';
                    
                    $product->save();
                }
                $i++;
            }
            fclose ( $handle );
        }
        return redirect(route('bulkUpload'))->with('msg', 'Item(s) added successfully.');
       */
    }

    private function random_number()
    {
        $random = str_random(10);
        
        if ( StoreCategory::where('id', $random)->count() == 0 )
            return $random;

        $this->random_number();
    }



}
