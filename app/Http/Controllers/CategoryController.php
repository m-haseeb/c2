<?php

namespace App\Http\Controllers;

use App\StoreCategory;
use App\StoreProduct;
use Illuminate\Http\Request;
use DB;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth' );
    }

    public function getCategory($id)
    {
        return StoreCategory::find($id);
    }

    public function getCategoryTree(Request $request)
    {
        $categoryCollection = StoreCategory::all();

        // return $categoryCollection;

        if ($request->product === 'yes')
            foreach ($categoryCollection as $item) {
                $item->parent_count = $item->products()->count();
            }

        $tree = $categoryCollection->where('parent_id', null);

        foreach ($tree as $mainNode) {
            $children = $categoryCollection->where('parent_id', $mainNode->id);
            if (!$children->isEmpty()) {
                $mainNode->children = $children;
                $this->checkChild($categoryCollection, $children);
            } else
                $mainNode->children = null;
        }

        return ['total' => $categoryCollection->count(), 'tree' => $tree];
    }

    private function checkChild($collection, $target)
    {
        foreach ($target as $node) {
            $temp = $collection->where('parent_id', $node->id);
            if ($temp->count() > 0) {
                $node->children = $temp;
                return $this->checkChild($collection, $temp);
            } else
                $node->children = null;
        }
        return $target;
    }

//Soft delete category
//    public function delete(Request $request)
//    {
//
//        $category = StoreCategory::find($request->id);
//        $category->parent_id = null;
//        $category->save();
//
//        $categoryCollection = $category->children;
//        foreach ($categoryCollection as $mainNode) {
//
//            $children = $mainNode->children;
//            if (!$children->isEmpty()) {
//                $this->deleteChildren($children);
//            }
//            $mainNode->delete();
//        }
//        $category->delete();
//    }
//
//    public function deleteChildren($children)
//    {
//        foreach ($children as $node) {
//            $temp = $node->children;
//            if ($temp->count() > 0) {
//                return $this->deleteChildren($temp);
//            }
//            $node->delete();
//        }
//        return $children;
//    }
    public function delete(Request $request)
    {

        $category = StoreCategory::find($request->id);

        $categoryCollection = $category->children;
        foreach ($categoryCollection as $mainNode) {

            $children = $mainNode->children;
            if (!$children->isEmpty()) {
                $this->deleteChildren($children);
            }
            $mainNode->forceDelete();
        }
        $category->forceDelete();
    }

    public function deleteChildren($children)
    {
        foreach ($children as $node) {
            $temp = $node->children;
            if ($temp->count() > 0) {
                return $this->deleteChildren($temp);
            }
            $node->products()->forceDelete();
            $node->forceDelete();
        }
        return $children;
    }

    public function create(Request $request)
    {

        $this->validate($request, [
            'cat_name' => 'required|string|max:255',
            'cat_parent' => 'string',
        ],
            ['cat_name.required' => "This field cannot be left empty."]);

        $newCategory = new StoreCategory();
        $newCategory->name = $request->cat_name;

        $dataSlug = $this->create_slug($request->cat_name, 'store_categories', 'slug');
       
        if ($request->filled('cat_parent') && $request->cat_parent !== "no_parent") {
            $newCategory->parent_id = $request->cat_parent;
            $parent = StoreCategory::find($request->cat_parent);
            $newCategory->level = $parent->level + 1;
            $newCategory->slug = $dataSlug;
            dd($newCategory);
            //$newCategory->save();
        } else {
            $newCategory->level = 1;
            $newCategory->slug = $dataSlug;
            dd($newCategory);
            //$newCategory->save();
        }

    }

    public function update(Request $request)
    {

        $this->validate($request, [
            'cat_name' => 'required|string|max:255',
            'cat_parent' => 'string',
            'id' => 'string|required',
        ],
            ['cat_name.required' => "This field cannot be left empty"]);

        $newCategory = StoreCategory::find($request->id);
        $newCategory->name = $request->cat_name;
            $dataSlug = $this->create_slug($request->cat_name, 'store_categories', 'slug', 'id',$request->id);

        if ($request->cat_parent !== "no_parent") {
            $newCategory->parent_id = $request->cat_parent;
            $parent = StoreCategory::find($request->cat_parent);
            $newCategory->level = $parent->level + 1;
            $newCategory->slug = $dataSlug;
            $newCategory->save();
        } else {
            $newCategory->slug = $dataSlug;            
            $newCategory->parent_id = null;
            $newCategory->level = 1;
            $newCategory->save();
        }

    }

    public function toggleActive(Request $request)
    {

        if ($request->toggle === "enable") {
            $toggle = 1;
        } else {
            $toggle = 0;
        }

        $category = StoreCategory::find($request->id);

        $categoryCollection = $category->children;
        foreach ($categoryCollection as $mainNode) {

            $children = $mainNode->children;
            if (!$children->isEmpty()) {
                $this->toggleActiveChildren($children, $toggle);
            }
            StoreProduct::where('store_category_id', $mainNode->id)->update(['isActive' => $toggle]);
            $mainNode->isActive = $toggle;
            $mainNode->save();
        }
        $category->isActive = $toggle;
        $category->save();
        StoreProduct::where('store_category_id', $category->id)->update(['isActive' => $toggle]);
    }

    public function toggleActiveChildren($children, $toggle)
    {
        foreach ($children as $node) {
            $temp = $node->children;
            if ($temp->count() > 0) {
                return $this->toggleActiveChildren($temp, $toggle);
            }
//            $node->products()->forceDelete();
            StoreProduct::where('store_category_id', $node->id)->update(['isActive' => $toggle]);
            $node->isActive = $toggle;
            $node->save();
        }
        return $children;
    }

    /**
    - Code by Haseeb
    */
    protected function create_slug($title, $table_name, $field_name, $nId_name = '', $nId = '', $count=0)
    {
        //echo "Product Id = ".$nId."<br>";
        $slug = strtolower(trim($title));
        
        $slug = str_replace(' ', '-', $slug); // Replaces all spaces with hyphens.
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug); // Removes special chars.
        $slug = preg_replace('/-+/', '-', $slug); // Replaces multiple hyphens with single one.
        //echo "count = ".$count;
        if($count==0)
        {   
            $slug = $slug;
        }
        else
        {
            $slug = $slug.'-'.$count;
        }
        
        $query = DB::table($table_name)->where($field_name, 'like', $slug.'%')->orWhere($field_name, 'like', $slug);
    
        if($nId_name != '' && $nId != '')
        {
            $query = $query->where($nId_name, '<>',$nId); " AND ".$nId_name." != ".$nId;
        }
        
        
        $query = $query->get();
        $total_records = count($query);
        if($total_records > 0)
        {
            return $this->create_slug($title, $table_name, $field_name, $nId_name, $nId, ++$count);
        }
        else
        {
            return $slug;
        }
    }


    public function create_all_slug($table_name)
    {
        $getAllData = DB::table($table_name)->where('slug', '')->orWhere('slug',NULL)->get();
        if(count($getAllData) > 0){
            foreach($getAllData as $gData){
                
                $slug = $this->create_slug($gData->name, $table_name, 'slug', 'id', $gData->id);
                DB::table($table_name)->where('id', $gData->id)->update(['slug' => $slug]);
                
            }
        }

    }
}
