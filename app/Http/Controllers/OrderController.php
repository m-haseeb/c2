<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\OrderDataTable;
use App\Orders;
use App\Order_details;
use App\Settings;
use App\Lists;
use App\States;
use App\Lib\Authorize;

use Carbon\Carbon;
use Config;
use DB;
use JMS\Serializer\Tests\Fixtures\Order;

class OrderController extends Controller
{
    //
    public function index(OrderDataTable $dataTable){
    	return $dataTable->render('order.list');
    }

    public function details($order_id = ''){
        if($order_id == '' || !is_numeric($order_id)){
            return redirect(route('orderManagement'));
        }
        $orders = Orders::where('id','=',$order_id)->first();
        if(empty($orders)){
            return redirect(route('orderManagement'));
        }
        return view('order.detail', ['active'=>'my-order', 'orders'=>$orders]);

    }

    public function updateDetails($order_id , Request $request){

        $order = Orders::find($order_id);


        $this->validate($request,[
            'status'=>'required',
        ],
        [
            'required' => "This field is required.",
        ]);
        $orders = Orders::find($order_id);
        $orders->status = $request->status;


        if ($request->status == "Canceled")
        {
            $test = new Authorize;
            $cancel_response = $test->refund_transaction($order);
            if ($cancel_response->getMessages()->getResultCode() == "Ok")
            {
                return redirect (route('orderManagement'))->with('success','Item Canceled successfully.');
            }
            else
            {
                return redirect()->back()->with('error', $cancel_response);
            }
        }
        else
        {
            $orders->save();
        }
        return redirect (route('orderManagement'))->with('success','Item updated successfully.');



    }


}
