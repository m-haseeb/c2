<?php

namespace App\Http\Controllers;

use App\StoreCategory;
use App\StoreProduct;
use DB;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;



class ProductsController extends Controller
{
    //

    public function index($cat_url){
        DB::enableQueryLog(); 
        $siteData = getSetting();
        
        if($cat_url == ''){
            return redirect()->route('index');
        }
        $getDataByUrl = StoreCategory::where('slug',$cat_url)->where('isActive',1);
        if($siteData['reiss_cat'] == 0){ // if reiss product disable
            $getDataByUrl = $getDataByUrl->where('is_user_cat',1); // get only User Categories
        }
        $getDataByUrl = $getDataByUrl->get()->first();

        if($getDataByUrl==NULL){
            return redirect(route('index'));
        }
        $newArray = $this->getAllSubChildIds($getDataByUrl->id, $siteData['reiss_cat']);
        $newArray = explode(',', $newArray );

        $parentArray = $this->getParents($getDataByUrl->id, $siteData['reiss_cat']);
        $parentArray = explode(',',$parentArray);
        
        if(!empty($getDataByUrl)){
            if(count($newArray) > 1){
            	$CategoryProducts = StoreProduct::whereIn('store_category_id',$newArray)->where('isActive',1);
            }else{
                $CategoryProducts = StoreProduct::where('store_category_id',$getDataByUrl->id)->where('isActive',1);
            }
            if($siteData['reiss_prod'] == 0){
                $CategoryProducts = $CategoryProducts->where('is_user_prod',1);
            }
            $CategoryProducts = $CategoryProducts->simplePaginate(15);
            //return view('front.products-listing', ['prodData' => $CategoryProducts]);
            return view('front.products-listing', array('prodData' => $CategoryProducts, 'pData'=> $parentArray, 'active'=>'products'));
    	}else{
    		return redirect()->route('index');
    	}
    }

    protected function getAllSubChildIds($id, $reissCat=0){
        $newArray = $id;
        $cat = StoreCategory::where('parent_id',$id)->where('isActive',1);
        if($reissCat == 0){
            $cat = $cat->where('is_user_cat', 1);
        }
        $cat = $cat->get()->pluck('id');
        foreach($cat as $ca){
            $newArray .= ','.$this->getAllSubChildIds($ca, $reissCat);
        }
        return $newArray;
    }

    protected function getParents($id, $reissCat=0){
        $parent = $id;
        $cat = StoreCategory::where('id',$id)->where('isActive',1);
         if($reissCat == 0){
            $cat = $cat->where('is_user_cat', 1);
        }
        $cat = $cat->get()->first();
        if($cat->parent_id > 0){
            $parent .= ','.$this->getParents($cat->parent_id, $reissCat);
        }
        return $parent;
    }

    public function product_detail($prod_url){
    	if($prod_url == ''){
    		return redirect()->route('index');
    	}
        $siteData = getSetting();
    	$prodData = DB::table('store_products')->where('slug',$prod_url);
        if($siteData['reiss_prod'] == 0){
            $prodData = $prodData->where('is_user_prod',1);
        }
        $prodData = $prodData->first();
    	if(!empty($prodData)){
			return view('front.products-details', ['prodData' => $prodData, 'active'=>'products']);
    	}else {
    		return redirect()->route('index');
    	}

    }
}
