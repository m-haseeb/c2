<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use Image;

class SettingsController extends Controller
{
    //
    public function index(){
    	$sData = Settings::find(1)->toArray();
    	return view('settings.setting',['data'=>$sData]);
	}

	public function setting_save(Request $request){
		$this->validate($request,[
			'facebook'=>'required',
			'linkin'=>'required',
			'twitter'=>'required',
			'youtube'=>'required',
			'site_name'=>'required',
			'site_email'=>'required|email',
			'site_phone'=>'required',
			// 'site_fax'=>'required',
			'site_address'=>'required',
			'show_price'=>'required',
			'reiss_prod'=>'required',
			'reiss_cat'=>'required',
			'footerDesc'=>'required',
			'pricePer'=>'required',
			
		],
		[
			'required'=>'This field is required.',
			'email'=>'Please enter the valid email address.'
		]);

        $saveData = Settings::find(1);
        
		if($request->hasFile('logo')){
        	$this->validate($request,[
            	'logo' => 'required|max:10000|mimes:jpeg,png,jpg'
            ]);
            $image = $request->file('logo');
			$input['logo'] = 'logo'.time().'.'.$image->getClientOriginalExtension();
			/** - Generate Thumb of image Start*/
			$destinationPath = public_path('/site_logo/thumb');
	        $img = Image::make($image->getRealPath());
	        $img->resize(100, 100, function ($constraint) {
				    $constraint->aspectRatio();
				})->save($destinationPath.'/'.$input['logo']);
			/** - Generate Thumb of Image End*/
			$destinationPath = public_path('/site_logo');
			$image->move($destinationPath, $input['logo']);
			$saveData->logo = $input['logo'];
        }

        $saveData->facebook = $request->facebook;
        $saveData->youtube = $request->youtube;
        $saveData->twitter = $request->twitter;
        $saveData->linkin = $request->linkin;
        $saveData->show_price = $request->show_price;
        $saveData->reiss_prod = $request->reiss_prod;
        $saveData->reiss_cat = $request->reiss_cat;

        $saveData->site_name = $request->site_name;
        $saveData->site_email = $request->site_email;
        $saveData->site_phone = $request->site_phone;
        $saveData->footerDesc = $request->footerDesc;
        $saveData->pricePer = $request->pricePer;
        // $saveData->site_fax = $request->site_fax;
        $saveData->site_address = $request->site_address;

        $saveData->save();

        return redirect(route('settings'))->with('msg','Information update successfully');
	}
}
