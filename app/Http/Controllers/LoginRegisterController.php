<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginRegisterController extends Controller
{
    //
    public function index(){
    	$title = 'Login and Register';
    	return view('front.login-register',['active'=>'lgnReg','title'=>$title]);
    }
}
