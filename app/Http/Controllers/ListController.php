<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Support\Facades\Mail;
use App\Lists;
use DB;
use PDF;
use App\Mail\My_list;
use App\Mail\OrderReview;

class ListController extends Controller
{
	public function __construct()
	{
		//
	}

	public function index($id )
	{

		$qty = input::get('qty');
		$type = input::get('type');
		$ifexists = Lists::where(['product_id' => $id])->where('userId', @$_COOKIE['userId'])->get()->first();
	
		if( !empty($ifexists) )
		{
			// Update
			$saveData = Lists::find($ifexists->id);
			$saveData->qty = $qty;
			$saveData->type = $type;
			$saveData->save();
			echo "Item updated successfully.";
		}
		else
		{
			// Save
			$saveData = new Lists();
			$saveData->qty = $qty;
			$saveData->product_id = $id;
			$saveData->type = $type;
			$saveData->userId = $_COOKIE['userId'];
			$saveData->save();
			echo "Item added successfully.";
		}
	}

	public function update_qty(){

		$qty = input::get('qty');
		$cart_id = input::get('cartId');
		$saveData =  Lists::find($cart_id);
		$saveData->qty = $qty;
		$saveData->save();
		echo "Item update successfully.";
	}

	public function remove_item(){
		$listId = input::get('listId');
		$findList = Lists::find($listId);
		$findList->delete();
		$listData = DB::table('lists')
			            ->join('store_products', 'store_products.id', '=', 'lists.product_id')
			            ->select('store_products.sku', 'store_products.name', 'store_products.price', 'store_products.is_user_prod', 'store_products.is_price', 'lists.id as listId', 'lists.*')
			            ->where('isActive',1)
			            ->where('userId',@$_COOKIE['userId']);
		$siteData = getSetting();
		if($siteData['reiss_prod']==0){
			$listData = $listData->where('is_user_prod',1);
		}
		$listData = $listData->get();
		
		//echo "Item remove successfully.";
		echo count($listData);

	}
	public function display_list(){
		$listData = DB::table('lists')
			            ->join('store_products', 'store_products.id', '=', 'lists.product_id')
			            ->select('store_products.sku', 'store_products.name', 'store_products.price', 'store_products.is_user_prod', 'store_products.is_price', 'lists.id as listId', 'lists.*')
			            ->where('isActive',1);
		$siteData = getSetting();
		if($siteData['reiss_prod']==0){
			$listData = $listData->where('is_user_prod',1);
		}
		$listData = $listData->get();

		return view('front.my-cart',['lData' => $listData, 'active'=>'my-list']);
	}

	public function download_list_pdf(){
		$listData = DB::table('lists')
			            ->join('store_products', 'store_products.id', '=', 'lists.product_id')
			            ->select('store_products.sku', 'store_products.name', 'store_products.price', 'store_products.is_user_prod', 'store_products.is_price', 'lists.id as listId', 'lists.*')
			            ->where('isActive',1);

		$siteData = getSetting();
		if($siteData['reiss_prod']==0){
			$listData = $listData->where('is_user_prod',1);
		}
		$listData = $listData->get();
		
        //return $pdf->stream();
        //return $pdf->download('my-cart.pdf');
        $pdf = PDF::loadView('front.pdf.my-cart-pdf',['lData' => $listData, 'siteData'=>$siteData]);
		return $pdf->download('my-list.pdf');

	}

	public function email_list(){
		// dd( request()->all() );

		// $email = input::get('email');
		// $name = input::get('name');

		$email = 'mikesmith1166@gmail.com';
		$name = 'Mike Smith';
		
		// dd( $email, $name );

		$listData = DB::table('lists')
			            ->join('store_products', 'store_products.id', '=', 'lists.product_id')
			            ->select('store_products.sku', 'store_products.name', 'store_products.price', 'lists.id as listId', 'lists.*')
			            ->where('isActive',1);

        // dd( $listData->get() );

		$siteData = getSetting();
		
		// dd( $siteData );

		if($siteData['reiss_prod']==0){
			$listData = $listData->where('is_user_prod',1);
		}

		$listData = $listData->get();

		// dd( $listData );
        

        $html = new My_list(['lData'=>$listData]);
        

  //    $pdf = PDF::loadView('front.pdf.my-cart-pdf',['lData' => $listData, 'siteData'=>$siteData]);
		// return $pdf->download('my-list.pdf');

        // dd( $html );

        //Mail::to('mikesmith1166@gmail.com', 'name')->send($html);
        
        Mail::to($email, $name)->send($html);

        return redirect()->route('home');
        // echo route('displayList');
	}

	public function email_list1(){

		dd($request->all());
		$all = request()->all();
		$all['logo'] = asset('front/images').'/logo.png';



		Mail::to( "mikesmith1166@gmail.com", "Mike Smith" )->send(new OrderReview( $all ));

		return redirect()->route('thankyou');



	}

}
