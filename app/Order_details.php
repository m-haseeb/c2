<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_details extends Model
{
    //
    protected $table = "order_details";

    public function order(){
    	$this->belongsTo('App\Orders');
    }
}
