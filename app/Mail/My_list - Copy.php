<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;



class My_list extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $lData;
    public $siteData;
    public $pdf;

    public function __construct($lData)
    {
        //
        $this->lData = $lData;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('front.pdf.my-cart-pdf2');
       // ->attachData($this->pdf, 'name.pdf', [
       //                  'mime' => 'application/pdf',
       //              ]);
    }
}
