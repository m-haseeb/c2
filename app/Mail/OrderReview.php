<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderReview extends Mailable
{
    use Queueable, SerializesModels;

    private $emailData;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $request_data )
    {
        //
        $this->emailData = $request_data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $all = $this->emailData;
        return $this->view('front.pdf.my-cart-pdf')->with('email_data', $all );
    }
}
