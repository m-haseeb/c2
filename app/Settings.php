<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    public static function store_name()
    {
    	return static::first()->store_name;
    }
}
