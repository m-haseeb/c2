<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    //
    protected $table = "order";

    public function detail(){
    	return $this->hasMany('App\Order_details');
    }
}
