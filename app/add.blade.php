<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 11-Dec-17
 * Time: 4:29 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Add Product')

@section('css')
    {{--for radios--}}
    <link rel="stylesheet" href="{{asset('/css/addCategory.css')}}">
@endsection

@section('js')
    <script type="text/javascript">
        let list = "";
        let loading = '<i class="fa fa-spinner fa-pulse fa-fw"></i>';
        const product_id = "{{Request::input('edit')}}";
        let product = null;

        $(document).ready(function () {

            if (product_id !== "") {
                $('#save').remove();
                loadProduct();
            }
            else {
                $('#edit').remove();
                init();
            }
        });

        function loadProduct() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'get',
                url: '{{route('reiss.product.get', Request::input('edit') )}}',
                success: function (response) {
                    product = response;
                    init(true);
                }
            });
        }

        function fillForm() {

            if (product !== null) {

                $('#product_name').val(product.name);
                $('#manufacturer_name').val(product.manufacturer);
                $('#sku').val(product.sku);
                tinymce.get("Description").setContent(product.Description);
                $('#Description').val(product.Description);
                $('#manufacturer_code').val(product.manufacturer_code);
                $('#upc').val(product.upc);
                $('#package_quantity').val(product.package_quantity);
                $('#case_quantity').val(product.case_quantity);
                $('#price').val(product.price);
                $('input[id = "' + product.store_category_id + '"]').prop('checked', true);

                if ( product.product_image != '' )
                {
                    $('#product_image').attr('src', `../products_images/thumbs/${product.product_image}`).css('display', 'block');
                }
            }
        }

        function init(loadForm) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'get',
                url: '{{route('api.categoriesWithOwn')}}',
                success: function (response) {

                    var tree =  $.map(response, function(item, index){
                                    if ( item.parent_id == '' || item.parent_id == null ) {
                                        return item;
                                    }
                                });

                    list += `<div class="checkbox"><ul class="wpsc_categories wpsc_top_level_categories">`;

                    $.each(tree, function (index, item) {
                        
                         list += `<li>
                                    <label for="${item.id}" class="${item.isActive === 0 ? 'text-danger' : ''}">
                                    <input type="radio" name="cat_group" id="${item.id}" ${index == 0 ? 'checked' : ''} ${item.isActive == 0 ? 'disabled' : ''} />${item.name}</label>`;

                        let children = getChildren(response, item.id);

                        if ( children.length > 0 )
                        {
                            list += `<span class='toggle'>+</span>`;
                            createSubList(response, children, false);
                            list += `</li>`;
                        }
                        else
                        {
                            list += `</li>`;
                        }
                    });

                    list += `</ul></div>`;
                    
                    $('#loading').remove();
                    
                    $('#category_list').append(list);
                    
                    if ( loadForm )
                    {
                        fillForm();
                    }
                }
            });
        }


        function getChildren(items, item_id)
        {
            return $.map(items, function(item, index){
                 if ( item.parent_id == item_id){ return item; }
            });
        }

        function createSubList(categories, items, expandable) {

            if ( expandable )
            {
                list += `<span class='toggle'>+</span><ul class="wpsc_categories wpsc_top_level_categories" style='display: none'>`;
            }
            else
            {
                list += `<ul class="wpsc_second_level_categories" style='display: none'>`;
            }

            $.each(items, function(index, item) {

                list += `<li>
                            <label for="${item.id}" class="${item.isActive === 0 ? 'text-danger' : ''}">
                            <input type="radio" name="cat_group" id="${item.id}" ${item.isActive == 0 ? 'disabled' : ''} />${item.name}</label>`;

                let children = getChildren(categories, item.id);

                if ( children.length > 0 )
                {
                    createSubList(categories, children, true);
                }
                else
                {
                    list += `</li>`;
                }
            });

            list += `</ul>`;
        }

        $('#category_list').on('click', '.toggle', function () {
            var $ul = $(this).next();
            $(this).html($ul.is(':visible') ? '+' : '&ndash;');
            $ul.slideToggle();
        });

        $('#save').click(function () {

            const save = $('#save');
            save.prop('disabled', 'disabled');
            save.html(loading);

            let form_data = new FormData();
            form_data.append('product_name', $('#product_name').val());
            form_data.append('manufacturer_name', $('#manufacturer_name').val());
            form_data.append('sku', $('#sku').val());

            var descrip =  window.parent.tinymce.get('Description').getContent();
            
            form_data.append('Description', descrip);

            form_data.append('manufacturer_code', $('#manufacturer_code').val());
            form_data.append('upc', $('#upc').val());
            form_data.append('package_quantity', $('#package_quantity').val());
            form_data.append('case_quantity', $('#case_quantity').val());
            form_data.append('category', $('input[name=cat_group]:checked').attr('id'));
            form_data.append('price', $('#price').val());
            form_data.append('product_thumb', $('input[type=file]')[0].files[0]); 
            $('.text-danger').text('');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'post',
                url: '{{route('reiss.product.create')}}',
                processData: false,
                contentType: false,
                data: form_data,
                success: function (response) {
                    window.location.assign('{{ route('product.all') }}' );

                    //location.reload(true);
                },
                error: function (data) {
                    save.prop('disabled', '');
                    save.html("Submit");

                    let response = data.responseJSON;
                    console.log(response);
                    $.each(response.errors, function (index, errors) {
                        $('#' + index).addClass("has-error");

                        let errorString = "";
                        $.each(errors, function (index, item) {
                            errorString += item;
                        });
                        $('.' + index).html(errorString);

                    });
                }
            });

        });

        $('#edit').click(function () {
            var descrip =  window.parent.tinymce.get('Description').getContent();
            
            const save = $('#save');
            save.prop('disabled', 'disabled');
            save.html(loading);

            let form_data = new FormData();
            form_data.append('id', product.id);
            form_data.append('product_name', $('#product_name').val());
            form_data.append('Description', descrip);
            form_data.append('manufacturer_name', $('#manufacturer_name').val());
            form_data.append('sku', $('#sku').val());
            form_data.append('manufacturer_code', $('#manufacturer_code').val());
            form_data.append('upc', $('#upc').val());
            form_data.append('package_quantity', $('#package_quantity').val());
            form_data.append('case_quantity', $('#case_quantity').val());
            form_data.append('price', $('#price').val());
            form_data.append('category', $('input[name=cat_group]:checked').attr('id'));
            form_data.append('product_thumb', $('input[type=file]')[0].files[0]);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'post',
                url: '{{route('reiss.product.update')}}',
                processData: false,
                contentType: false,
                data: form_data,
                success: function (response) {
                    // location.reload(true);
                    window.location.assign('{{ route('product.all') }}');

                },
                error: function (data) {
                    save.prop('disabled', '');
                    save.html("Submit");

                    let response = data.responseJSON;
                    console.log(response);
                    $.each(response.errors, function (index, errors) {
                        $('#' + index).addClass("has-error");

                        let errorString = "";
                        $.each(errors, function (index, item) {
                            errorString += item;
                        });
                        $('.' + index).html(errorString);

                    });
                }
            });

        });

        function previewFile() {
          var preview = document.querySelector('img');
          preview.style.display = 'none';
          var file    = document.querySelector('input[type=file]').files[0];
          var reader  = new FileReader();

          reader.onloadend = function () {
            preview.src = reader.result;
            preview.style.display = 'block';
          }

          if (file) {
            reader.readAsDataURL(file);
          } else {
            preview.src = "";

            if ( product.product_image != '' )
            {
                $('#product_image').attr('src', `../products_images/thumbs/${product.product_image}`).css('display', 'block');
            }
          }
        }
    </script>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="product_name" class="col-sm-2 control-label">Product Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="product_name" placeholder="Required">
                        <span class="text-danger product_name"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="manufacturer_name" class="col-sm-2 control-label">Manufacturer Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="manufacturer_name" placeholder="Required">
                        <span class="text-danger manufacturer_name"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sku" class="col-sm-2 control-label">SKU</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="sku" placeholder="Required">
                        <span class="text-danger sku"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="Description" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <textarea class="myeditor" name="Description" id="Description" cols="12" rows="5"></textarea>
                        <span class="text-danger Description"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="manufacturer_code" class="col-sm-2 control-label">Manufacturer Code</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="manufacturer_code" placeholder="Required">
                        <span class="text-danger manufacturer_code"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="upc" class="col-sm-2 control-label">UPC</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="upc" placeholder="Required">
                        <span class="text-danger upc"></span>
                    </div>
                    <div class="col-sm-10">
                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="package_quantity" class="col-sm-2 control-label">Package Quantity</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="package_quantity" placeholder="Required">
                        <span class="text-danger package_quantity"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="case_quantity" class="col-sm-2 control-label">Case Quantity</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="case_quantity" placeholder="Required">
                        <span class="text-danger case_quantity"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="price" class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="price" placeholder="Required">
                        <span class="text-danger price"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="page_image" class="col-sm-2 control-label">Product Thumb</label>
                    <div class="col-sm-10">
                        <img src="" height="100px" width="150px" alt="Image preview..." id="product_image" style="display: none">
                        <input type="file" class="form-control" id="page_image" name="page_image" placeholder="Required" onchange="previewFile()">
                        <span class="text-danger product_thumb"></span>
                    </div>
                </div>

            </div>
            <div id="category_list" class="radio">
                <h5 class="text-black text-bold"> Select Category (Required) </h5>
                <span class="text-danger">Red Categories Are Disabled</span>
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw" id="loading"></i>
            </div>
            <button type="button" class="btn btn-default" id="save">Submit</button>
            <button type="button" class="btn btn-default" id="edit">Update</button>
        </div>
    </div>


    <script>
    var root_url = '{{asset('/')}}';
    var base_url = '{{asset('/')}}';
    var ser = '{{ $_SERVER['DOCUMENT_ROOT'] }}/tiny_upload';

    </script>
    <script src="{{asset('/tiny/tinymce/js/tinymce/jquery.js')}}"></script>
    <script src="{{asset('/tiny/tinymce/js/tinymce/jquery.tinymce.min.js')}}"></script>
    <script src="{{asset('/tiny/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/tiny/common.js')}}"></script>
    <!-- tinymce configration end here.-->
@stop
