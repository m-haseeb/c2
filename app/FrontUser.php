<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class FrontUser extends Authenticatable

{

	use Notifiable;

	protected $guard = "front";
    //
    protected $fillable = [
        'first_name','last_name','email','password','passwordtext','street_address','city','state','zipcode','status'
    ];
}
