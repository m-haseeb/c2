<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\Cookie;

use App\States;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(999);

        
        
        $states = States::all();
        view()->share('states', $states); // Pass the data to all views
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        //require_once __DIR__.'/../Http/Helpers.php';
    }
}
