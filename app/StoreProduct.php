<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class StoreProduct extends Model
{
    use SoftDeletes;

    public function category()
    {
        return $this->belongsTo('App\StoreCategory', 'store_category_id');
    }
    public function getFeaturedProducts()
    {
        $featured = DB::table('store_products')->where('is_feature', 1)->get();
        return $featured;

    }

    
}
