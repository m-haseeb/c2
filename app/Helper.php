<?php

    //use Illuminate\Support\Facades\Cookie;
    //use Illuminate\Cookie\CookieJar;
    //use \Symfony\Component\HttpFoundation\Cookie;

    use App\StoreCategory;
    use App\StoreProduct;
    use App\Settings;
    use GuzzleHttp\Exception\GuzzleException;
    use GuzzleHttp\Client;

    use App\Lists;
    use App\Lib\Authorize;
    // use Config;
    use Carbon\Carbon;
// $this->api_url = Config::get('constants.reiss_master_api_url');


    function getSetting()
    {
        return Settings::find(1)->toArray();
    }

    function getSettingPrice()
    {
        $newData = Settings::find(1)->toArray();
        return $newData['pricePer']/100;
    }

    
    function getParents($id)
    {
        $parent = $id;
        
        $category = StoreCategory::where('id', $id)->where('isActive', 1)->first();

        if( !is_null($category) && $category->parent_id > 0 )
            $parent .= ',' . getParents($category->parent_id);

        return $parent;
    }
	
    function get_cat_by_level($level, $parent_id)
    {
        return 500;
        $categories = StoreCategory::where(['level' => $level]);
        
        if ( $level > 1 && $parent_id != '' )
            $categories = $categories->where(['parent_id' => $parent_id]);

        return $categories->get();
    }

    function displayCatTreeOnProduct($cat_id = 0, $pData, $reissCat = 0)
    {
        $cat = StoreCategory::where(['isActive'=>1, 'parent_id'=>$cat_id]);
    
        if($reissCat == 0){
            $cat = $cat->where('is_user_cat',1);
        }
    
        $cat = $cat->get();
    
        if(count($cat) > 0){
            $html = "<a href='javascript:void(0);' class='adrop'></a>";
            $html .= "<ul>";
            for($j=0; $j<count($cat); $j++){
                $active = '';
                if(in_array($cat[$j]->id, $pData)){
                    $active = 'active';
                }
                $html .= "<li class='has-sub ".$active."'>";
                $html .=    '<a href="'.route("products", ["cat_url" => $cat[$j]->slug]).'">'.$cat[$j]->name.'</a>';
                $html .= displayCatTreeOnProduct($cat[$j]->id, $pData, $reissCat);
                $html .= '</li>';
            }
            $html .= '</ul>';
            return $html;
        }
    }

    function displayCategoryTreeOnProductSideMenu($category_id, $parent_categories, $categoriesData)
    {
        $categories = collect($categoriesData)->filter(function($item, $key) use ($category_id) {
            return $item->parent_id == $category_id;
        })->values();

        if ( count($categories) > 0 )
        {
            $html = "<a href='javascript:void(0);' class='adrop'></a>";
            $html .= "<ul>";
            
            for( $j = 0; $j < count($categories); $j++ )
            {
                if ( $categories[$j]->isActive == true )
                {
                    $active = '';

                    if( in_array($categories[$j]->id, $parent_categories) )
                        $active = 'active';

                    $html .= "<li class='has-sub " . $active . "'>";
                    $html .=    '<a href="' . route("reiss.products", ["category_slug" => $categories[$j]->slug]) . '">' . $categories[$j]->name . '</a>';
                    $html .= displayCategoryTreeOnProductSideMenu($categories[$j]->id, $parent_categories, $categoriesData);
                    $html .= '</li>';
                }
            }

            $html .= '</ul>';
            return $html;
        }
    }

    function getLevelOneCategories()
    {
        //own api calling for category listing from reiss site...
        $categories = reissAndOwnCategories();

        $categories = collect($categories)->filter(function ($item, $key) {
                        return $item->level == 1 && $item->isActive == true;
                    })->values();

        return $categories;
    }

    function getCategories()
    {
        $categories = [];

        //own api calling for category listing from reiss site...
        $request = Request::create('api/getCategories', 'GET');
        $response = json_decode(Route::dispatch($request)->getContent());

        if ( $response && !isset($response->error) )
        {
            $categories = $response->categories;
        }

        return $categories;
    }

    function reissAndOwnCategories()
    {
        $own_categories = StoreCategory::where('isActive', 1)->orderBy('name','ASC')->get();
        
        $categories = getCategories();

        $settings = getSetting();

        if ( $settings['reiss_cat'] == 1 )
        {
            $merged = collect($categories)->merge($own_categories);
        }
        else
        {
            $merged = $own_categories;
        }

        return $merged;
    }



    



    function callOwnApi($url, $request_type = 'GET', $params = [])
    {
        //own api calling for category listing from reiss site...
        $request = Request::create($url, $request_type, $params);
        return json_decode(Route::dispatch($request)->getContent());
    }

    function UserCookieHelperFunction($userId = ''){

        if($userId != ''){
            if(!isset($_COOKIE['userId']) || !is_numeric($_COOKIE['userId']) ){
                setcookie('userId', $userId, time()+2592000, '/');
            }
        }else{
            $random_string = md5(microtime());
            if(!isset($_COOKIE['userId']) ){
                setcookie('userId', $random_string, time()+2592000, '/');
            }
        }
    }


    function countListCart(){

        $products = DB::table('lists')
                        ->join('store_products', 'store_products.id', '=', 'lists.product_id')
                        ->select('store_products.id', 'store_products.product_image', 'store_products.sku', 'store_products.name', 'store_products.price', 'store_products.is_user_prod', 'store_products.is_price', 'lists.id as listId', 'lists.qty')
                        ->where('isActive', 1)
                        ->where('lists.type', 'local')
                        ->where('lists.userId', @$_COOKIE['userId'])
                        ->get();

        $remote_products = Lists::where('userId', @$_COOKIE['userId'])->whereNotIn('product_id', $products->pluck('id')->toArray());
        $remote_products_ids = $remote_products->pluck('product_id', 'id')->toArray();
        $remote_products_qtys = $remote_products->pluck('qty', 'product_id')->toArray();

        //convert array of objects into array of arrays...
        $products = $products->map(function($item, $index){
                        $item->prodcut_location_type = 'local';
                        return (array)$item;
                    })->toArray();

        if ( count($remote_products_ids) )
        {
            $settings = Settings::first();

            $parameters = ['store_name' => $settings->store_name, 'product_ids' => $remote_products_ids];
            $url = \Config::get('constants.reiss_master_api_url').'getProductsByIds';

            try
            {
                $client = new Client();
                $result = $client->get($url, ['query' => $parameters]);
                
                $response = json_decode($result->getBody()->getContents(), true);

                if ( $response['status'] == 'success' )
                {
                    $remote_products_ids = array_flip($remote_products_ids);
                    
                    $response = array_map(function($item) use ($remote_products_ids, $remote_products_qtys, $settings) {
                                    $item['listId'] = $remote_products_ids[$item['id']];
                                    $item['is_price'] = $settings->reiss_prod_price;
                                    $item['qty'] = $remote_products_qtys[$item['id']];
                                    return $item;
                                }, $response['products']);
                    
                    $products = array_merge($products, $response);
                }
            }
            catch (GuzzleException $e)
            {
                // return ['error' => $e->getResponse()->getBody()->getContents()];
            }
        }

        return $products;

    }


    function getReissNewPrice($id){
        $reissNewPrice = \DB::table('reiss_product_price')->where('product_id', $id)->first();
        if(!empty($reissNewPrice)){
            $reissNewPrice = $reissNewPrice->product_price;
        }else{
            $reissNewPrice = 0.00;
        }
        return $newPrice = number_format($reissNewPrice,2,'.','');
    }

    function getReissNewPriceFront($id, $price){
        $reissNewPrice = \DB::table('reiss_product_price')->where('product_id', $id)->first();
        if(!empty($reissNewPrice)){
            $reissNewPrice = $reissNewPrice->product_price;
        }else{
            $reissNewPrice = $price;
        }
        return $newPrice = number_format($reissNewPrice,2,'.','');
    }

    function getFeatureProductBySlug($product_slug = '')
    {
        $parameters = ['store_name' => Settings::store_name()];
       
        $url = \Config::get('constants.reiss_master_api_url')."productDetails/$product_slug";
        $client = new Client();
        $result = $client->get($url, ['query' => $parameters]);
        $response = json_decode($result->getBody()->getContents());
        if ( $response->status == 'success' )
        {
            return $response->product;
        }else{
            return null;
        }
    }

    function getLevelOneCategoriesName()
    {
        //own api calling for category listing from reiss site...
        $categories = reissAndOwnCategories2();
        $categories = collect($categories)->filter(function ($item, $key) {
                        return $item->level == 1 && $item->isActive == true;
                    })->values();
        return $categories;
    }

    function reissAndOwnCategories2()
    {
        $own_categories = StoreCategory::where('isActive', 1)->orderBy('name','ASC')->get();
        $categories = getCategories();
        $settings = getSetting();
        if ( $settings['reiss_cat'] == 1 ){
            $merged = collect($categories)->merge($own_categories);
        }else{
            $merged = $own_categories;
        }
        $newArray = array();
        foreach($merged as $key => $mcat){
            $newArray[$mcat->name] = $key;
        }

        ksort($newArray);
        
        return $newArray;
        /*
        dd($newArray);
        //dd($merged);
        exit;
        return $merged;*/
    }


    /*function getCategories()
    {
        $categories = [];

        //own api calling for category listing from reiss site...
        $request = Request::create('api/getCategories', 'GET');
        $response = json_decode(Route::dispatch($request)->getContent());

        if ( $response && !isset($response->error) )
        {
            $categories = $response->categories;
        }

        return $categories;
    }*/

    