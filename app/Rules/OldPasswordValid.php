<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class OldPasswordValid implements Rule
{
    public $oldP = '';
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($oldPassword)
    {
        //
        $this->oldP = $oldPassword;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        return $this->oldP === $value;


    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Incorrect password.';
        //The old password you have entered is incorrect
    }
}
