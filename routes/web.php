<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('front.home');
// });



Route::get('/', 'HomepageController@index')->name('index');

Route::get('/about-us', 'HomepageController@about')->name('about');
Route::get('/contact-us', 'HomepageController@contact')->name('contact');
Route::post('contact-send','HomepageController@contact_send')->name('contactSend');
Route::get('/services', 'HomepageController@services')->name('services');
Route::get('/top-brands', 'HomepageController@brand')->name('brands');
Route::get('catalogs', 'HomepageController@pormotions')->name('catalogs');
Route::get('returns-freight-credit', 'HomepageController@returns')->name('returns');
Route::get('terms-and-conditions', 'HomepageController@terms')->name('terms');
Route::get('privacy-policy', 'HomepageController@privacy')->name('privacy');


Route::namespace('Auth')->group(function () {
    Route::get('login-register', 'FrontLoginRegisterController@showLoginRegisterForm')->name('loginRegister');
    Route::post('user-login', 'FrontLoginRegisterController@login')->name('loginFront');
    Route::post('user-register', 'FrontLoginRegisterController@register')->name('registerFront');
    Route::get('user-logout', 'FrontLoginRegisterController@logout')->name('logoutFront');

    Route::get('forgot-password', 'FrontLoginRegisterController@ShowForgotPasswordForm')->name('showForgotPassword');
    Route::post('forgot-password-email', 'FrontLoginRegisterController@ForgotPassword')->name('ForgotPassword');

    route::get('reset-password/{token}', 'FrontLoginRegisterController@showResetForm')->name('showResetPassword');
    route::post('reset-password-update', 'FrontLoginRegisterController@restPasswordUpdate')->name('showResetPasswordUpdate');
    
Route::get('logout', 'LoginController@logout');
});


Route::namespace('Account')->group(function(){
    Route::get('my-profile', 'MyAccountController@index')->name('myaccount');
    Route::get('change-profile', 'MyAccountController@editProfile')->name('editProfile');
    Route::post('update-account', 'MyAccountController@updateProfile')->name('updateProfile');
    Route::get('change-password', 'MyAccountController@changePassword')->name('passwordChange');
    Route::post('change-password-update', 'MyAccountController@changePasswordUpdate')->name('passwordChangeUpdate');
    Route::get('my-order', 'MyAccountController@myOrderList')->name('myOrder');
    Route::get('order-detail/{id}', 'MyAccountController@myOrderDetail')->name('orderDetail');

});


Route::get('/admin', function () {
    return redirect(route('login'));
});

// Route::get('/home', function () {
//     return redirect(route('myaccount'));
// });

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/products/{cat_url}', 'ProductsController@index')->name('products'); 
Route::get('/product-detail/{prod_url}', 'ProductsController@product_detail')->name('product-detail'); 

Route::get('add-to-list/{id}', 'ListController@index')->name('addToList');
// Route::get('my-list', 'ListController@display_list')->name('displayList');
Route::get('update-qty', 'ListController@update_qty')->name('updateQty');
Route::get('remove-item', 'ListController@remove_item')->name('removeItem');
Route::get('download-pdf', 'ListController@download_list_pdf')->name('downloadPdf');

//Route::get('email-list', 'ListController@email_list')->name('emailList');

    //Route::post('email-list', 'ListController@email_list1')->name('emailList');

Route::prefix('content')->group(function(){
    Route::get('all', 'ContentController@index')->name('content.all');/*->middleware('redirectIfNotLoggedInAsUser')*/
    Route::get('homepage-edit', 'ContentController@homepage')->name('content.edit-home');
    Route::post('update_home', 'ContentController@update_homepage')->name('content.updatehome');
    Route::get('edit-page/{id}', 'ContentController@edit_page')->name('content.edit-page');
    Route::post('update','ContentController@update_edit_page')->name('content.update');
    Route::post('upload_brands', 'ContentController@update_brands')->name('uploadbrands');
    //Route::post('upload_brand', 'ContentController@dropzoneStore')->name('uploadbrand');
});

Route::get('settings', 'SettingsController@index')->name('settings');
Route::post('settings-save', 'SettingsController@setting_save')->name('settingSave');

Route::get('manage-subscription', 'SubscriptionController@index')->name('subscription');
Route::get('billing-history', 'SubscriptionController@billing')->name('billing');
Route::get('video-tutorials', 'SubscriptionController@video_tutorial')->name('video');
Route::get('monthly-reports', 'SubscriptionController@monthly_reports')->name('reports');
Route::any('site-search', 'SearchController@index')->name('site-search');

Route::get('order-management', 'OrderController@index')->name('orderManagement');
Route::get('orderDetail/{id}', 'OrderController@details')->name('orderDetailAdmin');
Route::post('order-details-update/{id}', 'OrderController@updateDetails')->name('updateOrderDetail');

Route::get('user-management', 'UsersController@index')->name('userManagement');

Route::get('user-add', 'UsersController@addUser')->name('userAdd');
Route::post('user-add-save', 'UsersController@AddUserSave')->name('userAddSave');

Route::get('user-edit/{id}', 'UsersController@editUser')->name('userEdit');
Route::post('user-edit-update/{id}', 'UsersController@EditUserUpdate')->name('userEditUpdate');

Route::get('user-delete/{id}', 'UsersController@UserDelete')->name('userDelete');


Route::prefix('category')->group(function () {
    Route::view('all', 'category.all')->name('category.all');
    Route::view('add', 'category.add')->name('category.add');
    Route::get('getTree','CategoryController@getCategoryTree')->name('category.tree');
    Route::get('get/{id}','CategoryController@getCategory')->name('category.get');
    Route::post('delete','CategoryController@delete')->name('category.delete');
    Route::post('create','CategoryController@create')->name('category.create');
    Route::post('update','CategoryController@update')->name('category.update');
    Route::post('toggle','CategoryController@toggleActive')->name('category.toggle');
});

Route::prefix('product')->group(function () {
    Route::get('all', 'ProductController@index')->name('product.all');
    Route::view('add', 'product.add')->name('product.add');
    Route::get('get/{id}','ProductController@getProduct')->name('product.get');
    Route::post('create','ProductController@create')->name('product.create');
    Route::post('update','ProductController@update')->name('product.update');
    Route::post('toggle','ProductController@toggleActive')->name('product.toggle');

    Route::get('bulk-upload','ProductController@bulkUpload')->name('bulkUpload');
    Route::post('bulk-upload-save','ProductController@submitBulkUpload')->name('submitBulkUpload');


});

Route::prefix('product')->group(function () {
    Route::get('profile', 'UserController@index')->name('user.profile');
});

/**
- code By Haseeb
- Import data to Db from file
- category + products
- Create All slug by table name
- Code Start Here
*/
Route::get('testaddViaFile', 'Data_importController@addViaFile');
Route::get('test2import_php', 'Data_importController@import_php');
Route::get('/allSlugCreate/{table_name}', 'CategoryController@create_all_slug')->name('creteTableSlug');
/**
Code End Here
*/
Route::get('test','CategoryController@getCategoryTree');

Route::get('/test2/{fname}/{lname}', function($fname,$lname){
    return 'This is test '.$fname.' '.$lname;
});

Route::get('/admin/post/example', array('as'=>'admin.home', function(){
    $url = route('admin.home');

    return "This url is :: ".$url;
}));

// by Shakeel

Route::namespace('Reiss')->group(function() {
    Route::get('my-list', 'CartController@list')->name('displayList');
    Route::get('checkout', 'CartController@billingShipping')->name('checkout');
    Route::post('checkout-info', 'CartController@billShipSave')->name('billShipSave');
    Route::get('payment-info', 'CartController@paymentInfo')->name('paymentInfo');
    Route::post('payment-info-get', 'CartController@paymentInfoGet')->name('paymentInfoGet');
    Route::get('confirm-payment', 'CartController@cartConfirm')->name('cartConfirm');
    Route::get('order-payment', 'CartController@payment')->name('payment');

    Route::get('email-list', 'CartController@email_list1')->name('emailList');


    Route::get('thankyou', 'CartController@thankYou')->name('thankyou');

});

Route::prefix('reiss')->namespace('Reiss')->group(function() {

    // front end side...
    Route::get('products/{category_slug}', 'ProductController@list')->name('reiss.products');
    Route::get('product-details/{product_slug}', 'ProductController@details')->name('reiss.product.details');
    
    // admin side... 
    Route::view('products', 'product.reiss')->name('product.reiss'); 
    Route::get('product-price-edit/{id}', 'ProductController@reissProductPriceEdit')->name('reiss.productPrice');
    Route::post('product-price-updated/{id}', 'ProductController@reissProductPriceUpdated')->name('reiss.productPriceUpdated');
    Route::post('getNewPrice', 'ProductController@reissProductNewPrice')->name('reiss.productPriceNewPrice');

    
    Route::prefix('product')->group(function() {
        Route::get('get/{product_id}', 'ProductController@get')->name('reiss.product.get');
        Route::post('update', 'ProductController@update')->name('reiss.product.update');
        Route::post('delete', 'ProductController@delete')->name('reiss.product.delete');
        Route::post('create', 'ProductController@create')->name('reiss.product.create');
        Route::post('toggle', 'ProductController@toggle')->name('reiss.product.toggle');

        Route::get('bulk-edit', 'ProductController@bulkEdit')->name('product.bulkedit');
        Route::post('bulkupdate', 'ProductController@bulkupdate')->name('reiss.product.bulkupdate');
        
    });

    // admin side...
    Route::view('categories', 'category.reiss')->name('category.reiss');
    Route::prefix('category')->group(function() {
        Route::post('create', 'CategoryController@create')->name('reiss.category.create');
        Route::post('update', 'ProductController@update')->name('reiss.category.update');
        Route::post('toggle', 'CategoryController@toggle')->name('reiss.category.toggle');
    });
});
// by Shakeel


Route::view('/test', 'front.pdf.my-cart-pdf');

Route::get('/test1', function(){
    return view('front.pdf.my-cart-pdf');
});

Route::view('/testEmail', 'front.pdf.my-cart-pdf1');

Route::post('/test1', function(){
    $emailData = request()->all();

    return view('front.pdf.my-cart-pdf')->with('email_data', $emailData);
})->name('show_email');

