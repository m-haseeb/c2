<?php

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Illuminate\Http\Request;

use App\Settings;
use App\StoreProduct;
use App\StoreCategory;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('test', function(Request $request){
	dd(StoreCategory::find('RXTjlLbCNa'));
});

// by Shakeel
Route::namespace('Api')->group(function(){
    Route::get('getCategories', 'ApiController@getCategories')->name('api.categories');
    Route::get('getProducts', 'ApiController@getProducts')->name('api.products');
    Route::get('getProductsByIds', 'ApiController@productsByIds')->name('api.products.ids');
    Route::get('categoriesWithOwn',  function(){

    	$own_categories = StoreCategory::where('isActive', 1)->get();
        $categories = getCategories();
        $merged = collect($categories)->merge($own_categories);
        
        return $merged;

    })->name('api.categoriesWithOwn');
});
// by Shakeel