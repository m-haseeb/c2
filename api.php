<?php

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Illuminate\Http\Request;

use App\Settings;
use App\StoreProduct;
use App\StoreCategory;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('api')->group(function() {
    Route::get('getCategories', 'ApiController@getCategories')->name('api.categories');
    Route::get('getProducts', 'ApiController@getProducts')->name('api.products');
});
