<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 11-Dec-17
 * Time: 4:29 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Edit Product')

@section('css')
    {{--for radios--}}
    <link rel="stylesheet" href="{{asset('/css/addCategory.css')}}">
@endsection

@section('js')


    
@endsection

@section('content')




    {{ Form::open( array('name'=>'ReissNewPrice', 'class'=>'form-horizontal', 'url'=>route('reiss.productPriceUpdated',['id'=>$id]) ) ) }}
    <div class="box">
        <div class="box-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="price" class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="price" value="{{$newPrice}}" id="price" placeholder="Required">

                        <span class="text-danger price">{{$errors->first('price')}}</span>
                    </div>
                </div>

                

            </div>
            
            <button type="submit" class="btn btn-default" id="edit">Update</button>
        </div>
    </div>
    {{ Form::close() }}


    <script>
    var root_url = '{{asset('/')}}';
    var base_url = '{{asset('/')}}';
    var ser = '{{$_SERVER['DOCUMENT_ROOT']}}/tiny_upload';

    </script>
    <script src="{{asset('/tiny/tinymce/js/tinymce/jquery.js')}}"></script>
    <script src="{{asset('/tiny/tinymce/js/tinymce/jquery.tinymce.min.js')}}"></script>
    <script src="{{asset('/tiny/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/tiny/common.js')}}"></script>
    <!-- tinymce configration end here.-->

    <link rel="stylesheet" href="{{asset('/filer/css/jquery.filer.css')}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('/filer/css/themes/jquery.filer-dragdropbox-theme.css')}}" type="text/css" media="all" />

@stop
