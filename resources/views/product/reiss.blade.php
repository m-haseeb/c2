@php
/**
- Code For Riess Product Listing
*/
@endphp
@extends('adminlte::page')

@section('title', 'Reiss Products')

@section('js')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.0/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('/vendor/datatables/buttons.server-side.js')}}"></script>

    <script  type="text/javascript">
        
        $(document).ready(function () {
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
            loadProducts();
        });

        function loadProducts()
        {
            $('.box').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');

            $.ajax({
                method: 'get',
                url: '{{route('api.products')}}',
                success: function (response) {

                    if ( response.error === undefined )
                    {
                        let permissions = response.permissions;
                        let product_list = "";
                        let count = 1;
                        

                        $.each(response.products, function(index, item){

                            // var pp = item.price*0.02;
                            // var newP = parseFloat(item.price) + parseFloat(pp);
                            // newP = parseFloat(newP).toFixed(3);
                            //jjjj
                            var pID = item.id;
                            
                            product_list += `<tr>
                                                <td class="text-center">${count}</td>
                                                <td class="text-center">${item.name}</td>
                                                <td class="text-center">${item.manufacturer}</td>
                                                <td class="text-center">${item.sku}</td>
                                                <td class="text-center">${item.price}</td>
                                                <td class="text-center">${item.newPrice}</td>
                                                <td class="text-center">${item.manufacturer_code}</td>
                                                <td class="text-center">`;

                            //add buttons based on permissions... 
                            if ( item.isActive == true )
                            {
                                product_list += `<button type="button" class="active btn btn-success btn-sm" product_id="${item.id}">
                                                    <i class="fa fa-eye"></i>
                                                </button>&nbsp;`;
                            }

                            if ( item.isActive == false )
                            {
                                product_list += `<button type="button" class="deactive btn btn-warning btn-sm" product_id="${item.id}">
                                                    <i class="fa fa-eye-slash"></i>
                                                </button>&nbsp;`;
                            }

                            product_list += `<a class="btn btn-primary edit" href="{{url('reiss/product-price-edit/')}}/${item.id}" product_id="${item.id}">
                                                <i class="fa fa-pencil fa-lg"></i>
                                            </a>&nbsp;`;

                            product_list += `</td> 
                                            </tr>`;
                            count++;

                            //console.log('item product: ', item);
                        });

                        $('#product_table_body').html(product_list);
                        $( "#product_table" ).DataTable();

                        $('.overlay').remove();
                        
                        //if no permission for add... remove add button...
                        if ( !permissions.reiss_prod_add )
                            $('.box-tools.pull-right').remove();
                    }
                    else
                    {
                        $('.overlay').remove();
                    }
                }
            });
        }

        function toggle(id, toggle)
        {
            $.ajax({
                method: 'post',
                url: '{{route('reiss.product.toggle')}}',
                data: { id: id, toggle: toggle },
                success: function (response) {
                    // $( "#product_table" ).DataTable().draw();
                    loadProducts();
                }
            });
        }

        $('#product_table').on('click', '.active', function() {

            let id = $(this).attr('product_id');

            swal({
                title: "Are you sure?",
                text: "This is will disable this product!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((doIt) => {
                if (doIt) {
                    toggle(id, "disable");
                }
            });
        });

        $('#product_table').on('click', '.deactive', function () {

            let id = $(this).attr('product_id');

            swal({
                title: "Are you sure?",
                text: "This is will enable this product!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((doIt) => {
                if (doIt) {
                    toggle(id, "enable");
                }
            });
        });
    </script>


@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h6 class="box-title">Reiss Products</h6>
            <div class="box-tools pull-right">
                <div class="form-inline pull-left" style="margin-right: 10px">
                    <a class="btn btn-default btn-sm" href="{{route('product.add')}}"><i class="fa fa-plus"></i> &nbsp;Create Product</a>
                </div>
            </div>
        </div>
        <div class="box-body">
             <table class="table table-hover" id="product_table">
                <thead>
                    <tr>
                        <th width="10%">Id</th>
                        <th width="25%" class="text-center">Name</th>
                        <th width="10%" class="text-center">Manufacturer</th>
                        <th width="10%" class="text-center">Sku</th>
                        <th width="10%" class="text-center">Orginal Price</th>
                        <th width="10%" class="text-center">Updated Price</th>
                        <th width="15%" class="text-center">Manufacturer Code</th>
                        <th width="10%" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody id="product_table_body">

                </tbody>
            </table>
        </div>
    </div>
@stop
