<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 11-Dec-17
 * Time: 4:29 PM
 */?>
@extends('adminlte::page')

@section('title', 'My Products')

@section('js')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.0/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('/vendor/datatables/buttons.server-side.js')}}"></script>
    {!! $dataTable->scripts() !!}
    <script  type="text/javascript">
          $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
          
        function toggle(id, toggle) {

            $.ajax({
                method: 'post',
                url: '{{route('product.toggle')}}',
                data: {
                    id: id,
                    toggle: toggle
                },
                success: function (response) {
                    $( "#dataTableBuilder" ).DataTable().draw();
                }
            });
        }

        $('#dataTableBuilder').on('click', '.active', function () {

            let id = $(this).attr('product_id');

            swal({
                title: "Are you sure?",
                text: "This is will disable this product!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((doIt) => {
                if (doIt) {
                    toggle(id, "disable");
                }
            });
        });

        $('#dataTableBuilder').on('click', '.deactive', function () {

            let id = $(this).attr('product_id');

            swal({
                title: "Are you sure?",
                text: "This is will enable this product!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((doIt) => {
                if (doIt) {
                    toggle(id, "enable");
                }
            });
        });

        $('#dataTableBuilder').on('click', '.delete', function () {

            let id = $(this).attr('product_id');

            swal({
                title: "Are you sure?",
                text: "This will delete this product!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((doIt) => {
                if (doIt) {

                    $.ajax({
                        method: 'post',
                        url: '{{route('reiss.product.delete')}}',
                        data: { id: id },
                        success: function (response) {
                            $( "#dataTableBuilder" ).DataTable().draw();
                        }
                    });
                }
            });
        });
    </script>

@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h6 class="box-title">My All Products</h6>
            <div class="box-tools pull-right">
                <div class="form-inline pull-left" style="margin-right: 10px">
                    <a class="btn btn-default btn-sm" href="{{route('product.add')}}"><i class="fa fa-plus"></i> &nbsp;Create Product</a>
                </div>

                <div class="form-inline pull-left" style="margin-right: 10px">
                    <a class="btn btn-default btn-sm" href="{{route('bulkUpload')}}"><i class="fa fa-plus"></i> &nbsp;Bulk Upload</a>
                </div>
            </div>
        </div>
        <div class="box-body">
            {!! $dataTable->table() !!}
        </div>
    </div>
@stop
