<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 11-Dec-17
 * Time: 4:29 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Edit Product')

@section('css')
    {{--for radios--}}
    <link rel="stylesheet" href="{{asset('/css/addCategory.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('js')


    <script type="text/javascript">

        let list = "";
        let loading = '<i class="fa fa-spinner fa-pulse fa-fw"></i>';
        const product_id = "{{Request::input('edit')}}";
        let product = null;

        $(document).ready(function () {

            if (product_id !== "") {
                $('#save').remove();
                loadProduct();
            }
            else {
                $('#edit').remove();
                init();
            }
        });

        function loadProduct() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'get',
                url: '{{route('reiss.product.get', Request::input('edit') )}}',
                success: function (response) {
                    product = response;
                    //console.log(product);
                    init(true);
                }
            });
        }

        function fillForm() {

            if (product !== null) {

                $('#product_name').val(product.name);
                $('#sku').val(product.sku);
                $('#upc').val(product.upc);
                $('#price').val(product.price);
                if (product.is_feature == 1){
                    $('.toggle-off').click();
                    $('#toggle-trigger-feature-product').prop('checked', true);
                }
                else {
                    $('#toggle-trigger-feature-product').prop('checked', false);
                }
                $('input[id = "' + product.store_category_id + '"]').prop('checked', true);
                $('#AverageShippingCost').val(product.AverageShippingCost);
                $('#Description').val(product.Description);
                $('#BulletPointsClosure').val(product.BulletPointsClosure);
                $('#BulletPointsColor').val(product.BulletPointsColor);
                $('#BulletPointsMaterial').val(product.BulletPointsMaterial);
                $('#BulletPointsPockets').val(product.BulletPointsPockets);
                $('#ItemNumber').val(product.ItemNumber);
                $('#MinimumOrder').val(product.MinimumOrder);
                $('#ShipWeight').val(product.ShipWeight);
                $('#ShippingBoxDimensions').val(product.ShippingBoxDimensions);
                $('#ShipsVia').val(product.ShipsVia);
                $('#Standard').val(product.Standard);
                $('#TariffCode').val(product.TariffCode);
                $('#UsuallyShipsIn').val(product.UsuallyShipsIn);

                if ( product.product_image != '' && product.product_image != null) //&& product.product_image != null
                {
                    $('#product_image').attr('src', `../products_images/thumbs/${product.product_image}`).css('display', 'block');
                }else{
                    ///
                }
            }
        }

        function init(loadForm) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'get',
                url: '{{route('api.categoriesWithOwn')}}',
                success: function (response) {

                    var tree =  $.map(response, function(item, index){
                        if ( item.parent_id == '' || item.parent_id == null ) {
                            return item;
                        }
                    });

                    list += `<div class="checkbox"><ul class="wpsc_categories wpsc_top_level_categories">`;

                    $.each(tree, function (index, item) {

                        list += `<li>
                                    <label for="${item.id}" class="${item.isActive === 0 ? 'text-danger' : ''}">
                                    <input type="radio" name="cat_group" id="${item.id}" ${index == 0 ? 'checked' : ''} ${item.isActive == 0 ? 'disabled' : ''} />${item.name}</label>`;

                        let children = getChildren(response, item.id);

                        if ( children.length > 0 )
                        {
                            list += `<span class='toggle'>+</span>`;
                            createSubList(response, children, false);
                            list += `</li>`;
                        }
                        else
                        {
                            list += `</li>`;
                        }
                    });

                    list += `</ul></div>`;

                    $('#loading').remove();

                    $('#category_list').append(list);

                    if ( loadForm )
                    {
                        fillForm();
                    }
                }
            });
        }


        function getChildren(items, item_id)
        {
            return $.map(items, function(item, index){
                if ( item.parent_id == item_id){ return item; }
            });
        }

        function createSubList(categories, items, expandable) {

            if ( expandable )
            {
                list += `<span class='toggle'>+</span><ul class="wpsc_categories wpsc_top_level_categories" style='display: none'>`;
            }
            else
            {
                list += `<ul class="wpsc_second_level_categories" style='display: none'>`;
            }

            $.each(items, function(index, item) {

                list += `<li>
                            <label for="${item.id}" class="${item.isActive === 0 ? 'text-danger' : ''}">
                            <input type="radio" name="cat_group" id="${item.id}" ${item.isActive == 0 ? 'disabled' : ''} />${item.name}</label>`;

                let children = getChildren(categories, item.id);

                if ( children.length > 0 )
                {
                    createSubList(categories, children, true);
                }
                else
                {
                    list += `</li>`;
                }
            });

            list += `</ul>`;
        }

        $('#category_list').on('click', '.toggle', function () {
            var $ul = $(this).next();
            $(this).html($ul.is(':visible') ? '+' : '&ndash;');
            $ul.slideToggle();
        });



        $('#edit').click(function () {
            tinymce.triggerSave();

            const save = $('#edit');
            save.prop('disabled', 'disabled');
            save.html(loading);

            let form_data = new FormData();

            form_data.append('id', product.id);
            form_data.append('product_name', $('#product_name').val());
            form_data.append('sku', $('#sku').val());
            form_data.append('upc', $('#upc').val());
            form_data.append('price', $('#price').val());
            form_data.append('product_thumb', $('input[type=file]')[0].files[0]);
            if ($('#toggle-trigger-feature-product').is(':checked')) {
                form_data.append('feature_product', 1);
            }
            else {
                form_data.append('feature_product', 0);
            }
            form_data.append('category', $('input[name=cat_group]:checked').attr('id'));
            form_data.append('AverageShippingCost', $('#AverageShippingCost').val());
            form_data.append('Description', $('#Description').val());
            form_data.append('BulletPointsClosure', $('#BulletPointsClosure').val());
            form_data.append('BulletPointsColor', $('#BulletPointsColor').val());
            form_data.append('BulletPointsMaterial', $('#BulletPointsMaterial').val());
            form_data.append('BulletPointsPockets', $('#BulletPointsPockets').val());
            form_data.append('ItemNumber', $('#ItemNumber').val());
            form_data.append('MinimumOrder', $('#MinimumOrder').val());
            form_data.append('ShipWeight', $('#ShipWeight').val());
            form_data.append('ShippingBoxDimensions', $('#ShippingBoxDimensions').val());
            form_data.append('ShipsVia', $('#ShipsVia').val());
            form_data.append('Standard', $('#Standard').val());
            form_data.append('TariffCode', $('#TariffCode').val());
            form_data.append('UsuallyShipsIn', $('#UsuallyShipsIn').val());


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'post',
                url: '{{route('reiss.product.bulkupdate')}}',
                processData: false,
                contentType: false,
                data: form_data,
                success: function (response) {
                    //alert(1);
                    //location.reload(true);
                    window.location.assign('{{ route('product.all') }}');

                },
                error: function (data) {
                    save.prop('disabled', '');
                    save.html("Submit");

                    let response = data.responseJSON;

                    $.each(response.errors, function (index, errors) {
                        $('#' + index).addClass("has-error");

                        let errorString = "";
                        $.each(errors, function (index, item) {
                            errorString += item;
                        });
                        $('.' + index).html(errorString);

                    });
                }
            });

        });

        function previewFile() {
            var preview = document.querySelector('img');
            preview.style.display = 'none';
            var file    = document.querySelector('input[type=file]').files[0];
            var reader  = new FileReader();

            reader.onloadend = function () {
                preview.src = reader.result;
                preview.style.display = 'block';
            }

            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";

                if ( product.product_image != '' )
                {
                    $('#product_image').attr('src', `../products_images/thumbs/${product.product_image}`).css('display', 'block');
                }
            }
        }
    </script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js">
        $('#toggle-trigger-feature-product').click(function () {

        });
    </script>
@endsection

@section('content')





    <div class="box">
        <div class="box-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="product_name" class="col-sm-2 control-label">Product Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="product_name" placeholder="Required">
                        <span class="text-danger product_name"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ItemNumber" class="col-sm-2 control-label">Part/Item Number</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ItemNumber" placeholder="Required">
                        <span class="text-danger ItemNumber"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sku" class="col-sm-2 control-label">SKU</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="sku" placeholder="Required">
                        <span class="text-danger sku"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Description" class="col-sm-2 control-label">Description (plain text or html)</label>
                    <div class="col-sm-10">
                        <textarea class="myeditor" name="Description" id="Description" cols="12" rows="5">{{$product->Description}}</textarea>
                        <span class="text-danger Description"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="upc" class="col-sm-2 control-label">UPC</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="upc" placeholder="Required">
                        <span class="text-danger upc"></span>
                    </div>
                    <div class="col-sm-10">
                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="BulletPointsMaterial" class="col-sm-2 control-label">Bullet Points Key points(Material)</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="BulletPointsMaterial" placeholder="Required">
                        <span class="text-danger BulletPointsMaterial"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="BulletPointsColor" class="col-sm-2 control-label">Bullet Points Key points(Color)</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="BulletPointsColor" placeholder="Required">
                        <span class="text-danger BulletPointsColor"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="BulletPointsClosure" class="col-sm-2 control-label">Bullet Points Key points(Closure)</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="BulletPointsClosure" placeholder="Required">
                        <span class="text-danger BulletPointsClosure"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="BulletPointsPockets" class="col-sm-2 control-label">Bullet Points Key points(Pockets)</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="BulletPointsPockets" placeholder="Required">
                        <span class="text-danger BulletPointsPockets"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="MinimumOrder" class="col-sm-2 control-label">Minimum Order</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="MinimumOrder" placeholder="Optional">
                        <span class="text-danger MinimumOrder"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="AverageShippingCost" class="col-sm-2 control-label">Average Shipping Cost</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="AverageShippingCost" placeholder="Required">
                        <span class="text-danger AverageShippingCost"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="ShipsVia" class="col-sm-2 control-label">Ships Via</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ShipsVia" placeholder="Required">
                        <span class="text-danger ShipsVia"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="UsuallyShipsIn" class="col-sm-2 control-label">Usually Ships In</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="UsuallyShipsIn" placeholder="Required">
                        <span class="text-danger UsuallyShipsIn"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="ShippingBoxDimensions" class="col-sm-2 control-label">Shipping Box Dimensions</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ShippingBoxDimensions" placeholder="Required">
                        <span class="text-danger ShippingBoxDimensions"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="ShipWeight" class="col-sm-2 control-label">Ship Weight (lbs)</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ShipWeight" placeholder="Required">
                        <span class="text-danger ShipWeight"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="TariffCode" class="col-sm-2 control-label">Tariff Code</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="TariffCode" placeholder="Optional">
                        <span class="text-danger TariffCode"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="Standard" class="col-sm-2 control-label">Standard</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="Standard" placeholder="Required">
                        <span class="text-danger Standard"></span>
                    </div>
                </div>



                <div class="form-group">
                    <label for="price" class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="price" placeholder="Required">
                        <span class="text-danger price"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="page_image" class="col-sm-2 control-label">Product Thumb</label>
                    <div class="col-sm-10">
                        <img src="" height="100px" width="150px" alt="Image preview..." id="product_image" style="display: none">
                        <input type="file" class="form-control" id="page_image" name="page_image" placeholder="Required" onchange="previewFile()">
                        <span class="text-danger product_thumb"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="Feature" class="col-sm-2 control-label">Feature Product</label>
                    <div class="col-sm-10">
                        <input id="toggle-trigger-feature-product" name="feature-product" type="checkbox" data-toggle="toggle" value="">
                        <span class="text-danger feature-product"></span>
                    </div>
                </div>



            </div>
            <div id="category_list" class="radio">
                <h5 class="text-black text-bold"> Select Category (Required) </h5>
                <span class="text-danger">Red Categories Are Disabled</span>
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw" id="loading"></i>
            </div>
            {{-- <button type="button" class="btn btn-default" id="save">Submit</button> --}}
            <button type="button" class="btn btn-default" id="edit">Update</button>
        </div>
    </div>


    <script>
        var root_url = '{{asset('/')}}';
        var base_url = '{{asset('/')}}';
        var ser = '{{$_SERVER['DOCUMENT_ROOT']}}/tiny_upload';

    </script>
    <script src="{{asset('/tiny/tinymce/js/tinymce/jquery.js')}}"></script>
    <script src="{{asset('/tiny/tinymce/js/tinymce/jquery.tinymce.min.js')}}"></script>
    <script src="{{asset('/tiny/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/tiny/common.js')}}"></script>
    <!-- tinymce configration end here.-->


    <link rel="stylesheet" href="{{asset('/filer/css/jquery.filer.css')}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('/filer/css/themes/jquery.filer-dragdropbox-theme.css')}}" type="text/css" media="all" />

@stop
