<?php
/**

 * User: Haseeb
 * Date: 11-Dec-17
 * Time: 4:29 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Bulk Upload Products')

@section('css')
    {{--for radios--}}
    <link rel="stylesheet" href="{{asset('/css/addCategory.css')}}">
@endsection

@section('js')

@endsection

@section('content')

{{ Form::open(array('name'=>'productFile','url'=>route('submitBulkUpload'), 'files'=>true, 'enctype' =>'multipart/form-data' ) ) }}
    <div class="box">
        <div class="box-body">

            @if(session()->has('msg'))
            <div class="alert alert-info alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {!! session()->get('msg') !!}
            </div>
            @endif

            <div class="form-horizontal">
                
                <div class="form-group">
                    <label for="page_image" class="col-sm-2 control-label">Product File</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" id="page_image" name="page_image" accept=".txt,.TXT" placeholder="Required" />
                        <span class="text-danger product_thumb">{{ $errors->first('page_image')}}  {!! session()->get('csverror') !!}</span>
                    </div>
                </div>

            </div>
            
            <button type="submit" class="btn btn-default" id="save">Submit</button>
        </div>
    </div>
{{ Form::close() }}
@stop
