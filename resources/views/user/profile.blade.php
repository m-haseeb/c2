<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 12-Dec-17
 * Time: 5:09 PM
 */?>
@extends('adminlte::page')

@section('title', 'Profile')

@section('content')
    <div class="box">
        <div class="box-body">
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="first_name" class="col-sm-2 control-label">First Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="first_name" value="{{$user->first_name}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="last_name" class="col-sm-2 control-label">Last Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="last_name" value="{{$user->last_name}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="store_name" class="col-sm-2 control-label">Store Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="store_name" value="{{$user->store_name}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="store_id" class="col-sm-2 control-label">Store ID</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="store_id" value="{{$user->customer_id}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="Email" value="{{$user->email}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone_number" class="col-sm-2 control-label">Phone Number</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="phone_number" value="{{$user->phone}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="join_date" class="col-sm-2 control-label">Join Date</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="join_date" value="{{date('m-d-Y h:i',strtotime($user->created_at))}}" disabled>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
