<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 11-Dec-17
 * Time: 4:29 PM
 */?>
@extends('adminlte::page')

@section('title', 'Manage Subscription')

@section('js')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.0/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    {{-- <script src="{{asset('/vendor/datatables/buttons.server-side.js')}}"></script>
    {!! $dataTable->scripts() !!} --}}
 

    {{-- Ammar new css for billing and subscription --}}
    <link rel="stylesheet" href="{{asset('/vendor/adminlte/dist/css/newAdmin.css')}}">

@endsection

@section('content')

    <div class="box">
        <div class="box-header with-border">
            <h6 class="box-title">Manage Subscription</h6>
            <div class="box-tools pull-right">
                {{-- <div class="form-inline pull-left" style="margin-right: 10px">
                    <a class="btn btn-default btn-sm" href="{{route('product.add')}}"><i class="fa fa-plus"></i> &nbsp;Create Product</a>
                </div> --}}
            </div>
        </div>
        <div class="box-body">
            @if(session()->has('success'))
            <div class="alert alert-info alert-dismissable fade in">
                
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {!! session()->get('success') !!}
                
            </div>@endif
           
            <div class="tble-sec">
                <div class="tble-sec-tit">
                    <h5>September 2017</h5>
                </div>

                <div class="table-lowr">
                    <table class="table table-condensed">
                        <thead>
                          <tr>
                            <th style="width:16%;">Type</th>
                            <th style="width:15%;">Date</th>
                            <th style="width:12.5%;">Subscription ID</th>
                            <th style="width:12%;">Terms/Status</th>
                            <th style="width:12%;">Plan</th>
                            <th style="width:10.5%;">Amount</th>
                            <th style="width:11%;"></th>
                            <th style="width:11%;"></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="rd">Subscription Cancelled</td>
                            <td>2017-05-17 12:56PM</td>
                            <td>I-72UVXB02VGKG</td>
                            <td></td>
                            <td>Personal Plan</td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td class="gr">Subscription Created</td>
                            <td>2017-05-17 12:56PM</td>
                            <td>I-72UVXB02VGKG</td>
                            <td></td>
                            <td>Personal Plan</td>
                             <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td class="bl">Payment Mode</td>
                            <td>2017-05-17 12:56PM</td>
                            <td>I-72UVXB02VGKG</td>
                            <td>Completed</td>
                            <td>Personal Plan</td>
                             <td>USD 12.00</td>
                            <td><a href="#">View Invoice</a></td>
                            <td><a href="#">Download PDF</a></td>
                          </tr>
                        </tbody>
                      </table>


                </div>  

            </div>

            <div class="tble-sec">
                <div class="tble-sec-tit">
                    <h5>October 2017</h5>
                </div>

                <div class="table-lowr">
                    <table class="table table-condensed">
                        <thead>
                          <tr>
                            <th style="width:16%;">Type</th>
                            <th style="width:15%;">Date</th>
                            <th style="width:12.5%;">Subscription ID</th>
                            <th style="width:12%;">Terms/Status</th>
                            <th style="width:12%;">Plan</th>
                            <th style="width:10.5%;">Amount</th>
                            <th style="width:11%;"></th>
                            <th style="width:11%;"></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="yl">Subscription Cancelled</td>
                            <td>2017-05-17 12:56PM</td>
                            <td>I-72UVXB02VGKG</td>
                            <td></td>
                            <td>Business 1</td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                          
                        </tbody>
                      </table>


                </div>  

            </div>

            <div class="tble-sec">
                <div class="tble-sec-tit">
                    <h5>November 2017</h5>
                </div>

                <div class="table-lowr">
                    <table class="table table-condensed">
                        <thead>
                          <tr>
                            <th style="width:16%;">Type</th>
                            <th style="width:15%;">Date</th>
                            <th style="width:12.5%;">Subscription ID</th>
                            <th style="width:12%;">Terms/Status</th>
                            <th style="width:12%;">Plan</th>
                            <th style="width:10.5%;">Amount</th>
                            <th style="width:11%;"></th>
                            <th style="width:11%;"></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="rd">Subscription Cancelled</td>
                            <td>2017-05-17 12:56PM</td>
                            <td>I-72UVXB02VGKG</td>
                            <td></td>
                            <td>Business 1</td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td class="bl">Payment Mode</td>
                            <td>2017-05-17 12:56PM</td>
                            <td>I-72UVXB02VGKG</td>
                            <td>Completed</td>
                            <td>Business 1</td>
                             <td>USD 24.00</td>
                            <td><a href="#">View Invoice</a></td>
                            <td><a href="#">Download PDF</a></td>
                          </tr>
                        </tbody>
                      </table>


                </div>  

            </div>


             <div class="tble-sec">
                <div class="tble-sec-tit">
                    <h5>December 2017</h5>
                </div>

                <div class="table-lowr">
                    <table class="table table-condensed">
                        <thead>
                          <tr>
                            <th style="width:16%;">Type</th>
                            <th style="width:15%;">Date</th>
                            <th style="width:12.5%;">Subscription ID</th>
                            <th style="width:12%;">Terms/Status</th>
                            <th style="width:12%;">Plan</th>
                            <th style="width:10.5%;">Amount</th>
                            <th style="width:11%;"></th>
                            <th style="width:11%;"></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="bl">Payment Mode</td>
                            <td>2017-05-17 12:56PM</td>
                            <td>I-72UVXB02VGKG</td>
                            <td>Completed</td>
                            <td>Business 1</td>
                             <td>USD 24.00</td>
                            <td><a href="#">View Invoice</a></td>
                            <td><a href="#">Download PDF</a></td>
                          </tr>
                        </tbody>
                      </table>


                </div>  

            </div>




        </div>
    </div>
@stop
