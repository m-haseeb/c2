<?php
/**
 * Created by PhpStorm.
 * User: Haseeb
 * Date: 11-Dec-17
 * Time: 4:29 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Settings')

@section('css')
    {{--for radios--}}
    <link rel="stylesheet" href="{{asset('/css/addCategory.css')}}">
@endsection


@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h6 class="box-title">Settings</h6>
            <div class="box-tools pull-right">
                
            </div>
        </div>

        {{Form::open(array('name'=>'settings', 'files'=> true, 'url'=>route('settingSave') ) )}}
{{-- <h4 class="grey-text">Settings</h4> --}}
        <div class="box-body">
            @if(session()->has('msg'))
            <div class="alert alert-info alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {!! session()->get('msg') !!}
            </div>
            @endif

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="site_name" class="col-sm-2 control-label"> Site Name*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="site_name" value="{{old('site_name')!=''?old('site_name'):$data['site_name']}}" id="site_name" placeholder="Required">
                        <span class="text-danger">{{$errors->first('site_name')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="site_email" class="col-sm-2 control-label">Site Email*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="site_email" value="{{old('site_email')!=''?old('site_email'):$data['site_email']}}" id="site_email" placeholder="Required">
                        <span class="text-danger">{{$errors->first('site_email')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="site_phone" class="col-sm-2 control-label">Site Phone*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="site_phone" value="{{old('site_phone')!=''?old('site_phone'):$data['site_phone']}}" id="site_phone" placeholder="Required">
                        <span class="text-danger">{{$errors->first('site_phone')}}</span>
                    </div>
                </div>
                {{-- <div class="form-group">
                    <label for="site_fax" class="col-sm-2 control-label">Site Fax*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="site_fax" value="{{old('site_fax')!=''?old('site_fax'):$data['site_fax']}}" id="site_fax" placeholder="Required">
                        <span class="text-danger">{{$errors->first('site_fax')}}</span>
                    </div>
                </div> --}}

                <div class="form-group">
                    <label for="site_address" class="col-sm-2 control-label">Site Address*</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="site_address" name="site_address" cols=3 >{{old('site_address')!=''?old('site_address'):$data['site_address']}}</textarea>
                        <span class="text-danger">{{$errors->first('site_address')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="footerDesc" class="col-sm-2 control-label">Footer Description*</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="footerDesc" name="footerDesc" cols=3 >{{old('footerDesc')!=''?old('footerDesc'):$data['footerDesc']}}</textarea>
                        <span class="text-danger">{{$errors->first('footerDesc')}}</span>
                    </div>
                </div>
                

                <div class="form-group">
                    <label for="facebook" class="col-sm-2 control-label">Facebook Link*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="facebook" value="{{old('facebook')!=''?old('facebook'):$data['facebook']}}" id="facebook" placeholder="Required">
                        <span class="text-danger">{{$errors->first('facebook')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="linkin" class="col-sm-2 control-label">LinkedIn Link*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="linkin" value="{{old('linkin')!=''?old('linkin'):$data['linkin']}}"  id="linkin" placeholder="Required">
                        <span class="text-danger">{{$errors->first('linkin')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="twitter" class="col-sm-2 control-label">Twitter Link*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="twitter" value="{{old('twitter')!=''?old('twitter'):$data['twitter']}}"  id="twitter" placeholder="Required">
                        <span class="text-danger">{{$errors->first('twitter')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="youtube" class="col-sm-2 control-label">Youtube Link*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="youtube" value="{{old('youtube')!=''?old('youtube'):$data['youtube']}}"  id="youtube" placeholder="Required">
                        <span class="text-danger">{{$errors->first('youtube')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="logo" class="col-sm-2 control-label">Site Logo*</label>
                    <div class="col-sm-4">
                        <p><img src="{{asset('site_logo/thumb/'.$data['logo'])}}" alt="{{$data['logo']}}"></p>
                        <input type="file" class="form-control" name="logo" value="" id="logo" placeholder="Required">
                        <span class="text-danger">{{$errors->first('logo')}}</span>
                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="show_price" class="col-sm-2 control-label">Product Price*</label>
                    <div class="col-sm-10">
                        <select class="form-control"  name="show_price" id="show_price">
                            {{-- <option value="">Select</option> --}}
                            <option value="1" {{$data['show_price']==1?'selected':''}} >Enable</option>
                            <option value="0" {{$data['show_price']==0?'selected':''}} >Disable</option>
                        </select>
                        <span class="text-danger">{{$errors->first('show_price')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="reiss_prod" class="col-sm-2 control-label">Reiss Products*</label>
                    <div class="col-sm-10">
                        <select class="form-control"  name="reiss_prod" id="reiss_prod">
                            {{-- <option value="">Select</option> --}}
                            <option value="1" {{$data['reiss_prod']==1?'selected':''}} >Enable</option>
                            <option value="0" {{$data['reiss_prod']==0?'selected':''}} >Disable</option>
                        </select>
                        <span class="text-danger">{{$errors->first('reiss_prod')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="reiss_cat" class="col-sm-2 control-label">Reiss Categories*</label>
                    <div class="col-sm-10">
                        <select class="form-control"  name="reiss_cat" id="reiss_cat">
                            {{-- <option value="">Select</option> --}}
                            <option value="1" {{$data['reiss_cat']==1?'selected':''}} >Enable</option>
                            <option value="0" {{$data['reiss_cat']==0?'selected':''}} >Disable</option>
                        </select>
                        <span class="text-danger">{{$errors->first('reiss_cat')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="pricePer" class="col-sm-2 control-label">Reiss Product Price Upcharge In %*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="pricePer" value="{{old('pricePer')!=''?old('pricePer'):$data['pricePer']}}" id="pricePer" placeholder="Required">
                        <span class="text-danger">{{$errors->first('pricePer')}}</span>
                    </div>
                </div>

            </div>
            <input type="submit" value="Update" />
            {{Form::close()}}
    </div>
@stop
