@extends('front.layout')

@section('title')
    <title>Privacy Policy</title>
    @endsection

    @section('content')
    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
            <div class="n-wrapper">
            	
                <!-- Content Area Starts -->
                <div class="textual-Pages">
                	
                    <h2>Privacy Policy</h2>
                    
                    <h4>Privacy Policy</h4>
                    <p>
                    	This policy describes how we treat customer information.
                    </p>
                    <p>
                    	<small class="color-red">Effective Date: April 16, 2018</small><br />
                        This policy describes how we treat the personal information we collect, including when you use the site or contact us directly. By interacting with us, you consent to these practices.
                    </p>
                    
                    <h4>We use standard physical, electronic and procedural security measures.</h4>
                    <p>
                    	We use these measures to protect personal information in our control against loss or theft. We also use these measures to protect against unauthorized access. While we use standard security measures, the Internet is not 100% secure. We cannot guarantee that any of your personal information stored or sent to us will be completely safe. 	
                    </p>
                    
                    <h4>We collect information from and about you.</h4>
                    <p>
                    	This can include your name, address, telephone number or email address. We collect payment information (for example, we collect your credit card number when you buy merchandise on our site). We collect information you submit or post. We may collect demographic information like your gender or age range. We may collect information about the purchases you make. This could include the products you purchased and their prices. We collect other information. If you use our website, we may collect information about the browser you’re using. We might look at what site you came from, or what site you visit when you leave us.
                    </p>
                    
                    <h4>We collect information in different ways.</h4>
                    <p>
                    	We collect information directly from you. This can include when you create an account or make a purchase. We may also use tracking tools like browser cookies.
                    </p>
                    
                    <h4>We use information as we describe.</h4>
                    <p>
                    	We use information to improve our understanding of your interests and concerns.
                    </p>
                    <p>
                        We may use your information to make our website and products better. We use information to process your order and respond to your requests or questions. We use information for security purposes. We use information for marketing purposes; for example we may use your information to contact you about new products and special offers we think you'll find valuable. We may share information with third parties who perform services on our behalf, and to any successor to all or part of our business.
                    </p>
                    
                    <h4>We will disclose your information if we think we have to in order to comply with the law or to protect ourselves.</h4>
                    <p>
                    	This includes compliance with laws outside of the US that might apply to us. For example, we will share information to respond to a court order or subpoena. We may share it if a government agency or investigatory body requests. We might share information when we are investigating potential fraud. This might include fraud we think has happened during a sweepstakes or promotion.
                    </p>
                    
                    <h4>You may be able to access certain third party sites from our sites or apps.</h4>
                    <p>
                    	This includes social media sites. This policy does not apply to those third party sites. We strongly advise you to check the privacy policies of all third party sites you visit to find out how they are treating your personal information. We are not responsible for these third parties’ practices.
                    </p>
                    
                    <h4>We may update this Privacy Policy.</h4>
                    <p>
                    	We will notify you of any material changes as required by law. We will post an updated copy on our website. Please check this page for updates.
                    </p>
                    
                    
                    
                </div>
                <!-- Content Area Ends -->
                
            </div>
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->
      @endsection