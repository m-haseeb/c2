@extends('front.layout')

@section('title')
    <title>My Account</title>
@endsection


@section('content')
    
    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
        
            <!-- Myaccount change profile section starts -->
            <div class="myacc-cont">
            	<div class="n-wrapper">
                	<div class="myacc-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{route('index')}}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="{{route('myaccount')}}">My&nbsp;Account</a></li>
                                <li><span>/</span></li>
                                <li><a href="{{route('myaccount')}}">My&nbsp;Profile</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0)">Change&nbsp;Profile</a></li>
                            </ul>
                        </div><!-- bread navigation -->
                        
						<div class="cprofile">
                        	<div class="ma-tabs">
                                <ul>
                                    <li>
                                        <a href="{{route('myaccount')}}" class="tab-active">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="{{asset('front')}}/images/profile-ico-black.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/profile-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>My&nbsp;Profile</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="{{route('myOrder')}}" >
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="{{asset('front')}}/images/track-ico-blk.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/track-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>My&nbsp;Order</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="{{route('passwordChange')}}" >
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="{{asset('front')}}/images/password-ico-blk.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/password-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>Change&nbsp;Password</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    
                                   {{--  <li>
                                        <a href="#" onClick="return false">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="images/wish-ico-blk.png" alt="" class="nor">
                                                        <img src="images/wish-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>Wish&nbsp;List</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li> --}}
                                    
                                    <li>
                                        <a href="{{ route('logoutFront') }}" style="margin-right:0px;" >
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="{{asset('front')}}/images/logout-ico-blk.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/logout-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>Logout</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                </ul>
                            </div><!-- top tabs section -->
                            
                            <div class="mprf-cntnt">
                            	<div class="mpc-top">
                                	<h5>Change&nbsp;Profile</h5>
                                </div><!-- top heading section -->
                                
                                <div class="mpc-btm">
                                    {{ Form::open(array('name'=>'updateAccount', 'id'=>'updateAccount', 'url'=>route('updateProfile') ) ) }}{{--  --}}
                                	   <div class="cpform-row">
                                        	<div class="cpfr-left">
                                            	<div class="lrf-fields set-lrff">
                                    				<input type="text" name="regfname" value="{{old('regfname',$userInfo->first_name)}}" placeholder="First Name" >
                                                    <div class="input-error st-error">{{$errors->first('regfname')}} </div>
                                  				</div>
                                            </div>
                                            
                                            <div class="cpfr-right">
                                            	<div class="lrf-fields set-lrff">
                                    				<input type="text" name="reglname" value="{{old('reglname',$userInfo->last_name)}}" placeholder="Last Name" >
                                                    <div class="input-error st-error">{{$errors->first('reglname')}} </div>
                                  				</div>
                                            </div>
                                        </div><!-- text fields rows -->
                                        
                                        <div class="cpform-row">
                                        	<div class="lrf-fields set-lrff">
                                				<input type="text" name="regemail" value="{{old('regemail',$userInfo->email)}}" placeholder="Email" >
                                                <div class="input-error st-error">{{$errors->first('regemail')}} </div>
                              				</div>
                                        </div><!-- text fields rows -->
                                        
                                        
                                        
                                        <div class="cpform-row">
                                        	<div class="cpfr-left">
                                            	<div class="lrf-fields set-lrff">
                                    				<input type="text" name="regstrt" value="{{old('regstrt',$userInfo->street_address)}}" placeholder="Street" >
                                                    <div class="input-error st-error">{{$errors->first('regstrt')}} </div>
                                  				</div>
                                            </div>
                                            
                                            <div class="cpfr-right">
                                            	<div class="lrf-fields set-lrff">
                                    				<input type="text" name="regcity" value="{{old('regcity',$userInfo->city)}}" placeholder="City" >
                                                    <div class="input-error st-error">{{$errors->first('regcity')}} </div>
                                  				</div>
                                            </div>
                                        </div><!-- text fields rows -->
                                        
                                        <div class="cpform-row">
                                        	<div class="cpfr-left">
                                            	<div class="lrf-fields set-lrff">
                                    				<select name="regstate">
                                                        <option value=''>Select</option>
                                                        @foreach($states as $st)
                                                        <option value="{{$st->stat_name}}" {{(old('regstate',$userInfo->state) == $st->stat_name)?'selected':''}} >{{$st->stat_name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="input-error st-error">{{$errors->first('regstate')}} </div>
                                  				</div>
                                            </div>
                                            
                                            <div class="cpfr-right">
                                            	<div class="lrf-fields set-lrff">
                                    				<input type="text" name="regzip" value="{{old('regzip',$userInfo->zipcode)}}" placeholder="Zip" >
                                                    <div class="input-error st-error">{{$errors->first('regzip')}} </div>
                                  				</div>
                                            </div>
                                        </div><!-- text fields rows -->
                                        
										<div class="next-btn-wrap submit-cp">
                                            <div class="next-btn">
                                                <input type="submit" name="next" value="Submit">
                                            </div>
                                		</div><!-- submit button -->
                                    {{ Form::close() }}
                                </div><!-- bottom content section -->
                            </div><!-- bottom change profile section -->
                        </div><!-- change profile body section -->
                	</div>  
                </div>       
			</div>
            <!-- Myaccount change profile section ends -->
            
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->
    
@endsection