@extends('front.layout')

@section('title')
    <title>My Order</title>
@endsection


@section('content')
    
    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
        
            <!-- Myaccount Track your order section starts -->
            <div class="myacc-cont">
            	<div class="n-wrapper">
                	<div class="myacc-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{ route('index') }}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">My&nbsp;Account</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">My&nbsp;Order</a></li>
                            </ul>
                        </div><!-- bread navigation -->
                        
						<div class="track-ordr">
                        	<div class="ma-tabs">
                                <ul>
                                    <li>
                                        <a href="{{route('myaccount')}}">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="{{asset('front')}}/images/profile-ico-black.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/profile-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>My&nbsp;Profile</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="{{route('myOrder')}}" class="tab-active" >
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="{{asset('front')}}/images/track-ico-blk.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/track-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>My&nbsp;Order</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="{{route('passwordChange')}}" >
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="{{asset('front')}}/images/password-ico-blk.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/password-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>Change&nbsp;Password</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="{{ route('logoutFront') }}" style="margin-right:0px;" >
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="{{asset('front')}}/images/logout-ico-blk.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/logout-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>Logout</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                </ul>
                            </div><!-- top tabs section -->
                            
                            <div class="tordr-cntnt">
                            	<div class="mpc-top">
                                	<h5>My Order</h5>
                           	 	</div><!-- top heading section -->
                                
                                <div class="tordr-table">
                                	<table border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <th class="q-col1">
                                                    <h4>Order #</h4>
                                                </th>
                                                <th class="q-col2">
                                                    <h4>Name</h4>
                                                </th>
                                                <th class="q-col3">
                                                    <h4>Order Date</h4>
                                                </th>
                                                <th class="q-col4">
                                                    <h4>Amount</h4>
                                                </th>
                                                <th class="q-col5">
                                                    <h4>Status</h4>
                                                </th>
                                            </tr>
                                            @php
                                            $i= 1;
                                            @endphp
                                            @foreach($orders as $order)
                                            <tr class="{{ (($i%2) == 0)?'white':'colored' }}">
                                                <td colspan="6">
                                                    <a href="{{route('orderDetail',['id'=>$order->id])}}">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="q-col1" align="center">
                                                                        {{$order->id}}
                                                                    </td>
                                                                    <td class="q-col2" align="center">
                                                                        {{$order->user_fname.' '.$order->user_lname}}
                                                                    </td>
                                                                    <td class="q-col3" align="center">
                                                                        {{date('m-d-Y',strtotime($order->created_at))}}
                                                                    </td>
                                                                    <td class="q-col4" align="center">
                                                                        ${{number_format($order->totalPrice,2,'.','')}}
                                                                    </td>
                                                                    <td class="q-col5" align="center" style="border-right:none;">
                                                                        {{$order->status}}
                                                                    </td>
                                                                </tr>
                                                        	</tbody>
                                                        </table>
                                                    </a>
                                                </td>
                                            </tr>
                                            @php
                                            $i++;
                                            @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div><!-- table structure -->
                            </div><!-- bottom table/contnet section -->
                        </div><!-- track order body section -->
                	</div>  
                </div>       
			</div>
            <!-- Myaccount Track your order section ends -->
            
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->

@endsection