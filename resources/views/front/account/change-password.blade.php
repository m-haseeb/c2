@extends('front.layout')

@section('title')
    <title>My Account</title>
@endsection


@section('content')
    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
        
            <!-- Myaccount change password section starts -->
            <div class="myacc-cont">
            	<div class="n-wrapper">
                	<div class="myacc-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{route('index')}}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="{{route('myaccount')}}">My&nbsp;Account</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">Change&nbsp;Password</a></li>
                            </ul>
                        </div><!-- bread navigation -->
                        
						<div class="cprofile">
                        	
                            <div class="ma-tabs">
                                <ul>
                                    <li>
                                        <a href="{{route('myaccount')}}">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="{{asset('front')}}/images/profile-ico-black.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/profile-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>My&nbsp;Profile</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="{{route('myOrder')}}" >
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="{{asset('front')}}/images/track-ico-blk.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/track-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>My&nbsp;Order</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="javascript:void(0);" onClick="return false"  class="tab-active">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="{{asset('front')}}/images/password-ico-blk.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/password-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>Change&nbsp;Password</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    
                                   {{--  <li>
                                        <a href="#" onClick="return false">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="images/wish-ico-blk.png" alt="" class="nor">
                                                        <img src="images/wish-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>Wish&nbsp;List</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li> --}}
                                    
                                    <li>
                                        <a href="{{ route('logoutFront') }}" style="margin-right:0px;" >
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <img src="{{asset('front')}}/images/logout-ico-blk.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/logout-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>Logout</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="cpass-cntnt">
                            	<div class="mpc-top">
                                	<h5>Change&nbsp;Password</h5>
                                </div><!-- top heading section -->
                                
                                {{-- <h4>Manage your account information, order history, returns and more.</h4> --}}
                                
                                <div class="cpass-btm">
                                	<div class="cpbtm-lft">
                                    	{{ Form::open(array('name'=>'ChangePassword', 'id'=>'ChangePassword', 'url'=>route('passwordChangeUpdate') ) ) }}
                                        	<div class="lrf-fields">
                                    			<input type="password" name="oldpass" value="{{old('oldpass')}}" placeholder="Old Password">
                                                <div class="input-error st-error">{{$errors->first('oldpass')}} </div>
                                            </div>
                                            
                                            <div class="lrf-fields">
                                    			<input type="password" name="newpass" value="{{old('newpass')}}" placeholder="New Password">
                                                <div class="input-error st-error">{{$errors->first('newpass')}} </div>
                                            </div>
                                            
                                            <div class="lrf-fields">
                                    			<input type="password" name="cnfrmpass" value="" placeholder="Confirm Password">
                                                <div class="input-error st-error">{{$errors->first('cnfrmpass')}} </div>
                                            </div>
                                            
                                            <div class="sub-buttons">
												<input type="submit" value="Submit" name="cpass-submit">
                                        	</div>
                                        {{ Form::close() }}
                                    </div><!-- left fields section -->

                                    {{-- <div class="cpbtm-rgt">
                                        <img src="{{asset('front')}}/images/cpass-img.jpg" alt="">
                                    </div> --}}<!-- right image section -->
                                </div><!-- bottom content section -->
                            </div><!-- bottom change password section -->
                        </div><!-- change password body section -->
                	</div>  
                </div>       
			</div>
            <!-- Myaccount change password section ends -->
            
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->
    
 @endsection