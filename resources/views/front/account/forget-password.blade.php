@extends('front.layout')

@section('title')
    <title>Forgot Password</title>
@endsection


@section('content')
    
    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
        
            <!-- Forget Password section starts -->
            <div class="logreg-cont">
            	<div class="n-wrapper">
                	<div class="logreg-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{ route('index') }}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">Forgot&nbsp;Password</a></li>
                            </ul>
                        </div><!-- bread navigation -->
                        
                	 	<div class="logreg-form fpass">
                        	<div class="forget-password-wrap">
                            	<div class="fpass-inr">


                                    {{ Form::open(array('name'=>'forgot-password', 'id'=>'forgot-password', 'url'=>route('ForgotPassword') ) ) }}
                                        <h5>Forgot Password?</h5>
                                        <p>Sample Text Here Sample Text Here Sample Text Here</p>
                                        
                                        <div class="lrf-fields">
                                            <input type="text" name="email" value="{{ old('email') }}" placeholder="Email" />
                                            <div class="input-error st-error">{{$errors->first('email')}} 
                                                @if (session('status'))
                                                    {{ session('status') }}
                                                @endif
                                            </div>
                                        </div>

                                        
                                        <div class="sub-buttons logreg">
                                            <input type="submit" value="SUBMIT">
                                        </div>
                                    {{ Form::close() }}
                            	</div> 
                            </div><!-- login left section -->
                           
                        </div><!-- login and register sections -->
                	</div>
                </div>        
			</div>
            <!-- Forget Password section ends -->
            
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->


<!-- placeholder fixes script for IE 6 7 8 --> 
<script>
     (function ($) {
         $.support.placeholder = ('placeholder' in document.createElement('input'));
     })(jQuery);


     //fix for IE7 and IE8
     $(function () {
         if (!$.support.placeholder) {
             $("[placeholder]").focus(function () {
                 if ($(this).val() == $(this).attr("placeholder")) $(this).val("");
             }).blur(function () {
                 if ($(this).val() == "") $(this).val($(this).attr("placeholder"));
             }).blur();

             $("[placeholder]").parents("form").submit(function () {
                 $(this).find('[placeholder]').each(function() {
                     if ($(this).val() == $(this).attr("placeholder")) {
                         $(this).val("");
                     }
                 });
             });
         }
     });
</script>
 <!-- placeholder fixes script for IE 6 7 8 -->
 
@endsection 
 