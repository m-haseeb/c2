@extends('front.layout')

@section('title')
    <title>My Account</title>
@endsection


@section('content')
    
    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
        
            <!-- Myaccount my profile section starts -->
            <div class="myacc-cont">
            	<div class="n-wrapper">
                	<div class="myacc-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{ route('index') }}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">My&nbsp;Account</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">My&nbsp;Profile</a></li>
                            </ul>
                        </div><!-- bread navigation -->


                        
						<div class="cprofile">
                        	<div class="ma-tabs">
                            	<ul>
                                	<li>
                                    	<a href="{{route('myaccount')}}" class="tab-active">
                                        	<table cellpadding="0" cellspacing="0" border="0">
                                            	<tr>
                                                	<td>
                                                    	<img src="{{asset('front')}}/images/profile-ico-black.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/profile-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>My&nbsp;Profile</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    
                                    <li>
                                    	<a href="{{route('myOrder')}}" >
                                        	<table cellpadding="0" cellspacing="0" border="0">
                                            	<tr>
                                                	<td>
                                                    	<img src="{{asset('front')}}/images/track-ico-blk.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/track-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>My&nbsp;Order</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    
                                    <li>
                                    	<a href="{{route('passwordChange')}}" >
                                        	<table cellpadding="0" cellspacing="0" border="0">
                                            	<tr>
                                                	<td>
                                                    	<img src="{{asset('front')}}/images/password-ico-blk.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/password-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>Change&nbsp;Password</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    
                                   {{--  <li>
                                    	<a href="#" onClick="return false">
                                        	<table cellpadding="0" cellspacing="0" border="0">
                                            	<tr>
                                                	<td>
                                                    	<img src="images/wish-ico-blk.png" alt="" class="nor">
                                                        <img src="images/wish-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>Wish&nbsp;List</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li> --}}
                                    
                                    <li>
                                    	<a href="{{ route('logoutFront') }}" style="margin-right:0px;" >
                                        	<table cellpadding="0" cellspacing="0" border="0">
                                            	<tr>
                                                	<td>
                                                    	<img src="{{asset('front')}}/images/logout-ico-blk.png" alt="" class="nor">
                                                        <img src="{{asset('front')}}/images/logout-ico-wht.png" alt="" class="hov">
                                                    </td>
                                                    <td><span>Logout</span></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                </ul>
                            </div><!-- top tabs section -->
                            
                            <div class="mprf-cntnt">
                                @if(session()->has('profileMsg'))
                                <div class="flashMsg">{{session('profileMsg')}}</div>
                                @endif
                            	<div class="mpc-top">
                                	<h5>My&nbsp;Profile</h5>
                                </div><!-- top heading section -->
                                
                                <div class="mpc-btm">
                                	<div class="mpc-btm-col">
                                    	<div class="mpc-col-row">
											<h5>First Name:</h5>
											<span>{{$userInfo->first_name}}</span>
										</div>
                                        
                                        <div class="mpc-col-row">
											<h5>Last Name:</h5>
											<span>{{$userInfo->last_name}}</span>
										</div>
                                        
                                        <div class="mpc-col-row">
											<h5>Email:</h5>
											<span>{{$userInfo->email}}</span>
										</div>
                                        
                                        <!-- <div class="mpc-col-row">
											<h5>Password:</h5>
											<span>xxxxxxxxxx</span>
										</div> -->
                                        <div class="mpc-col-row">
                                            <h5>Zip:</h5>
                                            <span>{{$userInfo->zipcode}}</span>
                                        </div>
                                    </div><!-- list colums -->
                                    
                                    <div class="mpc-btm-col mpc-col-set">
                                    	<div class="mpc-col-row">
											<h5>Address:</h5>
											<span>{{$userInfo->street_address}}</span>
										</div>
                                        
                                        <div class="mpc-col-row">
											<h5>City:</h5>
											<span>{{$userInfo->city}}</span>
										</div>
                                        
                                        <div class="mpc-col-row">
											<h5>State:</h5>
											<span>{{$userInfo->state}}</span>
										</div>
                                        
                                        <!-- <div class="mpc-col-row">
											<h5>Zip:</h5>
											<span>{{$userInfo->zipcode}}</span>
										</div> -->
                                    </div><!-- list colums -->
                                    
                                    <div class="edit-img">
                                		<a href="{{route('editProfile')}}"><img src="{{ asset('front')}}/images/edit-ico.png" alt=""></a>
                                	</div><!-- edit image ico -->
                                </div><!-- bottom content section -->
                              </div><!-- bottom my profile section -->
                        </div><!-- my profile body section -->
                	</div>  
                </div>       
			</div>
            <!-- Myaccount my profile section ends -->
            
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->

@endsection