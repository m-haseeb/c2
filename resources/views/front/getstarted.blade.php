@extends('front.layout')

@section('title')
    <title>Get Started</title>
    @endsection

@section('pageScript')
  <!-- Page Description Starts -->
    <script type="text/javascript">
        var pageName = "contact";
        var subpageName = "none";
    </script>
    <!-- Page Description Ends -->
@endsection

@section('content')
<body class="blue-green subpage contact-page"> <!-- blue-green -->

    <!-- Header Starts -->
    @include("front.include.header")
    <!-- Header End -->

    <div class="cps-main-wrap contact-sections">
        
        <!-- Contact Page Starts -->

        <!-- Contact Bottom Section Starts -->
        <div class="segment1 contact-us-content space-huge">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-6 col-md-offset-3 getSpace">
                        <div class="well well-contact-form getstarted-box">
                            
                            <h3 class="font-22">
                                <strong>Get Started</strong>
                            </h3>
                            <p>
                                Fill out the form below and an Account Specialist will reach out to you to get the ball rolling.
                            </p>
                            
                            <div class="gf_browser_chrome gform_wrapper custom-contact-form_wrapper" id="gform_wrapper_1" style="">
                                <a id="gf_1" class="gform_anchor"></a>
                                {{ Form::open(array('class'=>'cps-contact-form', 'method'=>'post', 'url'=>route('submit-getstarted'))) }}
                                    <div class="gform_body">
                                       @if(session()->has('success'))
                                        <p class="input-success" style="color: #FFF; display: block;">Your message sent. Thanks for contacting.</p>
                                        @endif

                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">

                                                <br />
                                                <div class="row margin-bottom-20">
                                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                                        <label>First Name*</label>
                                                        <input id="name" type="text" name="first_name" value="{{ old('first_name') }}" placeholder="First Name" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                                        <label>Last Name*</label>
                                                        <input id="name" type="text" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name" />
                                                    </div>
                                                </div>

                                                <div class="row margin-bottom-20">
                                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                                        <label>Email*</label>
                                                        <input id="email" type="email" name="user_email" value="{{ old('user_email') }}" placeholder="Email" />
                                                    </div>
                                                </div>


                                                <div class="row margin-bottom-20">
                                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                                        <label>Phone*</label>
                                                        <input id="phone" type="tel" name="phone" value="{{ old('phone') }}" placeholder="Phone" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                       {{--  <p class="input-success" style="color: #FFF;">Your message sent. Thanks for contacting.</p>
                                        <p class="input-error" style="color: #FFF;">Sorry, something went wrong. try again later.</p> --}}
                                    </div>
                                    
                                    <div class="gform_footer top_label">
                                        <input type="submit" class="btn btn-green" value="Get Started" tabindex="32">
                                        <!--  id="gform_submit_button_1" -->
                                    </div>
                                    
                                {{ Form::close() }}
                            </div>
                        
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Contact Bottom Section Ends -->

        <!-- Contact Page Ends -->

        <!-- Subscription -->
        <div class="cps-section cps-section-padding cps-theme-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="cps-section-header text-center font-adjust" style="margin-bottom:10px;">
                            <h3 class="cps-section-title">Want to Reach New Customers?</h3>
                            <p style="padding:16px 0px;">We can have your website running in only days!</p>
                            <a class="btn btn-white" href="javascript:void(0);">Get Your Site Now</a>
                            <p style="padding-top: 3px;">or <a href="{{route('contact')}}" class="link-white">Get More Info</a></p>
                            <!-- <p class="cps-section-text">Subscribe us if you are willing to get acknowledge what we are doing. Know about the most update work and many more. We will not share your email with any third party or will not make any spam mail</p> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Subscription End -->     

    </div>

    <!-- Footer Starts -->
    @include("front.include.footer")
    <!-- Footer Ends -->

</body>
@endsection