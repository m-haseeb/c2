<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>My Cart</title>
</head>

<body>

    

<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#FFFFFF" align="center" nowrap> <!-- 935 -->
	
    
	<tr>
    	<td height="50px" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
    </tr>
    
    
    <tr>
    	<td align="center" valign="top">
        	{{-- <img src="{{asset('front')}}/images/logo.png" alt="" /> --}}
            <img src="{{ $email_data['logo'] }}" alt="" />
            
        </td>
    </tr>
    
	<tr>
    	<td height="40px" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
    </tr>
    
    <!-- Heading Section Starts -->
    <tr>
    	<td align="center" valign="middle">
        	<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                <tr>
                    <td align="center" valign="middle" style="font-family:Arial,sans-serif; font-weight:bold; font-size:24px; color:#df1c4b; line-height:1.4;">
                        Order Review
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
	<tr>
    	<td align="center" valign="middle" height="12px" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
    </tr>
    
    <!-- Border Container Starts -->
	<tr>
    	<td align="center" valign="middle" height="1px" bgcolor="#eeeeee" style="font-size:1px; background-color:#eeeeee;">&nbsp;</td>
    </tr>
    <!-- Border Container Ends -->
    
	<tr>
    	<td height="33px" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
    </tr>
    <!-- Heading Section Ends -->
    
    
    <!-- Top Row Section Starts -->
    <tr>
    	<td align="center" valign="middle">
        	<table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                	<!-- Left Section Starts -->
                    <td width="48%" align="left" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#f2462e">
                        	<tr>
                            	<td style="font-family: Arial,sans-serif; font-weight: bold; font-size: 16px; color: #FFF; line-height:1.5; padding:9px 13px 15px 13px;">
                                	Shipping Address
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding:10px 0px 0px 0px;">
                        	<tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	First Name:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['shipfname'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	Last Name:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['shiplname'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	Email:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['shipemail'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	Phone:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                    {{ $email_data['billShip']['shipphone'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	Address:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['shipaddress'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	City:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['shipcity'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	State:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['shipstate'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	Zip:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['shipzip'] }}
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- Left Section Ends -->
                    
                    <!-- Center Section Starts -->
                    <td width="4%" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
                    <!-- Center Section Ends -->
                    
                    <!-- Right Section Starts -->
                    <td width="48%" align="left" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#f2462e">
                        	<tr>
                            	<td style="font-family: Arial,sans-serif; font-weight: bold; font-size: 16px; color: #FFF; line-height:1.5; padding:9px 13px 15px 13px;">
                                	Billing Address
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding:10px 0px 0px 0px;">
                        	<tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	First Name:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['billfname'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	Last Name:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['billlname'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	Email:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['billemail'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	Phone:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['billphone'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	Address:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                    {{ $email_data['billShip']['billemail'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	City:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['billcity'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	State:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['billstate'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	Zip:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['billShip']['billzip'] }}
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- Right Section Ends -->
                    
                </tr>
            </table>
        </td>
    </tr>
    <!-- Top Row Section Ends -->
    
    
    <!-- Space Setting Starts -->
    <tr>
    	<td height="28px" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
    </tr>
    <!-- Space Setting Ends -->
    
    
    <!-- Middle Row Section Starts -->
    <tr>
    	<td align="center" valign="middle">
        	<table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                	<!-- Left Section Starts -->
                    <td width="48%" align="left" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#f2462e">
                        	<tr>
                            	<td style="font-family: Arial,sans-serif; font-weight: bold; font-size: 16px; color: #FFF; line-height:1.5; padding:9px 13px 15px 13px;">
                                	Credit Card Infomation
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding:10px 0px 0px 0px;">
                        	<tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	Card Holder Name:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['cardInfo']['cardName'] }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	Credit Card Number:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ substr($email_data['cardInfo']['cardNumber'],-4) }}
                                </td>
                            </tr>
                            <tr>
                            	<td width="42%" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:0px 10px 1px 14px;">
                                	Credit Card Expiry:
                                </td>
                            	<td width="58%" style="font-family: Arial,sans-serif; font-size: 14px; color: #000; line-height:1.5; padding:0px 14px 1px 5px;">
                                	{{ $email_data['cardInfo']['month'] }} - {{ $email_data['cardInfo']['year'] }}
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- Left Section Ends -->
                    
                    <!-- Center Section Starts -->
                    <td width="4%" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
                    <!-- Center Section Ends -->
                    
                    <!-- Right Section Starts -->
                    <td width="48%" align="left" valign="top">&nbsp;
                    	
                    </td>
                    <!-- Right Section Ends -->
                    
                </tr>
            </table>
        </td>
    </tr>
    <!-- Middle Row Section Ends -->
    
    
    <!-- Space Setting Starts -->
    <tr>
    	<td height="30px" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
    </tr>
    <!-- Space Setting Ends -->
    
    
    <!-- Below Order Table Section -->
    
    <!-- Header Section Starts -->
    <tr>
    	<td align="center" valign="top">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="border:1px solid #dddddd;">
                <tr> 
                    
                    <th width="49%" align="left" valign="middle" bgcolor="#f1f1f1" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 14px; color: #555555; line-height:1.5; padding:10px 13px 10px 13px;">
                        PRODUCT DETAILS
                    </th>
                    
                    <th width="17%" align="center" valign="middle" bgcolor="#f1f1f1" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 14px; color: #555555; line-height:1.5; padding:10px 13px 10px 13px;">
                        UNIT PRICE
                    </th>
                    
                    <th width="12%" align="center" valign="middle" bgcolor="#f1f1f1" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 14px; color: #555555; line-height:1.5; padding:10px 13px 10px 13px;">
                        QTY
                    </th>
                    
                    <th width="22%" align="center" valign="middle" bgcolor="#f1f1f1" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 14px; color: #555555; line-height:1.5; padding:10px 13px 10px 13px;">
                        TOTAL PRICE
                    </th>
                    
                </tr>
            </table>
        </td>
    </tr>
    <!-- Header Section Ends -->
    
    @php
    $totalPrice = 0;
    @endphp

    @for( $i = 0; $i < count($email_data['listData']); $i++ )
        <!-- Repeatable Row Starts -->
        <tr>
        	<td align="center" valign="top">
                <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#f9f9f9" style="border:1px solid #dddddd;border-top:none;">
                    <tr> 
                        
                        <td width="15%" align="left" valign="middle" style="font-family: Arial,sans-serif; font-size: 13px; color: #555555; line-height:1.5; padding:10px 13px 10px 13px;">
                            {{-- <img src="{{ $email_data['listData'][$i]['product_image'][$i] }}" alt="" width="90" /> --}}
                            @if ( isset($email_data['listData'][$i]['prodcut_location_type']) && $email_data['listData'][$i]['prodcut_location_type'] != '' )
                                <img src="{{asset('products_images/thumbs')}}/{{$email_data['listData'][$i]['product_image']}}" alt="" width="90" >
                            @else
                                <img src="https://reisshardware.com/product_images/{{$email_data['listData'][$i]['sku']}}.JPG" alt="" width="90" >
                            @endif

                            {{-- <img src="https://reisshardware.com/product_images/{{$email_data['listData'][$i]['sku']}}.JPG" alt="" width="90" /> --}}
                        </td>
                        
                        <td width="34%" align="left" valign="middle" style="font-family: Arial,sans-serif; font-size: 13px; color: #555555; line-height:1.5; padding:10px 13px 10px 13px;">
                            {{ $email_data['listData'][$i]['name'] }}
                        </td>
                        
                        <td width="17%" align="center" valign="middle" style="font-family: Arial,sans-serif; font-size: 13px; color: #555555; line-height:1.5; padding:10px 13px 10px 13px;">
                            {{ number_format($email_data['listData'][$i]['price'],2,'.','') }}
                        </td>
                        
                        <td width="12%" align="center" valign="middle" style="font-family: Arial,sans-serif; font-size: 13px; color: #555555; line-height:1.5; padding:10px 13px 10px 13px;">
                            {{ $email_data['listData'][$i]['qty'] }}
                        </td>
                        @php
                        $totalPrice += ($email_data['listData'][$i]['qty'] * $email_data['listData'][$i]['price'] );
                        @endphp
                        
                        <td width="22%" align="center" valign="middle" style="font-family: Arial,sans-serif; font-size: 13px; color: #555555; line-height:1.5; padding:10px 13px 10px 13px;">
                            {{ $email_data['listData'][$i]['qty'] * $email_data['listData'][$i]['price'] }}
                        </td>
                        
                    </tr>
                </table>
            </td>
        </tr>
    <!-- Repeatable Row Ends -->

    @endfor
    


    <!-- Space Setting Starts -->
    <tr>
    	<td height="23px" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
    </tr>
    <!-- Space Setting Ends -->
    
    
    <!-- Footer Grand Total Section Starts -->
    <tr>
    	<td align="center" valign="top">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr> 
                    
                    <th width="61%" align="left" valign="middle">&nbsp;
                    	
                    </th>
                    
                    <th width="22%" align="left" valign="middle" bgcolor="#f2f2f2" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:10px 13px 10px 13px; border:2px solid #FFF;">
                        GRAND TOTAL
                    </th>
                    
                    <th width="17%" align="center" valign="middle" bgcolor="#f2f2f2" style="font-family: Arial,sans-serif; font-weight: bold; font-size: 15px; color: #000; line-height:1.5; padding:10px 13px 10px 13px; border:2px solid #FFF;">
                        {{ number_format($totalPrice,2,'.','') }}
                    </th>
                    
                </tr>
            </table>
        </td>
    </tr>
    <!-- Footer Grand Total Section Starts -->
    
    
    
</table>

</body>
</html>
