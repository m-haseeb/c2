<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>My Cart</title>
</head>

<body>

<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#FFFFFF" align="center" nowrap> <!-- 935 -->
	
    
	<tr>
    	<td height="50px" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
    </tr>
    
    
    <tr>
    	<td align="center" valign="top">
        	<img src="{{asset('front')}}/images/logo.png" alt="" />
        </td>
    </tr>
    
	<tr>
    	<td height="40px" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
    </tr>
    
    <!-- Heading Section Starts -->
    <tr>
    	<td align="center" valign="middle">
        	<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                <tr>
                	<!--
                    <td width="30%" align="right" valign="middle" style="padding:0px 20px 0px 0px;">
                        <img src="../images/topleft-bdr.jpg" alt="" />
                    </td>
                    -->
                    <td align="center" valign="middle" style="font-family:Arial,sans-serif; font-weight:bold; font-size:22px; color:#00909e; line-height:1.4;">
                        MY LIST
                    </td>
                    <!--
                    <td width="30%" align="left" valign="middle" style="padding:0px 0px 0px 20px;">
                        <img src="../images/topright-bdr.jpg" alt="" />
                    </td>
                    -->
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td height="30px" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
    </tr>
    <!-- Heading Section Ends -->
    
    
    
    <!-- Header Section Starts -->
    <tr>
    	<td align="center" valign="top">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr> 
                    
                    <th width="17.66%" align="left" valign="middle" bgcolor="#f1f1f1" style="border-right:2px solid #FFF; font-family: Arial,sans-serif; font-weight: bold; font-size: 13px; color: #555555; line-height:1.5; padding:4px 14px 5px 5px;">
                        Image
                    </th>
                    
                    <th width="50%" align="left" valign="middle" bgcolor="#f1f1f1" style="border-right:2px solid #FFF; font-family: Arial,sans-serif; font-weight: bold; font-size: 13px; color: #555555; line-height:1.5; padding:4px 14px 5px 5px;">
                        Product Details
                    </th>
                    
                    <th width="17.17%" align="center" valign="middle" bgcolor="#f1f1f1" style="border-right:2px solid #FFF; font-family: Arial,sans-serif; font-weight: bold; font-size: 13px; color: #555555; line-height:1.5; padding:4px 14px 5px 5px;">
                        Unit Price
                    </th>
                    
                    <th width="15.17%" align="center" valign="middle" bgcolor="#f1f1f1" style="border-right:2px solid #FFF; font-family: Arial,sans-serif; font-weight: bold; font-size: 13px; color: #555555; line-height:1.5; padding:4px 14px 5px 5px;">
                        Qty
                    </th>
                    
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td height="20px" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
    </tr>
    <!-- Header Section Ends -->
    
    @if(count($lData) > 0)
        @foreach($lData as $list)
    <!-- Repeatable Row Starts -->
    <tr>
    	<td align="center" valign="top">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr> 
                    
                    <td width="17.66%" align="left" valign="middle" style="border-right:2px solid #FFF; font-family: Arial,sans-serif; font-size: 13px; color: #555555; line-height:1.5; padding:4px 14px 5px 5px;">
                        <img src="https://reisshardware.com/product_images/{{$list->sku}}.JPG" alt="" width="127" />
                    </td>
                    
                    <td width="50%" align="left" valign="middle" style="border-right:2px solid #FFF; font-family: Arial,sans-serif; font-size: 13px; color: #555555; line-height:1.5; padding:4px 14px 5px 5px;">
                        {{$list->name}}
                    </td>
                    
                    <td width="17.17%" align="center" valign="middle" style="border-right:2px solid #FFF; font-family: Arial,sans-serif; font-size: 13px; color: #555555; line-height:1.5; padding:4px 14px 5px 5px;">
                        @if($list->is_price && $siteData['show_price'])
                            ${{$list->price}}
                        @else
                            N/A
                        @endif
                    </td>
                    
                    <td width="15.17%" align="center" valign="middle" style="border-right:2px solid #FFF; font-family: Arial,sans-serif; font-size: 13px; color: #555555; line-height:1.5; padding:4px 14px 5px 5px;">
                        {{$list->qty}}
                    </td>
                    
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td height="2px" style="font-size:1px;"><img src="{{asset('front')}}/images/trans-pxl.png" alt="" style="width:100%; height:100%;" /></td>
    </tr>
    <!-- Repeatable Row Ends -->
    
        @endforeach
    @endif
    
</table>

</body>
</html>
