@if(auth('front')->check())
    @php
    UserCookieHelperFunction(auth('front')->user()->id);
    @endphp
 @else
    @php
    UserCookieHelperFunction();
    @endphp
@endif

<script type="text/javascript">

    $(document).ready(function(){

        $('.loader-main').fadeOut();
    });

</script>

<div class="loader-main">
    <img class="loading-gif-2" src="{{asset('front')}}/images/loader.gif" alt="preloader">
</div>

<header id="header" class="n-header">
    @php $settings = getSetting(); @endphp
	
    <div class="header-top">
        <div class="n-wrapper"> <!-- General Class -->
            <div class="n-container"><!-- General Class -->
            	<div class="tp-col n_left_float">
                	<span>Need a solution to fit your needs?<span class="h-break">Give us a call {{$settings['site_phone']}}</span></span>
                </div>
                <div class="tp-col n_right_float">
                	<ul>
                    	<li>
                        	<a href="javascript:void(0);" id="srch">Search</a>
                        </li>
                        {{-- 
                    	<li>
                        	<a href="09-login-register.php">Log In</a> / <a href="09-login-register.php">Register</a>
                        </li>
                        <li>
                        	<a href="10-1-1-my-account.php">My Account</a>
                        </li> --}}
                        @if(auth('front')->check())
                        <li>
                            <span>{{auth('front')->user()->first_name.' '.auth('front')->user()->last_name}}</span>
                            | 
                            <a href="{{route('myaccount')}}">My Account</a>

                            @php
                            UserCookieHelperFunction(auth('front')->user()->id);
                            @endphp
                        </li>
                        @else
                        <li>
                            @php
                            UserCookieHelperFunction();
                            @endphp
                            <a href="{{route('loginRegister')}}">Log In</a> / <a href="{{route('loginRegister')}}">Register</a>
                        </li>
                        @endif
                        <li class="last">
                        	<a href="{{route('displayList')}}">
                                <div class="cart-ico"></div>
                                
                                <div class="cart-txt">My Cart ({{ count( countListCart() ) }})</div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <!-- search starts -->
    <div class="search">
        <div class="srch-wrap">
            <div class="srch-inr">
                <div class="srch-lft">
                    <input type="text" id="sitesearch" name="search" value="" placeholder="Search"/>
                    <div class="dyn-list2" style="display:none;">
                        <div class="dyn-scroll">
                            <ul id="prod_list">
                                <li>sample text here</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="srch-rgt">
                    <img src="{{asset('front')}}/images/srch-ico.png" alt="" class="st-ico" />
                </div>
            </div>
        </div>
    </div>
    <!-- search ends -->
    
    <div class="header-bottom">
    	<div class="n-wrapper">
        	<div class="n-container">
                <div class="n-logo n_left_float">
                    <a href="{{route('index')}}">
                        <img src="{{asset('site_logo/'.$settings['logo'])}}" alt="" />
                    </a>
                </div>
                <nav class="naver">
                    <ul class="top-menu n_right_float uty-top-menu-1">
                        <li>
                            <a href="{{route('index')}}" class="{{ $active == 'home' ? 'current' : '' }}">Home</a>
                        </li>
                        <li>
                            <a href="{{route('about')}}" class="{{ $active == 'about' ? 'current' : '' }}" >About</a>
                        </li>
                        <li>
                            @php
                                $categories = getLevelOneCategories();
                                // $categoriesName = getLevelOneCategoriesName();
                                // echo '<pre>';
                                // print_r($categoriesName);
                                // exit;

                                $divStart = 0;
                                $classNo = 1;
                            @endphp
                            <!-- product drop down starts here -->
                            <div id="prodrop-wrap" class="droppedMenu">
                            	<a href="javascript:void(0);" class="{{ $active == 'products' ? 'current' : '' }}" onclick="return false;">Products</a>
                                @if( count($categories) > 0 )
                             	<div id="pd-outr" tabindex="0">
    								<div class="pd-inr">
                                        <div class="pd-cntnt">
                                            <div class="pdc-lft" style="width: 100% !important;">
                                                <div class="btm-tmenu">
                                                    @for( $i = 0; $i < count($categories); $i++ )
                                                            @if( $divStart == 0 )
                                                            <div class="tmenu-col1 col{{$classNo}}">
                                                             @php $divStart = 1; @endphp
                                                        @endif
                                                                <!-- links section -->
                                                                <div class="link-box">
                                                                    <div class="link-upbox">
                                                                        <img src="{{asset('front')}}/images/dd-arrow-solid.png">
                                                                        <a href="{{route('reiss.products', ['category_slug' => $categories[$i]->slug])}}"> {{$categories[$i]->name}}</a>
                                                                        <!-- <a href="{{route('products', ['cat_url' => $categories[$i]->slug])}}">{{$categories[$i]->name}}</a> -->
                                                                    </div>
                                                                </div>
                                                                <!-- links section -->

                                                        @if( ( ($i + 1) % ( round(count($categories)/3) ) ) == 0 )
                                                            @php $divStart = 0; $classNo++; @endphp
                                                            </div>
                                                        @endif
                                                    @endfor
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end of-dropdown class -->
                                @endif
                            </div>
          	 				<!-- product drop down ends here -->
                        </li>
                        
                        <li>
                        	<!-- services dropdown starts -->
                        	<div id="prodrop-wrap" class="droppedMenu">
                                <a href="{{route('services')}}" class="{{ $active == 'service' ? 'current' : '' }}" {{-- onclick="return false;" --}}>Services</a>
                                
                                {{-- <!-- Drop Down Starts -->
                               <div class="dropWrap" id="serviceDrop" style="display:none;">
                                    <div class="dropInner">
                                        <ul>
                                            <li><a href="03-1-services-wide-format.php">Wide Format Service</a></li>
                                            <li><a href="03-2-services-Office-equipment.php">Office Equipment Service</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Drop Down Ends --> --}}
                            </div>
                            <!-- services dropdown ends -->
                        </li>
                        
                        {{-- <li>
                            <a href="{{route('brands')}}" class="{{ $active == 'brands' ? 'current' : '' }}">Top Brands</a>
                        </li> --}}
                        
                        <li>
                            <a href="{{route('catalogs')}}" class="{{ $active == 'pormotion' ? 'current' : '' }}">Catalogs</a>
                        </li>

                        <li class="last">
                            <a href="{{route('contact')}}" class="{{ $active == 'contact' ? 'current' : '' }}">Contact Us</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>

<script type="text/javascript">

    $(document).ready(function(){

        $(document).on('click', '#srch', function(){
            $('#sitesearch').val('');
            $('#prod_list').html('');
            $('.dyn-list2').hide(); 
        });

        $(document).on('keyup', '#sitesearch', function(){
            
            var searchTearm = $(this).val();

            searchTearm = searchTearm.trim();
            
            if ( searchTearm.length >= 2 )
            {
                $.ajax({
                    url: '{{route('site-search')}}',
                    type: 'post',
                    data: {searchWord: searchTearm, _token: '{{csrf_token()}}'},
                })
                .done(function(result) {
                    $('#prod_list').html('');

                    if ( result != '' )
                    {
                        $('#prod_list').html(result);
                        $('.dyn-list2').show();
                    }
                    else
                    {
                        $('#prod_list').html('<li>No item found.</li>');
                        $('.dyn-list2').show();
                    }
                })
                .fail(function() {
                    $('#prod_list').html('');
                    console.log("error");
                });
            }
            else
            {
                $('#prod_list').html('');
                $('.dyn-list2').hide(); 
            }
        });
    });

</script>