<!--<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">-->

<link rel="shortcut icon" href="{{asset('front')}}/images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="{{asset('front')}}/images/favicon.png" type="image/x-icon">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300' rel='stylesheet' type='text/css' />

<link rel="stylesheet" type="text/css" href="{{asset('front')}}/css/slick.css"/>
<link rel="stylesheet" type="text/css" href="{{asset('front')}}/css/slick-theme.css"/>


<script src="{{asset('front')}}/js/jquery-1.10.1.min.js" type="text/javascript"></script>

<!-- <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script> -->

<script type="text/javascript" src="{{asset('front')}}/js/slick.js"></script>
<script type="text/javascript" src="{{asset('front')}}/js/slick.min.js"></script>

<script type="text/javascript">
    $(document).on('ready', function() {
      $(".regular").slick({
        dots: false,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1
      });

    });
</script>

<script src="{{asset('/front')}}/js/jquery.fs.naver.js"></script>

<script language="javascript" type="text/javascript">
<!--
function main_cont(){
	
	// For Browser Height
	 var myWidth;
	 var myHeight;
	 if( typeof( window.innerWidth ) == 'number' ) { 
	 //Non-IE 
	 myWidth = window.innerWidth;
	 myHeight = window.innerHeight;
	 } else if( document.documentElement &&
	 ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) { 
	 //IE 6+ in 'standards compliant mode' 
	 myWidth = document.documentElement.clientWidth; 
	 myHeight = document.documentElement.clientHeight; 
	 } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) { 
	 //IE 4 compatible 
	 myWidth = document.body.clientWidth; 
	 myHeight = document.body.clientHeight; 
	 }
	 
	 if(document.getElementById('header')){
	 	header = document.getElementById('header').offsetHeight;
	 }
	 
	 if(document.getElementById('footer')){
	 	footer = document.getElementById('footer').offsetHeight;
	 }
	 
	 if(document.getElementById('body-minheight')){
		 customHeight = myHeight - (header+footer);
		 document.getElementById('body-minheight').style.minHeight = customHeight + 'px';
	 }
	 
	 if(document.getElementById('imgTag')){
		imgSize = document.getElementById('imgTag').offsetHeight;
		document.getElementById('imgDiv').style.height=imgSize + 'px';
	}
	
	// End of Function	
}
-->

<!-- bodyHeight -->
$(window).ready(function() {
	
	// Product menu hides on click elsewhere starts
	$('#body-minheight').click(function(){
		if($("#pd-outr").length){
			$("#pd-outr").css("display", "none");
		}
	});
	$('.header-top').click(function(){
		if($("#pd-outr").length){
			$("#pd-outr").css("display", "none");
		}
	});
	// Product menu hides on click elsewhere ends
	
	
	// General menu hides on click elsewhere starts
	$('#body-minheight').click(function(){
		if($(".dropWrap").length){
			$(".dropWrap").css("display", "none");
		}
	});
	$('.header-top').click(function(){
		if($(".dropWrap").length){
			$(".dropWrap").css("display", "none");
		}
	});
	// General menu hides on click elsewhere ends
	
	// Search hides on click elsewhere starts
	$('#body-minheight').click(function(){
		if($(".search").length){
			$(".search").css("display", "none");
		}
	});
	// Search hides on click elsewhere ends
});

$(document).ready(function() {
 
	// Hides element directly after an accordion item with the class of expand.
	$('.top-menu li .droppedMenu a').next().hide();
	
	$('.top-menu li .droppedMenu a').click(function() {
		if($(this).hasClass('ddroped')) {
			
			//if open, close item
			$(this).next().fadeOut();
			$(this).removeClass('ddroped');
			$(this).removeClass('active');
		} else {
		// Closes any accordion items that may already be open.
			$(".ddroped").each(function () {
				$(this).next().fadeOut();
				$(this).removeClass('ddroped');
				$(this).removeClass('active');
			});  
			// Shows the element directly after the element with class .expand that was clicked on. 
			
			$(this).next().fadeIn(function() {
				var el = $(this);
				//scrollToDiv(el);
			});
			$(this).addClass('ddroped');
			$(this).addClass('active');
		}
		//return false;
	});
	// For Drop Down Menu About and Product Ends
	
});

</script>

<!-- popup code starts -->
<script type="text/javascript">
// Naver Menu Code Starts 
	$(document).ready(function() {
		$(".naver a").on("click", function(e) {
			//e.preventDefault();
			//e.stopPropagation();
		});
		$(".naver").naver({
			animated: true
		});
	});
	// Naver Menu Code Ends 	
$(document).ready(function() {
$('a.pop-window2').click(function() {
    
            //Getting the variable's value from a link 
    var loginBox = $(this).attr('href');

    //Fade in the Popup
    $(loginBox).fadeIn(300);
    
    //Set the center alignment padding + border see css style
    var popMargTop = ($(loginBox).height() + 24) / 2; 
    var popMargLeft = ($(loginBox).width() + 24) / 2; 
    
    $(loginBox).css({ 
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
    
    // Add the mask to body
    $('body').append('<div id="mask"></div>');
    $('#mask2').fadeIn(300);
    
    return false;
});

// When clicking on the button close or the mask layer the popup closed
$(document).on('click','a.close, #mask2', function() { 
  $('#mask2 , .alrt-popup2').fadeOut(300 , function() {
    $('#mask2').remove();  
}); 
return false;
});


$('a.pop-window').click(function() {
    
            //Getting the variable's value from a link 
    var loginBox = $(this).attr('href');

    //Fade in the Popup
    $(loginBox).fadeIn(300);
    
    //Set the center alignment padding + border see css style
    var popMargTop = ($(loginBox).height() + 24) / 2; 
    var popMargLeft = ($(loginBox).width() + 24) / 2; 
    
    $(loginBox).css({ 
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
    
    // Add the mask to body
    $('body').append('<div id="mask"></div>');
    $('#mask').fadeIn(300);
    
    return false;
});

// When clicking on the button close or the mask layer the popup closed
$(document).on('click','a.close, #mask', function() { 
  $('#mask , .alrt-popup').fadeOut(300 , function() {
    $('#mask').remove();  
}); 
return false;
});
});

</script>
<!-- popup code ends -->

<!-- search hide and show on click -->
<script type="text/javascript">
$(document).on('click',"#srch",function(){
    $(".search").toggle();
});

</script>