@php
$siteData = getSetting();
@endphp

<header id="header" class="n-header headr2">
    <div class="header-bottom">
    	<div class="n-wrapper">
        	<div class="n-container">
                <div class="n-logo n_left_float">
                    <a href="{{route('index')}}">
                        <img src="{{asset('site_logo/'.$siteData['logo'])}}" alt="" />
                    </a>
                </div>
                <div class="hdr-cntct">
                	<h4>Contact Us:&nbsp;<span>{{$siteData['site_phone']}}</span></h4>
                </div>
            </div>
        </div>
    </div>
</header>