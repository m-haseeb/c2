<!-- css fixes for I.E 7 -->
<!--[if IE 7]>
<link href="{{asset('front')}}/css/main_ie7_fixes.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!-- css fixes for I.E 7 Ends -->

<!-- css fixes for I.E 8 -->
<!--[if IE 8]>
<link href="{{asset('front')}}/css/main_ie8_fixes.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!-- css fixes for I.E 8 Ends -->

<!-- css fixes for I.E 9 -->
<!--[if IE 9]>
<link href="{{asset('front')}}/css/main_ie9_fixes.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!-- css fixes for I.E 9 Ends -->

<link rel="stylesheet" href="{{asset('/front')}}/css/jquery.fs.naver.css">
