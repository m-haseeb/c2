@php
$siteData = getSetting();
@endphp
<footer id="footer" class="n-footer">
	<div class="f-top">
        <div class="n-wrapper">
            <div class="n-container">
                <div class="footer-left-col">
                	<a href="{{route('index')}}">
                    	<img src="{{asset('site_logo/'.$siteData['logo'])}}" alt="" />
                    </a>
                    <p>
                    	{{ $siteData['footerDesc'] }}
                    </p>
                </div>
                
                <div class="f-right-cols">
                	<div class="fR-col1 ftr-lsting">
                    	<ul>
                        	<li>
                            	<h3>Links</h3>
                            </li>
                        	<li>
                            	<a href="{{route('index')}}" class="{{ $active == 'home' ? 'active' : '' }}">Home</a>
                            </li>
                        	<li>
                            	<a href="{{route('about')}}" class="{{ $active == 'about' ? 'active' : '' }}">About Us</a>
                            </li>
                            <li>
                            	<a href="{{route('services')}}" class="{{ $active == 'service' ? 'active' : '' }}">Services</a>
                            </li>
                            {{-- <li>
                                <a href="{{route('brands')}}">Top Brands</a>
                            </li> --}}
                        	<li>
                                <a href="{{route('catalogs')}}" class="{{ $active == 'pormotion' ? 'active' : '' }}">Catalogs</a>
                            </li>
                            <li>
                            	<a href="{{route('contact')}}" class="{{ $active == 'contact' ? 'active' : '' }}">Contact Us</a>
                            </li>
                            <li>
                            	<a href="{{route('returns')}}" class="{{ $active == 'returns' ? 'active' : '' }}">Returns, Freight, <br />and Credit</a>
                            </li>
                        </ul>
                    </div>
                    
                    <div class="fR-col2 ftr-lsting">
                    	<ul>
                        	<li>
                            	<h3>Social Media</h3>
                            </li>
                        	{{-- <li class="f-media">
                            	<a href="#" onClick="return false">
                                	<span><i class="fa fa-envelope"></i></span>
                                    Email Sign-Up
                                </a>
                                <!-- Pop Out Starts -->
                                <div class="rgt-popup">
                                    <div class="rgtpop-inr">
                                        <div class="pop-arrow"></div>
                                        
                                        <form method="post" name="abc">
                                            <div class="lrf-fields f-pop">
                                                <input type="text" name="email" value="" placeholder="Email">
                                            </div>
                                            <div class="submit-error"></div>
                                            <input class="fpop-sub" type="submit" value="Submit">
                                            
                                            <!--<p>Thank you!<br />Your information has been submitted.</p>-->
                                        </form>
                                    </div>
                                </div>
                                <!-- Pop Out Ends -->
                            </li> --}}
                        	<li class="f-media">
                            	<a href="{{$siteData['facebook']}}" target="_blank">
                                	<span><i class="fa fa-facebook"></i></span>
                                    Facebook
                                </a>
                            </li>
                        	<li class="f-media">
                            	<a href="{{$siteData['twitter']}}" target="_blank">
                                	<span><i class="fa fa-twitter"></i></span>
                                    Twitter
                                </a>
                            </li>
                        	<li class="f-media">
                            	<a href="{{$siteData['linkin']}}" target="_blank">
                                	<span><i class="fa fa-linkedin"></i></span>
                                    LinkedIn
                                </a>
                            </li>
                        	<li class="f-media"> 
                            	<a href="{{$siteData['youtube']}}" target="_blank">
                                	<span><i class="fa fa-youtube"></i></span>
                                    YouTube
                                </a>
                            </li>
                        </ul>
                    </div>
                    
                    <div class="fR-col3 ftr-lsting">
                    	<ul>
                        	<li>
                            	<h3>Contact Us</h3>
                            </li>
                        	<li class="f-media">
                            	<a href="javascript:void(0);" style="cursor:default;">
                                	<span><i class="fa fa-map-marker"></i></span>
                                    {{$siteData['site_address']}}
                                </a>
                            </li>
                        	<li class="f-media">
                            	<a name="phone" style="cursor:default;">
                                	<span><i class="fa fa-phone"></i></span>
                                    {{$siteData['site_phone']}}
                                </a>
                            </li>
                            <!--
                        	<li class="f-media">
                            	<a name="fax" style="cursor:default;">
                                	<span><i class="fa fa-fax"></i></span>
                                    {{$siteData['site_fax']}}
                                </a>
                            </li>
                            -->
                        	<li class="f-media">
                            	<a href="mailto:{{$siteData['site_email']}}">
                                	<span><i class="fa fa-envelope-o"></i></span>
                                    {{$siteData['site_email']}}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="footer-bottom">
    	<div class="n-wrapper">
        	<div class="n-container">
            	<img src="{{asset('front')}}/images/pay-cards.png" alt="" />
            	<p>
                <span>
                    © {{@date('Y')}} Hardware Store
                </span>
                <span><span class="remove">&nbsp;&nbsp;|&nbsp;&nbsp; </span><a href="{{route('terms')}}" class="{{ $active == 'terms' ? 'active' : '' }}">Terms and Conditions</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="{{route('privacy')}}" class="{{ $active == 'privacy' ? 'active' : '' }}">Privacy Policy</a> <span class="remove">&nbsp;&nbsp;|&nbsp;&nbsp;</span>
                </span>  
                <span>Website by <a href="https://xcelhub.com" target="_blank">XcelHub</a></p></span>
            </div>
        </div>
    </div>
</footer>

<!--<i class="icon-map-marker"></i>

<i class="fa fa-arrow-right"></i>

<i class="fa fa-twitter"></i>

<i class="fa fa-youtube"></i>

<i class="fa fa-linkedin"></i>

<i class="fa fa-facebook"></i>

<i class="fa fa-envelope-o"></i>

<i class="fa fa-phone"></i>

<i class="fa fa-envelope"></i>

<i class="fa fa-fax"></i>-->


