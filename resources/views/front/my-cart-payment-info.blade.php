@extends('front.layout')

@section('title')
    <title>Cart</title>
    @endsection

    @section('content')

    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
        
            <!-- MyCart Payment information section starts -->
            <div class="mycart-cont">
            	<div class="n-wrapper">
                	<div class="mycart-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{route('index')}}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="{{route('displayList')}}">My&nbsp;Cart</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">Payment&nbsp;Information</a></li>
                            </ul>
                        </div><!-- bread navigation -->
                        
                        <div class="pinfo-wrap">
                        	<div class="pinfo-head">
                            	<h5>Payment&nbsp;Information</h5>
                            </div><!-- heading top -->
                            
                            <div class="pinfo-form">
                            	<div class="pinfo-fwrap">
                                	<div class="pinfo-finr">
                                    	{{ Form::open(array('name'=>'paymentInfo', 'url'=>route('paymentInfoGet') ) ) }}

                                        @if(session()->has('msg'))
                                        <div class="error"> {!! session()->get('msg') !!} </div>
                                        @endif
                                        	<div class="piflds-row">
                                            	<div class="pifldr-lft">
                                                	<div class="lrf-fields set-lrff">
                                            			<input type="text" name="cardName" maxlength="25" value="{{old('cardName')}}" placeholder="Card Holder Name" >
                                        			</div>
                                                    <div class="error">{{$errors->first('cardName')}}</div>
                                                </div>
                                                <div class="pifldr-rght">
                                                	<div class="lrf-fields set-lrff">
                                            			<input type="text" maxlength="16" name="cardNumber" value="{{old('cardNumber')}}" autocomplete="off" placeholder="Credit Card Number" >
                                        			</div>
                                                    <div class="error">{{$errors->first('cardNumber')}}</div>
                                                </div>
                                            </div><!-- fields row -->
                                            
                                            <div class="piflds-row">
                                            	<div class="pifldr-lft">
                                                	<div class="lrf-fields set-lrff">
                                            			<input type="password" name="CVCNumber" maxlength="4" value="{{old('CVCNumber')}}" autocomplete="off" placeholder="CVC Number" >
                                        			</div>
                                                    <div class="error">{{$errors->first('CVCNumber')}}</div>
                                                </div>
                                                
                                                <div class="pifldr-rght">
                                                	<div class="lrf-fields set-lrff">
	                                                	<h5>Expiration Date</h5>
                                                        <div class="mcpi-expd">
	                                                    <select name="month" class="date-set">
	                                                        <option value=''>Month</option>
	                                                        @for($i=1; $i<=12; $i++)
																                            @if($i < 10)
                                                        		  <option value="{{'0'.$i}}" @if(old('month')==$i) selected @endif >{{$i}}</option>
                                                            @else
																                              <option value="{{$i}}" @if(old('month')==$i) selected @endif > {{$i}}</option>
                                                            @endif
                                                        
                                                    		@endfor 
	                                                    </select>
                                                        <div class="error">{{$errors->first('month')}}</div>
                                                        </div>
                                                        <div class="mcpi-expd mcpi-expd-rgt">
	                                                    <select name="year" class="date-set date-set-rgt" style="float:right;">
	                                                    	<option value=''>Year</option>
		                                                    @for($n=date("Y");$n<=(date("Y")+10);$n++)
		                                                		<option value="{{$n}}" @if(old('year')==$n) selected @endif >{{$n}}</option>
	    	                                          		@endfor
	                                                    </select>
                                                        <div class="error">{{$errors->first('year')}}</div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div><!-- fields row -->
                                            
                                            
                                            
                                            <div class="next-btn-wrap">
                                            	<div class="next-btn">
                                                	<input type="submit" name="next" value="Next">
                                                </div>
                                            </div><!-- next button -->
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div><!-- form section -->
                        </div><!-- main content section -->
                	</div>  
                </div>       
			</div>
            <!-- MyCart Payment information section ends -->
            
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->


       <!-- placeholder fixes script for IE 6 7 8 --> 
<script>
     (function ($) {
         $.support.placeholder = ('placeholder' in document.createElement('input'));
     })(jQuery);


     //fix for IE7 and IE8
     $(function () {
         if (!$.support.placeholder) {
             $("[placeholder]").focus(function () {
                 if ($(this).val() == $(this).attr("placeholder")) $(this).val("");
             }).blur(function () {
                 if ($(this).val() == "") $(this).val($(this).attr("placeholder"));
             }).blur();

             $("[placeholder]").parents("form").submit(function () {
                 $(this).find('[placeholder]').each(function() {
                     if ($(this).val() == $(this).attr("placeholder")) {
                         $(this).val("");
                     }
                 });
             });
         }
     });
 </script>
 <!-- placeholder fixes script for IE 6 7 8 -->




  @endsection