@extends('front.layout')

@section('title')
    <title>Features</title>
    @endsection
    
@section('pageScript')
    <!-- Page Description Starts -->
    <script type="text/javascript">
        var pageName = "features";
        var subpageName = "none";
    </script>
    <!-- Page Description Ends -->
@endsection

@section('content')
<body class="blue-green subpage"> <!-- blue-green -->

    <!-- Header Starts -->
    @include("front.include.header")
    <!-- Header End -->

    <!-- Page Header -->
    <div class="page-header style-9">
        <div class="container">
            <h2 class="page-title">Features</h2>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);">&nbsp;</a></li>
                <!-- <li class="active">Features</li> -->
            </ol>
        </div>
    </div>
    <!-- Page Header End -->

    <div class="cps-main-wrap features-wrap">

        <!-- ADDons -->
        <div class="cps-section feature-sec-pad2">
            <div class="container">

                <div class="row style4-nset no-margin">

                    <!-- Repeatable Section Starts -->
                    <div class="faqs nbdr-top">

                        <div class="row section-title">
                            <div class="col-sm-12 col-xs-12 no-padding">
                                <h4 class="cps-cta-title">
                                    Included with Simple Plan
                                </h4>
                            </div>
                        </div>
                      	
                        <!-- New Added Structure -->
                        <div class="cps-services style-4 clr-mbtm">
                        	
                        	<!-- Row Starts -->
                            <div class="row">
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon websiteaddress"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Website Address</h4>
                                            <p class="cps-service-text">
                                            	We’ll set up a “.com” web address that matches your brand.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon webaddresshosting"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Hosting Services</h4>
                                            <p class="cps-service-text">
                                            	Your site needs to exist on a server before anyone can find it, but don’t worry. We’ll handle all the technical details at no extra cost.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon mobilefriendly"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Mobile Friendly</h4>
                                            <p class="cps-service-text">
                                            	Your website will look good on any device - computers, tablets, and smartphones.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                            </div>
                            <!-- Row Ends -->

                        	<!-- Row Starts -->
                            <div class="row">
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon conatctform"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Contact Forms</h4>
                                            <p class="cps-service-text">
                                            	Customers who have questions will be able to contact your directly.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon websitesecurity"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Website Security</h4>
                                            <p class="cps-service-text">
                                            	We’ll protect you from hackers and other virtual threats.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon googlemaps"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Google Maps</h4>
                                            <p class="cps-service-text">
                                            	We’ll put you on the map, literally! Customers can use Google Maps to find your store and get directions.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                            </div>
                            <!-- Row Ends -->

                        	<!-- Row Starts -->
                            <div class="row">
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon dedicatedcustomersupport"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Dedicated Customer Support</h4>
                                            <p class="cps-service-text">
                                            	Have a question? Need help? We’re always here for you.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon moneyback"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">30 Day Money-Back Guarantee</h4>
                                            <p class="cps-service-text">
                                            	If you’re not satisfied with your website we’ll refund you the cost.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon googleanalaytics"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Google Analytics Setup</h4>
                                            <p class="cps-service-text">
                                            	Monitoring your site’s performance can give insights into what your customers want and help grow your business. 
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                            </div>
                            <!-- Row Ends -->

                        	<!-- Row Starts -->
                            <div class="row">
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon socialmedia"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Setup of 3 Social Media Pages</h4>
                                            <p class="cps-service-text">
                                            	We’ll get you started on social, giving your optimized pages for Facebook, Twitter, or other sites.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon customcontent"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">5 Pages of Custom Content</h4>
                                            <p class="cps-service-text">
                                            	Stand out from the crowd with pages that are all about your business.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                            </div>
                            <!-- Row Ends -->
    
                        </div>
						<!-- New Added Structure -->
                        
                    </div>
                    <!-- Repeatable Section Ends -->

                    <!-- Repeatable Section Starts -->
                    <div class="faqs nbdr-top">

                        <div class="row section-title">
                            <div class="col-sm-12 col-xs-12 no-padding">
                                <h4 class="cps-cta-title">
                                    Included with Professional Plan
                                </h4>
                            </div>
                        </div>

                        <!-- New Added Structure -->
                        <div class="cps-services style-4 clr-mbtm">
                        	
                        	<!-- Row Starts -->
                            <div class="row">
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon reisscatalog"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Display Reiss Catalog</h4>
                                            <p class="cps-service-text">
                                            	Uploading all your products takes a lot of time, so we’ll spare you the pain and upload the whole 12,000-piece Reiss Catalog to your site for you.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon productprice"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Add Your Products and Prices</h4>
                                            <p class="cps-service-text">
                                            	Got products for sale that aren’t in the Reiss Catalog? You can upload those too!
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon managewebsitecatagory"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Manage Website Categories</h4>
                                            <p class="cps-service-text">
                                            	We’ll make a site that’s easy for customers to navigate, so they can find what they want and buy it from you.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                            </div>
                            <!-- Row Ends -->

                        	<!-- Row Starts -->
                            <div class="row">
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon printouts"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Printouts & Save-to-Phone </h4>
                                            <p class="cps-service-text">
                                            	Customers can save product pages and show you exactly what they need when they come to your store.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                            </div>
                            <!-- Row Ends -->
    
                        </div>
						<!-- New Added Structure -->

                    </div>
                    <!-- Repeatable Section Ends -->



                    <!-- Repeatable Section Starts -->
                    <div class="faqs nbdr-top">

                        <div class="row section-title">
                            <div class="col-sm-12 col-xs-12 no-padding">
                                <h4 class="cps-cta-title">
                                    Included with Master Plan
                                </h4>
                            </div>
                        </div>
                        
                        <!-- New Added Structure -->
                        <div class="cps-services style-4 clr-mbtm">
                        	
                        	<!-- Row Starts -->
                            <div class="row">
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon socialmediapost"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Social Media Posts </h4>
                                            <p class="cps-service-text">
                                            	We’ll post up to 3 times weekly on two separate social accounts. 
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon monthlyemail"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Monthly Email Newsletter</h4>
                                            <p class="cps-service-text">
                                            	Our newsletters help you stay in touch with your customers and encourage them to come back.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon monthlywebperformance"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Monthly Website Performance Report</h4>
                                            <p class="cps-service-text">
                                            	Understand how well your website is doing with an easy-to-understand monthly report.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                            </div>
                            <!-- Row Ends -->
                        	
                        	<!-- Row Starts -->
                            <div class="row">
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon storepickup"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Store Pick-Up Ordering</h4>
                                            <p class="cps-service-text">
                                            	Let customers pay for what they want straight from your website!
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                            </div>
                            <!-- Row Ends -->
    
                        </div>
						<!-- New Added Structure -->

                    </div>
                    <!-- Repeatable Section Ends -->

                </div>

            </div>
        </div>
        <!-- ADDons end -->

        <!-- Subscription -->
        <div class="cps-section cps-section-padding cps-theme-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="cps-section-header text-center font-adjust" style="margin-bottom:10px;">
                            <h3 class="cps-section-title">Want to Reach New Customers?</h3>
                            <p style="padding:16px 0px;">We can have your website running in only days!</p>
                            <a class="btn btn-white" href="{{route('getstarted')}}">Get Your Site Now</a>
                            <p style="padding-top: 3px;">or <a href="{{route('contact')}}" class="link-white">Get More Info</a></p>
                            <!-- <p class="cps-section-text">Subscribe us if you are willing to get acknowledge what we are doing. Know about the most update work and many more. We will not share your email with any third party or will not make any spam mail</p> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Subscription End -->

    </div>

    <!-- Footer Starts -->
    @include("front.include.footer")
    <!-- Footer Ends -->

</body>
@endsection