@extends('front.layout')

@section('title')
    <title>Safety Line</title>
    @endsection

@section('pageScript')
    <!-- Page Description Starts -->
    <script type="text/javascript">
        var pageName = "home";
        var subpageName = "none";
    </script>
    <!-- Page Description Ends -->
@endsection

@section('content')
<body class="blue-green"> <!-- blue-green -->

    <!-- Header Starts Here -->
   @include("front.include.header")
    <!-- Header Ends Here -->

    <!-- Banner -->
    <div class="cps-banner style-13" id="banner">
        <div class="cps-banner-item cps-banner-item-14">
            <div class="cps-banner-content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 sm-text-center">
                            <h1 class="cps-banner-title fsize">
                                We Build and Manage Your <br />
                                Hardware Store’s Website <br />
                                So You Can Focus On <br />
                                What You Do Best.
                            </h1>
                            <div class="cps-button-group">
                                <a class="btn btn-green btn-set" href="{{ route('getstarted') }}">Get Started</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cps-banner-image hidden-sm hidden-xs">
            <div class="double-mock">
                <img class="back-mock" src="{{asset('/front')}}/images/banner/mock-6-laptop.png" alt="Laptop">
                <img class="front-mock" src="{{asset('/front')}}/images/banner/mock-6-mobile.png" alt="Mobile">
            </div>
        </div>
    </div>
    <!-- Banner End -->

    <div class="cps-main-wrap">
        


        <!-- Services -->
        <div class="cps-section cps-section-padding easyAfford" id="cps-service">
            <div class="container">

                <div class="row no-margin">
                    <div class="col-md-8 col-md-offset-2 col-xs-12">
                        <div class="cps-section-header text-center">
                            <h3 class="cps-cta-title">
                                Easy, Affordable Websites <br />
                                For Hardware Stores
                            </h3>
                            <p class="cps-section-text">
                                You’re a hardware expert, not a software expert. We’ll use our website and marketing expertise <br />
                                to help you reach new customers and grow your business.
                            </p>
                            <p>&nbsp;</p>
                            <div class="clearfix"></div>
                            <div class="button-group">
                                <a class="btn btn-green" href="{{ route('getstarted') }}">GET STARTED TODAY</a>
                            </div>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>

                <div class="row no-margin">
                    <div class="cps-services style-4"> <!-- clr-mbtm -->
                        <div class="row">
                            <div class="col-md-4 col-xs-6">
                                <div class="cps-service style-4">
                                    <div class="cps-service-icon">
                                        <span class="ti-file"></span>
                                    </div>
                                    <div class="cps-service-content">
                                        <h4 class="cps-service-title">Easy Websites</h4>
                                        <p class="cps-service-text">We take care of the design and layouts so you don’t have to.</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <div class="cps-service style-4">
                                    <div class="cps-service-icon">
                                        <span class="ti-layers"></span>
                                    </div>
                                    <div class="cps-service-content">
                                        <h4 class="cps-service-title">No Uploading</h4>
                                        <p class="cps-service-text">We’ll list your product catalog for you.<br /><br /></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <div class="cps-service style-4">
                                    <div class="cps-service-icon">
                                        <span class="ti-layers-alt cusIcon lowCost"></span>
                                    </div>
                                    <div class="cps-service-content">
                                        <h4 class="cps-service-title">Low Cost</h4>
                                        <p class="cps-service-text">Our prices start at just $99.</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 col-xs-6">
                                <div class="cps-service style-4">
                                    <div class="cps-service-icon">
                                        <span class="ti-comment-alt cusIcon mobileresponsive"></span>
                                    </div>
                                    <div class="cps-service-content">
                                        <h4 class="cps-service-title">Mobile Responsive</h4>
                                        <p class="cps-service-text">Customers can use your site on their phone, tablet, or computer.</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <div class="cps-service style-4">
                                    <div class="cps-service-icon">
                                        <span class="ti-announcement cusIcon customersupport"></span>
                                    </div>
                                    <div class="cps-service-content">
                                        <h4 class="cps-service-title">Customer Support</h4>
                                        <p class="cps-service-text">If you need any help using your site we’ll be here for you.</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <div class="cps-service style-4">
                                    <div class="cps-service-icon">
                                        <span class="ti-settings cusIcon moneyback"></span>
                                    </div>
                                    <div class="cps-service-content">
                                        <h4 class="cps-service-title">30 Day Money Back Guarantee</h4>
                                        <p class="cps-service-text">We’re so confident that you’ll love your website that we promise a full refund if you’re not satisfied.</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 col-xs-6">
                                <div class="cps-service style-4">
                                    <div class="cps-service-icon">
                                        <span class="ti-comments cusIcon socialmedia"></span>
                                    </div>
                                    <div class="cps-service-content">
                                        <h4 class="cps-service-title">Social Media</h4>
                                        <p class="cps-service-text">We’ll get you started on Facebook, Twitter, and other social platforms. </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <div class="cps-service style-4">
                                    <div class="cps-service-icon">
                                        <span class="ti-email cusIcon productcatalog"></span>
                                    </div>
                                    <div class="cps-service-content">
                                        <h4 class="cps-service-title">Product Catalog</h4>
                                        <p class="cps-service-text">We can upload the full 12,000 item Reiss catalog for you.</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <div class="cps-service style-4">
                                    <div class="cps-service-icon">
                                        <span class="ti-server cusIcon webaddresshosting"></span>
                                    </div>
                                    <div class="cps-service-content">
                                        <h4 class="cps-service-title">Free Web Address & Hosting</h4>
                                        <p class="cps-service-text">There’s no extra charge for creating and maintaining your “.com” website!</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!-- Services End -->



        <!-- Call t- action /get started -->
        <div class="cps-cta cps-gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center col-xs-12 sm-text-center"> <!-- col 7 -->
                        <h3 class="cps-cta-title">
                            XcelHub helps your store succeed with an affordable, easy-to-use website.
                        </h3>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 col-xs-12 text-center"> <!-- col 10 -->
                                <p class="cps-cta-text">
                                    You already have a fulltime job, and that’s running your store. Let us help you take a load off. We’ll build you a site that attracts customers and grows your business.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="col-md-5 col-xs-12 text-right sm-text-center">
                        <a class="btn cps-cta-btn btn-green" href="getstarted.php">Get Started Now</a> // class name > custSet
                    </div>
                    -->
                </div>
            </div>
        </div>
        <!-- Call t- action /get started end -->

        <!-- Testimonial -->
        <div class="cps-section" id="cps-testimonial"> <!-- cps-section-padding -->
            <div class="container-fluid"> <!-- container -->
                <div class="row">
                    <div class="cps-testimonial-container cps-image-bg-3 parallax" data-stellar-background-ratio="0.5">
                        <div class="cps-testimonials-wrap style-2">
                            <div class="owl-carousel testimonial-carousel" id="testimonial-carousel">
                                <div class="cps-testimonial-item">
                                    <blockquote class="no-quote">
                                        <span class="list-check">&#x2714;</span>  Quick Setup <br />
                                        <span class="list-check">&#x2714;</span>  No Hidden Fees <br />
                                        <span class="list-check">&#x2714;</span>  Money-Back Gaurantee <br />
                                        <span class="list-check">&#x2714;</span>  No Long-term Contracts <br />
                                        <span class="list-check">&#x2714;</span>  Free Domain &amp; Hosting <br />
                                    </blockquote>
                                    <!--
                                    <h5 class="cps-reviewer-name">Tom Z.Graze</h5>
                                    <p class="cps-reviewer-position">Founder &amp; CEO</p>
                                    <p class="cps-reviewer-company">CodePassenger LLC</p>
                                    -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonial End -->

        <!-- Subscription -->
        <div class="cps-section cps-section-padding cps-theme-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="cps-section-header text-center font-adjust" style="margin-bottom:10px;">
                            <h3 class="cps-section-title">Ready to Get Started?</h3>
                            <p>&nbsp;</p>
                            <a class="btn btn-white" href="{{route('getstarted')}}">Sign-up Now</a>
                            <p style="padding-top: 3px;">or <a href="{{route('contact')}}" class="link-white">Request a Consultation</a></p>
                            <!-- <p class="cps-section-text">Subscribe us if you are willing to get acknowledge what we are doing. Know about the most update work and many more. We will not share your email with any third party or will not make any spam mail</p> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Subscription End -->

    </div>

    
    <!-- Footer Starts Here -->
    @include("front.include.footer")
    <!-- Footer Starts Here -->

</body>
@endsection
