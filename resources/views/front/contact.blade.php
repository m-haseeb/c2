@extends('front.layout')

@section('title')
    <title>Contact</title>
    @endsection

    @section('content')

    @php
    $siteData = getSetting();
    @endphp
    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
			<!-- about us banner section starts -->
			<div class="cntact-cont">
            	<div class="contact-banr" style="background-image: url({{asset('front')}}/images/banners/contact-banr.jpg);">
            	<div class="n-wrapper">
                    <div class="c-banr-cntnt">
                    	<div class="bred-crum">
                        	<ul>
                            	<li><a href="{{route('index')}}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">Contact&nbsp;Us</a></li>
                            </ul>
                        </div><!-- top bread navigation -->
                        
                        <div class="banr-txt">
                        	<h5>Contact&nbsp;US</h5>
                        </div><!-- banner title section -->
                    </div>
				</div>
                </div>                   
            </div>
            <!-- about us banner section ends -->
			
            <!-- contact body section starts-->
            <div class="contact-bdy">
            	<div class="n-wrapper">
                	<div class="c-body-cntnt">
                        <div class="c-body-cntnt-para">
                            <h2>If you have any questions or would like to have a catalog, please feel free to contact us at <a href="mailto:safetylinecorp@gmail.com" target="_blank">safetylinecorp@gmail.com</a>. For additional service we highly suggest you visit our 
                            local partner branch nearest you:
                            </h2>
                        </div>
                    	<div class="cbc-col1">
                        	<div class="cbc-col1-wrap">
                            	<div class="cbc-col1w-top">
                                	<h5>Get In Touch</h5>
                                </div><!-- top get in touch section -->
                                
                                <div class="cbc-col1w-btm">
                                
                                	<div class="cbc-col1wb-row">
                                    	<img src="{{asset('front')}}/images/contact-loc.png">
                                        
                                        <p>{{str_replace('\r\n','<br/>',$siteData['site_address'])}}</p>
                                    </div><!-- bottom get in touch section row -->
                                    
                                    <div class="cbc-col1wb-row">
                                    	<img src="{{asset('front')}}/images/contact-phn.png">
                                        
                                        <p>{{$siteData['site_phone']}}</p>
                                    </div><!-- bottom get in touch section row -->
                                    
                                    <!--
                                    <div class="cbc-col1wb-row">
                                    	<img src="{{asset('front')}}/images/contact-fax.png">
                                        
                                        <p>{{$siteData['site_fax']}}</p>
                                    </div>
                                    -->
                                    <!-- bottom get in touch section row -->
                                    
                                    <div class="cbc-col1wb-row">
                                    	<a href="mailto:{{$siteData['site_email']}}">
                                            <img src="{{asset('front')}}/images/contact-email.png" />
                                            <p>{{$siteData['site_email']}}</p>
                                        </a>
                                    </div><!-- bottom get in touch section row -->
                                    
                                </div><!-- bottom get in touch section -->
                            </div>
                            <div class="cbc-col1-addrs">
                                <div class="cbc-col1-addrs-head">
                                    <h3>Safety Plaza</h3>
                                </div>
                                <div class="cbc-col1-addr-cont">
                                    <p>1101 E 177th Street<br /> 
                                    Bronx, NY 10460
                                    </p>
                                </div>
                            </div>
                            <div class="cbc-col1-addrs">
                                <div class="cbc-col1-addrs-head">
                                    <h3>Safety 4 You</h3>
                                </div>
                                <div class="cbc-col1-addr-cont">
                                    <p>796 E 140 Street<br />
                                    Bronx NY 10454
                                    </p>
                                </div>
                            </div>
                            <div class="cbc-col1-addrs">
                                <div class="cbc-col1-addrs-head">
                                    <h3>Safety R US</h3>
                                </div>
                                <div class="cbc-col1-addr-cont">
                                    <p>84-19 Astoria Blvd<br />
                                    East Elmhurst, NY 11370
                                    </p>
                                </div>
                            </div>
                            <div class="cbc-col1-addrs">
                                <div class="cbc-col1-addrs-head">
                                    <h3>Northern Safety Center</h3>
                                </div>
                                <div class="cbc-col1-addr-cont">
                                    <p>107-09 Northern Blvd<br /> 
                                    Corona, NY 11368
                                    </p>
                                </div>
                            </div>
                            <div class="cbc-col1-addrs">
                                <div class="cbc-col1-addrs-head">
                                    <h3>Queens Safety Center</h3>
                                </div>
                                <div class="cbc-col1-addr-cont">
                                    <p>7005 Queens Blvd<br /> 
                                    Woodside, NY 11377
                                    </p>
                                </div>
                            </div>
                            <div class="cbc-col1-addrs">
                                <div class="cbc-col1-addrs-head">
                                    <h3>Cross Bay Safety Center</h3>
                                </div>
                                <div class="cbc-col1-addr-cont">
                                    <p>107-12 Cross Bay Blvd,<br /> 
                                    Ozone Park, NY 11417
                                    </p>
                                </div>
                            </div>
                        </div><!-- left section -->
                       		
                        <div class="cbc-col2">
                        	<div class="cbc-col2-wrap">
                            	<div class="cbc-col2w-haed">
                                	<h5>Send us a Message</h5>
                                </div><!-- top head section -->
                                <div class="cbc-col2w-frm">
                                 @if(session()->has('success'))
                                
                                    <div class="color-gren">
                                        <p>Thank you for contacting us.</p>
                                        <a href="javascript:void(0);" class="close-me">X</a>
                                    </div>
                                @endif

                                	{{ Form::open(array('method'=>'POST', 'name'=>'contact-form', 'files'=> true, 'url'=>route('contactSend'))) }}
                                    	<label>Name</label>
                                        <div class="contact-filds">
                                        	<input type="text" name="uname" value="{{old('uname')}}">
                                        </div>
                                        <div class="error"><p>{{$errors->first('uname')}}</p></div>
                                        
                                        <label>Email</label>
                                        <div class="contact-filds">
                                        	<input type="text" name="uemail" value="{{old('uemail')}}">
                                        </div>
                                        <div class="error"><p>{{$errors->first('uemail')}}</p></div>
                                        
                                        <label>Phone</label>
                                        <div class="contact-filds">
                                        	<input type="text" name="uphn" value="{{old('uphn')}}">
                                        </div>
                                        <div class="error"><p>{{$errors->first('uphn')}}</p></div>
                                        
                                        <label>Message</label>
                                        <div class="contact-filds">
                                        	<textarea name="umessg">{{old('umessg')}}</textarea>
                                        </div>
                                        <div class="error"><p>{{$errors->first('umessg')}}</p></div>
                                        
                                        <div class="sub-buttons">
											<input type="submit" value="Submit" />
                                        </div>
                                    {{ Form::close()}}
                                </div><!-- contact form section -->
                            </div>
                            
                        </div><!-- right form section -->
                    </div>
                </div>
            </div>
            <!-- contact body section ends -->
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->
    <script>
        $(document).ready(function(){
            $(document).on('click','.close-me', function (){
                $(this).parent().remove();
            });
        });
    </script>
  @endsection