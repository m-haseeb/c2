@extends('front.layout')
@section('title')
    <title>{{$pData->title}}</title>
    @endsection

    @section('content')
    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
        
			<!-- services office equipment section starts -->
			<div class="banr-cont-srvs">
            	<div class="soe-banr" style="background-image: url({{asset('pages_images/'.$pData->image)}})">
                    <div class="n-wrapper">
                        <div class="soe-banr-cntnt">
                            <div class="bred-crum">
                                <ul>
                                    <li><a href="{{ route('index') }}">Home</a></li>
                                    <li><span>/</span></li>
                                    <li><a href="javascript:void(0);">{{$pData->title}}</a></li>
                                    
                                </ul>
                            </div><!-- top bread navigation -->
                            
                            <div class="sb-cntnt-title">
                                <h5>Services</h5>
                            </div><!-- services office equipment banner title section -->
                            
                            <!-- services office equipment item description section starts -->
                            <div class="soe-itmdesc">
                                <p>
                                    @php
                                    $des = str_replace("../../",asset('/'),$pData->description);
                                    echo $des;
                                    @endphp
                                </p>
                                 {{-- <div class="soe-itmdesc-row">
                                    <div class="soe-idr-inr">
                                        <div class="sbc-cntnt">
                                        
                                            <div class="sbc-sli-r-img">
                                                <img src="{{asset('front')}}/images/srvs-wide-mini.png" alt="" >
                                            </div><!--left image section -->
                                            
                                            <div class="sbc-sli-r-txt">
                                                <h5>Sample Text Here Sample Text Here</h5>
                                                <p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>
                                            </div><!--right text section -->
                                            
                                        </div><!-- item description left-->
                                    </div><!-- left item description content row -->
                                    
                                    <div class="soe-idr-inr go-right">
                                        <div class="sbc-cntnt">
                                        
                                            <div class="sbc-sli-r-img">
                                                <img src="{{asset('front')}}/images/srvs-wide-mini.png" alt="" >
                                            </div><!--left image section -->
                                            
                                            <div class="sbc-sli-r-txt">
                                                <h5>Sample Text Here Sample Text Here</h5>
                                                <p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>
                                            </div><!--right text section -->
                                            
                                        </div><!-- item description left-->
                                    </div><!-- right item description content row -->
                                 </div><!-- item description rows -->
                                
                                 <div class="soe-itmdesc-row">
                                    <div class="soe-idr-inr">
                                        <div class="sbc-cntnt">
                                        
                                            <div class="sbc-sli-r-img">
                                                <img src="{{asset('front')}}/images/srvs-wide-mini.png" alt="" >
                                            </div><!--left image section -->
                                            
                                            <div class="sbc-sli-r-txt">
                                                <h5>Sample Text Here Sample Text Here</h5>
                                                <p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>
                                            </div><!--right text section -->
                                            
                                        </div><!-- item description left-->
                                    </div><!-- left item description content row -->
                                    
                                    <div class="soe-idr-inr go-right">
                                        <div class="sbc-cntnt">
                                        
                                            <div class="sbc-sli-r-img">
                                                <img src="{{asset('front')}}/images/srvs-wide-mini.png" alt="" >
                                            </div><!--left image section -->
                                            
                                            <div class="sbc-sli-r-txt">
                                                <h5>Sample Text Here Sample Text Here</h5>
                                                <p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>
                                            </div><!--right text section -->
                                            
                                        </div><!-- item description left-->
                                    </div><!-- right item description content row -->
                                </div><!-- item description rows --> --}}
                            </div>
                            <!-- services office equipment item description section ends -->
                        </div>
                    </div>  
                </div>                 
            </div>
            <!-- services office equipment section ends -->

        </div>
        <!-- Subpage Structure Ends -->        
        
    </div>
    <!-- Site Body Ends Here -->
    @endsection