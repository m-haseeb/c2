@extends('front.layout')

    @section('title')
    <title>About Us</title>
    @endsection
    
    
    @section('pageScript')
    <!-- Page Description Starts -->
    <script type="text/javascript">
        var pageName = "aboutus";
        var subpageName = "none";
    </script>
    <!-- Page Description Ends -->

    <style type="text/css" id="wp-custom-css">
        .aboutpage .page-id-319 .menu-learn-more, 
        .aboutpage .tax-formats .menu-learn-more {
            display: none;
        }
        .aboutpage .banner.dark {
            background-color: #01050F;
        }
        .aboutpage .news-categories-events .entry-meta {
            display: none;
        }
        .aboutpage .newblog h5 {
            margin-top: 0;
            font-weight: 700;
            line-height: 1.3;
            color: #000;
              font-size:24px;
            text-transform: none;
            font-family: lftetica-extrabold,sans-serif;
        }
    </style>
    <style type="text/css" data-type="vc_custom-css">
        .aboutpage .icon-box-title {
            text-transform: none !important;
            font-size: 1.75em !important;
        }
        .aboutpage .content-info {
            margin-top: 0px !important;
        }
        .aboutpage .vc_btn3.vc_btn3-color-white, .aboutpage .vc_btn3.vc_btn3-color-white.vc_btn3-style-flat {
            color: #00b2e3 !important;
        }
    </style>
    <style type="text/css" data-type="vc_shortcodes-custom-css">
        .aboutpage .vc_custom_1471832198779{margin-top: 0px !important;border-top-width: 0px !important;padding-top: 0px !important;}
        .aboutpage .vc_custom_1470338277898{margin: 0px !important;border-width: 0px !important;padding: 0px !important;}
        .aboutpage .vc_custom_1470957382944{margin-bottom: 0px !important;}
        .aboutpage .vc_custom_1471903661476{padding-top: 20px !important;}
        .aboutpage .vc_custom_1470348933157{padding-top: 25px !important;}
        .aboutpage .vc_custom_1470348893781{padding-top: 10px !important;}
        .aboutpage .vc_custom_1470348771941{margin: 0px !important;border-width: 0px !important;padding: 0px !important;}
        .aboutpage .vc_custom_1470348682365{padding-bottom: 25px !important;}
        .aboutpage .vc_custom_1471878579966{padding-right: 15px !important;padding-left: 15px !important;}
        .aboutpage .vc_custom_1471878589690{padding-right: 15px !important;padding-left: 15px !important;}
        .aboutpage .vc_custom_1471878597741{padding-right: 15px !important;padding-left: 15px !important;}
        .aboutpage .vc_custom_1470957387973{margin-bottom: 0px !important;}
    </style>
    <noscript>
        <style type="text/css"> .aboutpage .wpb_animate_when_almost_visible { opacity: 1; }</style>
    </noscript>

    <style>
        @media(max-width: 1199px){
            .aboutpage .customer-filter button{
                font-size: 1em;
            }
        }
    </style>

@endsection

@section('content')
<body class="blue-green subpage"> <!-- blue-green -->

    <!-- Header Starts -->
    @include("front.include.header")
    <!-- Header End -->

    <div class="cps-main-wrap aboutpage">

        <!-- About Us Section Starts -->

        <!-- Banner Section Starts -->
        <header class="banner dark" role="banner" style="background-image: url({{asset('/front')}}/images/banner/why_us-header.jpg)">
            <style>
                @media (min-width: 768px) {
                    .overlay {
                    }
                    .banner_content_wrapper{
                        height: 430px; /* 490 */
                    }
                }
            </style>
            <div class="overlay" style="background-image:none">
                <div class="container">

                    <div class="banner_content_wrapper">
                        <div class="banner_content">
                            <h1>
                                <span class="first-span">XcelHub - helping hardware leaders</span> 
                                <span>grow their businesses online.</span>
                            </h1>
                            <!--
                            <div class="btn-row">
                                <a class="btn btn-lg btn-primary" href="#">TALK TO US TODAY</a>
                            </div>
                            -->
                        </div>
                    </div>

                </div>
            </div>
        </header>
        <!-- Banner Section Ends -->

        <!-- About us Sub Banner Section Starts -->
        <div class="cps-section" id="aboutSubBan"> <!-- cps-section-padding -->

            <!--
            <div class="aboutbanrow">
                <div class="col-xs-12">
                    <div class="cps-about-img">
                        <img class="img-responsive" src="images/bg/mock-bg.png" alt="...">
                    </div>
                </div>
            </div>
            -->
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="cps-section"> <!-- text-center -->
                            <!-- <h3 class="cps-section-title"></h3> -->
                            <p>
                                <strong>XcelHub</strong> is a website and online marketing solution for your hardware business. We are powered by Reiss, the experts who have been providing trade professionals, retail hardware stores, lumber yards, tool rental stores, home and garden outlets, locksmiths, and others with the products they need for over 40 years. 
                            </p>
                            <p>
                                At <strong>XcelHub</strong>, we work with Reiss to provide you with all the tools you need to grow your business and be found online. Our team of experts has spent years building perfect websites for small businesses, so you can count on us to deliver an appealing, functional website that is cost effective and will help your business thrive.
                            </p>
                            <p>
                                Through our partnership with Reiss, we are can upload the Reiss product catalog to your website, <strong>displaying the entire 12,000 piece Reiss product line,</strong> which includes over 200 trusted brands and 4,000 exclusive tools from Tuff Stuff. You’ll even be able to sell items you don’t regularly stock in your normal inventory!
                            </p>
                            <p>
                                Best of all, our services allow you to take a hands-off approach to running your website. We’ll worry about all the technical details like hosting, domains, content creation, and website security so you can focus on the rest of your business.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- About us Sub Banner Section end -->


        <!-- Services -->
        <div class="cps-section cps-section-padding" id="cps-aboutboxs">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-xs-12">
                        <div class="cps-section-header text-center">
                            <h3 class="cps-section-title">XcelHub Will:</h3>
                            <!-- <p class="cps-section-text">Text</p> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="cps-services style-3">
                        <div class="col-md-4 col-xs-6">
                            <div class="cps-service style-5">
                                <div class="cps-service-icon">
                                    <span class="ti-file cusIcon buildyourwebsite"></span>
                                </div>
                                <div class="cps-service-content">
                                    <h4 class="cps-service-title">Build your website for you.</h4>
                                    <!-- <p class="cps-service-text">Text</p> -->
                                    <a class="btn cps-cta-btn btn-green" href="{{route('getstarted')}}">LEARN MORE</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <div class="cps-service style-5">
                                <div class="cps-service-icon">
                                    <span class="ti-layers cusIcon hostingandsecurity"></span>
                                </div>
                                <div class="cps-service-content">
                                    <h4 class="cps-service-title">Handle all hosting and security.</h4>
                                    <a class="btn cps-cta-btn btn-green" href="{{route('getstarted')}}">LEARN MORE</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <div class="cps-service style-5">
                                <div class="cps-service-icon">
                                    <span class="ti-layers-alt cusIcon cmobilefriendly"></span>
                                </div>
                                <div class="cps-service-content">
                                    <h4 class="cps-service-title">Create a mobile-friendly site.</h4>
                                    <a class="btn cps-cta-btn btn-green" href="{{route('getstarted')}}">LEARN MORE</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-6 col-md-offset-2">
                            <div class="cps-service style-5">
                                <div class="cps-service-icon">
                                    <span class="ti-comment-alt cusIcon giveyoursiteaccess"></span>
                                </div>
                                <div class="cps-service-content">
                                    <h4 class="cps-service-title">Give your site access to the entire Reiss catalog.</h4>
                                    <a class="btn cps-cta-btn btn-green" href="{{route('getstarted')}}">LEARN MORE</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <div class="cps-service style-5">
                                <div class="cps-service-icon">
                                    <span class="ti-announcement cusIcon helpcustomers"></span>
                                </div>
                                <div class="cps-service-content">
                                    <h4 class="cps-service-title">Help customers find you and the products they want to buy.</h4>
                                    <a class="btn cps-cta-btn btn-green" href="{{route('getstarted')}}">LEARN MORE</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Services End -->


        <!-- About Us Section Ends -->

        <!-- Subscription -->
        <div class="cps-section cps-section-padding cps-theme-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="cps-section-header text-center font-adjust" style="margin-bottom:0px;">
                            <h3 class="cps-section-title">Don’t wait any longer, help your business grow with a custom website today!</h3>
                            <!-- <p style="padding:16px 0px;">We can have your website running in days!</p> -->
                            <p>&nbsp;</p>
                            <a class="btn btn-white" href="{{route('contact')}}">Get Your Website</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Subscription End -->

    </div>

    <!-- Footer Starts -->
    @include("front.include.footer")
    <!-- Footer Ends -->

</body>
@endsection