@extends('front.layout')

@section('title')
    <title>Cart</title>
    @endsection

    @section('content')

    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        {{-- <form name="order_review" action="{{ route('emailList') }}" method="post" enctype="multipart/form-data"> --}}
            {{-- {{ csrf_field() }} --}}
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
        
            <!-- Mycart order review section starts -->
            <div class="mycart-cont">
            	<div class="n-wrapper">
                	<div class="mycart-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{route('index')}}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="{{route('displayList')}}">My&nbsp;Cart</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">Order&nbsp;Review</a></li>
                            </ul>
                        </div><!-- bread navigation -->
                        
                	 	<div class="mc-addcart">
                            <div class="check-thead">
                                <h1>Order&nbsp;Review</h1>
                            </div><!-- top heading -->
                            
                            @if(session()->has('msg'))
                            <div class="error"> {!! session()->get('msg') !!} </div>
                            @endif



                            <div class="oreview-detail">
                            	<div class="ord-top">
                                	<div class="ord-toplft">
                                    	<div class="tlft-thead">
                                        	<h5>Shipping Address</h5>
                                        </div>
                                        
                                        <div class="tlft-btm">
                                        	<div class="lft-btm-wrap">
                                            	<div class="lft-btm-inr">
                                                	<div class="col-row">
                                                        <h5>First Name:</h5>
                                                        <span>{{$address['shipfname']}}</span>
                                                        <input type="hidden" name="shipfname" value="{{$address['shipfname']}}"/>
                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>Last Name:</h5>
                                                        <span>{{$address['shiplname']}}</span>
                                                        <input type="hidden" name="shiplname" value="{{$address['shiplname']}}"/>

                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>Email:</h5>
                                                        <span>{{$address['shipemail']}}</span>
                                                        <input type="hidden" name="shipemail" value="{{$address['shipemail']}}"/>

                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>Phone:</h5>
                                                        <span>{{$address['shipphone']}}</span>
                                                         <input type="hidden" name="shipphone" value="{{$address['shipphone']}}"/>
                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>Address:</h5>
                                                        <span>{{$address['shipaddress']}}</span>
                                                         <input type="hidden" name="shipaddress" value="{{$address['shipaddress']}}"/>
                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>City:</h5>
                                                        <span>{{$address['shipcity']}}</span>
                                                         <input type="hidden" name="shipcity" value="{{$address['shipcity']}}"/>
                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>State:</h5>
                                                        <span>{{$address['shipstate']}}</span>
                                                         <input type="hidden" name="shipstate" value="{{$address['shipstate']}}"/>
                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>Zip:</h5>
                                                        <span>{{$address['shipzip']}}</span>
                                                         <input type="hidden" name="shipzip" value="{{$address['shipzip']}}"/>
                                                    </div>
												</div>
                                            </div>
                                        </div>
                                    </div><!-- top left section -->

                                    
                                    <div class="ord-toplft toprght">
                                    	<div class="tlft-thead">
                                        	<h5>Billing Address</h5>
                                        </div>
                                        
                                        <div class="tlft-btm">
                                        	<div class="lft-btm-wrap">
                                            	<div class="lft-btm-inr">
                                                	<div class="col-row">
                                                        <h5>First Name:</h5>
                                                        <span>{{$address['billfname']}}</span>
                                                         <input type="hidden" name="billfname" value="{{$address['billfname']}}"/>
                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>Last Name:</h5>
                                                        <span>{{$address['billlname']}}</span>
                                                         <input type="hidden" name="billlname" value="{{$address['billlname']}}"/>
                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>Email:</h5>
                                                        <span>{{$address['billemail']}}</span>
                                                         <input type="hidden" name="billemail" value="{{$address['billemail']}}"/>
                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>Phone:</h5>
                                                        <span>{{$address['billphone']}}</span>
                                                         <input type="hidden" name="billphone" value="{{$address['billphone']}}"/>
                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>Address:</h5>
                                                        <span>{{$address['billaddress']}}</span>
                                                         <input type="hidden" name="billaddress" value="{{$address['billaddress']}}"/>
                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>City:</h5>
                                                        <span>{{$address['billcity']}}</span>
                                                         <input type="hidden" name="billcity" value="{{$address['billcity']}}"/>
                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>State:</h5>
                                                        <span>{{$address['billstate']}}</span>
                                                         <input type="hidden" name="billstate" value="{{$address['billstate']}}"/>
                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5>Zip:</h5>
                                                        <span>{{$address['billzip']}}</span>
                                                         <input type="hidden" name="billzip" value="{{$address['billzip']}}"/>
                                                    </div>
												</div>
                                            </div>
                                        </div>
                                    </div><!-- top right section -->
                                </div><!-- content section -->
                                
                                <div class="ord-top">
                                	<div class="ord-toplft">
                                    	<div class="tlft-thead">
                                        	<h5>Credit Card Infomation</h5>
                                        </div>
                                        
                                        <div class="tlft-btm">
                                        	<div class="lft-btm-wrap">
                                            	<div class="lft-btm-inr">
                                                	<div class="col-row">
                                                        <h5 class="set-crh">Card Holder Name:</h5>
                                                        <span class="set-crs">{{ $cartInfo['cardName'] }}</span>
                                                         <input type="hidden" name="cardName" value="{{ $cartInfo['cardName'] }}"/>
                                                    </div>
                                                    
                                                    <div class="col-row">
                                                        <h5 class="set-crh">Credit Card Number:</h5>
                                                        <span class="set-crs">{{ substr($cartInfo['cardNumber'], -4) }}</span>
                                                         <input type="hidden" name="cardNumber" value="{{ substr($cartInfo['cardNumber'], -4) }}"/>
                                                    </div>
                                                	
                                                	<div class="col-row">
                                                        <h5 class="set-crh">Credit Card Expiry:</h5>
                                                        <span class="set-crs">{{ $cartInfo['month'] }} - {{ $cartInfo['year'] }}</span>
                                                        <input type="hidden" name="month_year" value="{{ $cartInfo['month'] }} - {{ $cartInfo['year'] }}"/>

                                                    </div>
												</div>
                                            </div>
                                        </div>
                                    </div>		
                                </div><!-- content section -->
                            </div><!-- center order review -->
                            
                            <div class="adcart-cntnt">
                                <div class="adc-outr">
                                    <div class="adc-inr">
                                        <div class="adc-main">
                                            <div class="adc-inr-wrap">
                                                <div class="adc-row">
                                                
                                                    <div class="thw-img1">
                                                        <h1>Product Details</h1>
                                                    </div>
                                                    
                                                    <div class="thw-price1">
                                                        <h1>Unit Price</h1>
                                                    </div>
                                                    
                                                    <div class="thw-qty1">
                                                        <h1>Qty</h1>
                                                    </div>
                                                    
                                                    <div class="thw-tship tship-st">
                                                    	<h1>Total Price</h1>
                                                    </div>
                                                    
                                                    <!--<div class="thw-action1">
                                                        <h1>&nbsp;</h1>
                                                    </div>-->
                                                    
                                                </div>
                                            </div>
                                        </div><!-- top headings row -->
                                        @php
                                            $showPrice =1;
                                            $totalPrice = 0.0;
                                            $tax = 0.0;
                                            $shiping_charges = 0.0;
                                            $siteData = getSetting();
                                        @endphp
                                        @if(count($products) > 0)
                                            @foreach($products as $list)
                                        <div class="adc-main row-btm item{{$list['listId']}}">
                                            <div class="adc-inr-wrap">
                                                <div class="adc-row row-set f-row">
                                                    <div class="row-fcol1">
                                                        <div class="fcol-img">


                                                        @if ( isset($list['prodcut_location_type']) && $list['prodcut_location_type'] != '' )
                                                            @if(@$list['is_bulk'] == 1)
                                                                @if (file_exists(public_path('bulk_upload_img').'/'.$list['ImageFile'].'/'.$list['ImageFile'].'.jpg' ) )
                                                                    <img src="{{asset('bulk_upload_img')}}/{{$list['ImageFile']}}/{{$list['ImageFile'].'.jpg'}}" alt="">
                                                                    <input type="hidden" name="product_image[]" value="{{asset('bulk_upload_img')}}/{{$list['ImageFile']}}/{{$list['ImageFile'].'.jpg'}}"/>
                                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$list['ImageFile'].'/'.$list['ImageFile'].'.png' ) )
                                                                    <img src="{{asset('bulk_upload_img')}}/{{$list['ImageFile']}}/{{$list['ImageFile'].'.png'}}" alt="">
                                                                    <input type="hidden" name="product_image[]" value="{{asset('bulk_upload_img')}}/{{$list['ImageFile']}}/{{$list['ImageFile'].'.png'}}"/>
                                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$list['ImageFile'].'/'.$list['ImageFile'].'-01.jpg' ) )
                                                                    <img src="{{asset('bulk_upload_img')}}/{{$list['ImageFile']}}/{{$list['ImageFile'].'-01.jpg'}}" alt="">
                                                                    <input type="hidden" name="product_image[]" value="{{asset('bulk_upload_img')}}/{{$list['ImageFile']}}/{{$list['ImageFile'].'-01.jpg'}}"/>
                                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$list['ImageFile'].'/'.$list['ImageFile'].'-01.JPG' ) )
                                                                    <img src="{{asset('bulk_upload_img')}}/{{$list['ImageFile']}}/{{$list['ImageFile'].'-01.JPG'}}" alt="">
                                                                    <input type="hidden" name="product_image[]" value="{{asset('bulk_upload_img')}}/{{$list['ImageFile']}}/{{$list['ImageFile'].'-01.JPG'}}"/>
                                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$list['ImageFile'].'/'.$list['ImageFile'].'-01.png' ) )
                                                                    <img src="{{asset('bulk_upload_img')}}/{{$list['ImageFile']}}/{{$list['ImageFile'].'-01.png'}}" alt="">
                                                                    <input type="hidden" name="product_image[]" value="{{asset('bulk_upload_img')}}/{{$list['ImageFile']}}/{{$list['ImageFile'].'-01.png'}}"/>
                                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$list['ImageFile'].'/'.$list['ImageFile'].'-01.PNG' ) )
                                                                    <img src="{{asset('bulk_upload_img')}}/{{$list['ImageFile']}}/{{$list['ImageFile'].'-01.PNG'}}" alt="">
                                                                    <input type="hidden" name="product_image[]" value="{{asset('bulk_upload_img')}}/{{$list['ImageFile']}}/{{$list['ImageFile'].'-01.PNG'}}"/>
                                                                @endif
                                                            @else
                                                                <img src="{{asset('products_images/thumbs')}}/{{$list['product_image']}}" alt="">
                                                                <input type="hidden" name="product_image[]" value="{{asset('products_images/thumbs')}}/{{$list['product_image']}}"/>
                                                            @endif
                                                        @else
                                                            <img src="https://reisshardware.com/product_images/{{$list['sku']}}.JPG" alt="">
                                                            <input type="hidden" name="product_image[]" value="https://reisshardware.com/product_images/{{$list['sku']}}.JPG"/>
                                                        @endif
                                                        </div>
                                                        
                                                        <div class="fcol-txt1">
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <p>{{$list['name']}}</p>
                                                                        <input type="hidden" name="name[]" value="{{$list['name']}}"/>
                                                                    </td>
                                                                </tr>   
                                                            </table>
                                                        </div>
                                                    </div>
                                                    @php
                                                    if($list['is_price'] == 0){
                                                        $showPrice = 0;
                                                    }
                                                    @endphp


                                                    <div class="row-fcol1 scol">
                                                        <div class="fcol-txt scol">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        @if($list['is_price'] && $siteData['show_price'])
                                                                            @if ( isset($list['prodcut_location_type']) && $list['prodcut_location_type'] != '' )
                                                                                <p class="calculatePrice" data-price={{number_format($list['price'], 2, '.', '')}} data-cartId="{{$list['listId']}}" >${{number_format($list['price'],2,'.','')}}</p>
                                                                                <input type="hidden" name="unit_price[]" value="{{number_format($list['price'],2,'.','')}}"/>
                                                                            @else
                                                                                <p class="calculatePrice" data-price={{getReissNewPriceFront($list['id'], ($list['price']+($list['price']*getSettingPrice())) )}} data-cartId="{{$list['listId']}}" >${{getReissNewPriceFront($list['id'], ($list['price']+($list['price']*getSettingPrice())) )}}</p>
                                                                                <input type="hidden" name="unit_price[]" value="{{getReissNewPriceFront($list['id'], ($list['price']+($list['price']*getSettingPrice())) )}}"/>
                                                                            @endif
                                                                        @else
                                                                            N/A
                                                                        @endif
                                                                    </td>
                                                                </tr>   
                                                            </table>
                                                        </div>
                                                    </div>
                                                    
                                                    @php
                                                    if ( isset($list['prodcut_location_type']) && $list['prodcut_location_type'] != '' )
                                                        $totalPrice += ($list['price'] * $list['qty']);
                                                    else
                                                        $totalPrice += (getReissNewPriceFront($list['id'], ($list['price']+($list['price']*getSettingPrice())) ) * $list['qty']);
                                                    @endphp
                                                    <?php
                                                    if ($address['shipstate'] == "New Jersey")
                                                    {
                                                        $tax = ($totalPrice * 6.625)/100;

                                                    }
                                                    if ($address['store_shiping'] == "shipping")
                                                        {
                                                            if ($totalPrice <20)
                                                            {
                                                                $shiping_charges = 10.25;
                                                            }
                                                            elseif (($totalPrice <100)&&($totalPrice>=20))
                                                            {
                                                                $shiping_charges = 26.75;
                                                            }
                                                            elseif (($totalPrice <200)&&($totalPrice>=100))
                                                            {
                                                                $shiping_charges = 45.63;
                                                            }
                                                            elseif (($totalPrice <300)&&($totalPrice>=200))
                                                            {
                                                                $shiping_charges = 65.85;
                                                            }
                                                            elseif (($totalPrice <400)&&($totalPrice>=300))
                                                            {
                                                                $shiping_charges = 89.52;
                                                            }
                                                            elseif (($totalPrice <500)&&($totalPrice>=400))
                                                            {
                                                                $shiping_charges = 105.58;
                                                            }
                                                            else
                                                            {
                                                                $shiping_charges = 0.0;
                                                            }
                                                        }

                                                    $grand = $totalPrice + $tax + $shiping_charges;
                                                    ?>
                                                    
                                                    
                                                    <div class="row-fcol1 tcol">
                                                        <div class="fcol-txt tcol">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                       {{ $list['qty'] }}
                                                                       <input type="hidden" name="qty[]" value="{{ $list['qty'] }}"/>
                                                                    </td>
                                                                </tr>   
                                                            </table>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row-fcol1 tship tship-st">
                                                        <div class="fcol-txt fcol">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                    	@if($list['is_price'] && $siteData['show_price'])
                                                                            @if ( isset($list['prodcut_location_type']) && $list['prodcut_location_type'] != '' )
                                                                                <p>${{ $list['price'] * $list['qty'] }}</p>
                                                                                <input type="hidden" name="total[]" value="${{ $list['price'] * $list['qty'] }}"/>
                                                                            @else
                                                                                <p>${{ number_format(getReissNewPriceFront($list['id'], ($list['price']+($list['price']*getSettingPrice())) ) * $list['qty'],2,'.','') }}</p>
                                                                                <input type="hidden" name="total[]" value="${{ getReissNewPriceFront($list['id'], ($list['price']+($list['price']*getSettingPrice())) ) * $list['qty'] }}"/>
                                                                            @endif
                                                                        @else
                                                                            N/A
                                                                        @endif
                                                                    </td>
                                                                </tr>   
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                        </div><!-- bottom rows -->
                                        	@endforeach
                                        @endif
                                        
                                    </div>
                                </div><!-- top headings sectio row -->
                            </div><!-- main content section -->
                            

                            @if( count($products) > 0  )
                            <div class="adc-last">
                            	<div class="adc-lst-main">
                                	<div class="lm-inr">
                                		@if($showPrice == 1 && $siteData['show_price'])
                                    	<div class="prc-total">
                                            <div class="t-carier g-total">
                                                <div class="tc-lft">
                                                    <h5>Sub Total</h5>
                                                </div>
                                                <div class="tc-rgt">
                                                    <h5><span>${{number_format($totalPrice, 2, '.', '')}}</span></h5>
                                                    <input type="hidden" name="totalprice" value="${{number_format($totalPrice, 2, '.', '')}}"/>
                                                </div>
                                            </div>
                                            <div class="t-carier g-total">
                                                <div class="tc-lft">
                                                    <h5>Shipping Charges</h5>
                                                </div>


                                                <div class="tc-rgt">
                                                    <h5><span>@if((session('billShip')['store_shiping'] == "shipping")) {{number_format($shiping_charges, 2, '.', '')}} @else {{number_format(0.00, 2, '.', '')}} @endif</span></h5>
                                                    <input type="hidden" name="shiping_charges" value="@if(!(session('billShip')['store_shiping'] == "shipping")) {{number_format($shiping_charges, 2, '.', '')}} @else {{number_format(0.00, 2, '.', '')}} @endif"/>
                                                </div>
                                            </div>
                                            <div class="t-carier g-total">
                                                <div class="tc-lft">
                                                    <h5>Tax</h5>
                                                </div>
                                                <div class="tc-rgt">
                                                    <h5><span>${{number_format($tax, 2, '.', '')}}</span></h5>
                                                    <input type="hidden" name="tax" value="${{number_format($tax, 2, '.', '')}}"/>
                                                </div>
                                            </div>
                                            
                                            <div class="t-carier g-total">
                                            	<div class="tc-lft">
                                                	<h5>Grand Total</h5>
                                                </div>
                                                <div class="tc-rgt">
                                                	<h5><span>${{number_format($grand, 2, '.', '')}}</span></h5>
                                                    <input type="hidden" name="grand_total[]" value="${{number_format($grand, 2, '.', '')}}"/>
                                                </div>
                                            </div><!-- grand total -->
                                        </div><!-- top price section -->
                                        @endif
                                        <div class="btm-btns">
                                        	<!--
                                        	<div class="c-shoping">
                                            	<a href="#">Continue&nbsp;Shopping</a>
                                            </div>
                                            -->
                                            <div class="chckout">
                                            	<a href="{{route('payment')}}">Confirm Order</a>
                                            	{{-- <a href="{{route('emailList')}}" id="send_mail">email</a> --}}

                                                
                                                <!--<input type="button" name="ac-chkout" value="Confirm Order">-->
                                            </div>
                                        </div><!-- buttons -->
                                    </div>
                                </div>
                            </div><!-- bottom last section -->

                            @endif
                            
                        </div><!-- Mycart order review sections -->
                	</div>
                </div>        
			</div>
            <!-- Mycart order review section ends -->
            
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->

<script>
    $(document).ready(function(){
        
        $("#send_mail").click(function(e){
            e.preventDefault();
            $("form[name='order_review']").submit();

        });
    })
</script>


  @endsection