@extends('front.layout')

@section('title')
    <title>Product Detail</title>
@endsection


@section('content')
@php
$u = auth('front')->user();
UserCookieHelperFunction(@$u->id);
@endphp
<style type="text/css">

    .slider {
        width: 100%;
        margin: 40px auto;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
      color: black;
    }


    .slick-slide {
      transition: all ease-in-out .3s;
      /* opacity: .2; */
    }
    /*
    .slick-active {
      opacity: .5;
    }

    .slick-current {
      opacity: 1;
    }
    */

  </style>

    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
            <!-- products Details section starts -->
            <div class="products-cont">
                <div class="n-wrapper">
                    <div class="pdetails-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{route('index')}}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">Products</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">Product Detail</a></li>
                            </ul>
                        </div><!-- bread navigation --> 
                        
                        <div class="pdetails-wrap">
                            <div class="pdetails-top">
                                <div class="pdt-left">

                                    <div class="prolarge-image">
                                        @if ( $product_type == 'local')
                                            @if(@$prodData->is_bulk == 1)

                                                @if (file_exists(public_path('bulk_upload_img').'/'.$prodData->ImageFile.'/'.$prodData->ImageFile.'.jpg' ) )
                                                    <a class="lrg-img-hrf" href="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'.jpg'}}" target="_blank">
                                                        <img class="lrg-img-src" src="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'.jpg'}}" alt="">
                                                        <span class="pr-zm-ico"></span>
                                                    </a>
                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$prodData->ImageFile.'/'.$prodData->ImageFile.'.png' ) )
                                                    <a class="lrg-img-hrf" href="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'.png'}}" target="_blank">
                                                        <img class="lrg-img-src" src="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'.png'}}" alt="">
                                                        <span class="pr-zm-ico"></span>
                                                    </a>

                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$prodData->ImageFile.'/'.$prodData->ImageFile.'-01.jpg' ) )
                                                    <a class="lrg-img-hrf" href="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-01.jpg'}}" target="_blank">
                                                        <img class="lrg-img-src" src="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-01.jpg'}}" alt="">
                                                        <span class="pr-zm-ico"></span>
                                                    </a>
                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$prodData->ImageFile.'/'.$prodData->ImageFile.'-01.JPG' ) )
                                                    <a class="lrg-img-hrf" href="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-01.JPG'}}" target="_blank">
                                                        <img class="lrg-img-src" src="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-01.JPG'}}" alt="">
                                                        <span class="pr-zm-ico"></span>
                                                    </a>
                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$prodData->ImageFile.'/'.$prodData->ImageFile.'-01.png' ) )
                                                    <a class="lrg-img-hrf" href="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-01.png'}}" target="_blank">
                                                        <img class="lrg-img-src" src="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-01.png'}}" alt="">
                                                        <span class="pr-zm-ico"></span>
                                                    </a>
                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$prodData->ImageFile.'/'.$prodData->ImageFile.'-01.PNG' ) )
                                                    <a class="lrg-img-hrf" href="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-01.PNG'}}" target="_blank">
                                                        <img class="lrg-img-src" src="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-01.PNG'}}" alt="">
                                                        <span class="pr-zm-ico"></span>
                                                    </a>
                                                @endif
                                            @else
                                                @if($prodData->product_image != null && $prodData->product_image != '')
                                                <a href="{{asset('products_images')}}/{{$prodData->product_image}}" target="_blank">
                                                    <img src="{{asset('products_images')}}/{{$prodData->product_image}}" alt="">
                                                    <span class="pr-zm-ico"></span>
                                                </a>
                                                {{-- @else --}}

                                                @endif
                                            @endif
                                        @else
                                            <a href="https://reisshardware.com/product_images/{{$prodData->sku}}.JPG" target="_blank">
                                                <img src="https://reisshardware.com/product_images/{{$prodData->sku}}.JPG" alt="">
                                                <span class="pr-zm-ico"></span>
                                            </a>
                                        @endif
                                        <!-- <img src="https://reisshardware.com/product_images/{{$prodData->sku}}.JPG" alt="" > -->
                                    </div>
                                    @if ( $product_type == 'local')
                                        @if(@$prodData->is_bulk == 1)
                                    <!-- slider starts here -->
                                    <div class="proimg-slider">
                                        <section class="regular slider">
                                            @for($i = 1; $i<=7; $i++)
                                                @if (file_exists(public_path('bulk_upload_img').'/'.$prodData->ImageFile.'/'.$prodData->ImageFile.'.jpg' ) )
                                                <div>
                                                    <a class="sml-img" href="javascript:void(0);" data-file="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'.jpg'}}" >
                                                        <img src="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'.jpg'}}" alt="">
                                                    </a>
                                                </div>
                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$prodData->ImageFile.'/'.$prodData->ImageFile.'.png' ) )
                                                <div>
                                                    <a class="sml-img" href="javascript:void(0);" data-file="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'.png'}}" >
                                                        <img src="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'.png'}}" alt="">
                                                    </a>
                                                </div>
                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$prodData->ImageFile.'/'.$prodData->ImageFile.'-0'.$i.'.jpg' ) )
                                                <div>
                                                    <a class="sml-img" href="javascript:void(0);" data-file="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-0'.$i.'.jpg'}}" >
                                                        <img src="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-0'.$i.'.jpg'}}" alt="">
                                                    </a>
                                                </div>
                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$prodData->ImageFile.'/'.$prodData->ImageFile.'-0'.$i.'.JPG' ) )
                                                <div>
                                                    <a class="sml-img" href="javascript:void(0);" data-file="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-0'.$i.'.JPG'}}" >
                                                        <img src="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-0'.$i.'.JPG'}}" alt="">
                                                    </a>
                                                </div>
                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$prodData->ImageFile.'/'.$prodData->ImageFile.'-0'.$i.'.png' ) )
                                                <div>
                                                    <a class="sml-img" href="javascript:void(0);" data-file="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-0'.$i.'.png'}}" >
                                                        <img src="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-0'.$i.'.png'}}" alt="">
                                                    </a>
                                                </div>
                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$prodData->ImageFile.'/'.$prodData->ImageFile.'-0'.$i.'.PNG' ) )
                                                <div>
                                                    <a class="sml-img" href="javascript:void(0);" data-file="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-0'.$i.'.PNG'}}" >
                                                        <img src="{{asset('bulk_upload_img')}}/{{$prodData->ImageFile}}/{{$prodData->ImageFile.'-0'.$i.'.PNG'}}" alt="">
                                                    </a>
                                                </div>
                                                @endif
                                            @endfor
                                        </section>
                                    </div>
                                    <!-- slider starts ends -->
                                        @endif
                                    @endif
                                </div><!-- image section/gallery section -->
                                
                                <div class="pdt-rigt">
                                    <h1>{{$prodData->name}}</h1>

                                    <h2>SKU: {{$prodData->sku }}</h2>


                                    @php
                                        $settings = getSetting();
                                    @endphp

                                    @if ( $settings['show_price'] == 1 && $settings['reiss_prod_price'] == 1 && $product_type == 'reiss' )
                                        {{-- <span>${{number_format(($prodData->price+($prodData->price*getSettingPrice())), 2, '.', '')}}</span> --}}
                                        <span>${{number_format(getReissNewPriceFront($prodData->id, ($prodData->price+($prodData->price*getSettingPrice())) ), 2, '.', '')}}</span>
                                    @endif

                                    @if ( $settings['show_price'] == 1 && $product_type == 'local' )
                                        <span>${{number_format($prodData->price, 2, '.', '')}}</span>
                                    @endif
                                    


                                    @if($product_type == 'local')
                                    <!-- Discription Section Starts -->
                                    <div class="product-dis">
                                        {!! $prodData->Description !!}
                                        
                                    </div>
                                    <!-- Discription Section Ends -->
                                    
                                    
                                    @endif
                                    <div class="qantity">
                                        <h5>Qty</h5>
                                        
                                        <div class="q-box">
                                            <div class="qbox-inr">
                                                <a href="javascript:void(0);" class="minus">-</a>
                                                <input type="text" min="1" id='qty' name="qnum" value="1" class="qtyVal" readonly>   
                                                <a href="javascript:void(0);" class="set-plus plus" >+</a>
                                            </div>
                                        </div>
                                    </div><!-- quantity box section -->
                                    
                                    <a href="javascript:void(0);" id="addToList" class="pa-set ">Add To Cart</a><!-- add to cart button / List -->
                                    <a href="#popup" class="popupDisplay pa-set pop-window" style="display: none;" onClick="return false;">Add To Cart</a><!-- add to cart button -->

                                    <!-- popup -->
                                    <div id="popup" class="alrt-popup">
                                        <a href="#" class="close"><img src="{{asset('front')}}/images/close1.png" class="btn_close" title="Close Window" alt="Close" /></a>
                                        <h5 class='addMessage'></h5>
                                        <div class="pop-up-btn-sec full-poprow">
                                            <a href="#" class="close btn-1">Continue Shopping</a>
                                            <a href="{{route('displayList')}}" class="btn-2">View Cart</a>
                                        </div>
                                    </div>
                                    
                                    <div id="mask"></div>
                                    <!-- popup -->
                                         
                                </div><!-- right detais section -->
                            </div><!-- product details top section image zoomer and buttons -->
                            
                            <div class="pdetails-bottom">
                                <div class="accordian">
                                    <div class="accordion_example smk_accordion acc_with_icon"> 

                                    @if($product_type == 'reiss')
                                        <!-- Section 1 -->

                                        <div class="accordion_in">
                                                <div class="acc_head">
                                                    Description
                                                </div>
                                                
                                                <div class="acc_content" style="display: none;">
                                                    <!-- List Section Starts -->
                                                    <div class="nlistWrapper">
                                                        
                                                        <!-- Repeatable Row Starts -->
                                                        <div class="nl-row">
                                                            <span>Manufacturer:</span>&nbsp; {{$prodData->manufacturer }}
                                                        </div>
                                                        <!-- Repeatable Row Ends -->

                                                        <!-- Repeatable Row Starts -->
                                                        <div class="nl-row">
                                                            <span>Manufacturer Code:</span>&nbsp; {{$prodData->manufacturer_code }}
                                                        </div>
                                                        <!-- Repeatable Row Ends -->

                                                        <!-- Repeatable Row Starts -->
                                                        <div class="nl-row">
                                                            <span>SKU:</span>&nbsp; {{$prodData->sku }}
                                                        </div>
                                                        <!-- Repeatable Row Ends -->

                                                        <!-- Repeatable Row Starts -->
                                                        <div class="nl-row">
                                                            <span>UPC:</span>&nbsp; {{$prodData->upc }}
                                                        </div>
                                                        <!-- Repeatable Row Ends -->

                                                        <!-- Repeatable Row Starts -->
                                                        <div class="nl-row">
                                                            <span>Package Quantity:</span>&nbsp; {{$prodData->package_quantity }}
                                                        </div>
                                                        <!-- Repeatable Row Ends -->

                                                        <!-- Repeatable Row Starts -->
                                                        <div class="nl-row">
                                                            <span>Case Quantity:</span>&nbsp; {{$prodData->case_quantity }}
                                                        </div>
                                                        <!-- Repeatable Row Ends -->
                                                        
                                                        
                                                    </div>
                                                    <!-- List Section Ends -->
                                                </div>
                                            </div>
                                            
                                        {{-- <div class="accordion_in">
                                            <div class="acc_head">
                                                Disclaimer
                                            </div>
                                            <div class="acc_content" style="display: none;">
                                                Package sizes are for reference only. All prices listed are per each. Not responsible for typographical errors. Products may vary from illustration. Prices can only be viewed once a registered customer logs in. Prices are subject to change without notice. Call our office to confirm product quantity on hand and availability.
                                            </div>
                                        </div> --}}
                                    @else 
                                        @if(@$prodData->is_bulk)
                                            <div class="accordion_in">
                                                <div class="acc_head">
                                                    Description
                                                </div>
                                                <div class="acc_content" style="display: none;">
                                                <!-- List Section Starts -->
                                                <div class="nlistWrapper">
                                                    
                                                    <!-- Repeatable Row Starts -->
                                                    <div class="nl-row">
                                                        <span>Part/Item Number:</span>&nbsp; {{$prodData->ItemNumber }}
                                                    </div>
                                                    <!-- Repeatable Row Ends -->

                                                    <!-- Repeatable Row Starts -->
                                                    <div class="nl-row">
                                                        <span>SKU:</span>&nbsp; {{$prodData->sku }}
                                                    </div>
                                                    <!-- Repeatable Row Ends -->

                                                    <!-- Repeatable Row Starts -->
                                                    <div class="nl-row">
                                                        <span>UPC:</span>&nbsp; {{$prodData->upc }}
                                                    </div>
                                                    <!-- Repeatable Row Ends -->

                                                    <!-- Repeatable Row Starts -->
                                                    <div class="nl-row">
                                                        <span>Material:</span>&nbsp; {{$prodData->BulletPointsMaterial }}
                                                    </div>
                                                    <!-- Repeatable Row Ends -->

                                                    <!-- Repeatable Row Starts -->
                                                    <div class="nl-row">
                                                        <span>Color:</span>&nbsp; {{$prodData->BulletPointsColor }}
                                                    </div>
                                                    <!-- Repeatable Row Ends -->
                                                    <!-- Repeatable Row Starts -->
                                                    <div class="nl-row">
                                                        <span>Closure:</span>&nbsp; {{$prodData->BulletPointsClosure }}
                                                    </div>
                                                    <!-- Repeatable Row Ends -->
                                                    <!-- Repeatable Row Starts -->
                                                    <div class="nl-row">
                                                        <span>Pockets:</span>&nbsp; {{$prodData->BulletPointsPockets }}
                                                    </div>
                                                    <!-- Repeatable Row Ends -->
                                                    <!-- Repeatable Row Starts -->
                                                    <div class="nl-row">
                                                        <span>Minimum Order:</span>&nbsp; {{$prodData->MinimumOrder }}
                                                    </div>
                                                    <!-- Repeatable Row Ends -->
                                                    <!-- Repeatable Row Starts -->
                                                    {{-- <div class="nl-row">
                                                        <span>Average Shipping Cost:</span>&nbsp; {{$prodData->AverageShippingCost }}
                                                    </div> --}}
                                                    <!-- Repeatable Row Ends -->
                                                    <!-- Repeatable Row Starts -->
                                                   {{--  <div class="nl-row">
                                                        <span>Ships Via:</span>&nbsp; {{$prodData->ShipsVia }}
                                                    </div> --}}
                                                    <!-- Repeatable Row Ends -->
                                                    <!-- Repeatable Row Starts -->
                                                    {{-- <div class="nl-row">
                                                        <span>Usually Ships In:</span>&nbsp; {{$prodData->UsuallyShipsIn }}
                                                    </div> --}}
                                                    <!-- Repeatable Row Ends -->
                                                    
                                                    <!-- Repeatable Row Starts -->
                                                    {{-- <div class="nl-row">
                                                        <span>Shipping Box Dimensions:</span>&nbsp; {{$prodData->ShippingBoxDimensions }}
                                                    </div> --}}
                                                    <!-- Repeatable Row Ends -->
                                                    
                                                    <!-- Repeatable Row Starts -->
                                                    {{-- <div class="nl-row">
                                                        <span>Ship Weight (lbs):</span>&nbsp; {{$prodData->ShipWeight }}
                                                    </div> --}}
                                                    <!-- Repeatable Row Ends -->
                                                    
                                                    <!-- Repeatable Row Starts -->
                                                    {{-- <div class="nl-row">
                                                        <span>Tariff Code:</span>&nbsp; {{$prodData->TariffCode }}
                                                    </div> --}}
                                                    <!-- Repeatable Row Ends -->
                                                    
                                                    <!-- Repeatable Row Starts -->
                                                    <div class="nl-row">
                                                        <span>Standard:</span>&nbsp; {{$prodData->Standard }}
                                                    </div>
                                                    <!-- Repeatable Row Ends -->
                                                    
                                                </div>
                                                <!-- List Section Ends -->
                                                </div>
                                            </div>
                                        @else
                                            <div class="accordion_in">
                                                <div class="acc_head">
                                                    Description
                                                </div>
                                                
                                                <div class="acc_content" style="display: none;">
                                                    <!-- List Section Starts -->
                                                    <div class="nlistWrapper">
                                                        
                                                        <!-- Repeatable Row Starts -->
                                                        <div class="nl-row">
                                                            <span>Manufacturer:</span>&nbsp; {{$prodData->manufacturer }}
                                                        </div>
                                                        <!-- Repeatable Row Ends -->

                                                        <!-- Repeatable Row Starts -->
                                                        <div class="nl-row">
                                                            <span>Manufacturer Code:</span>&nbsp; {{$prodData->manufacturer_code }}
                                                        </div>
                                                        <!-- Repeatable Row Ends -->

                                                        <!-- Repeatable Row Starts -->
                                                        <div class="nl-row">
                                                            <span>SKU:</span>&nbsp; {{$prodData->sku }}
                                                        </div>
                                                        <!-- Repeatable Row Ends -->

                                                        <!-- Repeatable Row Starts -->
                                                        <div class="nl-row">
                                                            <span>UPC:</span>&nbsp; {{$prodData->upc }}
                                                        </div>
                                                        <!-- Repeatable Row Ends -->

                                                        <!-- Repeatable Row Starts -->
                                                        <div class="nl-row">
                                                            <span>Package Quantity:</span>&nbsp; {{$prodData->package_quantity }}
                                                        </div>
                                                        <!-- Repeatable Row Ends -->

                                                        <!-- Repeatable Row Starts -->
                                                        <div class="nl-row">
                                                            <span>Case Quantity:</span>&nbsp; {{$prodData->case_quantity }}
                                                        </div>
                                                        <!-- Repeatable Row Ends -->
                                                        
                                                        
                                                    </div>
                                                    <!-- List Section Ends -->
                                                </div>
                                            </div>

                                        @endif
                                    @endif
                                    </div>
                                </div>
                            </div><!-- product details bottom/accordian section  -->
                        </div><!-- main content section -->
                    </div>  
                </div>       
            </div>
            <!-- products Details section ends -->
        </div>
        <!-- Subpage Structure Ends -->
    </div>
    <!-- Site Body Ends Here -->

    <script type="text/javascript">

        $(document).ready(function(){
            $(document).on('click','.sml-img', function(){
                var imgpath = $(this).attr('data-file');
                $('.lrg-img-src').attr('src',imgpath);
                $('.lrg-img-hrf').attr('href',imgpath);
            });


            $('.acc_head').click();
        });

        $(document).ready(function(){

            $.ajaxSetup({headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });

            $(document).on('click', '#addToList', function(){
                
                var qty = $('#qty').val();

                $.ajax({
                    url: '{{route('addToList', $prodData->id)}}',
                    type: 'GET',
                    data: {qty: qty, type: '{{$product_type}}'},
                })
                .done(function(result){
                    $('.addMessage').text(result);
                    $('.popupDisplay').click();
                    console.log('done', result);
                })
                .fail(function(){
                    console.log("error");
                })
                .always(function(){
                    console.log("complete");
                });
            });

            $(document).on('click', '.minus', function(){
                
                var val = $(this).parent().find('.qtyVal').val();
                
                if ( val > 1 )
                    val = parseInt(val) - 1;

                $(this).parent().find('.qtyVal').val(val);
            });

            $(document).on('click', '.plus', function(){
                
                var val = $(this).parent().find('.qtyVal').val();
                val = parseInt(val) + 1;
                $(this).parent().find('.qtyVal').val(val);
            });
        });

    </script>

@endsection