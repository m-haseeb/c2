@extends('front.layout')

@section('title')
    <title>FAQ's</title>
    @endsection

@section('pageScript')
    <!-- Page Description Starts -->
    <script type="text/javascript">
        var pageName = "faquestions";
        var subpageName = "none";
    </script>
    <!-- Page Description Ends -->
@endsection

@section('content')

<body class="blue-green subpage"> <!-- blue-green -->

    <!-- Header Starts -->
    @include("front.include.header")
    <!-- Header End -->

    <!-- Page Header -->
    <div class="page-header style-9">
        <div class="container">
            <h2 class="page-title">FAQs</h2>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);">&nbsp;</a></li>
                <!-- <li class="active">Pricing</li> -->
            </ol>
        </div>
    </div>
    <!-- Page Header End -->

    <div class="cps-main-wrap">

        <!-- FAQ Section Starts -->
        <div class="cps-section faq-section-padding faq-settings" id="faq">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-xs-12">
                        <div class="cps-section-header text-center">
                            <h3 class="cps-section-title">Questions? We've got answers</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-xs-12">
                        <div class="cps-faq-accordion">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Do you really set everything up?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            Yes! Your account manager takes care of everything! From setting up the website to uploading your content. We’ll take care of your web needs while you focus on your business.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Is there any setup fee?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            No. There are no hidden costs or setup fees. 
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Can I keep my own domain name?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            Yes, if you already have a domain name you can still use it!
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFour">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Can I stop service at any time?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                        <div class="panel-body">
                                            Yes, if you’re not 100% satisfied with your website you can cancel anytime. There’s even a money-back guarantee for your first 30 days of service.
                                        </div>
                                    </div>
                                </div>


                                
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFive">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Is there a long-term commitment?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseFive">
                                        <div class="panel-body">
                                            No! Your service is month-to-month, so you never have to feel locked into a contract. 
                                        </div>
                                    </div>
                                </div>


                                
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingSix">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">What if I want to do more with my site?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseSix">
                                        <div class="panel-body">
                                            If you want to upgrade your package you can let us know at any time. We’ll be happy to help you unlock more features to grow your business.
                                        </div>
                                    </div>
                                </div>


                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingSeven">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">Can you tell me more about your product catalog?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseSeven">
                                        <div class="panel-body">
                                            Our product catalog contains 12,000 products. It comes complete with images and product descriptions. If you enable this feature, you’ll have a full inventory to display on your website, without having to lift a finger!
                                        </div>
                                    </div>
                                </div>


                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingEight">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">Do I need a webhost?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseEight">
                                        <div class="panel-body">
                                            XcelHub will handle all your hosting and domain needs at no extra cost to you. We even install a free SSL certificate to help protect your website. 
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FAQ Section Ends -->

        <!-- Subscription -->
        <div class="cps-section cps-section-padding cps-theme-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="cps-section-header text-center font-adjust" style="margin-bottom:10px;">
                            <h3 class="cps-section-title">Want to Reach New Customers?</h3>
                            <p style="padding:16px 0px;">We can have your website running in days!</p>
                            <a class="btn btn-white" href="{{route('getstarted')}}">Get Your Site Now</a>
                            <p style="padding-top: 3px;">or <a href="{{route('contact')}}" class="link-white">Get More Info</a></p>
                            <!-- <p class="cps-section-text">Subscribe us if you are willing to get acknowledge what we are doing. Know about the most update work and many more. We will not share your email with any third party or will not make any spam mail</p> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Subscription End -->

    </div>

    <!-- Footer Starts -->
    @include("front.include.footer")
    <!-- Footer Ends -->

<script>
	//this function runs every time you are scrolling

$(window).scroll(function() {
    var top_of_element = $(".pregular").offset().top;
    var startScroll = $(".pregular").offset().top + 100; /* 250 */
    var maxScroll = ($(".pregular").offset().top + $(".pregular").height()) - 240; /* 432 */
    var bottom_of_element = $(".pregular").offset().top + $(".pregular").outerHeight();
    var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
    var top_of_screen = $(window).scrollTop();

    if((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
        // The element is visible, do something
        console.log('in view');
        console.log(maxScroll);
        console.log(startScroll);
        if($(window).scrollTop() >= startScroll && $(window).scrollTop() < maxScroll ){
        	//start
        	
        	setTimeout(function(){ 
        		//$('.pfixed').fadeIn('fast');
        		//$('.pfixed').addClass('sticky');
        	}, 500);
        	if($('.sticky').length){
        		$('.sticky').css('top',(($(window).scrollTop()-$(".pregular").offset().top)+60)+'px');
        	}

        } else {
        	// Ends
        	setTimeout(function(){ 
        		//$('.pfixed').hide();
        		//$('.pfixed').removeClass('sticky');
        	}, 500);
        }
    }
    else {
        // The element is not visible, do something else
        console.log('Not in view');
    }
});
</script>

</body>
@endsection