@extends('front.layout')

    @section('content')

    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
        
            <!-- Mycart add to cart section starts -->
            <div class="mycart-cont">
            	<div class="n-wrapper">
                	<div class="mycart-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{route('index')}}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">My&nbsp;List</a></li>
                            </ul>
                        </div><!-- bread navigation -->
                        
                	 	<div class="mc-addcart">
                            <div class="check-thead chck-set">
                                <h1>My List</h1>
                            </div><!-- top heading -->
                            
                        
                            <div class="adcart-cntnt">
                                <div class="adc-outr">
                                    <div class="adc-inr">
                                        <div class="adc-main">
                                            <div class="adc-inr-wrap">
                                                <div class="adc-row">
                                                
                                                    <div class="thw-img1">
                                                        <h1>Product Details</h1>
                                                    </div>
                                                    
                                                    <div class="thw-price1">
                                                        <h1>Unit Price</h1>
                                                    </div>
                                                    <div class="thw-qty1">
                                                        <h1>Qty</h1>
                                                    </div>
                                                    <div class="thw-action1">
                                                        <h1>Action</h1>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div><!-- top headings row -->
                                        
                                        <div class="adc-main row-btm">
                                            <div class="adc-inr-wrap">
                                            @php
                                            $showPrice =1;
                                            $totalPrice = 0.0;
                                            $siteData = getSetting();
                                            @endphp
                                            @if(count($lData) > 0)
                                                @foreach($lData as $list)
                                                <div class="adc-row row-set f-row item{{$list->listId}}">
                                                    <div class="row-fcol1">
                                                        <div class="fcol-img">
                                                          <img src="https://reisshardware.com/product_images/{{$list->sku}}.JPG" alt="">
                                                        </div>
                                                        
                                                        <div class="fcol-txt1">
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        
                                                                        <p>{{$list->name}}</p>
                                                                    </td>
                                                                </tr>   
                                                            </table>
                                                        </div>
                                                    </div>
                                                    @php
                                                    if($list->is_price == 0){
                                                        $showPrice = 0;
                                                    }
                                                    @endphp
                                                    
                                                    <div class="row-fcol1 ship">
                                                        <div class="fcol-txt scol">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                    @if($list->is_price && $siteData['show_price'])
                                                                        <p class="calculatePrice" data-price={{number_format($list->price,2,'.','')}} data-cartId="{{$list->listId}}" >${{number_format($list->price,2,'.','')}}</p>
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                    </td>
                                                                </tr>   
                                                            </table>
                                                        </div>
                                                    </div>
                                                    @php
                                                    $totalPrice += ($list->price*$list->qty);
                                                    @endphp
                                                    <div class="row-fcol1 tcol">
                                                        <div class="fcol-txt tcol">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <div class="q-box">
                                                                            <div class="qbox-inr">
                                                                                <a href="javascript:void(0);" data-price={{number_format($list->price,2,'.','')}} data-total="{{$list->price*$list->qty}}"  data-cartId="{{$list->listId}}" class="minus qty">-</a>
                                                                                <input type="text"  min="1" id="qty{{$list->listId}}" name="qty" value="{{$list->qty}}" class="qtyVal" readonly>   
                                                                                <a href="javascript:void(0);" data-price={{number_format($list->price,2,'.','')}} data-total="{{$list->price*$list->qty}}" data-cartId="{{$list->listId}}" class="set-plus plus qty" >+</a>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>   
                                                            </table>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                
                                                    <div class="row-fcol1 fifcol">
                                                        <div class="fcol-txt fifcol">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <p style="padding-left:30px;"><a href="javascript:void(0);" class="remove-from-list" data-price={{number_format($list->price,2,'.','')}} data-listId="{{$list->listId}}" ><img src="{{asset('front')}}/images/mycart-action.jpg" alt=""></a></p>
                                                                    </td>
                                                                </tr>   
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div><!-- rows -->
                                                @endforeach
                                            @else
                                            <div class="adc-row row-set f-row">
                                                Your cart is empty.
                                            </div>
                                            @endif
                                            </div>
                                        </div><!-- bottom rows -->
                                        
                                        
                                    </div>
                                </div><!-- top headings sectio row -->
                            </div><!-- main content section -->
                            @if( count($lData) > 0  )
                            <div class="adc-last cart-btn">
                                @if($showPrice == 1 && $siteData['show_price'])
                                <div class="ttl-sec">
                                    <h5>Total</h5>
                                    <p>$<span class="totalPrice">{{number_format($totalPrice,2,'.','')}}</span></p>

                                </div>
                                @endif
                            	<div class="adc-lst-main">
                                	<div class="lm-inr">
                                    	<div class="btm-btns">
                                        	<div class="c-shoping">
                                            	<a style="display: none;" class="sent-email" href="{{route('emailList')}}">Email</a>
                                                <a href="#popup" class=" showEmailPopup pa-set pop-window"  onClick="return false;">Email</a><!-- add to cart button -->
                                                {{-- {{route('emailList')}} --}}
                                            </div>
                                            
                                            <div class="chckout">
                                            	<!--<input type="button" name="ac-chkout" value="CheckOut">-->
                                                <a href="{{route('downloadPdf')}}">Print</a>
                                            </div>
                                        </div><!-- buttons -->
                                    </div>
                                </div>
                            </div><!-- bottom last section -->
                            @endif
                    
                        </div><!-- Add to cart sections -->
                	</div>
                </div>        
			</div>
            <!-- Mycart add to cartr section ends -->
            
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->


<!-- popup -->
<div id="popup" class="alrt-popup new-ppup">
    <a href="javascript:void(0);" class="close sendEmailclose"><img src="{{asset('front')}}/images/close_pop_blue.png" class="btn_close" title="Close Window" alt="Close" /></a>
    <h5 class='addMessage'>Please Enter Your Name and Email Address</h5>
    <div class="fld-sec">
       <input type="text" value="" name="uname" id="name" placeholder="Name" />
       <div class="error"><p id="nameerror"></p></div> 
       <input type="text" value="" name=uemail id="email" placeholder="Email" /> 
       <div class="error"><p id="emailerror"></p></div>
       <input type="button" class="send-btn" value="Send">
    </div>
</div>
<div id="mask"></div>


<a href="#popup2" style="display: none;" class="success-msg pa-set pop-window2"  onClick="return false;">Email</a><!-- add to cart button -->
<div id="popup2" class="alrt-popup2 new-ppup">
    <a href="javascript:void(0);" class="close"><img src="{{asset('front')}}/images/close_pop_blue.png" class="btn_close" title="Close Window" alt="Close" /></a>
    <h5 class='add-Message'>Your email has been sent successfully.</h5>
</div>
<div id="mask2"></div>
<!-- popup -->

<script type="text/javascript">
function calculateTotalPrice(){
    var calculatePrice = $('.calculatePrice');
    var TotalPrice = 0.00;
    $.each(calculatePrice,function(i,v){
        var cart_id = $(this).attr('data-cartId');
        var price = $(this).attr('data-price');
        var qty = $('#qty'+cart_id).val();
        TotalPrice += (qty*price);
    });
    return TotalPrice;
}

</script>

<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function(){

    $(document).on('click','.minus',function(){
        var val = $(this).parent().find('.qtyVal').val();
        if(val>1){
            val = parseInt(val)-1;
            $(this).parent().find('.qtyVal').val(val);
            var tPrice = calculateTotalPrice();
            $('.totalPrice').text(tPrice.toFixed(2));
        }
    });
    $(document).on('click','.plus',function(){
        var val = $(this).parent().find('.qtyVal').val();
        
            val = parseInt(val)+1;
            $(this).parent().find('.qtyVal').val(val);
            var tPrice = calculateTotalPrice();
            $('.totalPrice').text(tPrice.toFixed(2));
    
    });



    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test(email);
    }
    $(document).on('click','.showEmailPopup', function(){
        $('#nameerror').text('');
        $('#emailerror').text('');
        $('#name').val('');
        $('#email').val('');
    });
    $(document).on('click','.send-btn', function(){
        var name = $('#name').val();
        var email = $('#email').val();
        var error = 0;
        if(name.length <= 0){
            error = 1;
            $('#nameerror').text('This field is required.');
        }else{
            $('#nameerror').text('');

        }

        if(email.length <= 0) {
            error = 1;
            $('#emailerror').text('This field is required.');
        }else if(!validateEmail(email)) {
            error = 1;
            $('#emailerror').text('Please enter the valid email address.');
        }else {
            $('#emailerror').text('');
        }

        if(error == 0){
            $.ajax({
                url: '{{route('emailList')}}',
                type: 'GET',
                data: {name: name, email: email},
            })
            .done(function(result) {
                $('.sendEmailclose').click();
                $('.success-msg').click();
                $('#nameerror').text('');
                $('#emailerror').text('');
                $('#name').val('');
                $('#email').val('');
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
            
        }
    });
});
$(document).ready(function(){
    $(document).on('click','.qty',function(){
        var cartId = $(this).attr('data-cartId');
        var qty = $('#qty'+cartId).val();
        $.ajax({
            url: '{{route('updateQty')}}',
            type: 'GET',
            aysnc: false,
            data: {qty: qty, cartId :cartId },
        })
        .done(function(result) {
            $('.addMessage').text(result);
            $('.popupDisplay').click();
            //console.log(result);
            
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    });
});

$(document).ready(function(){
    $(document).on('click','.remove-from-list', function (){
        var listId = $(this).attr('data-listId');
        var price = $(this).attr('data-price');
        var qty = $('#qty'+listId).val();
        var totalPrice = $('.totalPrice').text();
        totalPrice = parseFloat(totalPrice);
        qty = parseInt(qty);
        price = parseFloat(price);

        totalPrice -= (qty*price);

        $.ajax({
            url: '{{route('removeItem')}}',
            type: 'GET',
            data: {listId :listId },
        })
        .done(function(result) {
            //console.log(result);
            if(result > 0){
                $('.totalPrice').text(totalPrice.toFixed(2));
                $('.item'+listId).remove();
            }else{
                $('.item'+listId).text('Your cart is empty.');
                $('.cart-btn').hide();
            }

        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    });
});

</script>


<!-- plus minus ends -->


  @endsection