@extends('front.layout')

@section('title')
    <title>{{$pData->name}}</title>
    @endsection

 @section('content')
    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
        
            <!-- news section starts -->
            <div class="news-cont">
            	<div class="n-wrapper">
                	<div class="news-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{route('index')}}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0)">{{$pData->name}}</a></li>
                            </ul>
                        </div><!-- news top bread navigation -->
                        
                        <div class="np-desc">
                        	<h5>{{$pData->title}}</h5>
                            @php
                            $des = str_replace("../../",asset('/'),$pData->description);
                            echo $des;
                            @endphp
                        </div><!--news page description section -->
                        
                        <div class="np-artcls">
                        	<div class="npa-cntnt">
                            	<div class="npac-row">
                                	<img src="{{asset('pages_images/'.$pData->image)}}"/>	
                                </div><!-- article rows -->
                                
                                
                            </div>
                        </div><!--news  articles section -->
                	</div>
                    
					 
                </div>       
			</div>
            <!-- news section ends -->
            
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->
    
@endsection