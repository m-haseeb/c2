
<!-- Slider Ends -->
@extends('front.layout')

    @section('content')

<!-- Slider Starts -->
<!-- bxSlider Javascript file -->
<script src="{{asset('front')}}/js/boxslider/jquery.bxslider.min.js"></script>
<!-- bxSlider CSS file -->
<link href="{{asset('front')}}/js/boxslider/jquery.bxslider.css" rel="stylesheet" />


    <script type="text/javascript">

$(document).ready(function(){
  $('.bxslider2').bxSlider({
        auto: true,
        pause: 5000,
        mode: 'fade',
        autoStart: true
    });
});

$(window).load(function(){
    if($('.mheight').lenght){
        $.fn.setAllToMaxHeight=function(){
            return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
            }
            $('.mheight').setAllToMaxHeight();
    }
});
</script>

    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
       
        <!-- Top Banner Slider Starts -->
        <div class="main-banner-rp main-brand">
            
            <div class="frt-wrapper">	
                <h2>{{$pData->title}}</h2>
                <p>
                @php
                $des = str_replace("../../",asset('/'),$pData->description);
                echo $des;
                @endphp
                </p>
            </div>
        </div>
        <!-- Top Banner Slider Ends -->
        
        
        
        
        <!-- Featuerd products seciton begins here -->
        <div class="n-container">
        	<div class="frt-wrapper">
            	<div class="crl-rp">
                
                	<div class="frt-hd">
                    	<h2>{{$pData->name}}</h2>
                    </div>
                    
                    <div id="owl-demo" class="owl-carousel" style="float:left;">
                        <div class="item crl-pro-item mheight">
                        	<div class="frt-thm">
                                <table border="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <a href="javascript:void(0);"><img class="lazyOwl" data-src="{{asset('front')}}/images/logo-here.png" alt="Lazy Owl Image"></a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            
                        </div>
                        <div class="item crl-pro-item mheight" >
                        	<div class="frt-thm">
                                <table border="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <a href="javascript:void(0);"><img class="lazyOwl" data-src="{{asset('front')}}/images/logo-here.png" alt="Lazy Owl Image"></a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--<p>
                            	<a href="javascript:void(0);">Sample Text Here</a>
                                <br>
                            	<span>$00.00</span>
                            </p>-->
                        </div>
                        <div class="item crl-pro-item mheight">
                        	<div class="frt-thm">
                                <table border="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <a href="javascript:void(0);"><img class="lazyOwl" data-src="{{asset('front')}}/images/logo-here.png" alt="Lazy Owl Image"></a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            
                        </div>
                        <div class="item crl-pro-item mheight">
                        	<div class="frt-thm">
                                <table border="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <a href="javascript:void(0);"><img class="lazyOwl" data-src="{{asset('front')}}/images/logo-here.png" alt="Lazy Owl Image"></a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            
                        </div>
                        <div class="item crl-pro-item mheight">
                        	<div class="frt-thm">
                                <table border="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <a href="javascript:void(0);"><img class="lazyOwl" data-src="{{asset('front')}}/images/logo-here.png" alt="Lazy Owl Image"></a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            
                        </div>
                        <div class="item crl-pro-item mheight">
                        	<div class="frt-thm">
                                <table border="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <a href="javascript:void(0);"><img class="lazyOwl" data-src="{{asset('front')}}/images/logo-here.png" alt="Lazy Owl Image"></a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            
                        </div>
                        <div class="item crl-pro-item mheight">
                        	<div class="frt-thm">
                                <table border="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <a href="javascript:void(0);"><img class="lazyOwl" data-src="{{asset('front')}}/images/logo-here.png" alt="Lazy Owl Image"></a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            
                        </div>
                        <div class="item crl-pro-item mheight">
                        	<div class="frt-thm">
                                <table border="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <a href="javascript:void(0);"><img class="lazyOwl" data-src="{{asset('front')}}/images/logo-here.png" alt="Lazy Owl Image"></a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Featuerd products seciton ends here -->
                
    </div>
    <!-- Site Body Ends Here -->
    
    

<!-- Carousel js and css begins here -->
<script src="{{asset('front')}}/js/owl.carousel.js"></script>
<link href="{{asset('front')}}/assets/owl-carousel/owl.carousel.css" rel="stylesheet">
<script>
$(document).ready(function() {

  $("#owl-demo").owlCarousel({
	items : 4,
	lazyLoad : true,
	navigation : true,
	autoPlay : 2000,
	paginationSpeed : 500
  });

});
$(window).load(function() {
	
	if($('.crl-pro-item').length){
		$.fn.seAllheight = function(){
			return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
		}
		$('.crl-pro-item').seAllheight();
	}
	
});
</script> 
<!-- Carousel js and css ends here -->

@endsection