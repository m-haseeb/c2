@extends('front.layout')

    @section('content')

    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
        
            <!-- products Details section starts -->
            <div class="products-cont">
                <div class="n-wrapper">
                    <div class="pdetails-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{route('index')}}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">Products</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">Product&nbsp;Detail</a></li>
                            </ul>
                        </div><!-- bread navigation --> 
                        
                        <div class="pdetails-wrap">
                            <div class="pdetails-top">
                                <div class="pdt-left">
                                     <a href="https://reisshardware.com/product_images/{{$prodData->sku}}.JPG" target="_blank">
                                        <img src="https://reisshardware.com/product_images/{{$prodData->sku}}.JPG" alt="" >
                                        <span class="pr-zm-ico"></span>
                                    </a>
    
                                </div><!-- image section/gallery section -->
                                
                                <div class="pdt-rigt">
                                
                                    <h1>{{$prodData->name}}</h1>
                                    {{-- <h2>Product ID<div>:</div> {{$prodData->sku}}</h2> --}}
                                    @php 
                                    $siteData = getSetting();
                                    @endphp
                                    @if($siteData['show_price'] && $prodData->is_price)
                                    <span>${{number_format($prodData->price,'2','.','')}}</span>
                                    @endif
                                    
                                    <div class="qantity">
                                        <h5>Qty</h5>
                                        
                                        <div class="q-box">
                                            <div class="qbox-inr">
                                                <a href="javascript:void(0);" class="minus">-</a>
                                                <input type="text" min="1" id='qty' name="qnum" value="1" class="qtyVal" readonly>   
                                                <a href="javascript:void(0);" class="set-plus plus" >+</a>
                                            </div>
                                        </div>
                                    </div><!-- quantity box section -->
                                    
                                    <!--<div class="prgt-btn">
                                        <a href="05-4-reprographics-request-price-quote.php">Request&nbsp;Pricing</a>
                                    </div> request pricing button -->
                                    <a href="javascript:void(0);" id="addToList" class="pa-set ">Add To List</a><!-- add to cart button -->
                                    
                                    <a href="#popup" class="popupDisplay pa-set pop-window" style="display: none;" onClick="return false;">Add&nbsp;To&nbsp;List</a><!-- add to cart button -->
                                    {{-- <div class="prgt-btn">
                                        <a href="#">PDF&nbsp;Download</a>
                                    </div><!-- PDF download button -->
                                     --}}
                                    <!--<form method="post" name="add-cart">
                                        <!--<a href="#" class="pa-set">Add&nbsp;To&nbsp;Cart</a>-->
                                        <!--<input id="addcart" type="button" name="addcart" value="Add To Cart">
                                    </form><!-- add to cart button -->
                                    
                                   
                                     <!-- popup -->
                                        <div id="popup" class="alrt-popup">
                                            <a href="#" class="close"><img src="{{asset('front')}}/images/close_pop_blue.png" class="btn_close" title="Close Window" alt="Close" /></a>
                                            <h5 class='addMessage'></h5>
                                            <div class="pop-up-btn-sec">
                                                <a href="#" class="close btn-1">Continue Shopping</a>
                                                <a href="{{route('displayList')}}" class="btn-2">View List</a>

                                            </div>
                                        </div>
                                        <div id="mask"></div>
                                    <!-- popup -->
                                         
                                </div><!-- right detais section -->
                            </div><!-- product details top section image zoomer and buttons -->
                            
                            <div class="pdetails-bottom">
                                <div class="accordian">
                                    <div class="accordion_example smk_accordion acc_with_icon">     
                                        <!-- Section 1 -->
                                        <div class="accordion_in">
                                            <div class="acc_head">
                                                Disclaimer
                                            </div>
                                            <div class="acc_content" style="display: none;">
                                                Package sizes are for reference only. All prices listed are per each. Not responsible for typographical errors. Products may vary from illustration. Prices can only be viewed once a registered customer logs in. Prices are subject to change without notice. Call our office to confirm product quantity on hand and availability.
                                            </div>
                                        </div>
                    
                                       
                                    </div>
                                </div>
                            </div><!-- product details bottom/accordian section  -->
                        </div><!-- main content section -->
                    </div>  
                </div>       
            </div>
            <!-- products Details section ends -->
            
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->
    
<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function(){
    $(document).on('click','#addToList',function(){
        var qty = $('#qty').val();
        $.ajax({
            url: '{{route('addToList',$prodData->id)}}',
            type: 'GET',
            data: {qty: qty},
        })
        .done(function(result) {
            $('.addMessage').text(result);
            $('.popupDisplay').click();
            //console.log(result);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    });
});

</script>

<script type="text/javascript">
$(function(){
	$(document).on('click','.minus',function(){
		var val = $(this).parent().find('.qtyVal').val();
		if(val>1){
			val = parseInt(val)-1;
			$(this).parent().find('.qtyVal').val(val);
		}
	});
	$(document).on('click','.plus',function(){
		var val = $(this).parent().find('.qtyVal').val();
		
			val = parseInt(val)+1;
			$(this).parent().find('.qtyVal').val(val);
	
	});
});

</script>
<!-- plus minus ends -->
@endsection