@extends('front.layout')

    @section('content')


<!-- Left Menu Css & Js -->
<link rel="stylesheet" href="{{asset('front')}}/css/styles.css">
<script src="{{asset('front')}}/js/script.js"></script>
<!-- Left Menu Css & Js -->


    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">

            <!-- products listing section starts -->
            <div class="products-cont">
            	<div class="n-wrapper">
                	<div class="plisting-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{route('index')}}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">Products</a></li>
                            </ul>
                        </div><!-- bread navigation -->
                        
                        <!-- New Section Starts -->
                        <div class="new-pro-wrap">
                            
                            
                            <!-- Left Section Starts -->
                            <div class="npro-left">
                                
                                <h2>Category</h2>
                                
                                <!-- Left Menu Accordion Begins -->
                                <div id='cssmenu'>
                                    <ul>

                                        @php
                                        $mainCat = '';
                                        use App\StoreCategory;
                                        $mainCat = StoreCategory::where(['level' => 1, 'isActive'=>1]);
                                        $siteData = getSetting();
                                        if($siteData['reiss_cat'] == 0){
                                            $mainCat = $mainCat->where('is_user_cat',1);
                                        }
                                        $mainCat = $mainCat->get();
                                        @endphp
                                        @for($i=0; $i<count($mainCat); $i++)
                                    
                                        <li class="has-sub @if(in_array($mainCat[$i]->id,$pData)) active @endif"><!-- active-->
                                            
                                            <a href='{{route('products', ['cat_url' => $mainCat[$i]->slug])}}'>{{$mainCat[$i]->name}}</a>
                                            @php
                                            $html = displayCatTreeOnProduct($mainCat[$i]->id, $pData, $siteData['reiss_cat']);
                                            echo $html;
                                            @endphp

                                        </li>
                                        @endfor
                                        
                                    
                                    </ul>
                                </div>
                                <!-- Left Menu Accordion Ends -->

                            </div>
                            <!-- Left Section Ends -->


                            <!-- Right Section Starts -->
                            <div class="npro-right">
                                @if(count($prodData) > 0)
                                <!-- products section starts -->
                                <div class="products-wrap">
                                    @php
                                    $divStart = 0;
                                    @endphp
                                    @for($i=0; $i<count($prodData);$i++)
                                        @if($divStart == 0)
                                            <div class="products-row">
                                             @php
                                             $divStart = 1;
                                             @endphp
                                        @endif
                                	
                                    	<div class="pro-cntnt">
                                        	<div class="pro-img">
                                            	<a href="{{route('product-detail', ['prod_url' => $prodData[$i]->slug])}}"><img src="https://reisshardware.com/product_images/{{$prodData[$i]->sku}}.JPG" alt=""></a>
                                            </div>
                                            
                                            <div class="pro-info">
                                            	<h5>{{ $prodData[$i]->name }}</h5>
                                                @if($siteData['show_price'] && $prodData[$i]->is_price)
                                                <span>${{number_format($prodData[$i]->price,2,'.','')}}</span>
                                                @endif
                                            </div>
                                        </div><!-- product detail -->
                                        @if( ( ($i+1)%3) == 0)
                                            @php
                                            $divStart = 0;
                                            @endphp
                                            </div>
                                        @endif

                                    @endfor
                                    @if($divStart == 0)
                                        </div>
                                    @endif
                                        











                                   {{--  <div class="products-row">
                                    	<div class="pro-cntnt">
                                        	<div class="pro-img">
                                            	<a href="04-2-a-products-details.php"><img src="{{asset('front')}}/images/products/product-listing/mech-4.png" alt=""></a>
                                            </div>
                                            
                                            <div class="pro-info">
                                            	<h5>Product Name Here</h5>
                                                <span>$100.00</span>
                                            </div>
                                        </div><!-- product detail -->
                                        
                                        <div class="pro-cntnt">
                                        	<div class="pro-img">
                                            	<a href="04-2-a-products-details.php"><img src="{{asset('front')}}/images/products/product-listing/mech-5.png" alt=""></a>
                                            </div>
                                            
                                            <div class="pro-info">
                                            	<h5>Product Name Here</h5>
                                                <span>$100.00</span>
                                            </div>
                                        </div><!-- product detail -->
                                        
                                        <div class="pro-cntnt">
                                        	<div class="pro-img">
                                            	<a href="04-2-a-products-details.php"><img src="{{asset('front')}}/images/products/product-listing/mech-6.png" alt=""></a>
                                            </div>
                                            
                                            <div class="pro-info">
                                            	<h5>Product Name Here</h5>
                                                <span>$100.00</span>
                                            </div>
                                        </div><!-- product detail -->
                                    </div><!-- product content row -->
                                    
                                    <div class="products-row">
                                    	<div class="pro-cntnt">
                                        	<div class="pro-img">
                                            	<a href="04-2-a-products-details.php"><img src="{{asset('front')}}/images/products/product-listing/mech-7.png" alt=""></a>
                                            </div>
                                            
                                            <div class="pro-info">
                                            	<h5>Product Name Here</h5>
                                                <span>$100.00</span>
                                            </div>
                                        </div><!-- product detail -->
                                        
                                        <div class="pro-cntnt">
                                        	<div class="pro-img">
                                            	<a href="04-2-a-products-details.php"><img src="{{asset('front')}}/images/products/product-listing/mech-8.png" alt=""></a>
                                            </div>
                                            
                                            <div class="pro-info">
                                            	<h5>Product Name Here</h5>
                                                <span>$100.00</span>
                                            </div>
                                        </div><!-- product detail -->
                                        
                                        <div class="pro-cntnt">
                                        	<div class="pro-img">
                                            	<a href="04-2-a-products-details.php"><img src="{{asset('front')}}/images/products/product-listing/mech-9.png" alt=""></a>
                                            </div>
                                            
                                            <div class="pro-info">
                                            	<h5>Product Name Here</h5>
                                                <span>$100.00</span>
                                            </div>
                                        </div><!-- product detail -->
                                    </div><!-- product content row -->
                                    
                                    <div class="products-row">
                                    	<div class="pro-cntnt">
                                        	<div class="pro-img">
                                            	<a href="04-2-a-products-details.php"><img src="{{asset('front')}}/images/products/product-listing/mech-10.png" alt=""></a>
                                            </div>
                                            
                                            <div class="pro-info">
                                            	<h5>Product Name Here</h5>
                                                <span>$100.00</span>
                                            </div>
                                        </div><!-- product detail -->
                                        
                                        <div class="pro-cntnt">
                                        	<div class="pro-img">
                                            	<a href="04-2-a-products-details.php"><img src="{{asset('front')}}/images/products/product-listing/pro-sm-2.jpg" alt=""></a>
                                            </div>
                                            
                                            <div class="pro-info">
                                            	<h5>Product Name Here</h5>
                                                <span>$100.00</span>
                                            </div>
                                        </div><!-- product detail -->
                                        
                                        <div class="pro-cntnt">
                                        	<div class="pro-img">
                                            	<a href="04-2-a-products-details.php"><img src="{{asset('front')}}/images/products/product-listing/pro-sm-3.jpg" alt=""></a>
                                            </div>
                                            
                                            <div class="pro-info">
                                            	<h5>Product Name Here</h5>
                                                <span>$100.00</span>
                                            </div>
                                        </div><!-- product detail -->
                                    </div><!-- product content row --> --}}
                                </div>
                                <!-- products section ends -->
                                @else
                                <div class="products-wrap">
                                    <div class="products-row">
                                        <div class="pro-cntnt">
                                            No product found.
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <!-- pagination starts -->
                                <div class="news-paginton"> 
                                    <div class="pagination">
                                        {{ $prodData->links() }}

            							{{-- <a href="#" class="page">&lt;&lt;</a>
                                        <a href="#" class="page-active">1</a>
                                        <a href="#" class="page">2</a>
                                        <a href="#" class="page">3</a>
                                        <a href="#" class="page-active" id="next">&gt;&gt;</a> --}}
            						</div> 
                            	</div>
                                <!-- pagination ends -->

                            </div>
                            <!-- Right Section Ends -->

                        </div>
                        <!-- New Section Ends -->

                	</div>  
                </div>       
			</div>
            <!-- products listing section ends -->
            
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->
    @endsection