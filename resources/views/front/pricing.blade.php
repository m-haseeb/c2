@extends('front.layout')

@section('title')
    <title>Pricing</title>
    @endsection

@section('pageScript')
    <!-- Page Description Starts -->
    <script type="text/javascript">
        var pageName = "pricing-pg";
        var subpageName = "none";
    </script>
    <!-- Page Description Ends -->
@endsection

@section('content')
<body class="blue-green subpage"> <!-- blue-green -->

    <!-- Header Starts -->
    @include("front.include.header")
    <!-- Header End -->

    <!-- Page Header -->
    <div class="page-header style-9">
        <div class="container">
            <h2 class="page-title">Pricing</h2>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);">&nbsp;</a></li>
                <!-- <li class="active">Pricing</li> -->
            </ol>
        </div>
    </div>
    <!-- Page Header End -->

    <div class="cps-main-wrap">
        
        <!-- Custom Section Starts -->
        <div id="plans" class="segment1 pricing-section space-huge cps-section-padding" style="padding-bottom: 50px;">
            <div class="container">
                
                <div class="row hidden-xs hidden-sm">
                    <div class="col-md-10 pricing-table-wrap col-md-offset-1">

                    	<!-- fixed section starts -->
                    	<div class="pfixed" style="display: none;">
	                    	<table class="table table-bordered pricing-table">
	                            <tr class="top-gbtn">
	                                <td class="bdrset" width="275">&nbsp;</td>
	                                <td width="223">
	                                    <a class="btn btn-green btn-lg" href="{{route('getstarted')}}">Get Started</a>
	                                </td>
	                                <td width="223">
	                                    <a class="btn btn-green btn-lg" href="{{route('getstarted')}}">Get Started</a>
	                                </td>
	                                <td width="223">
	                                    <a class="btn btn-green btn-lg" href="{{route('getstarted')}}">Get Started</a>
	                                </td>
	                            </tr>
	                    	</table>
                    	</div>
                    	<!-- fixed section ends -->

                        <table class="table table-bordered pricing-table pregular">
                            
                            <thead>
                                
                                <tr class="top-hding">
                                    <th class="font-22"><strong><br />What We Do<br /> For You</strong></th>
                                    <th width="266">
                                        <span class="table-heading">
                                            <a href="#" target="_blank" class="font-white">
                                                <strong class="font-18">Simple Plan</strong>
                                            </a>
                                        </span>
                                        <span class="table-price">
                                            <strong class="font-20">$99</strong>
                                            <!-- <br>+ $499 onboarding fee  
                                            <a data-toggle="tooltip" data-placement="top" title="" rel="tooltip" data-original-title="Onboarding fee is charged on your sign up date and the fixed monthly fee begins 30 days later.">
                                                <span class="fa-stack fa-1x info-label">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-info fa-inverse fa-stack-1x"></i>
                                                </span>
                                            </a> -->
                                        </span>
                                    </th>
                                    <th>
                                        <span class="table-heading">
                                            <a href="#" target="_blank" class="font-white">
                                                <strong class="font-18">Professional Plan</strong>
                                            </a>
                                        </span>
                                        <span class="table-price">
                                            <strong class="font-20">$199</strong>
                                        </span>
                                    </th>
                                    <th>
                                        <span class="table-heading">
                                            <a href="#" target="_blank" class="font-white">
                                                <strong class="font-18">Master Plan</strong>
                                            </a>
                                        </span>
                                        <span class="table-price">
                                            <strong class="font-20">$399</strong>
                                        </span>
                                    </th>
                                </tr>

                                <tr class="top-gbtn">
	                                <td class="bdrset">&nbsp;</td>
	                                <td>
	                                    <a class="btn btn-green btn-lg" href="{{route('getstarted')}}">Get Started</a>
	                                </td>
	                                <td>
	                                    <a class="btn btn-green btn-lg" href="{{route('getstarted')}}">Get Started</a>
	                                </td>
	                                <td>
	                                    <a class="btn btn-green btn-lg" href="{{route('getstarted')}}">Get Started</a>
	                                </td>
	                            </tr>

                            </thead>

                            
                            <tbody id="content">
                                
                                <tr>
                                    <th class="innerHead" colspan="4">
                                        <strong class="full">We Create Your Website</strong>
                                    </th>
                                </tr>

                                <tr>
                                    <td>
                                        Website Address and Hosting Services
                                        <!-- <a data-toggle="tooltip" data-placement="top" title="" rel="tooltip" data-original-title="No templates! Stand out from the crowd with a completely customized website design based on your ideas and preferences."><span class="fa-stack fa-1x info-label"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-info fa-inverse fa-stack-1x"></i></span> </a>-->
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Mobile Friendly
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Contact Forms
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Website Security
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Google Maps
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Dedicated Customer Support
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        30 Day Money-Back Guarantee
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Google Analytics Setup
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Setup of 3 Social Media Pages (Facebook, Twitter, etc.)
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        5 Pages of Custom Content
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <th class="font-36 innerHead" colspan="4">
                                        <strong class="full">We Promote Your Products</strong>
                                    </th>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Display Reiss Catalog with 12,000 Products 
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Add Your Unique Products and Prices
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Manage Your Website Categories
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Product Printouts & Save-to-Phone Feature
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <th class="font-36 innerHead" colspan="4">
                                        <strong class="full">We Drive Sales and Grow Your Business</strong>
                                    </th>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Social Media Posts (3 post a week on two accounts)
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Monthly Email Newsletter
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Monthly Website Performance Report
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        Store Pick-Up Ordering
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <i class="fa fa-check fa-1x label-check"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="bdrset">&nbsp;</td>
                                    <td>
                                        <a class="btn btn-green btn-lg" href="getstarted.php">Get Started</a>
                                    </td>
                                    <td>
                                        <a class="btn btn-green btn-lg" href="getstarted.php">Get Started</a>
                                    </td>
                                    <td>
                                        <a class="btn btn-green btn-lg" href="getstarted.php">Get Started</a>
                                    </td>
                                </tr>
                                
                            </tbody>
                            
                        </table>
                    </div>
                </div>
                    
                <div class="row visible-xs visible-sm mobileview">

                    <div class="col-md-4 margin-bottom-20">
                        <div class="mobile-block block1">
                            <span class="table-heading"><a href="#" target="_blank" class="font-white"><strong class="font-28">Simple Plan</strong></a><br>
                            </span><span class="table-price"><strong class="font-36">$99</strong></span>
                        </div>
                        <div id="accordions-managed-website" class="panel-group">
                            <div class="panel panel-default">
                                
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordions-managed-website" href="#accordions-managed-website-group-1">We Create Your Website</a></h4>
                                </div>
                                <div id="accordions-managed-website-group-1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="fa-ul">
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Website Address and Hosting Services</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Mobile Friendly</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Contact Forms</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Website Security</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Google Maps</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Dedicated Customer Support</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> 30 Day Money-Back Guarantee</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Google Analytics Setup</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Setup of 3 Social Media Pages <br />(Facebook, Twitter, etc.)</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> 5 Pages of Custom Content</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="bottom-block">
                            <a class="btn btn-green" href="getstarted.php">Get Started</a>
                        </div>
                    </div>


                    <div class="col-md-4 margin-bottom-20">
                        <div class="mobile-block block1">
                            <span class="table-heading"><a href="#" target="_blank" class="font-white"><strong class="font-28">Professional Plan</strong></a><br>
                            </span><span class="table-price"><strong class="font-36">$199</strong></span>
                        </div>
                        <div id="accordions-managed-website" class="panel-group">
                            <div class="panel panel-default">
                                
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordions-managed-website" href="#accordions-managed-website-group-2">We Create Your Website</a></h4>
                                </div>
                                <div id="accordions-managed-website-group-2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="fa-ul">
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Website Address and Hosting Services</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Mobile Friendly</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Contact Forms</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Website Security</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Google Maps</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Dedicated Customer Support</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> 30 Day Money-Back Guarantee</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Google Analytics Setup</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Setup of 3 Social Media Pages<br /> (Facebook, Twitter, etc.)</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> 5 Pages of Custom Content</li>
                                        </ul>
                                    </div>
                                </div>


                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordions-managed-website" href="#accordions-managed-website-group-2a">We Show Customers Your Products</a></h4>
                                </div>
                                <div id="accordions-managed-website-group-2a" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="fa-ul">
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Display Reiss Catalog with 12,000 Products</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Add Your Unique Products and Prices</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Manage Your Website Categories</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Product Printouts & Save-to-Phone Feature</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="bottom-block">
                            <a class="btn btn-green" href="{{route('getstarted')}}">Get Started</a>
                        </div>
                    </div>


                    <div class="col-md-4 margin-bottom-20">
                        <div class="mobile-block block1">
                            <span class="table-heading"><a href="#" target="_blank" class="font-white"><strong class="font-28">Master Plan</strong></a><br>
                            </span><span class="table-price"><strong class="font-36">$399</strong></span>
                        </div>
                        <div id="accordions-managed-website" class="panel-group">
                            <div class="panel panel-default">
                                
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordions-managed-website" href="#accordions-managed-website-group-3">We Create Your Website</a></h4>
                                </div>
                                <div id="accordions-managed-website-group-3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="fa-ul">
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Website Address and Hosting Services</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Mobile Friendly</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Contact Forms</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Website Security</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Google Maps</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Dedicated Customer Support</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> 30 Day Money-Back Guarantee</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Google Analytics Setup</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Setup of 3 Social Media Pages<br /> (Facebook, Twitter, etc.)</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> 5 Pages of Custom Content</li>
                                        </ul>
                                    </div>
                                </div>


                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordions-managed-website" href="#accordions-managed-website-group-3a">We Show Customers Your Products</a></h4>
                                </div>
                                <div id="accordions-managed-website-group-3a" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="fa-ul">
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Display Reiss Catalog with 12,000 Products</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Add Your Unique Products and Prices</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Manage Your Website Categories</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Product Printouts & Save-to-Phone Feature</li>
                                        </ul>
                                    </div>
                                </div>


                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordions-managed-website" href="#accordions-managed-website-group-3b">We Drive Sales and Grow Your Business</a></h4>
                                </div>
                                <div id="accordions-managed-website-group-3b" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="fa-ul">
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Social Media Posts (3 post a week on two accounts)</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Monthly Email Newsletter</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Monthly Website Performance Report</li>
                                            <li><i class="fa fa-check fa-1x fa-li"></i> Store Pick-Up Ordering</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="bottom-block">
                            <a class="btn btn-green" href="{{route('getstarted')}}">Get Started</a>
                        </div>
                    </div>

                </div>
                
            </div>
        </div>
        <!-- Custom Section Ends -->


        <!-- ADDons -->
        <div class="cps-section cps-section-padding" style="padding-top: 0px">
            <div class="container">
                <div class="row no-margin">
                    <div class="col-md-8 col-md-offset-2 col-xs-12">
                        <div class="cps-section-header text-center">
                            <h3 class="cps-section-title">Add-Ons</h3>
                            <p class="cps-section-text" style="margin-top: 2px; margin-bottom: 8px;">Extra Options</p>
                        </div>
                    </div>
                </div>
                <div class="row no-margin">
                    <div class="faqs">

                        <!-- New Added Structure -->
                        <div class="cps-services style-4"> <!-- clr-mbtm -->
                            
                            <!-- Row Starts -->
                            <div class="row">
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon contentwriting"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Content Writing</h4>
                                            <p class="cps-service-text">
                                                Our expert writers will create unique pages for you (up to 250 words each). It could be a blog, an article, a custom webpage about what makes your business special, or anything else your site might need.<br />
                                                <span class="serPrice">$25 per page</span>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon socialmedia"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Social Media Marketing</h4>
                                            <p class="cps-service-text">
                                                We’ll create 3 posts every week for two social media platforms (e.g. Facebook, Twitter, or Instagram) that target your audience and help grow your business. <br />
                                                <span class="serPrice">$99 per month</span>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon seo"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">SEO</h4>
                                            <p class="cps-service-text">
                                                We’ll optimize your website and content to get your better results on Google and other search engines. <br />
                                                <span class="serPrice">$149 per month</span>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                            </div>
                            <!-- Row Ends -->

                            <!-- Row Starts -->
                            <div class="row">
                                
                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon logodesign"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Logo Design</h4>
                                            <p class="cps-service-text">
                                                We’ll create a unique logo image for your business. <br />
                                                <span class="serPrice">$99 (1-time fee)</span>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                                <!-- Column Starts Here -->
                                <div class="col-md-4 col-xs-6">
                                    <div class="cps-service style-4">
                                        <div class="cps-service-icon">
                                            <span class="ti-file cusIcon emailnewsletter"></span>
                                        </div>
                                        <div class="cps-service-content">
                                            <h4 class="cps-service-title">Email Newsletter</h4>
                                            <p class="cps-service-text">
                                                We’ll put together a monthly newsletter to advertise your store and your sales, and email it to former and potential new customers. <br />
                                                <span class="serPrice">$35 per month</span>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- Column Ends Here -->

                            </div>
                            <!-- Row Ends -->
    
                        </div>
                        <!-- New Added Structure -->

                    </div>
                </div>
            </div>
        </div>
        <!-- ADDons end -->

        <!-- Subscription -->
        <div class="cps-section cps-section-padding cps-theme-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="cps-section-header text-center font-adjust" style="margin-bottom:10px;">
                            <h3 class="cps-section-title">Want to Reach New Customers?</h3>
                            <p style="padding:16px 0px;">We can have your website running in days!</p>
                            <a class="btn btn-white" href="{{route('getstarted')}}">Get Your Site Now</a>
                            <p style="padding-top: 3px;">or <a href="{{route("contact")}}" class="link-white">Get More Info</a></p>
                            <!-- <p class="cps-section-text">Subscribe us if you are willing to get acknowledge what we are doing. Know about the most update work and many more. We will not share your email with any third party or will not make any spam mail</p> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Subscription End -->  

        <!-- FAQ Section Starts -->
        <div class="cps-section cps-section-padding faq-settings faq-bdrbtm" id="faq">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-xs-12">
                        <div class="cps-section-header text-center">
                            <h3 class="cps-section-title">Questions? We've got answers</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-xs-12">
                        <div class="cps-faq-accordion">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Do you really set everything up?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            Yes! Your account manager takes care of everything! From setting up the website to uploading your content. We’ll take care of your web needs while you focus on your business.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Is there any setup fee?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            No. There are no hidden costs or setup fees. 
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Can I keep my own domain name?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            Yes, if you already have a domain name you can still use it!
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFour">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Can I stop service at any time?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                        <div class="panel-body">
                                            Yes, if you’re not 100% satisfied with your website you can cancel anytime. There’s even a money-back guarantee for your first 30 days of service.
                                        </div>
                                    </div>
                                </div>


                                
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFive">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Is there a long-term commitment?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseFive">
                                        <div class="panel-body">
                                            No! Your service is month-to-month, so you never have to feel locked into a contract. 
                                        </div>
                                    </div>
                                </div>


                                
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingSix">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">What if I want to do more with my site?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseSix">
                                        <div class="panel-body">
                                            If you want to upgrade your package you can let us know at any time. We’ll be happy to help you unlock more features to grow your business.
                                        </div>
                                    </div>
                                </div>


                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingSeven">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">Can you tell me more about your product catalog?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseSeven">
                                        <div class="panel-body">
                                            Our product catalog contains 12,000 products. It comes complete with images and product descriptions. If you enable this feature, you’ll have a full inventory to display on your website, without having to lift a finger!
                                        </div>
                                    </div>
                                </div>


                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingEight">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">Do I need a webhost?</a>
                                        </h4>
                                    </div>
                                    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseEight">
                                        <div class="panel-body">
                                            XcelHub will handle all your hosting and domain needs at no extra cost to you. We even install a free SSL certificate to help protect your website. 
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FAQ Section Ends -->   

    </div>

    <!-- Footer Starts -->
    @include("front.include.footer")
    <!-- Footer Ends -->

<script>
	//this function runs every time you are scrolling

$(window).scroll(function() {
    var top_of_element = $(".pregular").offset().top;
    var startScroll = $(".pregular").offset().top + 100; /* 250 */
    var maxScroll = ($(".pregular").offset().top + $(".pregular").height()) - 240; /* 432 */
    var bottom_of_element = $(".pregular").offset().top + $(".pregular").outerHeight();
    var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
    var top_of_screen = $(window).scrollTop();

    if((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
        // The element is visible, do something
        console.log('in view');
        console.log(maxScroll);
        console.log(startScroll);
        if($(window).scrollTop() >= startScroll && $(window).scrollTop() < maxScroll ){
        	//start
        	
        	setTimeout(function(){ 
        		//$('.pfixed').fadeIn('fast');
        		//$('.pfixed').addClass('sticky');
        	}, 500);
        	if($('.sticky').length){
        		$('.sticky').css('top',(($(window).scrollTop()-$(".pregular").offset().top)+60)+'px');
        	}

        } else {
        	// Ends
        	setTimeout(function(){ 
        		//$('.pfixed').hide();
        		//$('.pfixed').removeClass('sticky');
        	}, 500);
        }
    }
    else {
        // The element is not visible, do something else
        console.log('Not in view');
    }
});
</script>

</body>
@endsection