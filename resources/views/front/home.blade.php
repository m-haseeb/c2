
@extends('front.layout')

@section('title')
    <title>Safety Line</title>
@endsection

@section('content')

    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->

        <!-- Top Banner Slider Starts -->
        <div class="main-banner-rp">
            <ul class="bxslider2">
                <li>
                    {{-- <a href="javascript:void(0);"> --}}
                    <img src="{{asset('pages_images/'.$pData->banner_img1)}}" alt="" />
                    <div class="trans-bg"></div>
                    <div class="slider-txt-rp">
                        <div class="n-container sldr-txt">
                            @php
                                $des = str_replace("../../",asset('/'),$pData->banner_text1);
                                $des = str_replace("../",asset('/'),$des);
                                echo $des;
                            @endphp
                        </div>
                    </div>
                    {{-- </a> --}}
                </li>
                <li>
                    {{-- <a href="javascript:void(0);"> --}}
                    <img src="{{asset('pages_images/'.$pData->banner_img2)}}" alt="" />
                    <div class="trans-bg"></div>
                    <div class="slider-txt-rp">
                        <div class="n-container sldr-txt">
                            @php
                                $des = str_replace("../../", asset('/'), $pData->banner_text2);
                                $des = str_replace("../", asset('/'), $des);
                                echo $des;
                            @endphp
                        </div>
                    </div>
                    {{-- </a> --}}
                </li>
                <li>
                    {{-- <a href="javascript:void(0);"> --}}
                    <img src="{{asset('pages_images/'.$pData->banner_img3)}}" alt="" />
                    <div class="trans-bg"></div>
                    <div class="slider-txt-rp">
                        <div class="n-container sldr-txt">
                            @php
                                $des = str_replace("../../",asset('/'),$pData->banner_text3);
                                $des = str_replace("../",asset('/'),$des);

                                echo $des;
                            @endphp
                        </div>
                    </div>
                    {{-- </a> --}}
                </li>

            </ul>

        </div>
        <!-- Top Banner Slider Ends -->



        <!-- Featuerd products seciton begins here -->
        @if(count($featured) !=0 )
            <div class="n-container">
                <div class="frt-wrapper">
                    <div class="crl-rp">

                        <div class="frt-hd">
                            <h2>featured Products</h2>
                        </div>
                        @php

                        @endphp

                        <div id="owl-demo" class="owl-carousel" style="float:left;">
                            @foreach($featured as $item)

                                <div class="item crl-pro-item mheight">
                                    <div class="frt-thm">
                                        <table border="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    @if($item->is_bulk == 1)
                                                        @if (file_exists(public_path('bulk_upload_img').'/'.$item->ImageFile.'/'.$item->ImageFile.'.jpg' ) )
                                                            <a href="{{route('reiss.product.details', ['product_slug' => $item->slug])}}"><img class="lazyOwl" data-src="{{asset('bulk_upload_img/'.$item->ImageFile.'/'. $item->ImageFile.'.jpg')}}" alt="Lazy Owl Image"></a>
                                                        @elseif (file_exists(public_path('bulk_upload_img').'/'.$item->ImageFile.'/'.$item->ImageFile.'.png' ) )
                                                            <a href="{{route('reiss.product.details', ['product_slug' => $item->slug])}}"><img class="lazyOwl" data-src="{{asset('bulk_upload_img/'.$item->ImageFile.'/'. $item->ImageFile.'.png')}}" alt="Lazy Owl Image"></a>
                                                        @elseif (file_exists(public_path('bulk_upload_img').'/'.$item->ImageFile.'/'.$item->ImageFile.'-01.jpg' ) )
                                                            <a href="{{route('reiss.product.details', ['product_slug' => $item->slug])}}"><img class="lazyOwl" data-src="{{asset('bulk_upload_img/'.$item->ImageFile.'/'. $item->ImageFile.'-01.jpg')}}" alt="Lazy Owl Image"></a>
                                                        @elseif (file_exists(public_path('bulk_upload_img').'/'.$item->ImageFile.'/'.$item->ImageFile.'-01.JPG' ) )
                                                            <a href="{{route('reiss.product.details', ['product_slug' => $item->slug])}}"><img class="lazyOwl" data-src="{{asset('bulk_upload_img/'.$item->ImageFile.'/'. $item->ImageFile.'-01.JPG')}}" alt="Lazy Owl Image"></a>
                                                        @elseif (file_exists(public_path('bulk_upload_img').'/'.$item->ImageFile.'/'.$item->ImageFile.'-01.png' ) )
                                                            <a href="{{route('reiss.product.details', ['product_slug' => $item->slug])}}"><img class="lazyOwl" data-src="{{asset('bulk_upload_img/'.$item->ImageFile.'/'. $item->ImageFile.'-01.png')}}" alt="Lazy Owl Image"></a>
                                                        @elseif (file_exists(public_path('bulk_upload_img').'/'.$item->ImageFile.'/'.$item->ImageFile.'-01.PNG' ) )
                                                            <a href="{{route('reiss.product.details', ['product_slug' => $item->slug])}}"><img class="lazyOwl" data-src="{{asset('bulk_upload_img/'.$item->ImageFile.'/'. $item->ImageFile.'-01.PNG')}}" alt="Lazy Owl Image"></a>
                                                        @endif
                                                    @else
                                                            @if($item->product_image != null && $item->product_image != '')
                                                                <a href="{{route('reiss.product.details', ['product_slug' => $item->slug])}}">
                                                                    <img class="lazyOwl" src="{{asset('products_images')}}/{{$item->product_image}}" alt="Lazy Owl Image">
                                                                </a>
                                                                @endif
                                                     @endif

                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <p>
                                        <a href="{{route('reiss.product.details', ['product_slug' => $item->slug])}}">{{$item->name}}</a><br>
                                        <span>${{$item->price}}</span>
                                        {{--{{number_format(getReissNewPriceFront($item->id, ($item->price+($item->price*getSettingPrice())) ), 2, '.', '')}}--}}
                                    </p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
    @endif
    <!-- Featuerd products seciton ends here -->


        <!-- why us section starts -->
        <div class="whuy-us-outr">
            <div class="n-wrapper">
                <div class="why-us-inr">
                    <div class="why-us-cntnt">
                        <h5>{{$pData->why_us_text}}</h5>

                        <div class="wuc-dtail">
                            <div class="wucd-txt">
                                @php
                                    $des = str_replace("../../",asset('/'),$pData->desc1);
                                    $des = str_replace("../",asset('/'),$des);
                                    echo $des;
                                @endphp
                                {{-- <div class="wucd-txt-row">
                                    <div class="wucd-tr-lft">
                                        <img src="{{asset('front')}}/images/sm-plotr.png">
                                    </div>

                                    <div class="wucd-tr-rgt">
                                        <h1>Sample Text Here</h1>
                                        <p>Sample Text Here Sample Text Here Sample Text Here.</p>
                                    </div>
                                </div>

                                <div class="wucd-txt-row">
                                    <div class="wucd-tr-lft">
                                        <img src="{{asset('front')}}/images/sm-plotr.png">
                                    </div>

                                    <div class="wucd-tr-rgt">
                                        <h1>Sample Text Here</h1>
                                        <p>Sample Text Here Sample Text Here Sample Text Here.</p>
                                    </div>
                                </div>

                                <div class="wucd-txt-row">
                                    <div class="wucd-tr-lft">
                                        <img src="{{asset('front')}}/images/sm-plotr.png">
                                    </div>

                                    <div class="wucd-tr-rgt">
                                        <h1>Sample Text Here</h1>
                                        <p>Sample Text Here Sample Text Here Sample Text Here.</p>
                                    </div>
                                </div> --}}

                            </div>

                            <div class="wucd-img">
                                <img src="{{asset('pages_images/'.$pData->why_us_img)}}">
                            </div>

                            <div class="wucd-txt txt-right">
                                @php
                                    $des = str_replace("../../",asset('/'),$pData->desc2);
                                    $des = str_replace("../",asset('/'),$des);
                                    echo $des;
                                @endphp
                                {{-- <div class="wucd-txt-row">
                                    <div class="wucd-tr-lft">
                                        <img src="{{asset('front')}}/images/sm-plotr.png">
                                    </div>

                                    <div class="wucd-tr-rgt">
                                        <h1>Sample Text Here</h1>
                                        <p>Sample Text Here Sample Text Here Sample Text Here.</p>
                                    </div>
                                </div>

                                <div class="wucd-txt-row">
                                    <div class="wucd-tr-lft">
                                        <img src="{{asset('front')}}/images/sm-plotr.png">
                                    </div>

                                    <div class="wucd-tr-rgt">
                                        <h1>Sample Text Here</h1>
                                        <p>Sample Text Here Sample Text Here Sample Text Here.</p>
                                    </div>
                                </div>

                                <div class="wucd-txt-row">
                                    <div class="wucd-tr-lft">
                                        <img src="{{asset('front')}}/images/sm-plotr.png">
                                    </div>

                                    <div class="wucd-tr-rgt">
                                        <h1>Sample Text Here</h1>
                                        <p>Sample Text Here Sample Text Here Sample Text Here.</p>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- why us section ends -->


        <!-- last homepage section starts -->
        <div class="lst-outr">
            <div class="lst-cntnt">
                <div class="lst-c-lft">
                    <div class="lst-cl-main">

                        <div class="d-c-opa"></div><!-- div for opacity -->

                        <div class="lst-cl-txt">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        @php
                                            $des = str_replace("../../",asset('/'),$pData->text1);
                                            $des = str_replace("../",asset('/'),$des);
                                            echo $des;
                                        @endphp
                                        {{-- <h1>Sample Text Here</h1>
                                        <h2>Sample Text here sample</h2>
                                        <h3><span>25%</span> off</h3> --}}
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <img src="{{asset('pages_images/'.$pData->image1)}}" alt="" />
                    </div>
                </div><!-- left image section -->

                <div class="lst-c-lft">
                    <div class="lst-cl-main">

                        <div class="d-c-opa"></div><!-- div for opacity -->

                        <div class="lst-cl-txt">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        @php
                                            $des = str_replace("../../",asset('/'),$pData->text2);
                                            $des = str_replace("../",asset('/'),$des);
                                            echo $des;
                                        @endphp
                                        {{-- <h1>Sample Text Here</h1>
                                        <h2>Sample Text here sample</h2>
                                        <h3><span>25%</span> off</h3> --}}
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <img src="{{asset('pages_images/'.$pData->image2)}}" alt="" />
                    </div>
                </div><!-- right image section -->
            </div>
        </div>
        <!-- last homepage section Ends -->

    </div>
    <!-- Site Body Ends Here -->
@endsection
 