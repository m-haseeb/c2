<!-- Script -->
<script src="{{asset('/front')}}/assets/js/jquery.min.js"></script>
<script src="{{asset('/front')}}/assets/js/jquery-migrate-3.0.1.min.js"></script>
<script src="{{asset('/front')}}/assets/js/bootstrap.min.js"></script>
<script src="{{asset('/front')}}/assets/js/owl.carousel.js"></script>
<script src="{{asset('/front')}}/assets/js/visible.js"></script>
<script src="{{asset('/front')}}/assets/js/jquery.stellar.min.js"></script>
<script src="{{asset('/front')}}/assets/js/jquery.countTo.js"></script>
<script src="{{asset('/front')}}/assets/js/imagesloaded.pkgd.min.js"></script>
<script src="{{asset('/front')}}/assets/js/isotope.pkgd.min.js"></script>
<script src="{{asset('/front')}}/assets/js/jquery.magnific-popup.min.js"></script>
<script src="{{asset('/front')}}/assets/js/jquery.ajaxchimp.min.js"></script>
<script src="{{asset('/front')}}/assets/js/plyr.js"></script>
<script src="{{asset('/front')}}/assets/js/swiper.min.js"></script>
<script src="{{asset('/front')}}/assets/js/slick.min.js"></script>
<script src="{{asset('/front')}}/js/custom.js"></script>


<!-- Active Page JS Starts -->
<script type="text/javascript">
	$('.navbar-nav li a').removeClass("active");
	if(pageName.length){
		$('.navbar-nav li a'+'.'+pageName).addClass("active");
	}
	
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
</script>
<!-- Active Page JS Ends -->