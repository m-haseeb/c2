
<!-- Subscription -->
<div class="cps-section subscription-style-2" id="subscription-area">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="subscription-social sm-text-center sm-bottom-30">
                    <label>Follow us on social: </label>
                    <a href="https://www.facebook.com/Xcel-Hub-322125021617712/" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/XcelHub" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="https://plus.google.com/u/0/111443339702040770842" target="_blank"><i class="fa fa-google-plus"></i></a>
                    <!-- <a href="#"><i class="fa fa-youtube"></i></a> -->
                    <a href="https://www.linkedin.com/company/xcelhub/" target="_blank"><i class="fa fa-linkedin"></i></a>
                    <!--
                    <a href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="#"><i class="fa fa-vimeo"></i></a>
                    <a href="#"><i class="fa fa-pinterest"></i></a>
                    <a href="#"><i class="fa fa-github"></i></a>
                    -->
                </div>
            </div>
            <div class="col-md-6 col-xs-12 text-right sm-text-center">
                <label>Subscribe to Our Newsletter</label>
                <form id="subscription" class="cps-subscription" action="#" method="post">
                    <input type="email" name="email" placeholder="Enter your email here"/>
                    <button type="submit"><i class="fa fa-paper-plane"></i></button>
                    <p class="newsletter-success"></p>
                    <p class="newsletter-error"></p>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Subscription End -->

<!-- Footer Inner Starts Here -->
<footer class="style-5">
    <div class="cps-footer-upper">
        <div class="container">
            <div class="cps-footer-widget-area">
                <div class="row">
                    <div class="col-md-4 col-sm-5 col-xs-12">
                        <div class="cps-widget about-widget">
                            <a class="cps-footer-logo" href="{{route('index')}}">
                                <img src="{{asset('/front')}}/images/logo-footer.png" alt="XcelHub Logo">
                            </a>
                            <p>
                                XcelHub is built exclusively for Hardware stores, Lumber and Locksmiths. We'll help your business grow, while staying at a price that best suits you.
                            </p>
                            <div class="cps-socials">
                                <a href="https://www.facebook.com/Xcel-Hub-322125021617712/" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/XcelHub" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a href="https://plus.google.com/u/0/111443339702040770842" target="_blank"><i class="fa fa-google-plus"></i></a>
                                <a href="https://www.linkedin.com/company/xcelhub/" target="_blank"><i class="fa fa-linkedin"></i></a>
                                <!-- <a href="#"><i class="fa fa-instagram"></i></a> -->
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-4 col-xs-12 col-md-offset-1">
                        <div class="cps-widget custom-menu-widget resources-widget">
                            <h4 class="cps-widget-title">Resources</h4>
                            <ul class="widget-menu">
                                <li><a href="blog-3-reasons-why-your-hardware-store-needs-a-website-today.php" class="blog1">3 Reasons Why Your Hardware Store Needs a Website Today</a></li>
                                <li><a href="blog-why-your-hardware-website-needs-to-look-good-on-smartphones.php" class="blog2">Why Your Hardware Website Needs to Look Good on Smartphones</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12 pull-right">
                        <div class="cps-widget custom-menu-widget">
                            <h4 class="cps-widget-title">Useful Links</h4>
                            <ul class="widget-menu">
                                <li><a href="{{route('features')}}" class="features {{$active=='features'?'active':''}}">Features</a></li>
                                <li><a href="{{route('pricing')}}" class="pricing pricing-pg {{$active=='pricing'?'active':''}}">Pricing</a></li>
                                <li><a href="{{route('about')}}" class="aboutus {{$active=='about'?'active':''}}">About us</a></li>
                                <li><a href="{{route('faq')}}" class="faquestions {{$active=='faq'?'active':''}}">FAQs</a></li>
                                <li><a href="{{route('contact')}}" class="contact {{$active=='contact'?'active':''}}">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--
                    <div class="col-md-3 col-sm-6 col-xs-12" style="display: none;">
                        <div class="cps-widget custom-menu-widget">
                            <h4 class="cps-widget-title">Useful Links</h4>
                            <ul class="widget-menu">
                                <li><a href="#">About us</a></li>
                                <li><a href="#">Our Services</a></li>
                                <li><a href="#">Our Team</a></li>
                                <li><a href="#">Testimonials</a></li>
                                <li><a href="#">Our Clients</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-12" style="display: none;">
                        <div class="cps-widget custom-menu-widget">
                            <h4 class="cps-widget-title">Useful Links</h4>
                            <ul class="widget-menu">
                                <li><a href="#">About us</a></li>
                                <li><a href="#">Our Services</a></li>
                                <li><a href="#">Our Team</a></li>
                                <li><a href="#">Testimonials</a></li>
                                <li><a href="#">Our Clients</a></li>
                            </ul>
                        </div>
                    </div>
					-->
                </div>
            </div>
        </div>
    </div>
    <div class="cps-footer-lower">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 xs-text-center text-center"> <!-- col-sm-6 -->
                    <p class="copyright">Copyright {{date('Y')}}, <a href="{{route('index')}}">XcelHub</a>. All Rights Reserved</p>
                </div>
                <!--
                <div class="col-sm-6 col-xs-12 text-right xs-text-center" style="display: none;">
                    <ul class="footer-menu">
                        <li><a href="#">Legal</a></li>
                        <li><a href="#">Terms &amp; Conditions</a></li>
                        <li><a href="contact.php" class="contact">Contact</a></li>
                    </ul>
                </div>
            	-->
            </div>
        </div>
    </div>
</footer>
<!-- Footer Inner Ends Here -->

<!-- Body Js Section Starts -->
@include("front.include.body-js")
<!-- Body Js Section Ends -->

<!-- Start of Async Drift Code -->
<!--
<script>
!function() {
  var t;
  if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
  t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
  t.factory = function(e) {
    return function() {
      var n;
      return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
    };
  }, t.methods.forEach(function(e) {
    t[e] = t.factory(e);
  }), t.load = function(t) {
    var e, n, o, i;
    e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
    n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
  });
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('n6hm7g5tav6c');
</script>
-->
<!-- End of Async Drift Code -->