<div class="collapse navbar-collapse" id="navbar-collapse">
    <ul class="nav navbar-nav navbar-right">
        <li><a href="{{route('index')}}" class="home">Home</a></li>
        <li><a href="{{route('features')}}" class="features">Features</a></li>
        <li><a href="{{route('pricing')}}" class="pricing-pg">Pricing</a></li>
        <li><a href="{{route('about')}}" class="aboutus">About Us</a></li>
        <li><a href="{{route('faq')}}" class="pricing faquestions">FAQs</a></li>
        <li><a href="{{route('contact')}}" class="contact">Contact</a></li>
        <li class="getstarted-sm-navbtn"><a href="{{ route('getstarted') }}">Get Started</a></li>
        <li class="getstarted-navbtn"><a class="btn cps-cta-btn btn-green" href="{{ route('getstarted') }}">Get Started</a></li>

    </ul>
</div>