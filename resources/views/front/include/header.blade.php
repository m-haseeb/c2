
<!-- Header Inner Content Starts -->
<nav class="navbar navbar-default style-10" data-spy="affix" data-offset-top="60">
    <div class="container-fluid">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{route('index')}}"><img src="{{asset('/front')}}/images/logo-3.png" alt="XcelHub Logo"></a>
        </div>
        
        <!-- Site Main Menu Starts -->
        @include("front.include.site-menu")
        <!-- Site Main Menu Ends -->
        
    </div>
</nav>
<!-- Header Inner Content Ends -->
