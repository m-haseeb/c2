
<!-- Site information -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="XcelHub">

<!-- External CSS -->
<link rel="stylesheet" href="{{asset('/front')}}/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="{{asset('/front')}}/assets/css/font-awesome.min.css">
<link rel="stylesheet" href="{{asset('/front')}}/assets/css/themify-icons.css">
<link rel="stylesheet" href="{{asset('/front')}}/assets/css/magnific-popup.css">
<link rel="stylesheet" href="{{asset('/front')}}/assets/css/owl.carousel.css">
<link rel="stylesheet" href="{{asset('/front')}}/assets/css/owl.transitions.css">
<link rel="stylesheet" href="{{asset('/front')}}/assets/css/plyr.css">
<link rel="stylesheet" href="{{asset('/front')}}/assets/css/swiper.min.css">
<link rel="stylesheet" href="{{asset('/front')}}/assets/css/slick.css">

<!-- Custom CSS -->
<link rel="stylesheet" href="{{asset('/front')}}/css/scheme/blue-green.css">
<link rel="stylesheet" href="{{asset('/front')}}/css/style.css">
<link rel="stylesheet" href="{{asset('/front')}}/css/responsive.css">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans%7CLato:400,600,900" rel="stylesheet">

<!-- Favicon -->
<link rel="icon" href="{{asset('/front')}}/images/favicon.png">
<link rel="apple-touch-icon" href="{{asset('/front')}}/images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('/front')}}/images/icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('/front')}}/images/icon-114x114.png">

<!--[if lt IE 9]>
    <script src="{{asset('/front')}}/assets/js/html5shiv.min.js"></script>
    <script src="{{asset('/front')}}/assets/js/respond.min.js"></script>
<![endif]-->