@extends('front.layout')

@section('title')
    <title>Thank You</title>
    @endsection

    @section('content')

    <!-- Site Body Starts Here -->
        <div id="body-minheight" class="site-body">
            <!-- Id used to adjust height of DIV if page is small -->
            <!-- Subpage Structure Starts -->
            <div class="subpage-rp min-width">
                <!-- Mycart shipping and billing section starts -->
                <div class="mycart-cont">
                    <div class="n-wrapper">
                        <div class="mycart-cntnt">
                            <div class="bred-crum bc-news">
                                <ul>
                                    <li><a href="{{route('index')}}">Home</a></li>
                                    <li><span>/</span></li>
                                    <li><a href="javascript:void(0);">Thank You</a></li>
                                </ul>
                            </div>
                            <!-- bread navigation -->
                            
                            <div class="mc-thank-wrap">
	                        	<div class="mc-thank">
	                            	<h5>Thank You</h5>
	                                <p>To view your order status, please visit the <a href="{{route('myOrder')}}">My Order</a> page through your My Account dashboard.
									</p>
	                            </div>
	                        </div><!-- content section -->



                        </div>
                    </div>
                </div>
                <!-- Mycart shipping and billing section ends -->
            </div>
            <!-- Subpage Structure Ends -->
        </div>
        <!-- Site Body Ends Here -->


        <!-- placeholder fixes script for IE 6 7 8 -->
@endsection