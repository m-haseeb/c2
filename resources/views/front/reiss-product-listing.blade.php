@extends('front.layout')

@section('title')
    <title>Products</title>
    @endsection

@section('content')

    <!-- Left Menu Css & Js -->
    <link rel="stylesheet" href="{{asset('front')}}/css/styles.css">
    <script src="{{asset('front')}}/js/script.js"></script>
    <!-- Left Menu Css & Js -->

    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
            <!-- products listing section starts -->
            <div class="products-cont">
            	<div class="n-wrapper">
                    <div class="plisting-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{route('index')}}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">Products</a></li>
                            </ul>
                        </div><!-- bread navigation -->
                        <!-- New Section Starts -->
                        <div class="new-pro-wrap">
                            <!-- Left Section Starts -->
                            <div class="npro-left">
                                <h2>Category</h2>
                                <!-- Left Menu Accordion Begins -->
                                <div id='cssmenu'>
                                    <ul>
                                        @php $settings = getSetting(); @endphp

                                        @for( $i = 0; $i < count($levelOneCategories); $i++ )
                                            @if ( $levelOneCategories[$i]->isActive == true )
                                                <li class="has-sub @if(in_array($levelOneCategories[$i]->id, $parent_categories)) active @endif">
                                                    <a href='{{route('reiss.products', ['category_slug' => $levelOneCategories[$i]->slug])}}'>{{$levelOneCategories[$i]->name}}</a>
                                                    @php 
                                                        $html = displayCategoryTreeOnProductSideMenu($levelOneCategories[$i]->id, $parent_categories, $categories);
                                                        echo $html;
                                                    @endphp
                                                </li>
                                            @endif
                                        @endfor
                                    </ul>
                                </div>
                                <!-- Left Menu Accordion Ends -->
                            </div>
                            <!-- Left Section Ends -->
                            <!-- Right Section Starts -->
                            <div class="npro-right">
                                @if ( count($products) > 0 )
                                    
                                <!-- products section starts -->
                                <div class="products-wrap">
                                    
                                    @php $divStart = 0; 

                                    @endphp

                                    @for( $i = 0; $i < count($products); $i++ )
                                        @if( $divStart == 0 )
                                            <div class="products-row">
                                            @php $divStart = 1; @endphp
                                        @endif

                                                <div class="pro-cntnt">
                                                    <div class="pro-img">
                                                        <a href="{{route('reiss.product.details', ['product_slug' => $products[$i]['slug']])}}">
                                                            @if(@$products[$i]['is_bulk'] == 1)
                                                                @if (file_exists(public_path('bulk_upload_img').'/'.$products[$i]['ImageFile'].'/'.$products[$i]['ImageFile'].'.jpg' ) )
                                                                    <img src="{{asset('bulk_upload_img')}}/{{$products[$i]['ImageFile']}}/{{$products[$i]['ImageFile'].'.jpg'}}" alt="">
                                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$products[$i]['ImageFile'].'/'.$products[$i]['ImageFile'].'.png' ) )
                                                                    <img src="{{asset('bulk_upload_img')}}/{{$products[$i]['ImageFile']}}/{{$products[$i]['ImageFile'].'.png'}}" alt="">
                                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$products[$i]['ImageFile'].'/'.$products[$i]['ImageFile'].'-01.jpg' ) )
                                                                    <img src="{{asset('bulk_upload_img')}}/{{$products[$i]['ImageFile']}}/{{$products[$i]['ImageFile'].'-01.jpg'}}" alt="">
                                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$products[$i]['ImageFile'].'/'.$products[$i]['ImageFile'].'-01.JPG' ) )
                                                                    <img src="{{asset('bulk_upload_img')}}/{{$products[$i]['ImageFile']}}/{{$products[$i]['ImageFile'].'-01.JPG'}}" alt="">
                                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$products[$i]['ImageFile'].'/'.$products[$i]['ImageFile'].'-01.png' ) )
                                                                    <img src="{{asset('bulk_upload_img')}}/{{$products[$i]['ImageFile']}}/{{$products[$i]['ImageFile'].'-01.png'}}" alt="">
                                                                @elseif (file_exists(public_path('bulk_upload_img').'/'.$products[$i]['ImageFile'].'/'.$products[$i]['ImageFile'].'-01.PNG' ) )
                                                                    <img src="{{asset('bulk_upload_img')}}/{{$products[$i]['ImageFile']}}/{{$products[$i]['ImageFile'].'-01.PNG'}}" alt="">
                                                                @endif
                                                            @else
                                                                @if ( (isset($product_type) && $product_type == 'local') || (isset($products[$i]['type']) && $products[$i]['type'] !== '') )
                                                                    <img src="{{asset('products_images/thumbs')}}/{{$products[$i]['product_image']}}" alt="">
                                                                @else
                                                                    <img src="https://reisshardware.com/product_images/{{$products[$i]['sku']}}.JPG" alt="">
                                                                @endif
                                                            @endif
                                                        </a>
                                                    </div>
                                                    
                                                    <div class="pro-info">
                                                        <h5><a href="{{route('reiss.product.details', ['product_slug' => $products[$i]['slug']])}}">{{ $products[$i]['name'] }}</a></h5>
                                                        <h5>SKU: {{ $products[$i]['sku']==''?'N/A':$products[$i]['sku'] }}</h5>
                                                        @if ($settings['show_price'] == 1 && $settings['reiss_prod_price'] == 1 && (isset($product_type) && $product_type == 'reiss') )
                                                            {{-- <span>${{number_format(($products[$i]['price']+($products[$i]['price']*getSettingPrice())), 2, '.', '')}}</span> --}}
                                                            <span>${{number_format(getReissNewPriceFront($products[$i]['id'], ($products[$i]['price']+($products[$i]['price']*getSettingPrice())) ), 2, '.', '')}}</span>
                                                        @endif

                                                        @if ( $settings['show_price'] == 1 && (isset($product_type) && $product_type == 'local') )
                                                            <span>${{number_format($products[$i]['price'], 2, '.', '')}}</span>
                                                        @endif
                                                    </div>
                                                </div><!-- product detail -->

                                        @if( (($i + 1) % 3) == 0 )
                                            @php $divStart = 0; @endphp
                                            </div>
                                        @endif
                                    @endfor
                                </div>
                               
                               <!-- products section ends -->
                                @else
                                    <div class="products-wrap">
                                        <div class="products-row">
                                            <div class="no-product">
                                                No product found.
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                
                                @if($total > 0)
                                <!-- pagination starts -->
                                <div class="news-paginton"> 
                                    <div class="pg-info">
                                        {{@$products->firstItem()}}-{{@$products->lastItem()}} of {{$total}} in <strong>{{$catName}}</strong>
                                    </div>

                                    <div class="pagination">
                                    @if ( count($products) > 0 )
                                        @if(isset($products))
                                            @if ( $products->currentPage() > 1 )
                                                 <a class="back" href="{{ $products->previousPageUrl() }}"> < </a>
                                            @endif
                                            {{-- @if($total > 30) --}}
                                                @php 
                                                $_page = @$_GET['page']; 
                                                $_loopS = 1;
                                                $_loopE = 7;
                                                if($_page >= 5){
                                                    $_loopS = $_page - 3;
                                                    $_loopE = $_page + 3;
                                                }
                                                $_loopE = $total/15;
                                                $_round = round($_loopE);
                                                if($_round < $_loopE){
                                                    $_loopE = $_round +1;
                                                }else{
                                                    $_loopE = $_round;
                                                }

                                                @endphp
                                            
                                                @for($i=1; $_loopS <= $_loopE && $i<=7; $_loopS++, $i++)
                                                    <a @if($_loopS==$_page) class="active" @endif href="{{Request::url()}}?page={{$_loopS}}">{{$_loopS}}</a>
                                                @endfor
                                            {{-- @endif --}}
                                            @if ( $products->hasMorePages() )
                                                <a class="next" href="{{ $products->nextPageUrl() }}"> > </a>
                                            @endif
                                        @endif
                                    @endif
                                    </div> 
                                </div>
                                <!-- pagination ends -->
                                @endif
                            </div>
                            <!-- Right Section Ends -->
                        </div>
                        <!-- New Section Ends -->
                	</div>
                </div>       
			</div>
            <!-- products listing section ends -->
        </div>
        <!-- Subpage Structure Ends -->
    </div>
    <!-- Site Body Ends Here -->

    {{-- {{@$products->links()}} --}}
    {{-- 'current_page' => $this->currentPage(),
    'data' => $this->items->toArray(),
    'first_page_url' => $this->url(1),
    'from' => $this->firstItem(),
    'last_page' => $this->lastPage(),
    'last_page_url' => $this->url($this->lastPage()),
    'next_page_url' => $this->nextPageUrl(),
    'path' => $this->path,
    'per_page' => $this->perPage(),
    'prev_page_url' => $this->previousPageUrl(),
    'to' => $this->lastItem(),
    'total' => $this->total(), --}}
@endsection


