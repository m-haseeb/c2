
<!doctype html>
<html>
<head>
<meta charset="utf-8">

@yield('title')

<link href="{{asset('front')}}/css/font-awesome.css" rel="stylesheet" type="text/css" media="projection,screen" />  <!-- For Icons -->
<link href="{{asset('front')}}/css/new.css" rel="stylesheet" type="text/css" media="projection,screen" />


<!-- accordian css added -->
<link type="text/css" rel="stylesheet" href="{{asset('front')}}/css/smk-accordion.css" media="projection,screen">
<!-- accordian css added -->

<!-- accordian jquery script added -->
<script src="{{asset('front')}}/js/jquery-1.10.1.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('front')}}/js/smk-accordion.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $(".accordion_example").smk_Accordion({
            showIcon: true, //boolean
            animation: true, //boolean
            closeAble: true, //boolean
            slideSpeed: 400 //integer, miliseconds
        });
    });
</script>
<!-- accordian jquery script added -->

<!-- Image Lightbox CSS Starts -->
<link rel="stylesheet" type="text/css" href="{{asset('front')}}/js/fancy/jquery.lightbox.css">
<!-- Image Light Box CSS Ends -->

<!-- popup css starts -->
<link rel="stylesheet" type="text/css" href="{{asset('front')}}/css/pop.css">
<!-- popup css ends -->

<!-- FOR RESPONSIVE STARTS -->
<!-- Google Chrome Frame for IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge" /> -->
<!-- mobile meta (hooray!) -->
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" /> 
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no" />
<link href="{{asset('front')}}/css/responsive-css.css" rel="stylesheet" type="text/css" media="projection,screen" />
<!--[if lt IE 9]>
  <script src="{{asset('front')}}/js/html5shiv.js"></script>
  <script src="{{asset('front')}}/js/respond.min.js"></script>
<![endif]-->
<!-- FOR RESPONSIVE ENDS -->



<!-- All I.E Fixes Starts -->
@include("front/includes/ie_fixes")
<!-- All I.E Fixes Ends -->


<!-- Js Files Starts -->
@include("front/includes/js")
<!-- Js Files Ends -->

<!-- Slider Starts -->
<!-- bxSlider Javascript file -->
<script src="{{asset('front')}}/js/boxslider/jquery.bxslider.min.js"></script>
<!-- bxSlider CSS file -->
<link href="{{asset('front')}}/js/boxslider/jquery.bxslider.css" rel="stylesheet" />
<script type="text/javascript">

$(document).ready(function(){
  $('.bxslider2').bxSlider({
		auto: true,
		pause: 5000,
		mode: 'fade',
		autoStart: true
	});
});

$(window).load(function(){
	if($('.mheight').lenght){
		$.fn.setAllToMaxHeight=function(){
			return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
			}
			$('.mheight').setAllToMaxHeight();
	}
});
</script>
<!-- Slider Ends -->
</head>

<body>

<div id="site-wrap" class="min-width">
	
    <!-- Header Starts Here -->
   @include("front/includes/header2")
    <!-- Header Ends Here -->

    @yield('content')

    <!-- Footer Starts Here -->
    @include("front/includes/footer")
    <!-- Footer Starts Here -->
    
    
</div>

<!-- Carousel js and css begins here -->
<script src="{{asset('front')}}/js/owl.carousel.js"></script>
<link href="{{asset('front')}}/assets/owl-carousel/owl.carousel.css" rel="stylesheet">
<script>
$(document).ready(function() {

  $("#owl-demo").owlCarousel({
	items : 4,
	lazyLoad : true,
	navigation : true,
	autoPlay : false,
	paginationSpeed : 500
  });

});
$(window).load(function() {
	
	if($('.crl-pro-item').length){
		$.fn.seAllheight = function(){
			return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
		}
		$('.crl-pro-item').seAllheight();
	}
	
});
</script> 
<!-- Carousel js and css ends here -->


<!-- Light box JS Starts -->
<script type="text/javascript" src="{{asset('front')}}/js/fancy/jquery.lightbox.js"></script>

<script type="text/javascript">
  // Initiate Lightbox
  $(function() {
    $('.pdt-left a').lightbox(); 
  });
</script>
<!-- Light box JS Ends -->
</body>
</html>
