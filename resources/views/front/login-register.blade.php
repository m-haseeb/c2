@extends('front.layout')

    @section('title')
    <title>Safety Line</title>
    @endsection
    
    @section('content')

    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
        
            <!-- login register section starts -->
            <div class="logreg-cont">
            	<div class="n-wrapper">
                	<div class="logreg-cntnt">
                        <div class="bred-crum bc-news">
                            <ul>
                                <li><a href="{{route('index')}}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void();">Log&nbsp;In-Register</a></li>
                            </ul>
                        </div><!-- bread navigation -->
                        
                        {{-- <div class="input-error st-error">
                        	fgsdfgdgdfgdfgdfgdf
                        </div> --}}<!-- error msg -->
                        
                	 	<div class="logreg-form">
                        	<div class="lrf-left">
                            	{{-- <form method="post" name="log-form"> --}}
                                {{ Form::open(array('name'=>'log-form', 'url'=>route('loginFront'), 'id'=>'login fom'))}}{{--  --}}
                                	<h5>Log in to your account</h5>
                                    
                                	<div class="lrf-fields">
                                    	<input type="text" name="loginemail" value="{{old('loginemail')}}" placeholder="Email" >
                                        <div class="input-error st-error">{{ $errors->first('loginemail') }} {{ session('login-message')  }} </div>
                                    </div>
                                    
                                    <div class="lrf-fields">
                                    	<input type="password" name="loginpass" value="{{old('loginpass')}}" placeholder="Password" >
                                        <div class="input-error st-error">{{$errors->first('loginpass')}} </div>
                                    </div>
                                    
                                    <a href="{{ route('showForgotPassword') }}">Forgot Password?</a>
                                    
                                    <div class="sub-buttons logreg">
										<input type="submit" value="LOGIN">
									</div>
                                {{ Form::close() }}
                                
                                {{-- <div class="protect">
                                	<h5><img src="images/lock-pro.png"><span>We protect your contact information.</span></h5>
                                </div>
                                
                                <div class="feturd-clints">
                                	<h5><span>Featured Clients:</span></h5>
                                </div>
                                
                                <img src="images/times-newyork.jpg">  --}}
                            </div><!-- login left section -->
                            
                            <div class="lrf-mid">
                            	<div class="lrf-m-wrap">
                            	OR
                                </div>
                            </div><!-- or separator section -->
                            
                            <div class="lrf-rgt">
                            	{{-- <form method="post" name="reg-form"> --}}
                                {{ Form::open(array('name'=>'reg-form', 'id'=>'registerForm', 'url'=>route('registerFront') ) ) }}{{--  --}}
                                	<h5>Register</h5>
                                    @if(session()->has('registerMsg'))
                                    <div class="flashMsg">{{session('registerMsg')}}</div>
                                    @endif
                                	<div class="lrf-fields">
                                    	<input type="text" name="regfname" value="{{old('regfname')}}" placeholder="First Name" >
                                        <div class="input-error st-error">{{$errors->first('regfname')}} </div>
                                    </div>
                                    
                                    <div class="lrf-fields">
                                    	<input type="text" name="reglname" value="{{old('reglname')}}" placeholder="Last Name" >
                                        <div class="input-error st-error">{{$errors->first('reglname')}} </div>
                                    </div>
                                    
                                    <div class="lrf-fields">
                                    	<input type="text" name="regemail" value="{{old('regemail')}}" placeholder="Email" >
                                        <div class="input-error st-error">{{$errors->first('regemail')}} </div>
                                    </div>
                                    
                                    <div class="lrf-fields">
                                    	<input type="text" autocomplete="off" name="regeconfrmemail" value="{{old('regeconfrmemail')}}" placeholder="Email Confirmation" >
                                        <div class="input-error st-error">{{$errors->first('regeconfrmemail')}} </div>
                                    </div>
                                    
                                    <div class="lrf-fields">
                                    	<input type="password" name="regpass" value="{{old('regpass')}}" placeholder="Password" >
                                        <div class="input-error st-error">{{$errors->first('regpass')}} </div>
                                    </div>
                                    
                                    <div class="lrf-fields">
                                    	<input type="password" autocomplete="off" name="regpconfrmpass" value="{{old('regpconfrmpass')}}" placeholder="Password Confirmation" >
                                        <div class="input-error st-error">{{$errors->first('regpconfrmpass')}} </div>
                                    </div>
                                    
                                    <div class="lrf-fields">
                                    	<input type="text" name="regstrt" value="{{old('regstrt')}}" placeholder="Street" >
                                        <div class="input-error st-error">{{$errors->first('regstrt')}} </div>
                                    </div>
                                    
                                    <div class="lrf-fields">
                                    	<input type="text" name="regcity" value="{{old('regcity')}}" placeholder="City" >
                                        <div class="input-error st-error">{{$errors->first('regcity')}} </div>
                                    </div>
                                    
                                    <div class="lrf-fields">
                                    	<select name="regstate">
                                        	<option value=''>Select</option>
                                            @foreach($states as $st)
                                            <option value="{{$st->stat_name}}" {{(old('regstate') == $st->stat_name)?'selected':''}} >{{$st->stat_name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="input-error st-error">{{$errors->first('regstate')}} </div>
                                    </div>
                                    
                                    <div class="lrf-fields">
                                    	<input type="text" name="regzip" value="{{old('regzip')}}" placeholder="Zip" >
                                        <div class="input-error st-error">{{$errors->first('regzip')}} </div>
                                    </div>
                                    
                                    <div class="sub-buttons logreg">
										<input type="submit" value="REGISTER">
									</div>
                                {{ Form::close() }}
                            </div><!-- register right section -->
                        </div><!-- login and register sections -->
                	</div>
                </div>
			</div>
            <!-- login register section ends -->
            
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->
    

<!-- placeholder fixes script for IE 6 7 8 --> 
<script>
     (function ($) {
         $.support.placeholder = ('placeholder' in document.createElement('input'));
     })(jQuery);


     //fix for IE7 and IE8
     $(function () {
         if (!$.support.placeholder) {
             $("[placeholder]").focus(function () {
                 if ($(this).val() == $(this).attr("placeholder")) $(this).val("");
             }).blur(function () {
                 if ($(this).val() == "") $(this).val($(this).attr("placeholder"));
             }).blur();

             $("[placeholder]").parents("form").submit(function () {
                 $(this).find('[placeholder]').each(function() {
                     if ($(this).val() == $(this).attr("placeholder")) {
                         $(this).val("");
                     }
                 });
             });
         }
     });
 </script>
 <!-- placeholder fixes script for IE 6 7 8 -->
 
 
 @endsection