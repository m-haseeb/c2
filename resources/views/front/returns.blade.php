@extends('front.layout')

@section('title')
    <title>Returns, Freight, and Credit</title>
    @endsection

    @section('content')
    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
            <div class="n-wrapper">
            	
                <!-- Content Area Starts -->
                <div class="textual-Pages">
                	
                    <h2>Returns, Freight, and Credit</h2>
                    
                    
                    
                    <h4>RETURNED GOODS &amp; CLAIMS</h4>
                    <p>
                    	To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging. We are not responsible for any return service and fees. Products must have been purchased through Safety Line in order to be eligible for return. Special and Custom orders are non-returnable.
                    </p>
                    <p>
                    	All claims for damaged goods or shortages must be reported within three (3) days of receipt of merchandise. If goods are shipped via UPS and damaged in shipping, it is the purchaser's responsibility to file the appropriate transportation claim.
                    </p>
                    
                    
                    <h4>NEW ACCOUNTS</h4>
                    <p>
                    	A completed Resale Certificate and must be submitted prior to any order being accepted. A completed credit application must accompany all requests for credit (“open account”). Credit application processing normally takes about 14 working days. Please contact us at <a href="mailto:safetylinecorp@gmail.com">safetylinecorp@gmail.com</a> for Credit Application.
                    </p>
                    
                    <h4>ACCOUNT TERMS</h4>
                    <p>
                    	30 Days for all approved accounts. A finance charge of 1.5% per month (18% per annum) will be added to any past due or unpaid balance. There will be a $50.00 fee for any returned check.
                    </p>
                    
                    <h4>FREIGHT</h4>
                    
                    <p>
                    	All UPS shipments incur a UPS shipping charge. UPS shipping charges are calculated once an order has been submitted, pulled from stock and weighed.
                    </p>
                    <p>
                    	All policies are subject to change, and are at the sole discretion of Safety Line Corp. and its affiliates.
                    </p>
                    
                    <h4>GENERAL CONDITION, ACCURACY AND COMPLETENESS</h4>
                    <p>
                    	We are not responsible if information made available on this site is not accurate, complete or current. We recommend e-mail inquiries with product numbers on our catalog. As our website is still undergoing development, items on catalog may not be on our website. Certain products may not be in stock or discontinued in which case we will inform you via e-mail.
                    </p>
                    <p>
                    	Prices for our products are subject to change without notice.
                    </p>
                    <p>
                    	We reserve the right at any time to modify or discontinue the Service (or any part or content thereof) without notice at any time. We reserve the right to refuse service to anyone for any reason at any time.
                    </p>
                    
                    <h4>SERVICE FEES (Non Refundable)</h4>
                    <p>
                    	There are fees for hand truck repairing service and printing service. For more details please contact us at <a href="mailto:safetylinecorp@gmail.com">safetylinecorp@gmail.com</a>


                    </p>


                    
                    
                </div>
                <!-- Content Area Ends -->
                
            </div>
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->
      @endsection