@extends('front.layout')

@section('title')
    <title>Terms and Conditions</title>
    @endsection

    @section('content')
    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
            <div class="n-wrapper">
            	
                <!-- Content Area Starts -->
                <div class="textual-Pages">
                	
                    <h2>Terms and Conditions</h2>
                    
                    <h4>CONDITIONS OF USE</h4>
                    <p>
                    	Safety Line and its associates provide their services to you subject to the following conditions. If you visit or shop on this website, you accept these conditions. Please read them carefully. ​
                    </p>
                    
                    <h4>ELECTRONIC COMMUNICATIONS</h4>
                    <p>
                    	When you visit Safety Line or send emails to us, you are communicating with us electronically. You consent to receive communications from us electronically. We will communicate with you by email or by posting notices on this site. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing.
                    </p>
                    
                    <h4>COPYRIGHT</h4>
                    <p>
                    	All content included on this site, such as text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations, and software, is the property of Safety Line or its content suppliers and protected by international copyright laws. The compilation of all content on this site is the exclusive property of Safety Line, with copyright authorship for this collection by XcelHub, and protected by international copyright laws.
                    </p>
                    
                    <h4>TRADEMARKS</h4>
                    <p>
                    	Safety Line trademarks and trade dress may not be used in connection with any product or service that is not Safety Line’s, in any manner that is likely to cause confusion among customers, or in any manner that disparages or discredits Safety Line. All other trademarks not owned by Safety Line that appear on this site are the property of their respective owners, who may or may not be affiliated with, connected to, or sponsored by Safety Line.
                    </p>
                    
                    <h4>LICENSE AND SITE ACCESS</h4>
                    <p>
                    	Safety Line grants you a limited license to access and make personal use of this site and not to download (other than page caching) or modify it, or any portion of it, except with express written consent of Safety Line and XcelHub. This license does not include any resale or commercial use of this site or its contents: any collection and use of any product listings, descriptions, or prices: any derivative use of this site or its contents: any downloading or copying of account information for the benefit of another merchant: or any use of data mining, robots, or similar data gathering and extraction tools. This site or any portion of this site may not be reproduced, duplicated, copied, sold, resold, visited, or otherwise exploited for any commercial purpose without express written consent of Safety Line and XcelHub. You may not frame or utilize framing techniques to enclose any trademark, logo, or other proprietary information (including images, text, page layout, or form) of Safety Line and our associates without express written consent. Any unauthorized use terminates the permission or license granted by Safety Line.
                    </p>
                    
                    <h4>YOUR MEMBERSHIP ACCOUNT</h4>
                    <p>
                    	If you use this site, you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer, and you agree to accept responsibility for all activities that occur under your account or password. If you are under 18, you may use our website only with involvement of a parent or guardian. Safety Line and its associates reserve the right to refuse service, terminate accounts, remove or edit content, or cancel orders in their sole discretion.
                    </p>
                    
                    <h4>PRODUCT DESCRIPTIONS</h4>
                    <p>
                    	Safety Line and its associates attempt to be as accurate as possible. However, Safety Line does not warrant that product descriptions or other content of this site is accurate, complete, reliable, current, or error-free. If a product offered by Safety Line itself is not as described, your sole remedy is to return it in unused condition.
                    </p>
                    
                    <h4>LEGAL DISCLAIMER</h4>
                    <p>
                    	By visiting Safety Line, you agree that the laws of the state of New Jersey, without regard to principles of conflict of laws, will govern these Conditions of Use and any dispute of any sort that might arise between you and Safety Line or its associates.
                    </p>
                    
                    <h4>DISPUTES</h4>
                    <p>
                    	Any dispute relating in any way to your visit to Safety Line or to products you purchase through Safety Line shall be submitted to confidential arbitration in New Jersey, except that, to the extent you have in any manner violated or threatened to violate Safety Line’s intellectual property rights, Safety Line may seek injunctive or other appropriate relief in any state or federal court in the state of New Jersey, and you consent to exclusive jurisdiction and venue in such courts. Arbitration under this agreement shall be conducted under the rules then prevailing of the American Arbitration Association. The arbitrators award shall be binding and may be entered as a judgment in any court of competent jurisdiction. To the fullest extent permitted by applicable law, no arbitration under this Agreement shall be joined to an arbitration involving any other party subject to this Agreement, whether through class arbitration proceedings or otherwise.
                    </p>
                    
                    <p>
                    	Questions regarding our Conditions of Usage, Privacy Policy, or other policy related material can be directed to our support staff by clicking on the "Contact Us" link in the top menu.
                    </p>
                    
                    
                </div>
                <!-- Content Area Ends -->
                
            </div>
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->
      @endsection