@extends('front.layout')

@section('title')
    <title>Cart</title>
    @endsection

    @section('content')

    <!-- Site Body Starts Here -->
        <div id="body-minheight" class="site-body">
            <!-- Id used to adjust height of DIV if page is small -->
            <!-- Subpage Structure Starts -->
            <div class="subpage-rp min-width">
                <!-- Mycart shipping and billing section starts -->
                <div class="mycart-cont">
                    <div class="n-wrapper">
                        <div class="mycart-cntnt">
                            <div class="bred-crum bc-news">
                                <ul>
                                    <li><a href="{{route('index')}}">Home</a></li>
                                    <li><span>/</span></li>
                                    <li><a href="{{route('displayList')}}">My&nbsp;Cart</a></li>
                                    <li><span>/</span></li>
                                    <li><a href="javascript:void(0);">Billing Info</a></li>
                                </ul>
                            </div>
                            <!-- bread navigation -->
                            <div class="mc-bilship">
                                <div class="bship-thead">
                                    <div class="bship-left">
                                        <h1>Shipping</h1>
                                    </div>
                                    <div class="bship-left bship-rgt">
                                        <h1>Billing Info</h1>
                                    </div>
                                </div>
                                <!-- top headings -->
                                <div class="billship-form">
                                    {{Form::open(array('name'=>'bship-form', 'files'=> true, 'url'=>route('billShipSave') ) )}}
                                        <div class="lrf-left">
                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="shipfname" name="shipfname" value="{{old('shipfname')}}" placeholder="First Name"  />
                                                <div class="error" id="Eshipfname">{{$errors->first('shipfname') }}</div>
                                            </div>
                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="shiplname" name="shiplname" value="{{old('shiplname')}}" placeholder="Last Name"  />
                                                <div class="error" id="Eshiplname">{{$errors->first('shiplname') }}</div>
                                            </div>
                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="shipemail" name="shipemail" value="{{old('shipemail')}}" placeholder="Email"  />
                                                <div class="error" id="Eshipemail">{{$errors->first('shipemail') }}</div>
                                            </div>
                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="shipphone" name="shipphone" value="{{old('shipphone')}}" placeholder="Phone"  />
                                                <div class="error" id="Eshipphone">{{$errors->first('shipphone') }}</div>
                                            </div>

                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="shipaddress" name="shipaddress" value="{{old('shipaddress')}}" placeholder="Address"  />
                                                <div class="error" id="Eshipaddress">{{$errors->first('shipaddress') }}</div>
                                            </div>


                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="shipcity" name="shipcity" value="{{old('shipcity')}}" placeholder="City"  />
                                                <div class="error" id="Eshipcity">{{$errors->first('shipcity') }}</div>
                                            </div>
                                            <div class="lrf-fields">
                                                <select id="shipstate" class="changeShip" name="shipstate">
                                                    <option value="">State</option>
                                                    @foreach($states as $stat)
                                                    <option value="{{$stat->stat_name}}" @if(old('shipState')==$stat->stat_name) selected @endif >{{$stat->stat_name}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="error" id="Eshipstate">{{$errors->first('shipstate') }}</div>
                                            </div>
                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="shipzip" name="shipzip" value="{{old('shipzip')}}" placeholder="Zip"  />
                                                <div class="error" id="Eshipzip">{{$errors->first('shipzip') }}</div>
                                            </div>
                                            <div class="lrf-fields">
                                                <select class="storeshiping" id="store-shiping" name="store_shiping">
                                                    <option value="">Select</option>
                                                    <option value="store_pickup">Store Pickup</option>
                                                    <option value="shipping">Shipping</option>
                                                </select>
                                                <div class="error" id="store-shiping">{{$errors->first('store_shiping') }}</div>
                                            </div>
                                        </div>
                                        <!-- billing left section -->
                                        <div class="lrf-rgt ship-rgt">
                                            <div class="chk-box">
                                                <input type="checkbox" id="sameShipping" name="sameShipping" value="1" class="cbox">
                                                <h1>Check here if address is the same for shipping</h1>
                                            <div class="error" id="EsameShipping"></div>
                                            </div>
                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="billfname" name="billfname" value="{{old('billfname')}}" placeholder="First Name" />
                                                <div class="error" id="Ebillfname">{{$errors->first('billfname') }}</div>
                                            </div>
                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="billlname" name="billlname" value="{{old('billlname')}}" placeholder="Last Name" />
                                                <div class="error" id="Ebilllname">{{$errors->first('billlname') }}</div>
                                            </div>
                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="billemail" name="billemail" value="{{old('billemail')}}" placeholder="Email" />
                                                <div class="error" id="Ebillemail">{{$errors->first('billemail') }}</div>
                                            </div>
                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="billphone" name="billphone" value="{{old('billphone')}}" placeholder="Phone" />
                                                <div class="error" id="Ebillphone">{{$errors->first('billphone') }}</div>
                                            </div>
                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="billaddress" name="billaddress" value="{{old('billaddress')}}" placeholder="Address"  />
                                                <div class="error" id="Ebilladdress">{{$errors->first('billaddress') }}</div>
                                            </div>
                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="billcity" name="billcity" value="{{old('billcity')}}" placeholder="City" />
                                                <div class="error" id="Ebillcity">{{$errors->first('billcity') }}</div>
                                            </div>
                                            <div class="lrf-fields">
                                                <select class="changeShip" id="billstate" name="billstate">
                                                <option value="">State</option>
                                                @foreach($states as $stat)
                                                <option value="{{$stat->stat_name}}" @if(old('billstate')==$stat->stat_name) selected @endif >{{$stat->stat_name}}</option>
                                                @endforeach
                                                </select>
                                                <div class="error" id="Ebillstate">{{$errors->first('billstate') }}</div>
                                            </div>
                                            <div class="lrf-fields">
                                                <input type="text" class="changeShip" id="billzip" name="billzip" value="{{old('billzip')}}" placeholder="Zip" />
                                                <div class="error" id="Ebillzip">{{$errors->first('billzip') }}</div>
                                            </div>

                                        </div>
                                        <!-- shipping right section -->
                                        <div class="next-btn-wrap">
                                            <div class="next-btn">
                                                <input type="submit" name="next" value="Next">
                                            </div>
                                        </div>
                                        <!-- next button -->
                                    {{Form::close()}}
                                </div>
                                <!-- billing and shipping form section -->
                            </div>
                            <!-- Billing and shipping sections -->
                        </div>
                    </div>
                </div>
                <!-- Mycart shipping and billing section ends -->
            </div>
            <!-- Subpage Structure Ends -->
        </div>
        <!-- Site Body Ends Here -->


        <!-- placeholder fixes script for IE 6 7 8 -->
<script>
(function($) {
    $.support.placeholder = ('placeholder' in document.createElement('input'));
})(jQuery);


//fix for IE7 and IE8
$(function() {
    if (!$.support.placeholder) {
        $("[placeholder]").focus(function() {
            if ($(this).val() == $(this).attr("placeholder")) $(this).val("");
        }).blur(function() {
            if ($(this).val() == "") $(this).val($(this).attr("placeholder"));
        }).blur();

        $("[placeholder]").parents("form").submit(function() {
            $(this).find('[placeholder]').each(function() {
                if ($(this).val() == $(this).attr("placeholder")) {
                    $(this).val("");
                }
            });
        });
    }
});

$(document).ready(function(){
    $(document).on('click', '#sameShipping', function(){
    //alert(1);
        var isCheck = $(this).is(':checked');
        if(isCheck){
            var shipfname = $('#shipfname').val();
            var shiplname = $('#shiplname').val();
            var shipemail = $('#shipemail').val();
            var shipphone = $('#shipphone').val();
            var shipcity = $('#shipcity').val();
            var shipaddress = $('#shipaddress').val();
            var shipstate = $('#shipstate').val();
            var shipzip = $('#shipzip').val();
            console.log("shipstate : " + shipstate);
            if(shipfname.length > 0 && shiplname.length > 0 && shipemail.length > 0 && shipphone.length > 0 && shipcity.length > 0 &&
                shipstate.length > 0 && shipzip.length > 0 && shipaddress.length > 0 ){

                 $('#billfname').val(shipfname);
                 $('#billlname').val(shiplname);
                 $('#billemail').val(shipemail);
                 $('#billphone').val(shipphone);
                 $('#billcity').val(shipcity);
                 $('#billaddress').val(shipaddress);
                 $('#billstate').val(shipstate);
                 $('#billzip').val(shipzip);

            }else{
                $('#EsameShipping').text('Please fill all the field for shipping address.');
            }
        }else{
                $('#EsameShipping').text('');
        }

    });

    $(document).on('change keyup', '.changeShip', function(){
        $('#sameShipping').prop('checked', false);
    });
});
</script>
<!-- placeholder fixes script for IE 6 7 8 -->




  @endsection