@extends('front.layout')

@section('title')
    <title>{{$pData->name}}</title>
    @endsection

    @section('content')
    <!-- Site Body Starts Here -->
    <div id="body-minheight" class="site-body"> <!-- Id used to adjust height of DIV if page is small -->
        
        <!-- Subpage Structure Starts -->
        <div class="subpage-rp min-width">
			<!-- about us banner section starts -->
			<div class="ab-banr-cont">
            	<div class="ab-banr" style="background-image:url({{asset('pages_images/'.$pData->image)}})">
            	<div class="n-wrapper">
                    <div class="ab-banr-cntnt">
                    	<div class="bred-crum">
                        	<ul>
                            	<li><a href="{{route('index')}}">Home</a></li>
                                <li><span>/</span></li>
                                <li><a href="javascript:void(0);">{{$pData->name}}</a></li>
                            </ul>
                        </div><!-- top bread navigation -->
                        
                        <div class="banr-txt">
                        	<h5>{{$pData->title}}</h5>
                        </div><!-- banner title section -->
                    </div>
				</div>
                </div>                   
            </div>
            <!-- about us banner section ends -->

            <!-- about us body section starts -->
            <div class="ab-bdy">
            	<div class="n-wrapper">
                	<div class="ab-bdy-cntnt">
                        <p>
                        @php
                        $des = str_replace("../../",asset('/'),$pData->description);
                        echo $des;
                        @endphp
                    	</p>
                    </div>
                </div>
            </div>
            <!-- about us body section ends -->
        </div>
        <!-- Subpage Structure Ends -->
        
        
    </div>
    <!-- Site Body Ends Here -->
      @endsection