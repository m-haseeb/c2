<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 07-Dec-17
 * Time: 11:13 AM
 */ ?>
@extends('adminlte::page')

@section('title','Add Store')

@section('content_header')
@stop

@section('content')
    <div class="register-box-body">
        <form action="{{ route('store.create') }}" method="post">
            {!! csrf_field() !!}
            <div class="form-group has-feedback {{ $errors->has('first_name') ? 'has-error' : '' }}">
                <input type="text" name="first_name" class="form-control" value="{{ old('first_name') }}"
                       placeholder="first name">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                @if ($errors->has('first_name'))
                    <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('last_name') ? 'has-error' : '' }}">
                <input type="text" name="last_name" class="form-control" value="{{ old('last_name') }}"
                       placeholder="last name">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                @if ($errors->has('last_name'))
                    <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('store_name') ? 'has-error' : '' }}">
                <input type="text" name="store_name" class="form-control" value="{{ old('store_name') }}"
                       placeholder="store name">
                <span class="glyphicon glyphicon-home form-control-feedback"></span>
                @if ($errors->has('store_name'))
                    <span class="help-block">
                            <strong>{{ $errors->first('store_name') }}</strong>
                        </span>
                @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('phone') ? 'has-error' : '' }}">
                <input type="text" name="phone" class="form-control" value="{{ old('phone') }}"
                       placeholder="phone">
                <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                @if ($errors->has('phone'))
                    <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                       placeholder="{{ trans('adminlte::adminlte.email') }}">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                <input type="password" name="password" class="form-control"
                       placeholder="{{ trans('adminlte::adminlte.password') }}">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                <input type="password" name="password_confirmation" class="form-control"
                       placeholder="{{ trans('adminlte::adminlte.retype_password') }}">
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                @endif
            </div>
            <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </form>
    </div>

@stop
