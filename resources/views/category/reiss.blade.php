<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 08-Dec-17
 * Time: 4:09 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Reiss Categories')

@section('css')
    <link rel="stylesheet" href="{{asset('css\allCategories.css')}}">
@endsection

@section('js')
    <script src="{{asset('js\aCollap\jquery.aCollapTable.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
        
        var category_list = "";
        var categories = [];
        var permissions = {};
        var count = 1;

        $(document).ready(function() {
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
            loadCategories();
        });

        function loadCategories()
        {
            $('.box').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');

            category_list = "";

            $.ajax({
                method: 'get',
                url: '{{route('api.categories')}}',
                data: { product: 'yes' },
                success: function (response) {

                    if ( response.error === undefined )
                    {
                        permissions = response.permissions;
                        categories = response.categories;

                        $('#cat_count').html(categories.length);

                        //if no permission for add... remove add button...
                        if ( !permissions.reiss_cat_add )
                            $('.add-category').remove();

                        var tree = $.map(categories, function(item, index){
                             if ( item.parent_id == '' || item.parent_id == null) {
                                return item;
                             }
                        });

                         $.each(tree, function(index, item){
                            
                            let childCount = countChildren(categories, item.id);

                            category_list += `<tr data-id="${count}" data-parent="">
                                                <td class="collection-item avatar">${item.name}</td>
                                                <td class="text-center">${childCount}</td>
                                                <td class="text-center">${item.products_count | 0}</td>
                                                <td class="text-center">`;

                            if ( item.isActive === true )
                            {
                                category_list += `<button type="button" class="active btn btn-success btn-sm" category_id="${item.id}">
                                                        <i class="fa fa-eye"></i>
                                                    </button>&nbsp;`;
                            }
                            else
                            {
                                category_list += `<button type="button" class="deactive btn btn-warning btn-sm" category_id="${item.id}">
                                                        <i class="fa fa-eye-slash"></i>
                                                    </button>&nbsp;`;
                            }

                            category_list += `</td> 
                                            </tr>`;

                            if ( childCount > 0 )
                            {
                                findChildren(categories, item.id, count, item.active);
                            }

                            count++;
                        });

                        $('#cat_table_body').html(category_list);

                        $('#cat_table').aCollapTable({
                            startCollapsed: true,
                            addColumn: false,
                            plusButton: '<i class="fa fa-plus expand_icon" aria-hidden="true"></i>',
                            minusButton: '<i class="fa fa-minus expand_icon" aria-hidden="true"></i>'
                        });

                        $('.overlay').remove();
                    }
                    else
                    {
                        $('.overlay').remove();
                    }
                }
            });
        }

        function countChildren(items, item_id)
        {
            return $.map(items, function(item, index){
                 if ( item.parent_id == item_id){ return item; }
            }).length;
        }

        function findChildren(items, item_id, parent_id, parentStatus)
        { 
            count++;

            let childrens = $.map(items, function(item, index){
                 if ( item.parent_id == item_id){ return item; }
            });

            if ( childrens.length )
            {
                $.each(childrens, function(index, item) {

                    let childCount = countChildren(categories, item.id);

                    category_list += `<tr data-id="${count}" data-parent="${parent_id}">
                                                <td class="collection-item avatar">${item.name}</td>
                                                <td class="text-center">${childCount}</td>
                                                <td class="text-center">${item.products_count | 0}</td>
                                                <td class="text-center">`;

                    if ( item.isActive === true )
                    {
                        category_list += `<button type="button" class="active btn btn-success btn-sm" category_id="${item.id}">
                                                <i class="fa fa-eye"></i>
                                            </button>&nbsp;`;
                    }
                    else
                    {
                        let disableState = '';

                        if ( item.isActive == parentStatus )
                            disableState = 'disabled';

                        category_list += `<button type="button" class="deactive btn btn-warning btn-sm" category_id="${item.id}" ${disableState}>
                                                <i class="fa fa-eye-slash"></i>
                                            </button>&nbsp;`;
                    }

                    category_list += `</td> 
                                    </tr>`;

                    if ( childCount > 0 )
                    {
                        findChildren(categories, item.id, count, item.active);
                    }

                    count++;
                });
            }
        }

        function toggle(category_id, toggle)
        {
            $('.box').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');

            $.ajax({
                method: 'post',
                url: '{{route('reiss.category.toggle')}}',
                data: { id: category_id, toggle: toggle },
                success: function (response) {
                    loadCategories();
                }
            });
        }

        $('#cat_table_body').on('click', '.active', function () {

            let id = $(this).attr('category_id');

            swal({
                title: "Are you sure?",
                text: "This is will disable this category, it's sub-categories and all associated products!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((doIt) => {
                if (doIt) {
                    toggle(id, "disable");
                }
            });
        });

        $('#cat_table_body').on('click', '.deactive', function () {

            let id = $(this).attr('category_id');

            swal({
                title: "Are you sure?",
                text: "This is will enable this category, it's sub-categories and all associated products!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((doIt) => {
                if (doIt) {
                    toggle(id, "enable");
                }
            });
        });

    </script>
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h6 class="box-title">Reiss Categories</h6>
            <div class="box-tools pull-right">
                <div class="form-inline pull-left" style="margin-right: 10px">
                    {{--<label for="show_filter">Show:--}}
                    {{--<select id="show_filter" class="form-control">--}}
                    {{--<option value="active">Active</option>--}}
                    {{--<option value="deleted">Deleted</option>--}}
                    {{--</select>--}}
                    {{--</label>--}}
                </div>
                <div class="label label-primary" style="vertical-align: sub">Total: <span id="cat_count"></span></div>
            </div>
        </div>
        <div class="box-body">
                <table class="table table-hover" id="cat_table">
                    <thead>
                    <tr>
                        <th width="50%">Categories</th>
                        <th width="10%" class="text-center">Sub-Categories</th>
                        <th width="20%" class="text-center">Products in Category</th>
                        <th width="30%" class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody id="cat_table_body">

                    </tbody>
                </table>
        </div>
    </div>
@stop
