<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 11-Dec-17
 * Time: 12:18 PM
 */?>
@extends('adminlte::page')

@section('title', 'Create Category')

@section('css')
    <link rel="stylesheet" href="{{asset('/css/addCategory.css')}}">
@endsection

@section('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
        let list = "";
        let loading = '<i class="fa fa-spinner fa-pulse fa-fw"></i>';
        const cat_id = "{{Request::input('edit')}}";
        let category = null;

        $(document).ready(function () {

            if (cat_id !== "")
            {
                $('#save').remove();
                loadCategory();
            }
            else
            {
                $('#edit').remove();
                init();
            }
        });

        function loadCategory() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'get',
                url: '{{route('category.get', Request::input('edit') )}}',
                success: function (response) {
                    category = response;
                    init(true);
                }
            });
        }

        function fillForm() {

            if (category !== null) {

                $('#cat_name').val(category.name);

                if (category.parent_id === null)
                {
                    $('input[id = "no_parent"]').prop('checked', true);
                }
                else {
                    $('input[id = "'+category.parent_id+'"]').prop('checked', true);
                }
            }
        }

        function init(loadForm) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'get',
                url: '{{route('category.tree')}}',
                success: function (response) {

                    list += "<div class=\"checkbox\"><ul class=\"wpsc_categories wpsc_top_level_categories\"><li><label for=\"no_parent\"><input type=\"radio\" name=\"cat_group\" id=\"no_parent\"/ checked>No Parent</label>";
                    $.each(response.tree, function (index, item) {
                        list += "<li><label for=\"" + item.id + "\"><input type=\"radio\" name=\"cat_group\" id=\"" + item.id + "\" ";

                        if(cat_id == item.id)
                            list +=  " disabled />" + item.name + "</label>";
                        else
                            list +=  "/>" + item.name + "</label>";

                        if (item.children !== null) {
                            list += "<span class='toggle'>+</span>";
                            createSubList(item.children, false);
                            list += "</li>";
                        }
                        else
                            list += "</li>";

                    });
                    list += "</ul></div>";
                    $('#loading').remove();
                    $('#category_list').append(list);
                    if(loadForm)
                        fillForm();

                }
            });

        }

        function createSubList(items, expandable) {
            if (expandable) {
                list += "<span class='toggle'>+</span><ul class=\"wpsc_categories wpsc_top_level_categories\" style='display: none'>";
            }
            else {
                list += "<ul class=\"wpsc_second_level_categories\" style='display: none'>";
            }

            $.each(items, function (index, item) {
                list += "<li><label for=\"" + item.id + "\"><input type=\"radio\" name=\"cat_group\" id=\"" + item.id + "\"";

                if(cat_id == item.id)
                    list +=  " disabled />" + item.name + "</label>";
                else
                    list +=  "/>" + item.name + "</label>";

                if (('children' in item) && item.children !== null) {
                    createSubList(item.children, true);
                }
                else
                    list += "</li>";

            });
            list += "</ul>";
        }

        $('#category_list').on('click', '.toggle', function () {
            var $ul = $(this).next();
            $(this).html($ul.is(':visible') ? '+' : '&ndash;');
            $ul.slideToggle();
        });

        $('#save').click(function () {

            const save = $('#save');
            save.prop('disabled', 'disabled');
            save.html(loading);

            let form_data = new FormData();
            form_data.append('cat_name', $('#cat_name').val());
            form_data.append('cat_parent', $('input[name=cat_group]:checked').attr('id'));

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'post',
                url: '{{route('category.create')}}',
                processData: false,
                contentType: false,
                data: form_data,
                success: function (response) {
                    location.reload(true);
                },
                error: function (data) {
                    save.prop('disabled', '');
                    save.html("Submit");
                    console.log(data);
                   
                    let response = data.responseJSON;
                    console.log(response);
                    $.each(response.errors, function (index, errors) {
                        $('#' + index).addClass("has-error");

                        let errorString = "";
                        $.each(errors, function (index, item) {
                            errorString += item;
                        });
                        $('.' + index).html(errorString);

                    });
                }
            });

        });

        $('#edit').click(function () {

            const save = $('#save');
            save.prop('disabled', 'disabled');
            save.html(loading);

            let form_data = new FormData();
            form_data.append('cat_name', $('#cat_name').val());
            form_data.append('cat_parent', $('input[name=cat_group]:checked').attr('id'));
            form_data.append('id', category.id);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'post',
                url: '{{route('category.update')}}',
                processData: false,
                contentType: false,
                data: form_data,
                success: function (response) {
                    location.reload(true);
                },
                error: function (data) {
                    save.prop('disabled', '');
                    save.html("Submit");

                    let response = data.responseJSON;
                    console.log(response);
                    $.each(response.errors, function (index, errors) {
                        $('#' + index).addClass("has-error");

                        let errorString = "";
                        $.each(errors, function (index, item) {
                            errorString += item;
                        });
                        $('.' + index).html(errorString);

                    });
                }
            });

        });
    </script>
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h6 class="box-title">Create a Category</h6>
            {{--<div class="box-tools pull-right">--}}
                {{--<div class="form-inline pull-left" style="margin-right: 10px">--}}
                    {{--<label for="show_filter">Show:--}}
                    {{--<select id="show_filter" class="form-control">--}}
                    {{--<option value="active">Active</option>--}}
                    {{--<option value="deleted">Deleted</option>--}}
                    {{--</select>--}}
                    {{--</label>--}}
                    {{--<a class="btn btn-default btn-sm" href="#"><i class="fa fa-plus"></i> &nbsp;Create Category</a>--}}
                {{--</div>--}}
                {{--<div class="label label-primary" style="vertical-align: sub">Total: <span id="cat_count"></span></div>--}}
            {{--</div>--}}
        </div>
        <div class="box-body">
            <form>
                <div class="form-group">
                    <label for="cat_name">Category Name</label>
                    <input type="text" class="form-control" id="cat_name" placeholder="Required" >
                    <span class="text-danger cat_name"></span>
                </div>
                <div id="category_list" class="radio">
                    <h5 class="text-black text-bold"> Select Parent (Required) </h5>
                    <span class="text-danger cat_parent"></span>
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw" id="loading"></i>
                </div>
            </form>
            <button type="button" class="btn btn-default" id="save">Submit</button>
            <button type="button" class="btn btn-default" id="edit">Update</button>
        </div>
    </div>
@stop
