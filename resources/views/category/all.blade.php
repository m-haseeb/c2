<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 08-Dec-17
 * Time: 4:09 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'My All Categories')

@section('css')
    <link rel="stylesheet" href="{{asset('css\allCategories.css')}}">
@endsection

@section('js')
    <script src="{{asset('js\aCollap\jquery.aCollapTable.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
        var category_list = "";
        var count = 1;

        $(document).ready(function () {
            loadCategories()
        });

        function loadCategories() {

            $('.box').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');

            category_list = "";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'get',
                url: '{{route('category.tree')}}',
                data: {
                    product: 'yes'
                },
                success: function (response) {

                    console.log(response);
                    
                    $('#cat_count').html(response.total);

                    $.each(response.tree, function (index, item) {
                        category_list += "<tr data-id=\"" + count + "\" data-parent=\"\"><td class=\"collection-item avatar\">" +
                            item.name + "</td><td class=\"text-center\">" + countChildren(item) + "</td><td class=\"text-center\">" + item.parent_count + "</td><td class=\"text-center\">" +
                            "<a class=\"btn btn-primary btn-sm\" category_id=\"" + item.id + "\" href='{{route('category.add')}}?edit=" + item.id + "'><i class=\"fa fa-pencil fa-lg\"></i> </a>&nbsp;";

                        //active button
                        if (item.isActive === 1)
                            category_list += "<button type=\"button\" class=\"active btn btn-success btn-sm\" category_id=\"" + item.id + "\"><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></button>&nbsp;";
                        else
                            category_list += "<button type=\"button\" class=\"deactive btn btn-warning btn-sm\" category_id=\"" + item.id + "\"><i class=\"fa fa-eye-slash fa-lg\" aria-hidden=\"true\"></i></button>&nbsp;";

                        //delete button
                        // category_list += "<button type=\"button\" class=\"delete btn btn-danger btn-sm\" category_id=\"" + item.id + "\"><i class=\"fa fa-trash-o fa-lg\"></i></button>";
                        category_list += '</td></tr>';

                        if (item.children !== null) {
                            createSubList(item.children, count,item.isActive );
                        }

                        count++;

                    });

                    $('#cat_table_body').html(category_list);

                    $('.overlay').remove();
                    
                    $('#cat_table').aCollapTable({
                        startCollapsed: true,
                        addColumn: false,
                        plusButton: '<i class="fa fa-plus expand_icon" aria-hidden="true"></i>',
                        minusButton: '<i class="fa fa-minus expand_icon" aria-hidden="true"></i>'
                    });
                }
            });

        }

        function createSubList(items, parent_id, parent_isActive) {
            count++;

            $.each(items, function (index, item) {
                category_list += "<tr data-id=\"" + count + "\" data-parent=\"" + parent_id + "\"><td class=\"collection-item avatar\">" +
                    item.name + "</td>" +
                    "<td class=\"text-center\">" + countChildren(item) + "</td><td class=\"text-center\">" + item.parent_count + "</td><td class=\"text-center\">" +
                    "<a class=\"btn btn-primary btn-sm\" category_id=\"" + item.id + "\" href='{{route('category.add')}}?edit=" + item.id + "'><i class=\"fa fa-pencil fa-lg\"></i> </a>&nbsp;";

                //active button
                let disabled = "";

                if (item.isActive === 1)
                {
                    // console.log("if "+"p:"+parent_isActive+"i:"+item.isActive);
                    // if(item.isActive === parent_isActive)
                    //     disabled = "disabled";

                    category_list += "<button type=\"button\" class=\"active btn btn-success btn-sm\" category_id=\"" + item.id + "\" "+disabled+"><i class=\"fa fa-eye fa-lg\" aria-hidden=\"true\"></i></button>&nbsp;";
                }
                else {
                    // console.log("else "+"p:"+parent_isActive+"i:"+item.isActive);
                    if (item.isActive === parent_isActive)
                        disabled = "disabled";

                    category_list += "<button type=\"button\" class=\"deactive btn btn-warning btn-sm\" category_id=\"" + item.id + "\" " + disabled + "><i class=\"fa fa-eye-slash fa-lg\" aria-hidden=\"true\"></i></button>&nbsp;";
                }

                //delete button
                // category_list += "<button type=\"button\" class=\"delete btn btn-danger btn-sm\" category_id=\"" + item.id + "\"><i class=\"fa fa-trash-o fa-lg\"></i></button>";

                category_list += '</td></tr>';

                if (('children' in item) && item.children !== null)
                    createSubList(item.children, count, item.isActive);

                count++;
            });

        }

        function countChildren(item) {
            if (('children' in item) && item.children !== null)
                return Object.keys(item.children).length;
            else
                return "0";
        }

        function deleteCat(cat_id) {
            $('.box').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'post',
                url: '{{route('category.delete')}}',
                data: {
                    id: cat_id
                },
                success: function (response) {
                    loadCategories();
                }
            });
        }

        function toggle(cat_id, toggle) {

            $('.box').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'post',
                url: '{{route('category.toggle')}}',
                data: {
                    id: cat_id,
                    toggle: toggle
                },
                success: function (response) {
                    loadCategories();
                }
            });
        }

        // $('#show_filter').change(function () {
        //     loadCategories();
        // });

        $('#cat_table_body').on('click', '.delete', function () {

            let id = $(this).attr('category_id');

            swal({
                title: "Are you sure?",
                text: "This is permanently delete this category, it's sub-categories and all associated products!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        deleteCat(id);
                    }
                });


        });

        $('#cat_table_body').on('click', '.active', function () {

            let id = $(this).attr('category_id');

            swal({
                title: "Are you sure?",
                text: "This is will disable this category, it's sub-categories and all associated products!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((doIt) => {
                    if (doIt) {
                        toggle(id, "disable");
                    }
                });


        });

        $('#cat_table_body').on('click', '.deactive', function () {

            let id = $(this).attr('category_id');

            swal({
                title: "Are you sure?",
                text: "This is will enable this category, it's sub-categories and all associated products!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((doIt) => {
                    if (doIt) {
                        toggle(id, "enable");
                    }
                });


        });

    </script>
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h6 class="box-title">My All Categories</h6>
            <div class="box-tools pull-right">
                <div class="form-inline pull-left" style="margin-right: 10px">
                    {{--<label for="show_filter">Show:--}}
                    {{--<select id="show_filter" class="form-control">--}}
                    {{--<option value="active">Active</option>--}}
                    {{--<option value="deleted">Deleted</option>--}}
                    {{--</select>--}}
                    {{--</label>--}}
                    <a class="btn btn-default btn-sm" href="{{route('category.add')}}"><i class="fa fa-plus"></i> &nbsp;Create
                        Category</a>
                </div>
                <div class="label label-primary" style="vertical-align: sub">Total: <span id="cat_count"></span></div>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-hover" id="cat_table">
                <thead>
                <tr>
                    <th width="50%">Categories</th>
                    <th width="10%" class="text-center">Sub-Categories</th>
                    <th width="20%" class="text-center">Products in Category</th>
                    <th width="30%" class="text-center">Action</th>
                </tr>
                </thead>
                <tbody id="cat_table_body">

                </tbody>
            </table>
        </div>
    </div>
@stop
