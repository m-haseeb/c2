<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 11-Dec-17
 * Time: 4:29 PM
 */?>
@extends('adminlte::page')

@section('title', 'Content Management')

@section('js')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.0/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('/vendor/datatables/buttons.server-side.js')}}"></script>
    {!! $dataTable->scripts() !!}
    <script  type="text/javascript">
        function toggle(id, toggle) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'post',
                url: '{{route('product.toggle')}}',
                data: {
                    id: id,
                    toggle: toggle
                },
                success: function (response) {
                    $( "#dataTableBuilder" ).DataTable().draw();
                }
            });
        }

        $('#dataTableBuilder').on('click', '.active', function () {

            let id = $(this).attr('product_id');

            swal({
                title: "Are you sure?",
                text: "This is will disable this product!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((doIt) => {
                    if (doIt) {
                        toggle(id, "disable");
                    }
                });


        });

        $('#dataTableBuilder').on('click', '.deactive', function () {

            let id = $(this).attr('product_id');

            swal({
                title: "Are you sure?",
                text: "This is will enable this product!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((doIt) => {
                    if (doIt) {
                        toggle(id, "enable");
                    }
                });


        });
    </script>

@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h6 class="box-title">Content Management</h6>
            <div class="box-tools pull-right">
                {{-- <div class="form-inline pull-left" style="margin-right: 10px">
                    <a class="btn btn-default btn-sm" href="{{route('product.add')}}"><i class="fa fa-plus"></i> &nbsp;Create Product</a>
                </div> --}}
            </div>
        </div>
        <div class="box-body">
            @if(session()->has('success'))
            <div class="alert alert-info alert-dismissable fade in">
                
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {!! session()->get('success') !!}
                
            </div>@endif
            {!! $dataTable->table() !!}
        </div>
    </div>
@stop
