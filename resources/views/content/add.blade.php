<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 11-Dec-17
 * Time: 4:29 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Add Product')

@section('css')
    {{--for radios--}}
    <link rel="stylesheet" href="{{asset('/css/addCategory.css')}}">
@endsection

@section('js')
    <script type="text/javascript">
        let list = "";
        let loading = '<i class="fa fa-spinner fa-pulse fa-fw"></i>';
        const product_id = "{{Request::input('edit')}}";
        let product = null;

        $(document).ready(function () {

            if (product_id !== "") {
                $('#save').remove();
                loadProduct();
            }
            else {
                $('#edit').remove();
                init();
            }
        });

        function loadProduct() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'get',
                url: '{{route('product.get', Request::input('edit') )}}',
                success: function (response) {
                    product = response;
                    init(true);
                }
            });
        }

        function fillForm() {

            if (product !== null) {

                $('#product_name').val(product.name);
                $('#manufacturer_name').val(product.manufacturer);
                $('#sku').val(product.sku);
                $('#manufacturer_code').val(product.manufacturer_code);
                $('#upc').val(product.upc);
                $('#package_quantity').val(product.package_quantity);
                $('#case_quantity').val(product.case_quantity);
                $('#price').val(product.price);
                $('input[id = "' + product.store_category_id + '"]').prop('checked', true);
            }
        }

        function init(loadForm) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'get',
                url: '{{route('category.tree')}}',
                success: function (response) {

                    list += "<div class=\"checkbox\"><ul class=\"wpsc_categories wpsc_top_level_categories\">";
                    $.each(response.tree, function (index, item) {

                        let text_class = "";
                        if (item.isActive === 0)
                            text_class = " text-danger ";

                        list += "<li><label for=\"" + item.id + "\" class='" + text_class + "' ><input type=\"radio\" name=\"cat_group\" id=\"" + item.id + "\" ";

                        if (index == 0) {//string with int compare
                            list += "checked/>" + item.name + "</label>";
                        }
                        else {
                            list += "/>" + item.name + "</label>";
                        }

                        if (item.children !== null) {
                            list += "<span class='toggle'>+</span>";
                            createSubList(item.children, false);
                            list += "</li>";
                        }
                        else
                            list += "</li>";

                    });
                    list += "</ul></div>";
                    $('#loading').remove();
                    $('#category_list').append(list);
                    if (loadForm)
                        fillForm();

                }
            });

        }

        function createSubList(items, expandable) {
            if (expandable) {
                list += "<span class='toggle'>+</span><ul class=\"wpsc_categories wpsc_top_level_categories\" style='display: none'>";
            }
            else {
                list += "<ul class=\"wpsc_second_level_categories\" style='display: none'>";
            }

            $.each(items, function (index, item) {

                let text_class = "";
                if (item.isActive === 0)
                    text_class = " text-danger ";

                list += "<li><label for=\"" + item.id + "\" class='" + text_class + "'><input type=\"radio\" name=\"cat_group\" id=\"" + item.id + "\"";

                if (product_id == item.id)
                    list += " disabled />" + item.name + "</label>";
                else
                    list += "/>" + item.name + "</label>";

                if (('children' in item) && item.children !== null) {
                    createSubList(item.children, true);
                }
                else
                    list += "</li>";

            });
            list += "</ul>";
        }

        $('#category_list').on('click', '.toggle', function () {
            var $ul = $(this).next();
            $(this).html($ul.is(':visible') ? '+' : '&ndash;');
            $ul.slideToggle();
        });

        $('#save').click(function () {

            const save = $('#save');
            save.prop('disabled', 'disabled');
            save.html(loading);

            let form_data = new FormData();
            form_data.append('product_name', $('#product_name').val());
            form_data.append('manufacturer_name', $('#manufacturer_name').val());
            form_data.append('sku', $('#sku').val());
            form_data.append('manufacturer_code', $('#manufacturer_code').val());
            form_data.append('upc', $('#upc').val());
            form_data.append('package_quantity', $('#package_quantity').val());
            form_data.append('case_quantity', $('#case_quantity').val());
            form_data.append('category', $('input[name=cat_group]:checked').attr('id'));
            form_data.append('price', $('#price').val());

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'post',
                url: '{{route('product.create')}}',
                processData: false,
                contentType: false,
                data: form_data,
                success: function (response) {
                    location.reload(true);
                },
                error: function (data) {
                    save.prop('disabled', '');
                    save.html("Submit");

                    let response = data.responseJSON;
                    console.log(response);
                    $.each(response.errors, function (index, errors) {
                        $('#' + index).addClass("has-error");

                        let errorString = "";
                        $.each(errors, function (index, item) {
                            errorString += item;
                        });
                        $('.' + index).html(errorString);

                    });
                }
            });

        });

        $('#edit').click(function () {

            const save = $('#save');
            save.prop('disabled', 'disabled');
            save.html(loading);

            let form_data = new FormData();
            form_data.append('id', product.id);
            form_data.append('product_name', $('#product_name').val());
            form_data.append('manufacturer_name', $('#manufacturer_name').val());
            form_data.append('sku', $('#sku').val());
            form_data.append('manufacturer_code', $('#manufacturer_code').val());
            form_data.append('upc', $('#upc').val());
            form_data.append('package_quantity', $('#package_quantity').val());
            form_data.append('case_quantity', $('#case_quantity').val());
            form_data.append('price', $('#price').val());
            form_data.append('category', $('input[name=cat_group]:checked').attr('id'));

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'post',
                url: '{{route('product.update')}}',
                processData: false,
                contentType: false,
                data: form_data,
                success: function (response) {
                    location.reload(true);
                },
                error: function (data) {
                    save.prop('disabled', '');
                    save.html("Submit");

                    let response = data.responseJSON;
                    console.log(response);
                    $.each(response.errors, function (index, errors) {
                        $('#' + index).addClass("has-error");

                        let errorString = "";
                        $.each(errors, function (index, item) {
                            errorString += item;
                        });
                        $('.' + index).html(errorString);

                    });
                }
            });

        });
    </script>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="product_name" class="col-sm-2 control-label">Product Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="product_name" placeholder="Required">
                        <span class="text-danger product_name"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="manufacturer_name" class="col-sm-2 control-label">Manufacturer Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="manufacturer_name" placeholder="Required">
                        <span class="text-danger manufacturer_name"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sku" class="col-sm-2 control-label">SKU</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="sku" placeholder="Required">
                        <span class="text-danger sku"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="manufacturer_code" class="col-sm-2 control-label">Manufacturer Code</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="manufacturer_code" placeholder="Required">
                        <span class="text-danger manufacturer_code"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="upc" class="col-sm-2 control-label">UPC</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="upc" placeholder="Required">
                        <span class="text-danger upc"></span>
                    </div>
                    <div class="col-sm-10">
                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="package_quantity" class="col-sm-2 control-label">Package Quantity</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="package_quantity" placeholder="Required">
                        <span class="text-danger package_quantity"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="case_quantity" class="col-sm-2 control-label">Case Quantity</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="case_quantity" placeholder="Required">
                        <span class="text-danger case_quantity"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="price" class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="price" placeholder="Required">
                        <span class="text-danger price"></span>
                    </div>
                </div>
            </div>
            <div id="category_list" class="radio">
                <h5 class="text-black text-bold"> Select Category (Required) </h5>
                <span class="text-danger">Red Categories Are Disabled</span>
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw" id="loading"></i>
            </div>
            <button type="button" class="btn btn-default" id="save">Submit</button>
            <button type="button" class="btn btn-default" id="edit">Update</button>
        </div>
    </div>
@stop
