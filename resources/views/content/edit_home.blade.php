<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 11-Dec-17
 * Time: 4:29 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Edit Homepage')

@section('css')
    {{--for radios--}}
    <link rel="stylesheet" href="{{asset('/css/addCategory.css')}}">
@endsection


@section('content')

<!-- tinymce configration start here.-->
<script>
var base_url = '{{asset('/')}}';
var ser = '<?php echo $_SERVER['DOCUMENT_ROOT']; ?>LP/b2bdemo/public/tiny_upload';

</script>
<script src="{{asset('/tiny/tinymce/js/tinymce/jquery.js')}}"></script>
<script src="{{asset('/tiny/tinymce/js/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('/tiny/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/tiny/common.js')}}"></script>
<!-- tinymce configration end here.-->



{{ Form::open(array('name'=>'editPage', 'files'=> true, 'url'=>route('content.updatehome')))}}
    <div class="box">
        <div class="box-body">
            <div class="form-horizontal">

                <div class="form-group">
                    <label for="banner_img1" class="col-sm-2 control-label">Banner Image 1*</label>
                    <div class="col-sm-4">
                        <p><img src="{{asset('pages_images/thumb/'.$pageData['banner_img1'])}}" alt="{{$pageData['banner_img1']}}"></p>
                        <input type="file" class="form-control" value="{{$pageData['banner_img1']}}" id="banner_img1" name="banner_img1" placeholder="Required">
                        <span class="text-danger banner_img1">{{$errors->first('banner_img1')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="banner_text1" class="col-sm-2 control-label">Description 1*</label>
                    <div class="col-sm-10">
                        <textarea class="form-control myeditor" id="banner_text1" name="banner_text1" rows="3" >{{$pageData['banner_text1']}}</textarea>
                        <span class="text-danger banner_text1">{{$errors->first('banner_text1')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="banner_img2" class="col-sm-2 control-label">Banner Image 2*</label>
                    <div class="col-sm-4">
                        <p><img src="{{asset('pages_images/thumb/'.$pageData['banner_img2'])}}" alt="{{$pageData['banner_img2']}}"></p>
                        <input type="file" class="form-control" value="{{$pageData['banner_img2']}}" id="banner_img2" name="banner_img2" placeholder="Required">
                        <span class="text-danger banner_img2">{{$errors->first('banner_img2')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="banner_text2" class="col-sm-2 control-label">Description 2*</label>
                    <div class="col-sm-10">
                        <textarea class="form-control myeditor" id="banner_text2" name="banner_text2" rows="3" >{{$pageData['banner_text2']}}</textarea>
                        <span class="text-danger banner_text2">{{$errors->first('banner_text2')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="banner_img3" class="col-sm-2 control-label">Banner Image 3*</label>
                    <div class="col-sm-4">
                        <p><img src="{{asset('pages_images/thumb/'.$pageData['banner_img3'])}}" alt="{{$pageData['banner_img3']}}"></p>
                        <input type="file" class="form-control" value="{{$pageData['banner_img3']}}" id="banner_img3" name="banner_img3" placeholder="Required">
                        <span class="text-danger banner_img3">{{$errors->first('banner_img3')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="banner_text3" class="col-sm-2 control-label">Description 3*</label>
                    <div class="col-sm-10">
                        <textarea class="form-control myeditor" id="banner_text3" name="banner_text3" rows="3" >{{$pageData['banner_text3']}}</textarea>
                        <span class="text-danger banner_text3">{{$errors->first('banner_text3')}}</span>
                    </div>
                </div>
            </div>
            <br/>
            <hr class="style1">
            <br/>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="why_us_text" class="col-sm-2 control-label">Why Us Text*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="why_us_text" value="{{$pageData['why_us_text']}}" name ='why_us_text' placeholder="Required">
                        <span class="text-danger why_us_text">{{$errors->first('why_us_text')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="desc1" class="col-sm-2 control-label">Description 1*</label>
                    <div class="col-sm-10">
                        <textarea class="form-control myeditor" id="desc1" name="desc1" rows="3" >{{$pageData['desc1']}}</textarea>
                        <span class="text-danger desc1">{{$errors->first('desc1')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="why_us_img" class="col-sm-2 control-label">Image*</label>
                    <div class="col-sm-4">
                        <p><img src="{{asset('pages_images/thumb/'.$pageData['why_us_img'])}}" alt="{{$pageData['why_us_img']}}"></p>
                        <input type="file" class="form-control" value="{{$pageData['why_us_img']}}" id="why_us_img" name="why_us_img" placeholder="Required">
                        <span class="text-danger why_us_img">{{$errors->first('why_us_img')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="desc2" class="col-sm-2 control-label">Description 2*</label>
                    <div class="col-sm-10">
                        <textarea class="form-control myeditor" id="desc2" name="desc2" rows="3" >{{$pageData['desc2']}}</textarea>
                        <span class="text-danger desc2">{{$errors->first('desc2')}}</span>
                    </div>
                </div>
            </div>
            <br/>
            <hr class="style1">
            <br/>

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="image1" class="col-sm-2 control-label">Image 1*</label>
                    <div class="col-sm-4">
                        <p><img src="{{asset('pages_images/thumb/'.$pageData['image1'])}}" alt="{{$pageData['image1']}}"></p>
                        <input type="file" class="form-control" value="{{$pageData['image1']}}" id="image1" name="image1" placeholder="Required">
                        <span class="text-danger image1">{{$errors->first('image1')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="col-sm-2 control-label">Description 1*</label>
                    <div class="col-sm-10">
                        <textarea class="form-control myeditor" id="text1" name="text1" rows="3" >{{$pageData['text1']}}</textarea>
                        <span class="text-danger text1">{{$errors->first('text1')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="image2" class="col-sm-2 control-label">Image 2*</label>
                    <div class="col-sm-4">
                        <p><img src="{{asset('pages_images/thumb/'.$pageData['image2'])}}" alt="{{$pageData['image2']}}"></p>
                        <input type="file" class="form-control" value="{{$pageData['image2']}}" id="image2" name="image2" placeholder="Required">
                        <span class="text-danger image2">{{$errors->first('image2')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="text2" class="col-sm-2 control-label">Description 2*</label>
                    <div class="col-sm-10">
                        <textarea class="form-control myeditor" id="text2" name="text2" rows="3" >{{$pageData['text2']}}</textarea>
                        <span class="text-danger text2">{{$errors->first('text2')}}</span>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-default" id="edit">Update</button>
        </div>
    </div>
{{ Form::close()}}
@stop
