<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 11-Dec-17
 * Time: 4:29 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Edit '.$pageData['name'])

@section('css')
    {{--for radios--}}
    <link rel="stylesheet" href="{{asset('/css/addCategory.css')}}">
@endsection


@section('content')

<!-- tinymce configration start here.-->
<script>
var root_url = '{{asset('/')}}';
var base_url = '{{asset('/')}}';
var ser = '<?php echo $_SERVER['DOCUMENT_ROOT']; ?>LP/c2/public/tiny_upload';

</script>
<script src="{{asset('/tiny/tinymce/js/tinymce/jquery.js')}}"></script>
<script src="{{asset('/tiny/tinymce/js/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('/tiny/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/tiny/common.js')}}"></script>
<!-- tinymce configration end here.-->

<link rel="stylesheet" href="{{asset('/filer/css/jquery.filer.css')}}" type="text/css" media="all" />
<link rel="stylesheet" href="{{asset('/filer/css/themes/jquery.filer-dragdropbox-theme.css')}}" type="text/css" media="all" />


{{ Form::open(array('name'=>'editPage', 'files'=> true, 'url'=>route('content.update').'?id='.$pageData['id']))}}
    <div class="box">
        <div class="box-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="page_title" class="col-sm-2 control-label">Title*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="page_title" value="{{$pageData['title']}}" name ='page_title' placeholder="Required">
                        <span class="text-danger page_title">{{$errors->first('page_title')}}</span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="page_description" class="col-sm-2 control-label">Description*</label>
                    <div class="col-sm-10">
                        <textarea class="form-control myeditor" id="page_description" name="page_description" rows="5" >{{$pageData['description']}}</textarea>
                        <span class="text-danger page_description">{{$errors->first('page_description')}}</span>
                    </div>
                </div>
                @if($pageData['id'] == 2 || $pageData['id'] == 3 || $pageData['id'] == 5)
                <div class="form-group">
                    <label for="page_image" class="col-sm-2 control-label">@php echo $pageData['id'] == 5?'Image*':'Page Image*'; @endphp</label>
                    <div class="col-sm-4">
                        <p><img src="{{asset('pages_images/thumb/'.$pageData['image'])}}" alt="{{$pageData['image']}}"></p>
                        <input type="file" class="form-control" value="{{$pageData['image']}}" id="page_image" name="page_image" placeholder="Required">
                        <span class="text-danger page_image">{{$errors->first('page_image')}}</span>
                    </div>
                </div>
                @endif

                @if($pageData['id'] == 4)
 
                {{-- <div class="form-group">
                    <label for="page_image" class="col-sm-2 control-label">Brand Images*</label>
                    <div class="col-sm-4">
                        <input type="file" name="page_image" id="filer_input2" multiple>
                        <span class="text-danger page_image">{{$errors->first('page_image')}}</span>
                    </div>
                </div> --}}
                {{-- <div class="form-group">
                    <label for="page_image" class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-sm-5">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <colgroup>
                                <col width="1%">
                                <col width="87%">
                                <col width="12%">
                            </colgroup>
                            <thead>
                            <tr>
                                <th align="left">&nbsp;</th>
                                <th align="left">Images</th>
                                <th align="center">Action</th>
                            </tr>
                            </thead>
                            <tbody id="imagesdata">
                            @if(count($images) > 0 )
                               @foreach($images as $image)
                            <tr>
                                <td align="left">&nbsp;</td>
                                <td align="left"><img src="{{asset('pages_images/thumb/'.$image['banner_image'])}}" alt="{{ $image['banner_image'] }}" /></td>
                                <td align="center">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <colgroup>
                                            <col width="100%">
                                        </colgroup>
                                        <tr>
                                            <td align="center">
                                                <a title="Delete" href="#" onclick="return confirm('Are you sure you want to delete selected item(s)?');"  class="tik-cross-btns p-del-btn-n"></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>  --}}
                @endif

            <button type="submit" class="btn btn-default" id="edit">Update</button>
             
            </div>
        </div>
    </div>

{{ Form::close() }}


<script src="{{asset('/filer/js/jquery.filer.min.js')}}" type="text/javascript"></script>
<script>
$(document).ready(function(){

 
    //Example 2

    $("#filer_input2").filer({
        limit: null,
        maxSize: null,
        extensions: ['jpg', 'jpeg', 'png'],
        changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">Browse Files</a></div></div>',
        showThumbs: false,
        theme: "dragdropbox",
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
            dragContainer: null,
        },
        uploadFile: {
            url: '{{route('uploadbrands')}}',
            type: 'POST',
            data: { _token: '{{csrf_token()}}' },

            enctype: 'multipart/form-data',
            synchron: true,
            beforeSend: function(){},
            success: function(data, itemEl, listEl, boxEl, newInputEl, inputEl, id){
                var parent = itemEl.find(".jFiler-jProgressBar").parent(),
                    new_file_name = JSON.parse(data),
                    filerKit = inputEl.prop("jFiler");
                    console.log(data);
                $('.remo').remove();
                var msg = "return confirm('Are you sure you want to delete selected item(s)?');";
                var html = '<tr><td align="left">&nbsp;</td><td align="left"><img src="'+root_url+'resources/home_image/thumb/'+new_file_name.name+'" alt="'+new_file_name.name+'" /></td><td align="center"><table border="0" cellspacing="0" cellpadding="0"><colgroup><col width="100%"></colgroup><tr><td align="center"><a title="Delete" href="'+base_url+'admin/pages/image_delete/'+new_file_name.image_id+'" onclick="'+msg+'"  class="tik-cross-btns p-del-btn-n"></a></td></tr></table></td></tr>';
                $('#imagesdata').append(html);
                
            },
            error: function(el){
                var parent = el.find(".jFiler-jProgressBar").parent();
                el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                    $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                });
            },
            statusCode: null,
            onProgress: null,
            onComplete: null
        },
        files: null,
        addMore: false,
        allowDuplicates: true,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        onSelect: null,
        afterShow: null,
        onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
            var filerKit = inputEl.prop("jFiler"),
                file_name = filerKit.files_list[id].name;

            $.post('./php/ajax_remove_file.php', {file: file_name});
        },
        onEmpty: null,
        options: null,
        dialogs: {
            alert: function(text) {
                return alert(text);
            },
            confirm: function (text, callback) {
                confirm(text) ? callback() : null;
            }
        },
        captions: {
            button: "Choose Files",
            feedback: "Choose files To Upload",
            feedback2: "files were chosen",
            drop: "Drop file here to Upload",
            removeConfirmation: "Are you sure you want to remove this file?",
            errors: {
                filesLimit: "Only 9 files are allowed to be uploaded.",
                filesType: "Only Images are allowed to be uploaded.",
                filesSize: "Thease file is too large! Please upload file up to 4 MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to 20 MB."
            }
        }
    });
    
})

function delete_fun(){
}
</script>
<script src="{{asset('dropzone.js')}}"></script>
<script type="text/javascript">

    Dropzone.options.imageUpload = {
        maxFilesize         :       1,
        acceptedFiles: ".jpeg,.jpg,.png,.gif"
    };
</script>
@stop
