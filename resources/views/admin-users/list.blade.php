
@extends('adminlte::page')

@section('title', 'User Management')

@section('js')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.0/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('/vendor/datatables/buttons.server-side.js')}}"></script>
    {!! $dataTable->scripts() !!}
 

    {{-- Ammar new css for billing and subscription --}}
    <link rel="stylesheet" href="{{asset('/vendor/adminlte/dist/css/newAdmin.css')}}">

@endsection

@section('content')

    <div class="box">
        <div class="box-header with-border">
            <h6 class="box-title"> User Management</h6>
            <div class="box-tools pull-right">
                <div class="form-inline pull-left" style="margin-right: 10px">
                    <a class="btn btn-default btn-sm" href="{{route('userAdd')}}"><i class="fa fa-plus"></i> &nbsp;Add New User</a>
                </div>
            </div>
        </div>
        <div class="box-body">
            @if(session()->has('success'))
            <div class="alert alert-info alert-dismissable fade in">
                
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {!! session()->get('success') !!}
                
            </div>@endif
           

            {!! $dataTable->table() !!}

        </div>
    </div>
@stop
