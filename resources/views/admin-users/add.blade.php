<?php
/**
 * Created by PhpStorm.
 * User: hamza.zafar
 * Date: 11-Dec-17
 * Time: 4:29 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Add User')

@section('css')
    {{--for radios--}}
    <link rel="stylesheet" href="{{asset('/css/addCategory.css')}}">
@endsection

@section('content')
    {{ Form::open(array('name'=>'add-form', 'id'=>'addUser', 'url'=>route('userAddSave') ) ) }}{{--  --}}

    <div class="box">
        <div class="box-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="first_name" class="col-sm-2 control-label">First Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="first_name" value="{{old('first_name')}}" id="first_name" placeholder="Required">
                        <span class="text-danger first_name"> {{$errors->first('first_name')}} </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="last_name" class="col-sm-2 control-label">Last Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="last_name" value="{{old('last_name')}}" id="last_name" placeholder="Required">
                        <span class="text-danger last_name">{{$errors->first('last_name')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="email" value="{{old('email')}}" id="email" placeholder="Required">
                        <span class="text-danger email">{{$errors->first('email')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="password" value="{{old('password')}}" id="password" placeholder="Required">
                        <span class="text-danger password">{{$errors->first('password')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="street" class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="street" value="{{old('street')}}" id="street" placeholder="Required">
                        <span class="text-danger street">{{$errors->first('street')}}</span>
                    </div>
                    <div class="col-sm-10">
                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="city" class="col-sm-2 control-label">City</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="city" value="{{old('city')}}" id="city" placeholder="Required">
                        <span class="text-danger city">{{$errors->first('city')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="state" class="col-sm-2 control-label">State</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="state">
                            <option value=''>Select</option>
                            @foreach($states as $st)
                            <option value="{{$st->stat_name}}" {{(old('state') == $st->stat_name)?'selected':''}} >{{$st->stat_name}}</option>
                            @endforeach
                        </select>
                        <span class="text-danger state">{{$errors->first('state')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="zip" class="col-sm-2 control-label">Zip Code</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="zip" value="{{old('zip')}}" id="zip" placeholder="Required">
                        <span class="text-danger zip">{{$errors->first('zip')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="status" class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="status" name="status">
                            <option value=''>Select</option>
                            <option value="Active" {{(old('status') == "Active")?'selected':''}} >Active</option>
                            <option value="Inactive" {{(old('status') == "Inactive")?'selected':''}} >Inactive</option>
                        </select>
                        <span class="text-danger status">{{$errors->first('status')}}</span>
                    </div>
                </div>

            </div>
            
            <button type="submit" class="btn btn-default" id="save">Submit</button>
        </div>
    </div>
    {{ Form::close() }}
@stop
