 @extends('adminlte::page') 
 @section('title', 'Order Detail')
 @section('content')
<div class="box">
    <div class="box-body back-end-od">
        {{ Form::open(array('name'=>'OrderDetail', 'class'=>'form-horizontal', 'url'=>route('updateOrderDetail',['id'=>$orders->id]) ) ) }}
        <div class="mpc-top">
            <h5>Order Detail</h5>
        </div>
        <!-- top heading -->
        <div class="oreview-detail">
            <div class="ord-top">

                <div class="ord-toplft">
                    <div class="tlft-thead">
                        <h5>Billing Address</h5>
                    </div>
                    <div class="tlft-btm">
                        <div class="lft-btm-wrap">
                            <div class="lft-btm-inr">
                                <div class="col-row">
                                    <h5>First Name:</h5>
                                    <span>{{ $orders->billfname }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>Last Name:</h5>
                                    <span>{{ $orders->billlname }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>Email:</h5>
                                    <span>{{ $orders->billemail }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>Phone:</h5>
                                    <span>{{ $orders->billphone }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>Address:</h5>
                                    <span>{{ $orders->billaddress }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>City:</h5>
                                    <span>{{ $orders->billcity }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>State:</h5>
                                    <span>{{ $orders->billstate }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>Zip:</h5>
                                    <span>{{ $orders->billzip }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ord-toplft toprght">
                    <div class="tlft-thead">
                        <h5>Shipping Address</h5>
                    </div>
                    <div class="tlft-btm">
                        <div class="lft-btm-wrap">
                            <div class="lft-btm-inr">
                                <div class="col-row">
                                    <h5>First Name:</h5>
                                    <span>{{ $orders->shipfname }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>Last Name:</h5>
                                    <span>{{ $orders->shiplname }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>Email:</h5>
                                    <span>{{ $orders->shipemail }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>Phone:</h5>
                                    <span>{{ $orders->shipphone }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>Address:</h5>
                                    <span>{{ $orders->shipaddress }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>City:</h5>
                                    <span>{{ $orders->shipcity }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>State:</h5>
                                    <span>{{ $orders->shipstate }}</span>
                                </div>
                                <div class="col-row">
                                    <h5>Zip:</h5>
                                    <span>{{ $orders->shipzip }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- top left section -->
            </div>
            <!-- content section -->
            <div class="ord-top">
                <div class="ord-toplft">
                    <div class="tlft-thead">
                        <h5>Credit Card Infomation</h5>
                    </div>
                    <div class="tlft-btm">
                        <div class="lft-btm-wrap">
                            <div class="lft-btm-inr">
                                <div class="col-row">
                                    <h5 class="set-crh">Card Holder Name:</h5>
                                    <span class="set-crs">{{ $orders->cardName }}</span>
                                </div>
                                <div class="col-row">
                                    <h5 class="set-crh">Credit Card Number:</h5>
                                    <span class="set-crs">{{ $orders->cardNumber }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content section -->
        </div>
        <!-- center track order details -->
        <div class="adcart-cntnt">
            <div class="adc-outr">
                <div class="adc-inr">
                    <div class="adc-main">
                        <div class="adc-inr-wrap">
                            <div class="adc-row">
                                <div class="thw-img1">
                                    <h1>Product Details</h1>
                                </div>
                                <div class="thw-price1">
                                    <h1>Unit Price</h1>
                                </div>
                                <div class="thw-qty1">
                                    <h1>Qty</h1>
                                </div>
                                <div class="thw-tprice1 st-tprc">
                                    <h1>Total Price</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- top headings row -->
                    @if(!empty($orders->detail()->get() ) ) @foreach($orders->detail()->get() as $detail)
                    <div class="adc-main row-btm">
                        <div class="adc-inr-wrap">
                            <div class="adc-row row-set f-row border-b-od">
                                <div class="row-fcol1">
                                    <div class="fcol-img">
                                        <img src="{{$detail->imgPath}}" alt="">
                                    </div>
                                    <div class="fcol-txt1">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <p>{{$detail->name}}</p>
                                                    {{--
                                                    <p>Product&nbsp;#:&nbsp;123456</p> --}}
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="row-fcol1 scol">
                                    <div class="fcol-txt scol">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <p>${{number_format($detail->price,2,'.','')}}</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="row-fcol1 tcol">
                                    <div class="fcol-txt tcol">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    {{$detail->qty}}
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="row-fcol1 fcol st-tprc">
                                    <div class="fcol-txt fcol">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <p>${{number_format($detail->subTotal,2,'.','')}}</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- rows --->
                        </div>
                    </div>
                    <!-- bottom rows -->
                    @endforeach @endif
                </div>
            </div>
            <!-- top headings sectio row -->
        </div>
        <!-- main content section -->
        <div class="adc-last">

            <div class="form-group">
                <label for="store_shiping" class="col-sm-2 control-label">Shipping Type</label>
                <div class="col-sm-10">
                    <p class="form-control" id="store_shiping" name="store_shiping">
                        @if(($orders->store_shiping) == "shipping") Shipping @elseif(($orders->store_shiping) == "store_pickup") Store Pickup @endif
                    </p>
                    <span class="text-danger store_shiping">{{$errors->first('store_shiping')}}</span>
                </div>
            </div>

            <div class="form-group">
                <label for="status" class="col-sm-2 control-label">Status</label>
                <div class="col-sm-10">
                    <select class="form-control" id="status" name="status">
                        <option value=''>Select</option>
                        <option value="Processing" {{(old( 'status',$orders->status) == "Processing")?'selected':''}} >Processing</option>
                        <option value="Completed" {{(old( 'status',$orders->status) == "Completed")?'selected':''}} >Completed</option>
                    </select>
                    <span class="text-danger status">{{$errors->first('status')}}</span>
                </div>
            </div>
            <div class="adc-lst-main">
                <div class="lm-inr">
                    <div class="prc-total">
                        <div class="t-carier g-total">
                            <div class="tc-lft">
                                <h5>Sub Total</h5>
                            </div>
                            <div class="tc-rgt">
                                <h5><span>${{number_format($orders->sub_total,2,'.','')}}</span></h5>
                            </div>

                            <div class="tc-lft">
                                <h5>Shipping Charges</h5>
                            </div>
                            <div class="tc-rgt">
                                <h5><span>${{number_format($orders->shiping_charges,2,'.','')}}</span></h5>
                            </div>

                            <div class="tc-lft">
                                <h5>Tax</h5>
                            </div>
                            <div class="tc-rgt">
                                <h5><span>${{number_format($orders->tax,2,'.','')}}</span></h5>
                            </div>

                            <div class="tc-lft">
                                <h5>Grand Total</h5>
                            </div>
                            <div class="tc-rgt">
                                <h5><span>${{number_format($orders->totalPrice,2,'.','')}}</span></h5>
                            </div>
                        </div>
                        <!-- grand total -->
                    </div>
                    <!-- top price section -->
                </div>
            </div>
        </div>
        <!-- bottom last section -->
        <button type="submit" class="btn btn-default" id="save">Submit</button>
        {{ Form::close() }}
    </div>
</div>
@stop