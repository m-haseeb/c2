<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReissProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reiss_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('manufacturer');
            $table->text('name');
            $table->string('sku');
            $table->string('upc');
            $table->string('manufacturer_code');
            $table->integer('package_quantity')->nullable()->default(null);
            $table->integer('case_quantity')->nullable()->default(null);
            $table->integer('price')->unsigned();
            $table->integer('reiss_category_id')->unsigned();
            $table->foreign('reiss_category_id')->references('id')->on('reiss_categories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reiss_products');
    }
}
