<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomepagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homepages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('banner_img1');
            $table->text('banner_text1');
            $table->string('banner_img2');
            $table->text('banner_text2');
            $table->string('banner_img3');
            $table->text('banner_text3');
            $table->string('why_us_text');
            $table->longText('desc1');
            $table->string('why_us_img');
            $table->longText('desc2');
            $table->string('image1');
            $table->longText('text1');
            $table->string('image2');
            $table->longText('text2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homepages');
    }
}
