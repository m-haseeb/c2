<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureTiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_tiers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tiers_id')->unsigned();
            $table->foreign('tiers_id')->references('id')->on('tiers');
            $table->integer('features_id')->unsigned();
            $table->foreign('features_id')->references('id')->on('features');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_tiers');
    }
}
