<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('manufacturer');
            $table->text('name');
            $table->string('sku');
            $table->string('upc');
            $table->string('manufacturer_code');
            $table->integer('package_quantity');
            $table->string('price');
            $table->integer('case_quantity');
            $table->boolean('isActive')->default('1');
            $table->integer('store_category_id')->unsigned();
            $table->foreign('store_category_id')->references('id')->on('store_categories')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_products');
    }
}
