<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\StoreProduct::class, function (Faker $faker) {
    return [
        'manufacturer' => $faker->company,
        'name' => $faker->name,
        'sku' => $faker->word,
        'upc' => $faker->isbn10,
        'manufacturer_code' => $faker->isbn10,
        'package_quantity' => mt_rand(1,100),
        'case_quantity' => mt_rand(1,100),
        'store_category_id' => mt_rand(1,718),
    ];
});